## Magma.Logger

This is a Logging library that is able to be used within any project you choose. It can be used in conjunction with the other Magma Assemblies or it can be used alone.

### Example Code:

```c#
using System;
using Magma.Logger;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.AddLogger(new ConsoleLogger());
            Log.AddLogger(new DebugLogger());
            Log.AddLogger(new FileLogger("logs/", "log.txt"));

            Log.Error<Program>("I'm an error!");
            Log.Warning<Program>("I'm a warning!");
            Log.Trace<Program>("I trace the code!");
            Log.Info<Program>("I print info!");
            Log.Debug<Program>("I print to the debug console only in debug mode!");
            Log.Assert<Program>(1 > 0, "I print an error message if condition is false!");
            Log.AssertNotNull<Program>(obj, "I print an error message if value is null!");
        }
    }
}
```
