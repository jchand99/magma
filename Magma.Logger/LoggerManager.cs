using System;
using System.Collections.Generic;
using System.Diagnostics;
using Magma.Logger.Util;

namespace Magma.Logger
{
    /// <summary>
    /// LoggerManager manages all loggers created.
    /// </summary>
    internal class LoggerManager
    {
        /// <summary>
        /// Collection of loggers attached to their names.
        /// </summary>
        private Dictionary<string, Logger> _Loggers;

        /// <summary>
        /// Lock object to make logger able to write correctly on multiple threads.
        /// </summary>
        /// <returns></returns>
        private static object _Lock = new object();

        /// <summary>
        /// Constructor for LoggerManager.
        /// </summary>
        internal LoggerManager()
        {
            _Loggers = new Dictionary<string, Logger>();
        }

        /// <summary>
        /// Adds a logger to the <seealso cref="LoggerManager"/>.
        /// </summary>
        /// <param name="logger">Logger to add.</param>
        /// <returns>True if logger was added successfully, false if logger already exists with that name.</returns>
        internal bool AddLogger(Logger logger)
        {
            if (!_Loggers.ContainsKey(logger.Name))
            {
                return _Loggers.TryAdd(logger.Name, logger);
            }
            return false;
        }

        /// <summary>
        /// Calls the print method on all the loggers in the collection
        /// passing the <seealso cref="LogMessage"/> to the loggers.
        /// </summary>
        /// <param name="logMessage">LogMessage to print.</param>
        [DebuggerStepThrough]
        internal void Print(LogMessage logMessage)
        {
            lock (_Lock)
            {
                foreach (var logger in _Loggers.Values)
                {
                    logger.Print(logMessage);
                }
            }
        }

        /// <summary>
        /// Calls the print method on all the loggers in the collection
        /// passing the <seealso cref="LogMessage"/> to the loggers and
        /// changes the color of the console.
        /// </summary>
        /// <param name="logMessage">LogMessage to print.</param>
        /// <param name="color">Color of the console to print.</param>
        [DebuggerStepThrough]
        internal void Print(LogMessage logMessage, ConsoleColor color)
        {
            lock (_Lock)
            {
                Console.ForegroundColor = color;
                foreach (var logger in _Loggers.Values)
                {
                    logger.Print(logMessage);
                }
                Console.ResetColor();
            }
        }
    }
}
