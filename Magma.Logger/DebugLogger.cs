using System.Diagnostics;
using Magma.Logger.Util;

namespace Magma.Logger
{
    /// <summary>
    /// Prints message to debug console.
    /// </summary>
    public class DebugLogger : Logger
    {
        /// <summary>
        /// Constructor for DebugLogger, defaults name to 'DebugLogger'.
        /// </summary>
        public DebugLogger()
        {
            Name = "DebugLogger";
        }

        /// <summary>
        /// Print method called by <seealso cref="LoggerManager"/>.
        /// </summary>
        /// <param name="logMessage">LogMessage to print to debug console.</param>
        internal override void Print(LogMessage logMessage)
        {
            Debug.WriteLine(logMessage.ToString());
        }
    }
}
