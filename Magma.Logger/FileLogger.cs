using System;
using System.IO;
using Magma.Logger.Util;

namespace Magma.Logger
{
    /// <summary>
    /// Prints message to specified file.
    /// </summary>
    public class FileLogger : Logger, IDisposable
    {
        /// <summary>
        /// StreamWriter to write to file.
        /// </summary>
        private StreamWriter _StreamWriter;

        /// <summary>
        /// DirectoryInfo for info about the directory the file is located.
        /// </summary>
        private DirectoryInfo _DirectoryInfo;

        /// <summary>
        /// StreamWriter disposed value.
        /// </summary>
        private bool disposedValue;

        /// <summary>
        /// Constructor for FileLogger, defaults name to 'FileLogger'.
        /// </summary>
        /// <param name="filePath">Path to file logger will write in.</param>
        /// <param name="fileName">Name of file to write in.</param>
        public FileLogger(string filePath, string fileName)
        {
            Name = "FileLogger";
            if (!Directory.Exists(filePath)) _DirectoryInfo = Directory.CreateDirectory(filePath);
            string pathAndFile = Path.Combine(filePath, fileName);
            if (!File.Exists(pathAndFile)) using (FileStream fs = File.Create(pathAndFile)) { }
            _StreamWriter = new StreamWriter(pathAndFile, true);
        }

        /// <summary>
        /// Print method called by <seealso cref="LoggerManager"/>.
        /// </summary>
        /// <param name="logMessage">LogMessage to write to file.</param>
        internal override void Print(LogMessage logMessage)
        {
            _StreamWriter.WriteLine(logMessage.ToString());
        }

        /// <summary>
        /// Dispose of FileLogger and close the stream.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Console.WriteLine("DISPOSING");
                    _StreamWriter.Dispose();
                }
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~FileLogger()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>
        /// Dispose of FileLogger and close the stream.
        /// </summary>
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
