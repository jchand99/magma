using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using Magma.Logger.Util;

namespace Magma.Logger
{
    /// <summary>
    /// Logs message to loggers.
    /// </summary>
    public static class Log
    {
        /// <summary>
        /// <seealso cref="LoggerManager"/> to send message to loggers.
        /// </summary>
        private static LoggerManager _LoggerManager;

        /// <summary>
        /// Constructor for Log. Creates <seealso cref="LoggerManager"/>.
        /// </summary>
        static Log()
        {
            _LoggerManager = new LoggerManager();
        }

        /// <summary>
        /// Adds logger to system.
        /// </summary>
        /// <param name="logger">Logger to add.</param>
        /// <returns></returns>
        public static bool AddLogger(Logger logger) => _LoggerManager.AddLogger(logger);

        /// <summary>
        /// Fatal error
        /// </summary>
        /// <param name="message">Message to log.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [DebuggerStepThrough]
        public static void Fatal<T>(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            Print(LogLevel.Fatal, message, ConsoleColor.DarkRed, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Error
        /// </summary>
        /// <param name="message">Message to log.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [DebuggerStepThrough]
        public static void Error<T>(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            Print(LogLevel.Error, message, ConsoleColor.Red, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Warning
        /// </summary>
        /// <param name="message">Message to log.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [DebuggerStepThrough]
        public static void Warning<T>(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            Print(LogLevel.Warning, message, ConsoleColor.Yellow, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Info
        /// </summary>
        /// <param name="message">Message to log.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [DebuggerStepThrough]
        public static void Info<T>(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            Print(LogLevel.Info, message, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Debug
        /// </summary>
        /// <param name="message">Message to log.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void Debug<T>(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            Print(LogLevel.Debug, message, ConsoleColor.Blue, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Trace
        /// </summary>
        /// <param name="message">Message to log.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [DebuggerStepThrough]
        public static void Trace<T>(string message, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            Print(LogLevel.Trace, message, ConsoleColor.Green, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Assert method called on failure.
        /// </summary>
        /// <param name="failureMessage">Message to log as failure.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <param name="className">Calling class name.</param>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        private static void Assert(string failureMessage, string methodName, int lineNumber, string className)
        {
            Print(LogLevel.Assert, failureMessage, ConsoleColor.Red, methodName, lineNumber, className);
        }

        /// <summary>
        /// Asserts if object is null or not.
        ///
        /// <para>Success if object is not null.</para>
        /// <para>Failure if object is null.</para>
        /// </summary>
        /// <param name="condition">Object to assert nullity</param>
        /// <param name="failureMessage">Message to log on failure.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
#nullable enable
        public static void AssertNotNull<T>(object? condition, string failureMessage, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            if (condition == null) Assert(failureMessage, methodName, lineNumber, typeof(T).Name);
        }
#nullable disable

        /// <summary>
        /// Asserts if bool condition is true or false.
        ///
        /// <para>Success if condition is true.</para>
        /// <para>Failure if condition is false.</para>
        /// </summary>
        /// <param name="condition">bool to check logical truth.</param>
        /// <param name="failureMessage">Message to log on failure.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void Assert<T>(bool condition, string failureMessage, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            if (!condition) Assert(failureMessage, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Asserts if condition is greater than 0.
        ///
        /// <para>Success if condition is greater than 0.</para>
        /// <para>Failure if condition is less than or equal to 0.</para>
        /// </summary>
        /// <param name="condition">Object to assert nullity</param>
        /// <param name="failureMessage">Message to log on failure.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void Assert<T>(int condition, string failureMessage, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            if (condition <= 0) Assert(failureMessage, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Asserts if condition is greater than 0.
        ///
        /// <para>Success if condition is greater than 0.</para>
        /// <para>Failure if condition is less than or equal to 0.</para>
        /// </summary>
        /// <param name="condition">Object to assert nullity</param>
        /// <param name="failureMessage">Message to log on failure.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void Assert<T>(long condition, string failureMessage, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            if (condition <= 0) Assert(failureMessage, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Asserts if condition is greater than 0.
        ///
        /// <para>Success if condition is greater than 0.</para>
        /// <para>Failure if condition is less than or equal to 0.</para>
        /// </summary>
        /// <param name="condition">Object to assert nullity</param>
        /// <param name="failureMessage">Message to log on failure.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void Assert<T>(float condition, string failureMessage, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            if (condition <= 0.0f) Assert(failureMessage, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Asserts if condition is greater than 0.
        ///
        /// <para>Success if condition is greater than 0.</para>
        /// <para>Failure if condition is less than or equal to 0.</para>
        /// </summary>
        /// <param name="condition">Object to assert nullity</param>
        /// <param name="failureMessage">Message to log on failure.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <typeparam name="T">Calling class name.</typeparam>
        [DebuggerHidden]
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public static void Assert<T>(double condition, string failureMessage, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0) where T : notnull
        {
            if (condition <= 0.0d) Assert(failureMessage, methodName, lineNumber, typeof(T).Name);
        }

        /// <summary>
        /// Prints message with no color change.
        /// </summary>
        /// <param name="level">Log level of message.</param>
        /// <param name="message">Text of message.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <param name="className">Calling class name.</param>
        [DebuggerHidden]
        [DebuggerStepThrough]
        private static void Print(LogLevel level, string message, string methodName, int lineNumber, string className)
        {
            LogMessage logMessage = new LogMessage(level, Thread.CurrentThread.Name, message, className, methodName, lineNumber);
            _LoggerManager.Print(logMessage);
        }

        /// <summary>
        /// Prints message with color change.
        /// /// </summary>
        /// <param name="level">Log level of message.</param>
        /// <param name="message">Text of message.</param>
        /// <param name="methodName">Calling method name.</param>
        /// <param name="color">Color of console text.</param>
        /// <param name="lineNumber">Line number executed.</param>
        /// <param name="className">Calling class name.</param>
        [DebuggerHidden]
        [DebuggerStepThrough]
        private static void Print(LogLevel level, string message, ConsoleColor color, string methodName, int lineNumber, string className)
        {
            LogMessage logMessage = new LogMessage(level, Thread.CurrentThread.Name, message, className, methodName, lineNumber);
            _LoggerManager.Print(logMessage, color);
        }
    }
}
