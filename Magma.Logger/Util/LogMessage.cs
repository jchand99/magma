using System;

namespace Magma.Logger.Util
{
    /// <summary>
    /// LogMessage struct that contains all the details of the message sent to the logger.
    /// </summary>
    internal struct LogMessage
    {
        /// <summary>
        /// <seealso cref="LogLevel"/> of message.
        /// </summary>
        internal LogLevel Level;

        /// <summary>
        /// DateTime of message.
        /// </summary>
        private DateTime DateTime;

        /// <summary>
        /// Name of thread message was written on.
        /// </summary>
        internal string ThreadName;

        /// <summary>
        /// Text of message.
        /// </summary>
        internal string Message;

        /// <summary>
        /// Name of class sending message.
        /// </summary>
        internal string ClassName;

        /// <summary>
        /// Name of method sending message.
        /// </summary>
        internal string MethodName;

        /// <summary>
        /// Line number message was sent from.
        /// </summary>
        internal int LineNumber;

        /// <summary>
        /// Constructor of LogMessage.
        /// </summary>
        /// <param name="level">Level of message.</param>
        /// <param name="threadName">Thread message was sent on.</param>
        /// <param name="message">Message text.</param>
        /// <param name="className">Name of class sending message.</param>
        /// <param name="methodName">Name of method sending message.</param>
        /// <param name="lineNumber">Line number message was sent from.</param>
        internal LogMessage(LogLevel level, string threadName, string message, string className, string methodName, int lineNumber)
        {
            Level = level;
            DateTime = DateTime.Now;
            ThreadName = threadName;
            Message = message;
            ClassName = className;
            MethodName = methodName;
            LineNumber = lineNumber;
        }

        /// <summary>
        /// Returns <seealso cref="LogMessage"/> as a string.
        /// </summary>
        /// <returns>Message as string.</returns>
        public override string ToString()
        {
            return string.IsNullOrEmpty(ThreadName) ?
            $"[{DateTime.Now}] [{Level}] [{ClassName}|{MethodName}|{LineNumber}]: {Message}" :
            $"[{DateTime.Now}] [{Level}] [{ThreadName}|{ClassName}|{MethodName}|{LineNumber}]: {Message}";
        }
    }
}
