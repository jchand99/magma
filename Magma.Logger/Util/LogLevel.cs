namespace Magma.Logger.Util
{
    /// <summary>
    /// LogLevel used to determine the level of the current <seealso cref="LogMessage"/>.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// For when the code has a fatal error.
        /// </summary>
        Fatal,

        /// <summary>
        /// For when the code has an error that is not fatal.
        /// </summary>
        Error,

        /// <summary>
        /// For when the code has an error that is not fatal.
        /// </summary>
        Warning,

        /// <summary>
        /// For when the goal is to print normal info.
        /// </summary>
        Info,

        /// <summary>
        /// For when the the goal is to print info to the debug console.
        /// </summary>
        Debug,

        /// <summary>
        /// For when the goal is to trace the code.
        /// </summary>
        Trace,

        /// <summary>
        /// For asserting whether something is as expected.
        /// </summary>
        Assert = Error,
    }
}
