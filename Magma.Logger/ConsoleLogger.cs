using System;
using Magma.Logger.Util;

namespace Magma.Logger
{
    /// <summary>
    /// Logs message to the console.
    /// </summary>
    public class ConsoleLogger : Logger
    {

        /// <summary>
        /// Constructor of logger defaults name to 'ConsoleLogger'.
        /// </summary>
        public ConsoleLogger()
        {
            Name = "ConsoleLogger";
        }

        /// <summary>
        /// Print method called by <seealso cref="LoggerManager"/>.
        /// </summary>
        /// <param name="logMessage">LogMessage to write to console.</param>
        internal override void Print(LogMessage logMessage)
        {
            Console.WriteLine(logMessage.ToString());
        }
    }
}
