using System.Diagnostics;
using Magma.Logger.Util;

namespace Magma.Logger
{
    /// <summary>
    /// Logger that outputs trace.
    /// </summary>
    public class TraceLogger : Logger
    {
        /// <summary>
        /// Constructor for TraceLogger.
        /// </summary>
        public TraceLogger()
        {
            Name = "TraceLogger";
        }

        /// <summary>
        /// Print method that sends the log message to be printed.
        /// </summary>
        /// <param name="logMessage"></param>
        internal override void Print(LogMessage logMessage)
        {
            Trace.WriteLine(logMessage.ToString());
        }
    }
}
