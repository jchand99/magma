using Magma.Logger.Util;

namespace Magma.Logger
{
    /// <summary>
    /// Logger class
    /// </summary>
    public abstract class Logger
    {
        /// <summary>
        /// Name of logger.
        /// </summary>
        /// <value>String name of logger created.</value>
        public string Name { get; set; }

        /// <summary>
        /// Prints the <seealso cref="LogMessage"/> to the different loggers.
        /// </summary>
        /// <param name="logMessage">Log message to print.</param>
        internal abstract void Print(LogMessage logMessage);
    }
}
