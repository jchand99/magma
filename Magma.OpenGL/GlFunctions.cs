using System;
using System.Text;

namespace Magma.OpenGL
{
    public static unsafe partial class Gl
    {
        /// <summary> Set the active program object for a program pipeline object</summary>
        /// <para>
        /// Description: glActiveShaderProgram sets the linked program named by program to
        /// be the active program for the program pipeline object pipeline. The active program in the active program pipeline object is
        /// the target of calls to <see cref="Gl.Uniform"/> when no program has been made current through a call to <see cref="Gl.UseProgram(uint)"/>.
        /// </para>
        /// <para>
        /// Errors: <see cref="ErrorCode.InvalidOperation"/> is generated if pipeline is not a name previously returned from a call to <see cref="Gl.GenProgramPipelines(int, uint[])"/>
        /// or if such a name has been deleted by a call to <see cref="Gl.DeleteProgramPipelines(int, uint[])"/>.
        /// </para>
        /// <seealso cref="Gl.GenProgramPipelines(int, uint[])"/>
        /// <seealso cref="Gl.DeleteProgramPipelines(int, uint[])"/>
        /// <seealso cref="Gl.IsProgramPipeline(uint)"/>
        /// <seealso cref="Gl.UseProgram(uint)"/>
        /// <seealso cref="Gl.Uniform"/>
        /// <param name="pipeline">Specifies the program pipeline object to set the active program object for.</param>
        /// <param name="program">Specifies the program object to set as the active program pipeline object pipeline.</param>
        public static void ActiveShaderProgram(uint pipeline, uint program) => Delegates.glActiveShaderProgram(pipeline, program);
        public static void ActiveTexture(int texture) => Delegates.glActiveTexture(texture);
        public static void AttachShader(uint program, uint shader) => Delegates.glAttachShader(program, shader);
        public static void BeginConditionalRender(uint id, ConditionalRenderType mode) => Delegates.glBeginConditionalRender(id, mode);
        public static void EndConditionalRender() => Delegates.glEndConditionalRender();
        public static void BeginQuery(QueryTarget target, uint id) => Delegates.glBeginQuery(target, id);
        public static void EndQuery(QueryTarget target) => Delegates.glEndQuery(target);
        public static void BeginQueryIndexed(QueryTarget target, uint index, uint id) => Delegates.glBeginQueryIndexed(target, index, id);
        public static void EndQueryIndexed(QueryTarget target, uint index) => Delegates.glEndQueryIndexed(target, index);
        public static void BeginTransformFeedback(BeginFeedbackMode primitiveMode) => Delegates.glBeginTransformFeedback(primitiveMode);
        public static void EndTransformFeedback() => Delegates.glEndTransformFeedback();
        public static void BindAttribLocation(uint program, uint index, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                Delegates.glBindAttribLocation(program, index, namePtr);
        }
        public static void BindBuffer(BufferTarget target, uint buffer) => Delegates.glBindBuffer(target, buffer);
        public static void BindBufferBase(BufferTarget target, uint index, uint buffer) => Delegates.glBindBufferBase(target, index, buffer);
        public static void BindBufferRange(BufferTarget target, uint index, uint buffer, IntPtr offset, IntPtr size) => Delegates.glBindBufferRange(target, index, buffer, offset, size);
        public static void BindBuffersBase(BufferTarget target, uint first, int count, uint[] buffers)
        {
            fixed (uint* buffersPtr = buffers)
                Delegates.glBindBuffersBase(target, first, count, buffersPtr);
        }
        public static void BindBuffersRange(BufferTarget target, uint first, int count, uint[] buffers, IntPtr[] offsets, IntPtr[] sizes)
        {
            fixed (uint* buffersPtr = buffers)
            fixed (IntPtr* offsetsPtr = offsets)
            fixed (IntPtr* sizesPtr = sizes)
                Delegates.glBindBuffersRange(target, first, count, buffersPtr, offsetsPtr, sizesPtr);
        }
        public static void BindFragDataLocation(uint program, uint colorNumber, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                Delegates.glBindFragDataLocation(program, colorNumber, namePtr);
        }
        public static void BindFragDataLocationIndexed(uint program, uint colorNumber, uint index, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                Delegates.glBindFragDataLocationIndexed(program, colorNumber, index, namePtr);
        }
        public static void BindFramebuffer(FramebufferTarget target, uint framebuffer) => Delegates.glBindFramebuffer(target, framebuffer);
        public static void BindImageTexture(uint unit, uint texture, int level, bool layered, int layer, BufferAccess access, PixelInternalFormat format) => Delegates.glBindImageTexture(unit, texture, level, layered, layer, access, format);
        public static void BindImageTextures(uint first, int count, uint[] textures)
        {
            fixed (uint* texturesPtr = textures)
                Delegates.glBindImageTextures(first, count, texturesPtr);
        }
        public static void BindProgramPipeline(uint pipeline) => Delegates.glBindProgramPipeline(pipeline);
        public static void BindRenderbuffer(RenderbufferTarget target, uint renderbuffer) => Delegates.glBindRenderbuffer(target, renderbuffer);
        public static void BindSampler(uint unit, uint sampler) => Delegates.glBindSampler(unit, sampler);
        public static void BindSamplers(uint first, int count, uint[] samplers)
        {
            fixed (uint* samplersPtr = samplers)
                Delegates.glBindSamplers(first, count, samplersPtr);
        }
        public static void BindTexture(TextureTarget target, uint texture) => Delegates.glBindTexture(target, texture);
        public static void BindTextures(uint first, int count, uint[] textures)
        {
            fixed (uint* texturesPtr = textures)
                Delegates.glBindTextures(first, count, texturesPtr);
        }
        public static void BindTextureUnit(uint unit, uint texture) => Delegates.glBindTextureUnit(unit, texture);
        public static void BindTransformFeedback(NvTransformFeedback2 target, uint id) => Delegates.glBindTransformFeedback(target, id);
        public static void BindVertexArray(uint array) => Delegates.glBindVertexArray(array);
        public static void BindVertexBuffer(uint bindingindex, uint buffer, IntPtr offset, IntPtr stride) => Delegates.glBindVertexBuffer(bindingindex, buffer, offset, stride);
        public static void VertexArrayVertexBuffer(uint vaobj, uint bindingindex, uint buffer, IntPtr offset, int stride) => Delegates.glVertexArrayVertexBuffer(vaobj, bindingindex, buffer, offset, stride);
        public static void BindVertexBuffers(uint first, int count, uint[] buffers, IntPtr[] offsets, int[] strides)
        {
            fixed (uint* buffersPtr = buffers)
            fixed (IntPtr* offsetsPtr = offsets)
            fixed (int* stridesPtr = strides)
                Delegates.glBindVertexBuffers(first, count, buffersPtr, offsetsPtr, stridesPtr);
        }
        public static void VertexArrayVertexBuffers(uint vaobj, uint first, int count, uint[] buffers, IntPtr[] offsets, int[] strides)
        {
            fixed (uint* buffersPtr = buffers)
            fixed (IntPtr* offsetsPtr = offsets)
            fixed (int* stridesPtr = strides)
                Delegates.glVertexArrayVertexBuffers(vaobj, first, count, buffersPtr, offsetsPtr, stridesPtr);
        }
        public static void BlendColor(float red, float green, float blue, float alpha) => Delegates.glBlendColor(red, green, blue, alpha);
        public static void BlendEquation(BlendEquationMode mode) => Delegates.glBlendEquation(mode);
        public static void BlendEquationi(uint buf, BlendEquationMode mode) => Delegates.glBlendEquationi(buf, mode);
        public static void BlendEquationSeparate(BlendEquationMode modeRGB, BlendEquationMode modeAlpha) => Delegates.glBlendEquationSeparate(modeRGB, modeAlpha);
        public static void BlendEquationSeparatei(uint buf, BlendEquationMode modeRGB, BlendEquationMode modeAlpha) => Delegates.glBlendEquationSeparatei(buf, modeRGB, modeAlpha);
        public static void BlendFunc(BlendingFactorSrc sfactor, BlendingFactorDest dfactor) => Delegates.glBlendFunc(sfactor, dfactor);
        public static void BlendFunci(uint buf, BlendingFactorSrc sfactor, BlendingFactorDest dfactor) => Delegates.glBlendFunci(buf, sfactor, dfactor);
        public static void BlendFuncSeparate(BlendingFactorSrc srcRGB, BlendingFactorDest dstRGB, BlendingFactorSrc srcAlpha, BlendingFactorDest dstAlpha) => Delegates.glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha);
        public static void BlendFuncSeparatei(uint buf, BlendingFactorSrc srcRGB, BlendingFactorDest dstRGB, BlendingFactorSrc srcAlpha, BlendingFactorDest dstAlpha) => Delegates.glBlendFuncSeparatei(buf, srcRGB, dstRGB, srcAlpha, dstAlpha);
        public static void BlitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, ClearBufferMask mask, BlitFramebufferFilter filter) => Delegates.glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
        public static void BlitNamedFramebuffer(uint readFramebuffer, uint drawFramebuffer, int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, ClearBufferMask mask, BlitFramebufferFilter filter) => Delegates.glBlitNamedFramebuffer(readFramebuffer, drawFramebuffer, srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
        public static void BufferData<T>(BufferTarget target, int size, T[] data, BufferUsageHint usage) where T : unmanaged
        {
            fixed (void* p = data)
                Delegates.glBufferData(target, size, p, usage);
        }
        public static void BufferData<T>(BufferTarget target, int size, Span<T> data, BufferUsageHint usage) where T : unmanaged
        {
            fixed (void* p = data)
                Delegates.glBufferData(target, size, p, usage);
        }
        public static void BufferData<T>(BufferTarget target, int size, ref T data, BufferUsageHint usage) where T : unmanaged
        {
            fixed (T* ptr = &data)
                Delegates.glBufferData(target, size, ptr, usage);
        }
        public static void NamedBufferData(uint buffer, int size, IntPtr data, BufferUsageHint usage) => Delegates.glNamedBufferData(buffer, size, data, usage);
        public static void BufferStorage(BufferTarget target, int size, IntPtr data, uint flags) => Delegates.glBufferStorage(target, size, data, flags);
        public static void NamedBufferStorage(uint buffer, int size, IntPtr data, uint flags) => Delegates.glNamedBufferStorage(buffer, size, data, flags);
        public static void BufferSubData(BufferTarget target, int offset, int size, IntPtr data) => Delegates.glBufferSubData(target, offset, size, data);
        public static void NamedBufferSubData(uint buffer, IntPtr offset, int size, IntPtr data) => Delegates.glNamedBufferSubData(buffer, offset, size, data);
        public static void BufferSubData<T>(BufferTarget target, int offset, int size, ref T data) where T : unmanaged
        {
            fixed (T* ptr = &data)
                Delegates.glBufferSubDataVoidPtr(target, offset, size, ptr);
        }
        public static void NamedBufferSubData<T>(uint buffer, IntPtr offset, int size, ref T data) where T : unmanaged
        {
            fixed (T* ptr = &data)
                Delegates.glNamedBufferSubDataVoidPtr(buffer, offset, size, ptr);
        }
        public static void BufferSubData<T>(BufferTarget target, int offset, int size, Span<T> data) where T : unmanaged
        {
            fixed (T* ptr = data)
                Delegates.glBufferSubDataVoidPtr(target, offset, size, ptr);
        }
        public static void NamedBufferSubData<T>(uint buffer, IntPtr offset, int size, Span<T> data) where T : unmanaged
        {
            fixed (T* ptr = data)
                Delegates.glNamedBufferSubDataVoidPtr(buffer, offset, size, ptr);
        }

        public static void BufferSubData<T>(BufferTarget target, int offset, int size, ReadOnlySpan<T> data) where T : unmanaged
        {
            fixed (T* ptr = data)
                Delegates.glBufferSubDataVoidPtr(target, offset, size, ptr);
        }
        public static void NamedBufferSubData<T>(uint buffer, IntPtr offset, int size, ReadOnlySpan<T> data) where T : unmanaged
        {
            fixed (T* ptr = data)
                Delegates.glNamedBufferSubDataVoidPtr(buffer, offset, size, ptr);
        }

        public static FramebufferErrorCode CheckFramebufferStatus(FramebufferTarget target) => Delegates.glCheckFramebufferStatus(target);
        public static FramebufferErrorCode CheckNamedFramebufferStatus(uint framebuffer, FramebufferTarget target) => Delegates.glCheckNamedFramebufferStatus(framebuffer, target);
        public static void ClampColor(ClampColorTarget target, ClampColorMode clamp) => Delegates.glClampColor(target, clamp);
        public static void Clear(ClearBufferMask mask) => Delegates.glClear(mask);
        public static void ClearBufferiv(ClearBuffer buffer, int drawbuffer, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glClearBufferiv(buffer, drawbuffer, valuePtr);
        }
        public static void ClearBufferuiv(ClearBuffer buffer, int drawbuffer, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glClearBufferuiv(buffer, drawbuffer, valuePtr);
        }
        public static void ClearBufferfv(ClearBuffer buffer, int drawbuffer, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glClearBufferfv(buffer, drawbuffer, valuePtr);
        }
        public static void ClearBufferfi(ClearBuffer buffer, int drawbuffer, float depth, int stencil) => Delegates.glClearBufferfi(buffer, drawbuffer, depth, stencil);
        public static void ClearNamedFramebufferiv(uint framebuffer, ClearBuffer buffer, int drawbuffer, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glClearNamedFramebufferiv(framebuffer, buffer, drawbuffer, valuePtr);
        }
        public static void ClearNamedFramebufferuiv(uint framebuffer, ClearBuffer buffer, int drawbuffer, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glClearNamedFramebufferuiv(framebuffer, buffer, drawbuffer, valuePtr);
        }
        public static void ClearNamedFramebufferfv(uint framebuffer, ClearBuffer buffer, int drawbuffer, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glClearNamedFramebufferfv(framebuffer, buffer, drawbuffer, valuePtr);
        }
        public static void ClearNamedFramebufferfi(uint framebuffer, ClearBuffer buffer, int drawbuffer, float depth, int stencil) => Delegates.glClearNamedFramebufferfi(framebuffer, buffer, drawbuffer, depth, stencil);
        public static void ClearBufferData(BufferTarget target, SizedInternalFormat internalFormat, PixelInternalFormat format, PixelType type, IntPtr data) => Delegates.glClearBufferData(target, internalFormat, format, type, data);
        public static void ClearNamedBufferData(uint buffer, SizedInternalFormat internalFormat, PixelInternalFormat format, PixelType type, IntPtr data) => Delegates.glClearNamedBufferData(buffer, internalFormat, format, type, data);
        public static void ClearBufferSubData(BufferTarget target, SizedInternalFormat internalFormat, IntPtr offset, IntPtr size, PixelInternalFormat format, PixelType type, IntPtr data) => Delegates.glClearBufferSubData(target, internalFormat, offset, size, format, type, data);
        public static void ClearNamedBufferSubData(uint buffer, SizedInternalFormat internalFormat, IntPtr offset, int size, PixelInternalFormat format, PixelType type, IntPtr data) => Delegates.glClearNamedBufferSubData(buffer, internalFormat, offset, size, format, type, data);
        public static void ClearColor(float red, float green, float blue, float alpha) => Delegates.glClearColor(red, green, blue, alpha);
        public static void ClearDepth(double depth) => Delegates.glClearDepth(depth);
        public static void ClearDepthf(float depth) => Delegates.glClearDepthf(depth);
        public static void ClearStencil(int s) => Delegates.glClearStencil(s);
        public static void ClearTexImage(uint texture, int level, PixelInternalFormat format, PixelType type, IntPtr data) => Delegates.glClearTexImage(texture, level, format, type, data);
        public static void ClearTexSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelInternalFormat format, PixelType type, IntPtr data) => Delegates.glClearTexSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
        public static ArbSync ClientWaitSync(IntPtr sync, uint flags, ulong timeout) => Delegates.glClientWaitSync(sync, flags, timeout);
        public static void ClipControl(ClipControlOrigin origin, ClipControlDepth depth) => Delegates.glClipControl(origin, depth);
        public static void ColorMask(bool red, bool green, bool blue, bool alpha) => Delegates.glColorMask(red, green, blue, alpha);
        public static void ColorMaski(uint buf, bool red, bool green, bool blue, bool alpha) => Delegates.glColorMaski(buf, red, green, blue, alpha);
        public static void CompileShader(uint shader) => Delegates.glCompileShader(shader);
        public static void CompressedTexImage1D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int border, int imageSize, IntPtr data) => Delegates.glCompressedTexImage1D(target, level, internalFormat, width, border, imageSize, data);
        public static void CompressedTexImage2D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int border, int imageSize, IntPtr data) => Delegates.glCompressedTexImage2D(target, level, internalFormat, width, height, border, imageSize, data);
        public static void CompressedTexImage3D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int depth, int border, int imageSize, IntPtr data) => Delegates.glCompressedTexImage3D(target, level, internalFormat, width, height, depth, border, imageSize, data);
        public static void CompressedTexSubImage1D(TextureTarget target, int level, int xoffset, int width, PixelFormat format, int imageSize, IntPtr data) => Delegates.glCompressedTexSubImage1D(target, level, xoffset, width, format, imageSize, data);
        public static void CompressedTextureSubImage1D(uint texture, int level, int xoffset, int width, PixelInternalFormat format, int imageSize, IntPtr data) => Delegates.glCompressedTextureSubImage1D(texture, level, xoffset, width, format, imageSize, data);
        public static void CompressedTexSubImage2D(TextureTarget target, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, int imageSize, IntPtr data) => Delegates.glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data);
        public static void CompressedTextureSubImage2D(uint texture, int level, int xoffset, int yoffset, int width, int height, PixelInternalFormat format, int imageSize, IntPtr data) => Delegates.glCompressedTextureSubImage2D(texture, level, xoffset, yoffset, width, height, format, imageSize, data);
        public static void CompressedTexSubImage3D(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, int imageSize, IntPtr data) => Delegates.glCompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
        public static void CompressedTextureSubImage3D(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelInternalFormat format, int imageSize, IntPtr data) => Delegates.glCompressedTextureSubImage3D(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
        public static void CopyBufferSubData(BufferTarget readTarget, BufferTarget writeTarget, IntPtr readOffset, IntPtr writeOffset, IntPtr size) => Delegates.glCopyBufferSubData(readTarget, writeTarget, readOffset, writeOffset, size);
        public static void CopyNamedBufferSubData(uint readBuffer, uint writeBuffer, IntPtr readOffset, IntPtr writeOffset, int size) => Delegates.glCopyNamedBufferSubData(readBuffer, writeBuffer, readOffset, writeOffset, size);
        public static void CopyImageSubData(uint srcName, BufferTarget srcTarget, int srcLevel, int srcX, int srcY, int srcZ, uint dstName, BufferTarget dstTarget, int dstLevel, int dstX, int dstY, int dstZ, int srcWidth, int srcHeight, int srcDepth) => Delegates.glCopyImageSubData(srcName, srcTarget, srcLevel, srcX, srcY, srcZ, dstName, dstTarget, dstLevel, dstX, dstY, dstZ, srcWidth, srcHeight, srcDepth);
        public static void CopyTexImage1D(TextureTarget target, int level, PixelInternalFormat internalFormat, int x, int y, int width, int border) => Delegates.glCopyTexImage1D(target, level, internalFormat, x, y, width, border);
        public static void CopyTexImage2D(TextureTarget target, int level, PixelInternalFormat internalFormat, int x, int y, int width, int height, int border) => Delegates.glCopyTexImage2D(target, level, internalFormat, x, y, width, height, border);
        public static void CopyTexSubImage1D(TextureTarget target, int level, int xoffset, int x, int y, int width) => Delegates.glCopyTexSubImage1D(target, level, xoffset, x, y, width);
        public static void CopyTextureSubImage1D(uint texture, int level, int xoffset, int x, int y, int width) => Delegates.glCopyTextureSubImage1D(texture, level, xoffset, x, y, width);
        public static void CopyTexSubImage2D(TextureTarget target, int level, int xoffset, int yoffset, int x, int y, int width, int height) => Delegates.glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height);
        public static void CopyTextureSubImage2D(uint texture, int level, int xoffset, int yoffset, int x, int y, int width, int height) => Delegates.glCopyTextureSubImage2D(texture, level, xoffset, yoffset, x, y, width, height);
        public static void CopyTexSubImage3D(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) => Delegates.glCopyTexSubImage3D(target, level, xoffset, yoffset, zoffset, x, y, width, height);
        public static void CopyTextureSubImage3D(uint texture, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) => Delegates.glCopyTextureSubImage3D(texture, level, xoffset, yoffset, zoffset, x, y, width, height);
        public static void CreateBuffer(out uint buffer)
        {
            fixed (uint* ptr = &buffer)
                Delegates.glCreateBuffers(1, ptr);
        }
        public static void CreateBuffers(int n, out uint[] buffers)
        {
            buffers = new uint[n];
            fixed (uint* buffersPtr = buffers)
                Delegates.glCreateBuffers(n, buffersPtr);
        }
        public static void CreateFramebuffer(out uint id)
        {
            fixed (uint* ptr = &id)
                Delegates.glCreateFramebuffers(1, ptr);
        }
        public static void CreateFramebuffers(int n, out uint[] ids)
        {
            ids = new uint[n];
            fixed (uint* idsPtr = ids)
                Delegates.glCreateFramebuffers(n, idsPtr);
        }
        public static uint CreateProgram() => Delegates.glCreateProgram();
        public static void CreateProgramPipeline(out uint pipeline)
        {
            fixed (uint* ptr = &pipeline)
                Delegates.glCreateProgramPipelines(1, ptr);
        }
        public static void CreateProgramPipelines(int n, out uint[] pipelines)
        {
            pipelines = new uint[n];
            fixed (uint* pipelinesPtr = pipelines)
                Delegates.glCreateProgramPipelines(n, pipelinesPtr);
        }
        public static void CreateQuery(QueryTarget target, out uint id)
        {
            fixed (uint* ptr = &id)
                Delegates.glCreateQueries(target, 1, ptr);
        }
        public static void CreateQueries(QueryTarget target, int n, out uint[] ids)
        {
            ids = new uint[n];
            fixed (uint* idsPtr = ids)
                Delegates.glCreateQueries(target, n, idsPtr);
        }
        public static void CreateRenderBuffer(out uint renderbuffer)
        {
            fixed (uint* ptr = &renderbuffer)
                Delegates.glCreateRenderbuffers(1, ptr);
        }
        public static void CreateRenderbuffers(int n, out uint[] renderbuffers)
        {
            renderbuffers = new uint[n];
            fixed (uint* renderbuffersPtr = renderbuffers)
                Delegates.glCreateRenderbuffers(n, renderbuffersPtr);
        }
        public static void CreateSampler(out uint sampler)
        {
            fixed (uint* ptr = &sampler)
                Delegates.glCreateSamplers(1, ptr);
        }
        public static void CreateSamplers(int n, out uint[] samplers)
        {
            samplers = new uint[n];
            fixed (uint* samplersPtr = samplers)
                Delegates.glCreateSamplers(n, samplersPtr);
        }
        public static uint CreateShader(ShaderType shaderType) => Delegates.glCreateShader(shaderType);
        public static uint CreateShaderProgramv(ShaderType type, int count, out string[] strings)
        {
            strings = new string[count];
            var arr = stackalloc byte*[strings.Length];
            for (int i = 0; i < strings.Length; i++)
            {
                fixed (byte* ptr = Encoding.UTF8.GetBytes(strings[i]))
                    arr[i] = ptr;
            }
            return Delegates.glCreateShaderProgramv(type, count, arr);
        }
        public static void CreateTexture(TextureTarget target, out uint texture)
        {
            fixed (uint* ptr = &texture)
                Delegates.glCreateTextures(target, 1, ptr);
        }
        public static void CreateTextures(TextureTarget target, int n, out uint[] textures)
        {
            textures = new uint[n];
            fixed (uint* texturePtr = textures)
                Delegates.glCreateTextures(target, n, texturePtr);
        }
        public static void CreateTransformFeedback(out uint id)
        {
            fixed (uint* ptr = &id)
                Delegates.glCreateTransformFeedbacks(1, ptr);
        }
        public static void CreateTransformFeedbacks(int n, out uint[] ids)
        {
            ids = new uint[n];
            fixed (uint* idsPtr = ids)
                Delegates.glCreateTransformFeedbacks(n, idsPtr);
        }
        public static void CreateVertexArray(out uint array)
        {
            fixed (uint* ptr = &array)
                Delegates.glCreateVertexArrays(1, ptr);
        }
        public static void CreateVertexArrays(int n, out uint[] arrays)
        {
            arrays = new uint[n];
            fixed (uint* arraysPtr = arrays)
                Delegates.glCreateVertexArrays(n, arraysPtr);
        }
        public static void CullFace(CullFaceMode mode) => Delegates.glCullFace(mode);
        public static void DeleteBuffer(uint buffer) => Delegates.glDeleteBuffers(1, &buffer);
        public static void DeleteBuffers(int n, uint[] buffers)
        {
            fixed (uint* ptr = buffers)
                Delegates.glDeleteBuffers(n, ptr);
        }
        public static void DeleteFramebuffer(uint framebuffer) => Delegates.glDeleteFramebuffers(1, &framebuffer);
        public static void DeleteFramebuffers(int n, uint[] framebuffers)
        {
            fixed (uint* framebuffersPtr = framebuffers)
                Delegates.glDeleteFramebuffers(n, framebuffersPtr);
        }
        public static void DeleteProgram(uint program) => Delegates.glDeleteProgram(program);
        public static void DeleteProgramPipeline(uint pipeline) => Delegates.glDeleteProgramPipelines(1, &pipeline);
        public static void DeleteProgramPipelines(int n, uint[] pipelines)
        {
            fixed (uint* pipelinesPtr = pipelines)
                Delegates.glDeleteProgramPipelines(n, pipelinesPtr);
        }
        public static void DeleteQuery(uint id) => Delegates.glDeleteQueries(1, &id);
        public static void DeleteQueries(int n, uint[] ids)
        {
            fixed (uint* idsPtr = ids)
                Delegates.glDeleteQueries(n, idsPtr);
        }
        public static void DeleteRenderBuffer(uint renderbuffer) => Delegates.glDeleteRenderbuffers(1, &renderbuffer);
        public static void DeleteRenderbuffers(int n, uint[] renderbuffers)
        {
            fixed (uint* renderbuffersPtr = renderbuffers)
                Delegates.glDeleteRenderbuffers(n, renderbuffersPtr);
        }
        public static void DeleteSampler(uint sampler) => Delegates.glDeleteSamplers(1, &sampler);
        public static void DeleteSamplers(int n, uint[] samplers)
        {
            fixed (uint* samplersPtr = samplers)
                Delegates.glDeleteSamplers(n, samplersPtr);
        }
        public static void DeleteShader(uint shader) => Delegates.glDeleteShader(shader);
        public static void DeleteSync(IntPtr sync) => Delegates.glDeleteSync(sync);
        public static void DeleteTexture(uint texture) => Delegates.glDeleteTextures(1, &texture);
        public static void DeleteTextures(int n, uint[] textures)
        {
            fixed (uint* texturesPtr = textures)
                Delegates.glDeleteTextures(n, texturesPtr);
        }
        public static void DeleteTransformFeedback(uint id) => Delegates.glDeleteTransformFeedbacks(1, &id);
        public static void DeleteTransformFeedbacks(int n, uint[] ids)
        {
            fixed (uint* idsPtr = ids)
                Delegates.glDeleteTransformFeedbacks(n, idsPtr);
        }
        public static void DeleteVertexArray(uint array) => Delegates.glDeleteVertexArrays(1, &array);
        public static void DeleteVertexArrays(int n, uint[] arrays)
        {
            fixed (uint* ptr = arrays)
                Delegates.glDeleteVertexArrays(n, ptr);
        }
        public static void DepthFunc(DepthFunction func) => Delegates.glDepthFunc(func);
        public static void DepthMask(bool flag) => Delegates.glDepthMask(flag);
        public static void DepthRange(double nearVal, double farVal) => Delegates.glDepthRange(nearVal, farVal);
        public static void DepthRangef(float nearVal, float farVal) => Delegates.glDepthRangef(nearVal, farVal);
        public static void DepthRangeArrayv(uint first, int count, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glDepthRangeArrayv(first, count, vPtr);
        }
        public static void DepthRangeIndexed(uint index, double nearVal, double farVal) => Delegates.glDepthRangeIndexed(index, nearVal, farVal);
        public static void DetachShader(uint program, uint shader) => Delegates.glDetachShader(program, shader);
        public static void DispatchCompute(uint num_groups_x, uint num_groups_y, uint num_groups_z) => Delegates.glDispatchCompute(num_groups_x, num_groups_y, num_groups_z);
        public static void DispatchComputeIndirect(IntPtr indirect) => Delegates.glDispatchComputeIndirect(indirect);
        public static void DrawArrays(BeginMode mode, int first, int count) => Delegates.glDrawArrays(mode, first, count);
        public static void DrawArraysIndirect(BeginMode mode, IntPtr indirect) => Delegates.glDrawArraysIndirect(mode, indirect);
        public static void DrawArraysInstanced(BeginMode mode, int first, int count, int primcount) => Delegates.glDrawArraysInstanced(mode, first, count, primcount);
        public static void DrawArraysInstancedBaseInstance(BeginMode mode, int first, int count, int primcount, uint baseinstance) => Delegates.glDrawArraysInstancedBaseInstance(mode, first, count, primcount, baseinstance);
        public static void DrawBuffer(DrawBufferMode buf) => Delegates.glDrawBuffer(buf);
        public static void NamedFramebufferDrawBuffer(uint framebuffer, DrawBufferMode buf) => Delegates.glNamedFramebufferDrawBuffer(framebuffer, buf);
        public static void DrawBuffers(int n, DrawBuffersEnum[] bufs)
        {
            fixed (DrawBuffersEnum* bufsPtr = bufs)
                Delegates.glDrawBuffers(n, bufsPtr);
        }
        public static void NamedFramebufferDrawBuffers(uint framebuffer, int n, DrawBufferMode[] bufs)
        {
            fixed (DrawBufferMode* bufsPtr = bufs)
                Delegates.glNamedFramebufferDrawBuffers(framebuffer, n, bufsPtr);
        }
        public static void DrawElements(BeginMode mode, int count, DrawElementsType type, IntPtr indices) => Delegates.glDrawElements(mode, count, type, indices);
        public static void DrawElementsBaseVertex(BeginMode mode, int count, DrawElementsType type, IntPtr indices, int basevertex) => Delegates.glDrawElementsBaseVertex(mode, count, type, indices, basevertex);
        public static void DrawElementsIndirect(BeginMode mode, DrawElementsType type, IntPtr indirect) => Delegates.glDrawElementsIndirect(mode, type, indirect);
        public static void DrawElementsInstanced(BeginMode mode, int count, DrawElementsType type, IntPtr indices, int primcount) => Delegates.glDrawElementsInstanced(mode, count, type, indices, primcount);
        public static void DrawElementsInstancedBaseInstance(BeginMode mode, int count, DrawElementsType type, IntPtr indices, int primcount, uint baseinstance) => Delegates.glDrawElementsInstancedBaseInstance(mode, count, type, indices, primcount, baseinstance);
        public static void DrawElementsInstancedBaseVertex(BeginMode mode, int count, DrawElementsType type, IntPtr indices, int primcount, int basevertex) => Delegates.glDrawElementsInstancedBaseVertex(mode, count, type, indices, primcount, basevertex);
        public static void DrawElementsInstancedBaseVertexBaseInstance(BeginMode mode, int count, DrawElementsType type, IntPtr indices, int primcount, int basevertex, uint baseinstance) => Delegates.glDrawElementsInstancedBaseVertexBaseInstance(mode, count, type, indices, primcount, basevertex, baseinstance);
        public static void DrawRangeElements(BeginMode mode, uint start, uint end, int count, DrawElementsType type, IntPtr indices) => Delegates.glDrawRangeElements(mode, start, end, count, type, indices);
        public static void DrawRangeElementsBaseVertex(BeginMode mode, uint start, uint end, int count, DrawElementsType type, IntPtr indices, int basevertex) => Delegates.glDrawRangeElementsBaseVertex(mode, start, end, count, type, indices, basevertex);
        public static void DrawTransformFeedback(NvTransformFeedback2 mode, uint id) => Delegates.glDrawTransformFeedback(mode, id);
        public static void DrawTransformFeedbackInstanced(BeginMode mode, uint id, int primcount) => Delegates.glDrawTransformFeedbackInstanced(mode, id, primcount);
        public static void DrawTransformFeedbackStream(NvTransformFeedback2 mode, uint id, uint stream) => Delegates.glDrawTransformFeedbackStream(mode, id, stream);
        public static void DrawTransformFeedbackStreamInstanced(BeginMode mode, uint id, uint stream, int primcount) => Delegates.glDrawTransformFeedbackStreamInstanced(mode, id, stream, primcount);
        public static void Enable(EnableCap cap) => Delegates.glEnable(cap);
        public static void Disable(EnableCap cap) => Delegates.glDisable(cap);
        public static void Enablei(EnableCap cap, uint index) => Delegates.glEnablei(cap, index);
        public static void Disablei(EnableCap cap, uint index) => Delegates.glDisablei(cap, index);
        public static void EnableVertexAttribArray(uint index) => Delegates.glEnableVertexAttribArray(index);
        public static void DisableVertexAttribArray(uint index) => Delegates.glDisableVertexAttribArray(index);
        public static void EnableVertexArrayAttrib(uint vaobj, uint index) => Delegates.glEnableVertexArrayAttrib(vaobj, index);
        public static void DisableVertexArrayAttrib(uint vaobj, uint index) => Delegates.glDisableVertexArrayAttrib(vaobj, index);
        public static IntPtr FenceSync(ArbSync condition, uint flags) => Delegates.glFenceSync(condition, flags);
        public static void Finish() => Delegates.glFinish();
        public static void Flush() => Delegates.glFlush();
        public static void FlushMappedBufferRange(BufferTarget target, IntPtr offset, IntPtr length) => Delegates.glFlushMappedBufferRange(target, offset, length);
        public static void FlushMappedNamedBufferRange(uint buffer, IntPtr offset, int length) => Delegates.glFlushMappedNamedBufferRange(buffer, offset, length);
        public static void FramebufferParameteri(FramebufferTarget target, FramebufferPName pname, int param) => Delegates.glFramebufferParameteri(target, pname, param);
        public static void NamedFramebufferParameteri(uint framebuffer, FramebufferPName pname, int param) => Delegates.glNamedFramebufferParameteri(framebuffer, pname, param);
        public static void FramebufferRenderbuffer(FramebufferTarget target, FramebufferAttachment attachment, RenderbufferTarget renderbuffertarget, uint renderbuffer) => Delegates.glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer);
        public static void NamedFramebufferRenderbuffer(uint framebuffer, FramebufferAttachment attachment, RenderbufferTarget renderbuffertarget, uint renderbuffer) => Delegates.glNamedFramebufferRenderbuffer(framebuffer, attachment, renderbuffertarget, renderbuffer);
        public static void FramebufferTexture(FramebufferTarget target, FramebufferAttachment attachment, uint texture, int level) => Delegates.glFramebufferTexture(target, attachment, texture, level);
        public static void FramebufferTexture1D(FramebufferTarget target, FramebufferAttachment attachment, TextureTarget textarget, uint texture, int level) => Delegates.glFramebufferTexture1D(target, attachment, textarget, texture, level);
        public static void FramebufferTexture2D(FramebufferTarget target, FramebufferAttachment attachment, TextureTarget textarget, uint texture, int level) => Delegates.glFramebufferTexture2D(target, attachment, textarget, texture, level);
        public static void FramebufferTexture3D(FramebufferTarget target, FramebufferAttachment attachment, TextureTarget textarget, uint texture, int level, int layer) => Delegates.glFramebufferTexture3D(target, attachment, textarget, texture, level, layer);
        public static void NamedFramebufferTexture(uint framebuffer, FramebufferAttachment attachment, uint texture, int level) => Delegates.glNamedFramebufferTexture(framebuffer, attachment, texture, level);
        public static void FramebufferTextureLayer(FramebufferTarget target, FramebufferAttachment attachment, uint texture, int level, int layer) => Delegates.glFramebufferTextureLayer(target, attachment, texture, level, layer);
        public static void NamedFramebufferTextureLayer(uint framebuffer, FramebufferAttachment attachment, uint texture, int level, int layer) => Delegates.glNamedFramebufferTextureLayer(framebuffer, attachment, texture, level, layer);
        public static void FrontFace(FrontFaceDirection mode) => Delegates.glFrontFace(mode);
        public static void GenBuffers(int n, out uint[] buffers)
        {
            buffers = new uint[n];
            fixed (uint* buffersPtr = &buffers[0])
                Delegates.glGenBuffers(n, buffersPtr);
        }
        public static void GenerateMipmap(GenerateMipmapTarget target) => Delegates.glGenerateMipmap(target);
        public static void GenerateTextureMipmap(uint texture) => Delegates.glGenerateTextureMipmap(texture);
        public static void GenFramebuffers(int n, out uint[] ids)
        {
            ids = new uint[n];
            fixed (uint* idsPtr = &ids[0])
                Delegates.glGenFramebuffers(n, idsPtr);
        }
        public static void GenProgramPipelines(int n, out uint[] pipelines)
        {
            pipelines = new uint[n];
            fixed (uint* pipelinesPtr = &pipelines[0])
                Delegates.glGenProgramPipelines(n, pipelinesPtr);
        }
        public static void GenQueries(int n, out uint[] ids)
        {
            ids = new uint[n];
            fixed (uint* idsPtr = &ids[0])
                Delegates.glGenQueries(n, idsPtr);
        }
        public static void GenRenderbuffers(int n, out uint[] renderbuffers)
        {
            renderbuffers = new uint[n];
            fixed (uint* renderbuffersPtr = &renderbuffers[0])
                Delegates.glGenRenderbuffers(n, renderbuffersPtr);
        }
        public static void GenSamplers(int n, out uint[] samplers)
        {
            samplers = new uint[n];
            fixed (uint* samplersPtr = &samplers[0])
                Delegates.glGenSamplers(n, samplersPtr);
        }
        public static void GenTextures(int n, out uint[] textures)
        {
            textures = new uint[n];
            fixed (uint* texturesPtr = &textures[0])
                Delegates.glGenTextures(n, texturesPtr);
        }
        public static void GenTransformFeedbacks(int n, out uint[] ids)
        {
            ids = new uint[n];
            fixed (uint* idsPtr = &ids[0])
                Delegates.glGenTransformFeedbacks(n, idsPtr);
        }
        public static void GenVertexArrays(int n, out uint[] arrays)
        {
            arrays = new uint[n];
            fixed (uint* arraysPtr = &arrays[0])
                Delegates.glGenVertexArrays(n, arraysPtr);
        }

        internal static int GetLength(GetPName pName)
        {
            if (pName == GetPName.CompressedTextureFormats)
            {
                GetIntegerv(GetPName.NumCompressedTextureFormats, out var data);
                return data[0];
            }
            else if (pName == GetPName.ProgramBinaryFormats)
            {
                GetIntegerv(GetPName.NumProgramBinaryFormats, out var data);
                return data[0];
            }

            return pName switch
            {
                GetPName.AliasedLineWidthRange => 2,
                GetPName.BlendColor => 4,
                GetPName.ColorClearValue => 4,
                GetPName.ColorWritemask => 4,
                GetPName.MaxComputeWorkGroupCount => 3,
                GetPName.MaxComputeWorkGroupSize => 3,
                GetPName.DepthRange => 2,
                GetPName.MaxViewportDims => 2,
                GetPName.PointSizeRange => 2,
                GetPName.ScissorBox => 4,
                GetPName.SmoothLineWidthRange => 2,
                GetPName.Viewport => 4,
                GetPName.ViewportBoundsRange => 2,
                _ => 1,
            };
        }

        public static void GetBooleanv(GetPName pname, out Span<bool> data)
        {
            data = new bool[GetLength(pname)];
            fixed (bool* ptr = data)
                Delegates.glGetBooleanv(pname, ptr);
        }
        public static void GetDoublev(GetPName pname, out Span<double> data)
        {
            data = new double[GetLength(pname)];
            fixed (double* ptr = data)
                Delegates.glGetDoublev(pname, ptr);
        }
        public static void GetFloatv(GetPName pname, out Span<float> data)
        {
            data = new float[GetLength(pname)];
            fixed (float* ptr = data)
                Delegates.glGetFloatv(pname, ptr);
        }
        public static void GetIntegerv(GetPName pname, out Span<int> data)
        {
            data = new int[GetLength(pname)];
            fixed (int* ptr = data)
                Delegates.glGetIntegerv(pname, ptr);
        }
        public static void GetInteger64v(GetPName pname, out Span<long> data)
        {
            data = new long[GetLength(pname)];
            fixed (long* ptr = data)
                Delegates.glGetInteger64v(pname, ptr);
        }
        public static void GetBooleani_v(GetPName target, uint index, out bool data)
        {
            fixed (bool* ptr = &data)
                Delegates.glGetBooleani_v(target, index, ptr);
        }
        public static void GetIntegeri_v(GetPName target, uint index, out int data)
        {
            fixed (int* ptr = &data)
                Delegates.glGetIntegeri_v(target, index, ptr);
        }
        public static void GetFloati_v(GetPName target, uint index, out float data)
        {
            fixed (float* ptr = &data)
                Delegates.glGetFloati_v(target, index, ptr);
        }
        public static void GetDoublei_v(GetPName target, uint index, out double data)
        {
            fixed (double* ptr = &data)
                Delegates.glGetDoublei_v(target, index, ptr);
        }
        public static void GetInteger64i_v(GetPName target, uint index, out long data)
        {
            fixed (long* ptr = &data)
                Delegates.glGetInteger64i_v(target, index, ptr);
        }
        public static void GetActiveAtomicCounterBufferiv(uint program, uint bufferIndex, AtomicCounterParameterName pname, out Span<int> @params)
        {
            fixed (int* ptr = @params)
                Delegates.glGetActiveAtomicCounterBufferiv(program, bufferIndex, pname, ptr);
        }
#nullable enable
        public static unsafe void GetActiveAttrib(uint program, uint index, int bufSize, out int length, out int size, out ActiveAttribType type, out string? name)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
            fixed (int* sizePtr = &size)
            fixed (ActiveAttribType* typePtr = &type)
                Delegates.glGetActiveAttrib(program, index, bufSize, lengthPtr, sizePtr, typePtr, arr);
            if (length == 0) name = null;
            else name = Encoding.UTF8.GetString(arr, length);
        }
        public static unsafe void GetActiveSubroutineName(uint program, ShaderType shadertype, uint index, int bufsize, out int length, out string? name)
        {
            var arr = stackalloc byte[bufsize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetActiveSubroutineName(program, shadertype, index, bufsize, lengthPtr, arr);
            if (length == 0) name = null;
            else name = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable
        public static void GetActiveSubroutineUniformiv(uint program, ShaderType shadertype, uint index, SubroutineParameterName pname, out Span<int> values)
        {
            switch (pname)
            {
                case SubroutineParameterName.CompatibleSubroutines:
                    GetActiveSubroutineUniformiv(program, shadertype, index, SubroutineParameterName.NumCompatibleSubroutines, out var v);
                    values = new int[v[0]];
                    break;
                default:
                    values = new int[1];
                    break;
            }

            fixed (int* ptr = values)
                Delegates.glGetActiveSubroutineUniformiv(program, shadertype, index, pname, ptr);
        }
#nullable enable
        public static unsafe void GetActiveSubroutineUniformName(uint program, ShaderType shadertype, uint index, int bufsize, out int length, out string? name)
        {
            var arr = stackalloc byte[bufsize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetActiveSubroutineUniformName(program, shadertype, index, bufsize, lengthPtr, arr);
            if (length == 0) name = null;
            else name = Encoding.UTF8.GetString(arr, length);
        }
        public static unsafe void GetActiveUniform(uint program, uint index, int bufSize, out int length, out int size, out ActiveUniformType type, out string? name)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
            fixed (int* sizePtr = &size)
            fixed (ActiveUniformType* typePtr = &type)
                Delegates.glGetActiveUniform(program, index, bufSize, lengthPtr, sizePtr, typePtr, arr);
            if (length == 0) name = null;
            else name = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable
        public static void GetActiveUniformBlockiv(uint program, uint uniformBlockIndex, ActiveUniformBlockParameter pname, out Span<int> @params)
        {
            switch (pname)
            {
                case ActiveUniformBlockParameter.UniformBlockActiveUniformIndices:
                    GetActiveUniformBlockiv(program, uniformBlockIndex, ActiveUniformBlockParameter.UniformBlockActiveUniforms, out var p);
                    @params = new int[p[0]];
                    break;
                default:
                    @params = new int[1];
                    break;
            }

            fixed (int* ptr = @params)
                Delegates.glGetActiveUniformBlockiv(program, uniformBlockIndex, pname, ptr);
        }
#nullable enable
        public static unsafe void GetActiveUniformBlockName(uint program, uint uniformBlockIndex, int bufSize, out int length, out string? uniformBlockName)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetActiveUniformBlockName(program, uniformBlockIndex, bufSize, lengthPtr, arr);
            if (length == 0) uniformBlockName = null;
            else uniformBlockName = Encoding.UTF8.GetString(arr, length);
        }
        public static unsafe void GetActiveUniformName(uint program, uint uniformIndex, int bufSize, out int length, out string? uniformName)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetActiveUniformName(program, uniformIndex, bufSize, lengthPtr, arr);
            if (length == 0) uniformName = null;
            else uniformName = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable
        public static void GetActiveUniformsiv(uint program, int uniformCount, out uint[] uniformIndices, ActiveUniformType pname, out int[] @params)
        {
            uniformIndices = new uint[uniformCount];
            @params = new int[uniformCount];
            fixed (uint* uniformIndicesPtr = uniformIndices)
            fixed (int* paramsPtr = @params)
                Delegates.glGetActiveUniformsiv(program, uniformCount, uniformIndicesPtr, pname, @paramsPtr);
        }
#nullable enable
        public static void GetAttachedShaders(uint program, int maxCount, out int count, out uint[]? shaders)
        {
            fixed (int* countPtr = &count)
            {
                uint shaderPtr = 0;
                uint* walkPtr = &shaderPtr;
                Delegates.glGetAttachedShaders(program, maxCount, countPtr, walkPtr);
                if (count == 0) shaders = null;
                else
                {
                    shaders = new uint[count];
                    for (int i = 0; i < count; i++)
                        shaders[i] = *(walkPtr + i);
                }
            }
        }
#nullable disable
        public static int GetAttribLocation(uint program, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetAttribLocation(program, namePtr);
        }
        public static void GetBufferParameteriv(BufferTarget target, BufferParameterName value, out int data)
        {
            fixed (int* ptr = &data)
                Delegates.glGetBufferParameteriv(target, value, ptr);
        }
        public static void GetBufferParameteri64v(BufferTarget target, BufferParameterName value, out long data)
        {
            fixed (long* ptr = &data)
                Delegates.glGetBufferParameteri64v(target, value, ptr);
        }
        public static void GetNamedBufferParameteriv(uint buffer, BufferParameterName pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetNamedBufferParameteriv(buffer, pname, ptr);
        }
        public static void GetNamedBufferParameteri64v(uint buffer, BufferParameterName pname, out long @params)
        {
            fixed (long* ptr = &@params)
                Delegates.glGetNamedBufferParameteri64v(buffer, pname, ptr);
        }
        public static void GetBufferPointerv(BufferTarget target, BufferPointer pname, out IntPtr @params)
        {
            fixed (IntPtr* @paramsPtr = &@params)
                Delegates.glGetBufferPointerv(target, pname, @paramsPtr);
        }
        public static void GetNamedBufferPointerv(uint buffer, BufferPointer pname, out IntPtr @params)
        {
            fixed (IntPtr* @paramsPtr = &@params)
                Delegates.glGetNamedBufferPointerv(buffer, pname, @paramsPtr);
        }
        public static void GetBufferSubData(BufferTarget target, IntPtr offset, IntPtr size, out IntPtr data)
        {
            fixed (IntPtr* dataPtr = &data)
                Delegates.glGetBufferSubData(target, offset, size, dataPtr);
        }
        public static void GetNamedBufferSubData(uint buffer, IntPtr offset, int size, out IntPtr data)
        {
            fixed (IntPtr* dataPtr = &data)
                Delegates.glGetNamedBufferSubData(buffer, offset, size, dataPtr);
        }
        public static void GetCompressedTexImage(TextureTarget target, int level, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetCompressedTexImage(target, level, pixelsPtr);
        }
        public static void GetnCompressedTexImage(TextureTarget target, int level, int bufSize, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetnCompressedTexImage(target, level, bufSize, pixelsPtr);
        }
        public static void GetCompressedTextureImage(uint texture, int level, int bufSize, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetCompressedTextureImage(texture, level, bufSize, pixelsPtr);
        }
        public static void GetCompressedTextureSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetCompressedTextureSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth, bufSize, pixelsPtr);
        }
        public static ErrorCode GetError() => Delegates.glGetError();
        public static int GetFragDataIndex(uint program, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetFragDataIndex(program, namePtr);
        }
        public static int GetFragDataLocation(uint program, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetFragDataLocation(program, namePtr);
        }
        public static void GetFramebufferAttachmentParameteriv(FramebufferTarget target, FramebufferAttachment attachment, FramebufferParameterName pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetFramebufferAttachmentParameteriv(target, attachment, pname, ptr);
        }
        public static void GetNamedFramebufferAttachmentParameteriv(uint framebuffer, FramebufferAttachment attachment, FramebufferParameterName pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetNamedFramebufferAttachmentParameteriv(framebuffer, attachment, pname, ptr);
        }
        public static void GetFramebufferParameteriv(FramebufferTarget target, FramebufferPName pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetFramebufferParameteriv(target, pname, ptr);
        }
        public static void GetNamedFramebufferParameteriv(uint framebuffer, FramebufferPName pname, out int param)
        {
            fixed (int* ptr = &param)
                Delegates.glGetNamedFramebufferParameteriv(framebuffer, pname, ptr);
        }
        public static GraphicResetStatus GetGraphicsResetStatus() => Delegates.glGetGraphicsResetStatus();
        public static void GetInternalformativ(TextureTarget target, PixelInternalFormat internalFormat, GetPName pname, int bufSize, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetInternalformativ(target, internalFormat, pname, bufSize, ptr);
        }
        public static void GetInternalformati64v(TextureTarget target, PixelInternalFormat internalFormat, GetPName pname, int bufSize, out long @params)
        {
            fixed (long* ptr = &@params)
                Delegates.glGetInternalformati64v(target, internalFormat, pname, bufSize, ptr);
        }
        public static void GetMultisamplefv(GetMultisamplePName pname, uint index, out Span<float> vals)
        {
            vals = new float[2];
            fixed (float* ptr = vals)
                Delegates.glGetMultisamplefv(pname, index, ptr);
        }
#nullable enable
        public static unsafe void GetObjectLabel(ObjectLabelType identifier, uint name, int bufSize, out int length, out string? label)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetObjectLabel(identifier, name, bufSize, lengthPtr, arr);
            if (length == 0) label = null;
            else label = Encoding.UTF8.GetString(arr, length);
        }
        public static unsafe void GetObjectPtrLabel(out IntPtr ptr, int bufSize, out int length, out string? label)
        {
            var arr = stackalloc byte[bufSize];
            fixed (IntPtr* pointerPtr = &ptr)
            fixed (int* lengthPtr = &length)
                Delegates.glGetObjectPtrLabel(pointerPtr, bufSize, lengthPtr, arr);
            if (length == 0) label = null;
            else label = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable
        public static void GetPointerv(GetPointerParameter pname, out IntPtr @params)
        {
            fixed (IntPtr* @paramsPtr = &@params)
                Delegates.glGetPointerv(pname, @paramsPtr);
        }
        public static unsafe void GetProgramiv(uint program, ProgramParameter pname, out int @params)
        {
            fixed (int* p = &@params)
                Delegates.glGetProgramiv(program, pname, p);
        }
#nullable enable
        public static unsafe void GetProgramInfoLog(uint program, int maxLength, out int length, out string? infoLog)
        {
            var arr = stackalloc byte[maxLength];
            fixed (int* lengthPtr = &length)
                Delegates.glGetProgramInfoLog(program, maxLength, lengthPtr, arr);
            if (length == 0) infoLog = null;
            else infoLog = Encoding.UTF8.GetString(arr, length);
        }

        public static void GetProgramBinary(uint program, int bufsize, out int length, out int[]? binaryFormat, out IntPtr binary)
        {
            fixed (IntPtr* binaryPtr = &binary)
            fixed (int* lengthPtr = &length)
            {
                int binaryFormatPtr = 0;
                int* walkPtr = &binaryFormatPtr;
                Delegates.glGetProgramBinary(program, bufsize, lengthPtr, walkPtr, binaryPtr);
                if (length == 0) binaryFormat = null;
                else
                {
                    binaryFormat = new int[length];
                    for (int i = 0; i < length; i++)
                        binaryFormat[i] = *(walkPtr + i);
                }
            }
        }
#nullable disable
        public static void GetProgramInterfaceiv(uint program, ProgramInterface programInterface, ProgramInterfaceParameterName pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetProgramInterfaceiv(program, programInterface, pname, ptr);
        }
        public static void GetProgramPipelineiv(uint pipeline, int pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetProgramPipelineiv(pipeline, pname, ptr);
        }
#nullable enable
        public static unsafe void GetProgramPipelineInfoLog(uint pipeline, int bufSize, out int length, out string? infoLog)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetProgramPipelineInfoLog(pipeline, bufSize, lengthPtr, arr);
            if (length == 0) infoLog = null;
            else infoLog = Encoding.UTF8.GetString(arr, length);
        }
        public static void GetProgramResourceiv(uint program, ProgramInterface programInterface, uint index, int propCount, out ProgramResourceParameterName[]? props, int bufSize, out int length, out int[]? @params)
        {
            fixed (int* lengthPtr = &length)
            {
                ProgramResourceParameterName programResourceParameterName = default;
                int paramsPtr = 0;

                ProgramResourceParameterName* programResourceParameterNameWalkPtr = &programResourceParameterName;
                int* walkPtr = &paramsPtr;

                Delegates.glGetProgramResourceiv(program, programInterface, index, propCount, programResourceParameterNameWalkPtr, bufSize, lengthPtr, walkPtr);

                if (propCount == 0) props = null;
                else
                {
                    props = new ProgramResourceParameterName[propCount];
                    for (int i = 0; i < propCount; i++)
                        props[i] = *(programResourceParameterNameWalkPtr + i);
                }

                if (length == 0) @params = null;
                else
                {
                    @params = new int[length];
                    var pawalkPtr = &paramsPtr;
                    for (int i = 0; i < length; i++)
                        @params[i] = *(walkPtr + i);
                }
            }
        }
#nullable enable
        public static uint GetProgramResourceIndex(uint program, ProgramInterface programInterface, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetProgramResourceIndex(program, programInterface, namePtr);
        }
        public static int GetProgramResourceLocation(uint program, ProgramInterface programInterface, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetProgramResourceLocation(program, programInterface, namePtr);
        }
        public static int GetProgramResourceLocationIndex(uint program, ProgramInterface programInterface, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetProgramResourceLocationIndex(program, programInterface, namePtr);
        }
#nullable enable
        public static unsafe void GetProgramResourceName(uint program, ProgramInterface programInterface, uint index, int bufSize, out int length, out string? name)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetProgramResourceName(program, programInterface, index, bufSize, lengthPtr, arr);
            if (length == 0) name = null;
            else name = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable
        public static void GetProgramStageiv(uint program, ShaderType shadertype, ProgramStageParameterName pname, out int values)
        {
            fixed (int* ptr = &values)
                Delegates.glGetProgramStageiv(program, shadertype, pname, ptr);
        }
        public static void GetQueryIndexediv(QueryTarget target, uint index, GetQueryParam pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetQueryIndexediv(target, index, pname, ptr);
        }
        public static void GetQueryiv(QueryTarget target, GetQueryParam pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetQueryiv(target, pname, ptr);
        }
        public static void GetQueryObjectiv(uint id, GetQueryObjectParam pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetQueryObjectiv(id, pname, ptr);
        }
        public static void GetQueryObjectuiv(uint id, GetQueryObjectParam pname, out uint @params)
        {
            fixed (uint* ptr = &@params)
                Delegates.glGetQueryObjectuiv(id, pname, ptr);
        }
        public static void GetQueryObjecti64v(uint id, GetQueryObjectParam pname, out long @params)
        {
            fixed (long* ptr = &@params)
                Delegates.glGetQueryObjecti64v(id, pname, ptr);
        }
        public static void GetQueryObjectui64v(uint id, GetQueryObjectParam pname, out ulong @params)
        {
            fixed (ulong* ptr = &@params)
                Delegates.glGetQueryObjectui64v(id, pname, ptr);
        }
        public static void GetRenderbufferParameteriv(RenderbufferTarget target, RenderbufferParameterName pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetRenderbufferParameteriv(target, pname, ptr);
        }
        public static void GetNamedRenderbufferParameteriv(uint renderbuffer, RenderbufferParameterName pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetNamedRenderbufferParameteriv(renderbuffer, pname, ptr);
        }

        internal static int GetSamplerParameterLength(TextureParameterName pname)
        {
            return pname switch
            {
                TextureParameterName.TextureBorderColor => 4,
                _ => 1
            };
        }
        public static void GetSamplerParameterfv(uint sampler, TextureParameterName pname, out Span<float> @params)
        {
            @params = new float[GetSamplerParameterLength(pname)];
            fixed (float* ptr = @params)
                Delegates.glGetSamplerParameterfv(sampler, pname, ptr);
        }
        public static void GetSamplerParameteriv(uint sampler, TextureParameterName pname, out Span<int> @params)
        {
            @params = new int[GetSamplerParameterLength(pname)];
            fixed (int* ptr = @params)
                Delegates.glGetSamplerParameteriv(sampler, pname, ptr);
        }
        public static void GetSamplerParameterIiv(uint sampler, TextureParameterName pname, out Span<int> @params)
        {
            @params = new int[GetSamplerParameterLength(pname)];
            fixed (int* ptr = @params)
                Delegates.glGetSamplerParameterIiv(sampler, pname, ptr);
        }
        public static void GetSamplerParameterIuiv(uint sampler, TextureParameterName pname, out Span<uint> @params)
        {
            @params = new uint[GetSamplerParameterLength(pname)];
            fixed (uint* ptr = @params)
                Delegates.glGetSamplerParameterIuiv(sampler, pname, ptr);
        }
        public static unsafe void GetShaderiv(uint shader, ShaderParameter pname, out int @params)
        {
            fixed (int* p = &@params)
                Delegates.glGetShaderiv(shader, pname, p);
        }
#nullable enable
        public static unsafe void GetShaderInfoLog(uint shader, int maxLength, out int length, out string? infoLog)
        {
            var arr = stackalloc byte[maxLength];
            fixed (int* lengthPtr = &length)
                Delegates.glGetShaderInfoLog(shader, maxLength, lengthPtr, arr);
            if (length == 0) infoLog = null;
            else infoLog = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable

        public static void GetShaderPrecisionFormat(ShaderType shaderType, int precisionType, out int[] range, out int precision)
        {
            range = new int[2];
            var rangePtr = stackalloc int[2];
            fixed (int* precisionPtr = &precision)
                Delegates.glGetShaderPrecisionFormat(shaderType, precisionType, rangePtr, precisionPtr);
            range[0] = rangePtr[0];
            range[1] = rangePtr[1];
        }
#nullable enable
        public static unsafe void GetShaderSource(uint shader, int bufSize, out int length, out string? source)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
                Delegates.glGetShaderSource(shader, bufSize, lengthPtr, arr);
            if (length == 0) source = null;
            else source = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable
        public static string GetString(StringName name)
        {
            byte* ptr = Delegates.glGetString(name);
            byte* walkPtr = ptr;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(ptr, (int)(walkPtr - ptr));
        }
        public static string GetStringi(StringName name, uint index)
        {
            byte* ptr = Delegates.glGetStringi(name, index);
            byte* walkPtr = ptr;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(ptr, (int)(walkPtr - ptr));
        }
        public static uint GetSubroutineIndex(uint program, ShaderType shadertype, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetSubroutineIndex(program, shadertype, namePtr);
        }
        public static int GetSubroutineUniformLocation(uint program, ShaderType shadertype, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetSubroutineUniformLocation(program, shadertype, namePtr);
        }
        public static void GetSynciv(IntPtr sync, ArbSync pname, int bufSize, out int length, out int[] values)
        {
            fixed (int* lengthPtr = &length)
            {
                int valuesPtr = 0;
                int* walkPtr = &valuesPtr;
                Delegates.glGetSynciv(sync, pname, bufSize, lengthPtr, walkPtr);
                values = new int[length];
                for (int i = 0; i < length; i++)
                    values[i] = *(walkPtr + i);
            }
        }
        public static void GetTexImage(TextureTarget target, int level, PixelFormat format, PixelType type, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetTexImage(target, level, format, type, pixelsPtr);
        }
        public static void GetnTexImage(TextureTarget target, int level, PixelFormat format, PixelType type, int bufSize, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetnTexImage(target, level, format, type, bufSize, pixelsPtr);
        }
        public static void GetTextureImage(uint texture, int level, PixelFormat format, PixelType type, int bufSize, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetTextureImage(texture, level, format, type, bufSize, pixelsPtr);
        }
        public static void GetTexLevelParameterfv(GetPName target, int level, GetTextureLevelParameter pname, out float @params)
        {
            fixed (float* ptr = &@params)
                Delegates.glGetTexLevelParameterfv(target, level, pname, ptr);
        }
        public static void GetTexLevelParameteriv(GetPName target, int level, GetTextureLevelParameter pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetTexLevelParameteriv(target, level, pname, ptr);
        }
        public static void GetTextureLevelParameterfv(uint texture, int level, GetTextureLevelParameter pname, out float @params)
        {
            fixed (float* ptr = &@params)
                Delegates.glGetTextureLevelParameterfv(texture, level, pname, ptr);
        }
        public static void GetTextureLevelParameteriv(uint texture, int level, GetTextureLevelParameter pname, out int @params)
        {
            fixed (int* ptr = &@params)
                Delegates.glGetTextureLevelParameteriv(texture, level, pname, ptr);
        }

        internal static int GetTexParameterLength(GetTextureParameter pname)
        {
            return pname switch
            {
                GetTextureParameter.TextureBorderColor => 4,
                _ => 1,
            };
        }
        public static void GetTexParameterfv(TextureTarget target, GetTextureParameter pname, Span<float> @params)
        {
            @params = new float[GetTexParameterLength(pname)];
            fixed (float* ptr = @params)
                Delegates.glGetTexParameterfv(target, pname, ptr);
        }
        public static void GetTexParameteriv(TextureTarget target, GetTextureParameter pname, Span<int> @params)
        {
            @params = new int[GetTexParameterLength(pname)];
            fixed (int* ptr = @params)
                Delegates.glGetTexParameteriv(target, pname, ptr);
        }
        public static void GetTexParameterIiv(TextureTarget target, GetTextureParameter pname, Span<int> @params)
        {
            @params = new int[GetTexParameterLength(pname)];
            fixed (int* ptr = @params)
                Delegates.glGetTexParameterIiv(target, pname, ptr);
        }
        public static void GetTexParameterIuiv(TextureTarget target, GetTextureParameter pname, Span<uint> @params)
        {
            @params = new uint[GetTexParameterLength(pname)];
            fixed (uint* ptr = @params)
                Delegates.glGetTexParameterIuiv(target, pname, ptr);
        }
        public static void GetTextureParameterfv(uint texture, GetTextureParameter pname, Span<float> @params)
        {
            @params = new float[GetTexParameterLength(pname)];
            fixed (float* ptr = @params)
                Delegates.glGetTextureParameterfv(texture, pname, ptr);
        }
        public static void GetTextureParameteriv(uint texture, GetTextureParameter pname, Span<int> @params)
        {
            @params = new int[GetTexParameterLength(pname)];
            fixed (int* ptr = @params)
                Delegates.glGetTextureParameteriv(texture, pname, ptr);
        }
        public static void GetTextureParameterIiv(uint texture, GetTextureParameter pname, Span<int> @params)
        {
            @params = new int[GetTexParameterLength(pname)];
            fixed (int* ptr = @params)
                Delegates.glGetTextureParameterIiv(texture, pname, ptr);
        }
        public static void GetTextureParameterIuiv(uint texture, GetTextureParameter pname, Span<uint> @params)
        {
            @params = new uint[GetTexParameterLength(pname)];
            fixed (uint* ptr = @params)
                Delegates.glGetTextureParameterIuiv(texture, pname, ptr);
        }
        public static void GetTextureSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, int bufSize, out IntPtr pixels)
        {
            fixed (IntPtr* pixelsPtr = &pixels)
                Delegates.glGetTextureSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, bufSize, pixelsPtr);
        }
        public static void GetTextureSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, int bufSize, out Span<byte> pixels)
        {
            pixels = new byte[width * height * depth];
            fixed (byte* pixelsPtr = pixels)
                Delegates.glGetTextureSubImageSpan(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, bufSize, pixelsPtr);
        }
        public static void GetTransformFeedbackiv(uint xfb, TransformFeedbackParameterName pname, out int param)
        {
            fixed (int* ptr = &param)
                Delegates.glGetTransformFeedbackiv(xfb, pname, ptr);
        }
        public static void GetTransformFeedbacki_v(uint xfb, TransformFeedbackParameterName pname, uint index, out int param)
        {
            fixed (int* ptr = &param)
                Delegates.glGetTransformFeedbacki_v(xfb, pname, index, ptr);
        }
        public static void GetTransformFeedbacki64_v(uint xfb, TransformFeedbackParameterName pname, uint index, out long param)
        {
            fixed (long* ptr = &param)
                Delegates.glGetTransformFeedbacki64_v(xfb, pname, index, ptr);
        }
#nullable enable
        public static unsafe void GetTransformFeedbackVarying(uint program, uint index, int bufSize, out int length, out int size, out ActiveAttribType type, out string? name)
        {
            var arr = stackalloc byte[bufSize];
            fixed (int* lengthPtr = &length)
            fixed (int* sizePtr = &size)
            fixed (ActiveAttribType* typePtr = &type)
                Delegates.glGetTransformFeedbackVarying(program, index, bufSize, lengthPtr, sizePtr, typePtr, arr);
            if (length == 0) name = null;
            else name = Encoding.UTF8.GetString(arr, length);
        }
#nullable disable
        public static void GetUniformfv(uint program, int location, out float @params)
        {
            fixed (float* @paramsPtr = &@params)
                Delegates.glGetUniformfv(program, location, @paramsPtr);
        }
        public static void GetUniformiv(uint program, int location, out int @params)
        {
            fixed (int* @paramsPtr = &@params)
                Delegates.glGetUniformiv(program, location, @paramsPtr);
        }
        public static void GetUniformuiv(uint program, int location, out uint @params)
        {
            fixed (uint* @paramsPtr = &@params)
                Delegates.glGetUniformuiv(program, location, @paramsPtr);
        }
        public static void GetUniformdv(uint program, int location, out double @params)
        {
            fixed (double* @paramsPtr = &@params)
                Delegates.glGetUniformdv(program, location, @paramsPtr);
        }
        public static void GetnUniformfv(uint program, int location, int bufSize, out float @params)
        {
            fixed (float* @paramsPtr = &@params)
                Delegates.glGetnUniformfv(program, location, bufSize, @paramsPtr);
        }
        public static void GetnUniformiv(uint program, int location, int bufSize, out int @params)
        {
            fixed (int* @paramsPtr = &@params)
                Delegates.glGetnUniformiv(program, location, bufSize, @paramsPtr);
        }
        public static void GetnUniformuiv(uint program, int location, int bufSize, out uint @params)
        {
            fixed (uint* @paramsPtr = &@params)
                Delegates.glGetnUniformuiv(program, location, bufSize, @paramsPtr);
        }
        public static void GetnUniformdv(uint program, int location, int bufSize, out double @params)
        {
            fixed (double* @paramsPtr = &@params)
                Delegates.glGetnUniformdv(program, location, bufSize, @paramsPtr);
        }
        public static uint GetUniformBlockIndex(uint program, string uniformBlockName)
        {
            fixed (byte* uniformBlockNamePtr = Encoding.UTF8.GetBytes(uniformBlockName))
                return Delegates.glGetUniformBlockIndex(program, uniformBlockNamePtr);
        }
#nullable enable
        public static void GetUniformIndices(uint program, int uniformCount, string[]? uniformNames, out uint[]? uniformIndices)
        {
            if (uniformCount != 0)
            {
                uniformIndices = new uint[uniformCount];
                uniformNames = new string[uniformCount];
                var uniformNamesPtr = stackalloc byte*[uniformCount];
                var uniformIndicesPtr = stackalloc uint[uniformCount];
                Delegates.glGetUniformIndices(program, uniformCount, uniformNamesPtr, uniformIndicesPtr);
                for (int i = 0; i < uniformCount; i++)
                {
                    uniformIndices[i] = uniformIndicesPtr[i];
                    byte* walkPtr = uniformNamesPtr[i];
                    while (*walkPtr != 0) walkPtr++;
                    uniformNames[i] = Encoding.UTF8.GetString(uniformNamesPtr[i], (int)(walkPtr - uniformNamesPtr[i]));
                }
            }
            else
            {
                uniformNames = null;
                uniformIndices = null;
            }
        }
#nullable disable
        public static int GetUniformLocation(uint program, string name)
        {
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name))
                return Delegates.glGetUniformLocation(program, namePtr);
        }
        public static void GetUniformSubroutineuiv(ShaderType shadertype, int location, out uint values)
        {
            fixed (uint* valuesPtr = &values)
                Delegates.glGetUniformSubroutineuiv(shadertype, location, valuesPtr);
        }
        public static void GetVertexArrayIndexed64iv(uint vaobj, uint index, VertexAttribParameter pname, out long param)
        {
            fixed (long* paramPtr = &param)
                Delegates.glGetVertexArrayIndexed64iv(vaobj, index, pname, paramPtr);
        }
        public static void GetVertexArrayIndexediv(uint vaobj, uint index, VertexAttribParameter pname, out int param)
        {
            fixed (int* paramPtr = &param)
                Delegates.glGetVertexArrayIndexediv(vaobj, index, pname, paramPtr);
        }
        public static void GetVertexArrayiv(uint vaobj, VertexAttribParameter pname, out int param)
        {
            fixed (int* paramPtr = &param)
                Delegates.glGetVertexArrayiv(vaobj, pname, paramPtr);
        }
        public static void GetVertexAttribdv(uint index, VertexAttribParameter pname, out double @params)
        {
            fixed (double* @paramsPtr = &@params)
                Delegates.glGetVertexAttribdv(index, pname, @paramsPtr);
        }
        public static void GetVertexAttribfv(uint index, VertexAttribParameter pname, out float @params)
        {
            fixed (float* @paramsPtr = &@params)
                Delegates.glGetVertexAttribfv(index, pname, @paramsPtr);
        }
        public static void GetVertexAttribiv(uint index, VertexAttribParameter pname, out int @params)
        {
            fixed (int* @paramsPtr = &@params)
                Delegates.glGetVertexAttribiv(index, pname, @paramsPtr);
        }
        public static void GetVertexAttribIiv(uint index, VertexAttribParameter pname, out int @params)
        {
            fixed (int* @paramsPtr = &@params)
                Delegates.glGetVertexAttribIiv(index, pname, @paramsPtr);
        }
        public static void GetVertexAttribIuiv(uint index, VertexAttribParameter pname, out uint @params)
        {
            fixed (uint* @paramsPtr = &@params)
                Delegates.glGetVertexAttribIuiv(index, pname, @paramsPtr);
        }
        public static void GetVertexAttribLdv(uint index, VertexAttribParameter pname, out double @params)
        {
            fixed (double* @paramsPtr = &@params)
                Delegates.glGetVertexAttribLdv(index, pname, @paramsPtr);
        }
        public static void GetVertexAttribPointerv(uint index, VertexAttribPointerParameter pname, out IntPtr pointer)
        {
            fixed (IntPtr* pointerPtr = &pointer)
                Delegates.glGetVertexAttribPointerv(index, pname, pointerPtr);
        }
        public static void Hint(HintTarget target, HintMode mode) => Delegates.glHint(target, mode);
        public static void InvalidateBufferData(uint buffer) => Delegates.glInvalidateBufferData(buffer);
        public static void InvalidateBufferSubData(uint buffer, IntPtr offset, IntPtr length) => Delegates.glInvalidateBufferSubData(buffer, offset, length);
        public static void InvalidateFramebuffer(FramebufferTarget target, int numAttachments, FramebufferAttachment[] attachments)
        {
            fixed (FramebufferAttachment* attachmentsPtr = attachments)
                Delegates.glInvalidateFramebuffer(target, numAttachments, attachmentsPtr);
        }
        public static void InvalidateNamedFramebufferData(uint framebuffer, int numAttachments, FramebufferAttachment[] attachments)
        {
            fixed (FramebufferAttachment* attachmentsPtr = attachments)
                Delegates.glInvalidateNamedFramebufferData(framebuffer, numAttachments, attachmentsPtr);
        }
        public static void InvalidateSubFramebuffer(FramebufferTarget target, int numAttachments, FramebufferAttachment[] attachments, int x, int y, int width, int height)
        {
            fixed (FramebufferAttachment* attachmentsPtr = attachments)
                Delegates.glInvalidateSubFramebuffer(target, numAttachments, attachmentsPtr, x, y, width, height);
        }
        public static void InvalidateNamedFramebufferSubData(uint framebuffer, int numAttachments, FramebufferAttachment[] attachments, int x, int y, int width, int height)
        {
            fixed (FramebufferAttachment* attachmentsPtr = attachments)
                Delegates.glInvalidateNamedFramebufferSubData(framebuffer, numAttachments, attachmentsPtr, x, y, width, height);
        }
        public static void InvalidateTexImage(uint texture, int level) => Delegates.glInvalidateTexImage(texture, level);
        public static void InvalidateTexSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth) => Delegates.glInvalidateTexSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth);
        public static bool IsBuffer(uint buffer) => Delegates.glIsBuffer(buffer);
        public static bool IsEnabled(EnableCap cap) => Delegates.glIsEnabled(cap);
        public static bool IsEnabledi(EnableCap cap, uint index) => Delegates.glIsEnabledi(cap, index);
        public static bool IsFramebuffer(uint framebuffer) => Delegates.glIsFramebuffer(framebuffer);
        public static bool IsProgram(uint program) => Delegates.glIsProgram(program);
        public static bool IsProgramPipeline(uint pipeline) => Delegates.glIsProgramPipeline(pipeline);
        public static bool IsQuery(uint id) => Delegates.glIsQuery(id);
        public static bool IsRenderbuffer(uint renderbuffer) => Delegates.glIsRenderbuffer(renderbuffer);
        public static bool IsSampler(uint id) => Delegates.glIsSampler(id);
        public static bool IsShader(uint shader) => Delegates.glIsShader(shader);
        public static bool IsSync(IntPtr sync) => Delegates.glIsSync(sync);
        public static bool IsTexture(uint texture) => Delegates.glIsTexture(texture);
        public static bool IsTransformFeedback(uint id) => Delegates.glIsTransformFeedback(id);
        public static bool IsVertexArray(uint array) => Delegates.glIsVertexArray(array);
        public static void LineWidth(float width) => Delegates.glLineWidth(width);
        public static void LinkProgram(uint program) => Delegates.glLinkProgram(program);
        public static void LogicOp(LogicOpCode opcode) => Delegates.glLogicOp(opcode);
        public static IntPtr MapBuffer(BufferTarget target, BufferAccess access) => Delegates.glMapBuffer(target, access);
        public static IntPtr MapNamedBuffer(uint buffer, BufferAccess access) => Delegates.glMapNamedBuffer(buffer, access);
        public static IntPtr MapBufferRange(BufferTarget target, IntPtr offset, IntPtr length, BufferAccessMask access) => Delegates.glMapBufferRange(target, offset, length, access);
        public static IntPtr MapNamedBufferRange(uint buffer, IntPtr offset, int length, uint access) => Delegates.glMapNamedBufferRange(buffer, offset, length, access);
        public static void MemoryBarrier(uint barriers) => Delegates.glMemoryBarrier(barriers);
        public static void MemoryBarrierByRegion(uint barriers) => Delegates.glMemoryBarrierByRegion(barriers);
        public static void MinSampleShading(float value) => Delegates.glMinSampleShading(value);
        public static void MultiDrawArrays(BeginMode mode, int[] first, int[] count, int drawcount)
        {
            fixed (int* firstPtr = first)
            fixed (int* countPtr = count)
                Delegates.glMultiDrawArrays(mode, firstPtr, countPtr, drawcount);
        }
        public static void MultiDrawArraysIndirect(BeginMode mode, IntPtr indirect, int drawcount, int stride) => Delegates.glMultiDrawArraysIndirect(mode, indirect, drawcount, stride);
        public static void MultiDrawElements(BeginMode mode, int[] count, DrawElementsType type, IntPtr indices, int drawcount)
        {
            fixed (int* countPtr = count)
                Delegates.glMultiDrawElements(mode, countPtr, type, indices, drawcount);
        }
        public static void MultiDrawElementsBaseVertex(BeginMode mode, int[] count, DrawElementsType type, IntPtr indices, int drawcount, int[] basevertex)
        {
            fixed (int* countPtr = count)
            fixed (int* basevertexPtr = basevertex)
                Delegates.glMultiDrawElementsBaseVertex(mode, countPtr, type, indices, drawcount, basevertexPtr);
        }
        public static void MultiDrawElementsIndirect(BeginMode mode, DrawElementsType type, IntPtr indirect, int drawcount, int stride) => Delegates.glMultiDrawElementsIndirect(mode, type, indirect, drawcount, stride);
        public static void ObjectLabel(ObjectLabelType identifier, uint name, int length, string label)
        {
            fixed (byte* labelPtr = Encoding.UTF8.GetBytes(label))
                Delegates.glObjectLabel(identifier, name, length, labelPtr);
        }
        public static void ObjectPtrLabel(IntPtr ptr, int length, string label)
        {
            fixed (byte* labelPtr = Encoding.UTF8.GetBytes(label))
                Delegates.glObjectPtrLabel(ptr, length, labelPtr);
        }
        public static void PatchParameteri(int pname, int value) => Delegates.glPatchParameteri(pname, value);
        public static void PatchParameterfv(int pname, float[] values)
        {
            fixed (float* valuesPtr = values)
                Delegates.glPatchParameterfv(pname, valuesPtr);
        }
        public static void PixelStoref(PixelStoreParameter pname, float param) => Delegates.glPixelStoref(pname, param);
        public static void PixelStorei(PixelStoreParameter pname, int param) => Delegates.glPixelStorei(pname, param);
        public static void PointParameterf(PointParameterName pname, float param) => Delegates.glPointParameterf(pname, param);
        public static void PointParameteri(PointParameterName pname, int param) => Delegates.glPointParameteri(pname, param);
        public static void PointParameterfv(PointParameterName pname, float[] @params)
        {
            fixed (float* @paramsPtr = @params)
                Delegates.glPointParameterfv(pname, @paramsPtr);
        }
        public static void PointParameteriv(PointParameterName pname, int[] @params)
        {
            fixed (int* @paramsPtr = @params)
                Delegates.glPointParameteriv(pname, @paramsPtr);
        }
        public static void PointSize(float size) => Delegates.glPointSize(size);
        public static void PolygonMode(MaterialFace face, PolygonModeType mode) => Delegates.glPolygonMode(face, mode);
        public static void PolygonOffset(float factor, float units) => Delegates.glPolygonOffset(factor, units);
        public static void PrimitiveRestartIndex(uint index) => Delegates.glPrimitiveRestartIndex(index);
        public static void ProgramBinary(uint program, int binaryFormat, IntPtr binary, int length) => Delegates.glProgramBinary(program, binaryFormat, binary, length);
        public static void ProgramParameteri(uint program, Version32 pname, int value) => Delegates.glProgramParameteri(program, pname, value);
        public static void ProgramUniform1f(uint program, int location, float v0) => Delegates.glProgramUniform1f(program, location, v0);
        public static void ProgramUniform2f(uint program, int location, float v0, float v1) => Delegates.glProgramUniform2f(program, location, v0, v1);
        public static void ProgramUniform3f(uint program, int location, float v0, float v1, float v2) => Delegates.glProgramUniform3f(program, location, v0, v1, v2);
        public static void ProgramUniform4f(uint program, int location, float v0, float v1, float v2, float v3) => Delegates.glProgramUniform4f(program, location, v0, v1, v2, v3);
        public static void ProgramUniform1i(uint program, int location, int v0) => Delegates.glProgramUniform1i(program, location, v0);
        public static void ProgramUniform2i(uint program, int location, int v0, int v1) => Delegates.glProgramUniform2i(program, location, v0, v1);
        public static void ProgramUniform3i(uint program, int location, int v0, int v1, int v2) => Delegates.glProgramUniform3i(program, location, v0, v1, v2);
        public static void ProgramUniform4i(uint program, int location, int v0, int v1, int v2, int v3) => Delegates.glProgramUniform4i(program, location, v0, v1, v2, v3);
        public static void ProgramUniform1ui(uint program, int location, uint v0) => Delegates.glProgramUniform1ui(program, location, v0);
        public static void ProgramUniform2ui(uint program, int location, int v0, uint v1) => Delegates.glProgramUniform2ui(program, location, v0, v1);
        public static void ProgramUniform3ui(uint program, int location, int v0, int v1, uint v2) => Delegates.glProgramUniform3ui(program, location, v0, v1, v2);
        public static void ProgramUniform4ui(uint program, int location, int v0, int v1, int v2, uint v3) => Delegates.glProgramUniform4ui(program, location, v0, v1, v2, v3);
        public static void ProgramUniform1fv(uint program, int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniform1fv(program, location, count, valuePtr);
        }
        public static void ProgramUniform2fv(uint program, int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniform2fv(program, location, count, valuePtr);
        }
        public static void ProgramUniform3fv(uint program, int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniform3fv(program, location, count, valuePtr);
        }
        public static void ProgramUniform4fv(uint program, int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniform4fv(program, location, count, valuePtr);
        }
        public static void ProgramUniform1iv(uint program, int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glProgramUniform1iv(program, location, count, valuePtr);
        }
        public static void ProgramUniform2iv(uint program, int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glProgramUniform2iv(program, location, count, valuePtr);
        }
        public static void ProgramUniform3iv(uint program, int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glProgramUniform3iv(program, location, count, valuePtr);
        }
        public static void ProgramUniform4iv(uint program, int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glProgramUniform4iv(program, location, count, valuePtr);
        }
        public static void ProgramUniform1uiv(uint program, int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glProgramUniform1uiv(program, location, count, valuePtr);
        }
        public static void ProgramUniform2uiv(uint program, int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glProgramUniform2uiv(program, location, count, valuePtr);
        }
        public static void ProgramUniform3uiv(uint program, int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glProgramUniform3uiv(program, location, count, valuePtr);
        }
        public static void ProgramUniform4uiv(uint program, int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glProgramUniform4uiv(program, location, count, valuePtr);
        }
        public static void ProgramUniformMatrix2fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix2fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix3fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix3fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix4fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix4fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix2x3fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix2x3fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix3x2fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix3x2fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix2x4fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix2x4fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix4x2fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix4x2fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix3x4fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix3x4fv(program, location, count, transpose, valuePtr);
        }
        public static void ProgramUniformMatrix4x3fv(uint program, int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glProgramUniformMatrix4x3fv(program, location, count, transpose, valuePtr);
        }
        public static void ProvokingVertex(ProvokingVertexMode provokeMode) => Delegates.glProvokingVertex(provokeMode);
        public static void QueryCounter(uint id, QueryTarget target) => Delegates.glQueryCounter(id, target);
        public static void ReadBuffer(ReadBufferMode mode) => Delegates.glReadBuffer(mode);
        public static void NamedFramebufferReadBuffer(ReadBufferMode framebuffer, BeginMode mode) => Delegates.glNamedFramebufferReadBuffer(framebuffer, mode);
        public static void ReadPixels(int x, int y, int width, int height, PixelFormat format, PixelType type, int[] data)
        {
            fixed (int* dataPtr = data)
                Delegates.glReadPixels(x, y, width, height, format, type, dataPtr);
        }
        public static void ReadnPixels(int x, int y, int width, int height, PixelFormat format, PixelType type, int bufSize, int[] data)
        {
            fixed (int* dataPtr = data)
                Delegates.glReadnPixels(x, y, width, height, format, type, bufSize, dataPtr);
        }
        public static void RenderbufferStorage(RenderbufferTarget target, RenderbufferStorageType internalFormat, int width, int height) => Delegates.glRenderbufferStorage(target, internalFormat, width, height);
        public static void NamedRenderbufferStorage(uint renderbuffer, RenderbufferStorageType internalFormat, int width, int height) => Delegates.glNamedRenderbufferStorage(renderbuffer, internalFormat, width, height);
        public static void RenderbufferStorageMultisample(RenderbufferTarget target, int samples, RenderbufferStorageType internalFormat, int width, int height) => Delegates.glRenderbufferStorageMultisample(target, samples, internalFormat, width, height);
        public static void NamedRenderbufferStorageMultisample(uint renderbuffer, int samples, RenderbufferStorageType internalFormat, int width, int height) => Delegates.glNamedRenderbufferStorageMultisample(renderbuffer, samples, internalFormat, width, height);
        public static void SampleCoverage(float value, bool invert) => Delegates.glSampleCoverage(value, invert);
        public static void SampleMaski(uint maskNumber, uint mask) => Delegates.glSampleMaski(maskNumber, mask);
        public static void SamplerParameterf(uint sampler, TextureParameterName pname, float param) => Delegates.glSamplerParameterf(sampler, pname, param);
        public static void SamplerParameteri(uint sampler, TextureParameterName pname, int param) => Delegates.glSamplerParameteri(sampler, pname, param);
        public static void SamplerParameterfv(uint sampler, TextureParameterName pname, float[] @params)
        {
            fixed (float* @paramsPtr = @params)
                Delegates.glSamplerParameterfv(sampler, pname, @paramsPtr);
        }
        public static void SamplerParameteriv(uint sampler, TextureParameterName pname, int[] @params)
        {
            fixed (int* @paramsPtr = @params)
                Delegates.glSamplerParameteriv(sampler, pname, @paramsPtr);
        }
        public static void SamplerParameterIiv(uint sampler, TextureParameterName pname, int[] @params)
        {
            fixed (int* @paramsPtr = @params)
                Delegates.glSamplerParameterIiv(sampler, pname, @paramsPtr);
        }
        public static void SamplerParameterIuiv(uint sampler, TextureParameterName pname, uint[] @params)
        {
            fixed (uint* @paramsPtr = @params)
                Delegates.glSamplerParameterIuiv(sampler, pname, @paramsPtr);
        }
        public static void Scissor(int x, int y, int width, int height) => Delegates.glScissor(x, y, width, height);
        public static void ScissorArrayv(uint first, int count, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glScissorArrayv(first, count, vPtr);
        }
        public static void ScissorIndexed(uint index, int left, int bottom, int width, int height) => Delegates.glScissorIndexed(index, left, bottom, width, height);
        public static void ScissorIndexedv(uint index, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glScissorIndexedv(index, vPtr);
        }
        public static void ShaderBinary(int count, uint[] shaders, int binaryFormat, IntPtr binary, int length)
        {
            fixed (uint* shadersPtr = shaders)
                Delegates.glShaderBinary(count, shadersPtr, binaryFormat, binary, length);
        }
#nullable enable
        public static void ShaderSource(uint shader, int count, string[] @string, int[]? length)
        {
            var ptrs = stackalloc byte*[@string.Length];
            for (int i = 0; i < @string.Length; i++)
            {
                fixed (byte* valuePtr = Encoding.UTF8.GetBytes(@string[i]))
                    ptrs[i] = valuePtr;
            }

            fixed (int* lengthPtr = length)
                Delegates.glShaderSource(shader, count, ptrs, lengthPtr);
        }
        public static void ShaderSource(uint shader, string @string, int? length)
        {
            fixed (byte* ptr = Encoding.UTF8.GetBytes(@string))
                if (length == null) Delegates.glShaderSource(shader, 1, &ptr, null);
                else
                {
                    int* lengthPtr = (int*)&length;
                    Delegates.glShaderSource(shader, 1, &ptr, lengthPtr);
                }
        }
        public static void ShaderSource(uint shader, byte[] @string, int[]? length)
        {
            fixed (byte* stringPtr = @string)
                if (length == null) Delegates.glShaderSource(shader, 1, &stringPtr, null);
                else
                {
                    fixed (int* lengthPtr = length)
                        Delegates.glShaderSource(shader, 1, &stringPtr, lengthPtr);
                }
        }
#nullable disable
        public static void ShaderStorageBlockBinding(uint program, uint storageBlockIndex, uint storageBlockBinding) => Delegates.glShaderStorageBlockBinding(program, storageBlockIndex, storageBlockBinding);
        public static void StencilFunc(StencilFunction func, int @ref, uint mask) => Delegates.glStencilFunc(func, @ref, mask);
        public static void StencilFuncSeparate(StencilFace face, StencilFunction func, int @ref, uint mask) => Delegates.glStencilFuncSeparate(face, func, @ref, mask);
        public static void StencilMask(uint mask) => Delegates.glStencilMask(mask);
        public static void StencilMaskSeparate(StencilFace face, uint mask) => Delegates.glStencilMaskSeparate(face, mask);
        public static void StencilOp(StencilOpType sfail, StencilOpType dpfail, StencilOpType dppass) => Delegates.glStencilOp(sfail, dpfail, dppass);
        public static void StencilOpSeparate(StencilFace face, StencilOpType sfail, StencilOpType dpfail, StencilOpType dppass) => Delegates.glStencilOpSeparate(face, sfail, dpfail, dppass);
        public static void TexBuffer(TextureBufferTarget target, SizedInternalFormat internalFormat, uint buffer) => Delegates.glTexBuffer(target, internalFormat, buffer);
        public static void TextureBuffer(uint texture, SizedInternalFormat internalFormat, uint buffer) => Delegates.glTextureBuffer(texture, internalFormat, buffer);
        public static void TexBufferRange(BufferTarget target, SizedInternalFormat internalFormat, uint buffer, IntPtr offset, IntPtr size) => Delegates.glTexBufferRange(target, internalFormat, buffer, offset, size);
        public static void TextureBufferRange(uint texture, SizedInternalFormat internalFormat, uint buffer, IntPtr offset, int size) => Delegates.glTextureBufferRange(texture, internalFormat, buffer, offset, size);
        public static void TexImage1D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int border, PixelFormat format, PixelType type, IntPtr data) => Delegates.glTexImage1D(target, level, internalFormat, width, border, format, type, data);
        public static void TexImage1D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int border, PixelFormat format, PixelType type, byte[] data)
        {
            fixed (byte* ptr = data)
                Delegates.glTexImage1DBytePtr(target, level, internalFormat, width, border, format, type, ptr);
        }
        public static void TexImage2D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int border, PixelFormat format, PixelType type, IntPtr data) => Delegates.glTexImage2D(target, level, internalFormat, width, height, border, format, type, data);
        public static void TexImage2D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int border, PixelFormat format, PixelType type, byte[] data)
        {
            fixed (byte* ptr = data)
                Delegates.glTexImage2DBytePtr(target, level, internalFormat, width, height, border, format, type, ptr);
        }
        public static void TexImage2DMultisample(TextureTargetMultisample target, int samples, PixelInternalFormat internalFormat, int width, int height, bool fixedsamplelocations) => Delegates.glTexImage2DMultisample(target, samples, internalFormat, width, height, fixedsamplelocations);
        public static void TexImage3D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int depth, int border, PixelFormat format, PixelType type, IntPtr data) => Delegates.glTexImage3D(target, level, internalFormat, width, height, depth, border, format, type, data);
        public static void TexImage3D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int depth, int border, PixelFormat format, PixelType type, byte[] data)
        {
            fixed (byte* ptr = data)
                Delegates.glTexImage3DBytePtr(target, level, internalFormat, width, height, depth, border, format, type, ptr);
        }
        public static void TexImage3DMultisample(TextureTargetMultisample target, int samples, PixelInternalFormat internalFormat, int width, int height, int depth, bool fixedsamplelocations) => Delegates.glTexImage3DMultisample(target, samples, internalFormat, width, height, depth, fixedsamplelocations);
        public static void TexParameterf(TextureTarget target, TextureParameterName pname, float param) => Delegates.glTexParameterf(target, pname, param);
        public static void TexParameteri(TextureTarget target, TextureParameterName pname, TextureParameter param) => Delegates.glTexParameteri(target, pname, param);
        public static void TextureParameterf(uint texture, TextureParameterName pname, float param) => Delegates.glTextureParameterf(texture, pname, param);
        public static void TextureParameteri(uint texture, TextureParameterName pname, TextureParameter param) => Delegates.glTextureParameteri(texture, pname, param);
        public static void TexParameterfv(TextureTarget target, TextureParameterName pname, float[] @params)
        {
            fixed (float* @paramsPtr = @params)
                Delegates.glTexParameterfv(target, pname, @paramsPtr);
        }
        public static void TexParameteriv(TextureTarget target, TextureParameterName pname, int[] @params)
        {
            fixed (int* @paramsPtr = @params)
                Delegates.glTexParameteriv(target, pname, @paramsPtr);
        }
        public static void TexParameterIiv(TextureTarget target, TextureParameterName pname, int[] @params)
        {
            fixed (int* @paramsPtr = @params)
                Delegates.glTexParameterIiv(target, pname, @paramsPtr);
        }
        public static void TexParameterIuiv(TextureTarget target, TextureParameterName pname, uint[] @params)
        {
            fixed (uint* @paramsPtr = @params)
                Delegates.glTexParameterIuiv(target, pname, @paramsPtr);
        }
        public static void TextureParameterfv(uint texture, TextureParameterName pname, float[] paramtexture)
        {
            fixed (float* @paramtexturePtr = @paramtexture)
                Delegates.glTextureParameterfv(texture, pname, paramtexturePtr);
        }
        public static void TextureParameteriv(uint texture, TextureParameterName pname, int[] param)
        {
            fixed (int* paramPtr = param)
                Delegates.glTextureParameteriv(texture, pname, paramPtr);
        }
        public static void TextureParameterIiv(uint texture, TextureParameterName pname, int[] @params)
        {
            fixed (int* @paramsPtr = @params)
                Delegates.glTextureParameterIiv(texture, pname, @paramsPtr);
        }
        public static void TextureParameterIuiv(uint texture, TextureParameterName pname, uint[] @params)
        {
            fixed (uint* @paramsPtr = @params)
                Delegates.glTextureParameterIuiv(texture, pname, @paramsPtr);
        }
        public static void TexStorage1D(TextureTarget target, int levels, PixelInternalFormat internalFormat, int width) => Delegates.glTexStorage1D(target, levels, internalFormat, width);
        public static void TextureStorage1D(uint texture, int levels, PixelInternalFormat internalFormat, int width) => Delegates.glTextureStorage1D(texture, levels, internalFormat, width);
        public static void TexStorage2D(TextureTarget target, int levels, PixelInternalFormat internalFormat, int width, int height) => Delegates.glTexStorage2D(target, levels, internalFormat, width, height);
        public static void TextureStorage2D(uint texture, int levels, PixelInternalFormat internalFormat, int width, int height) => Delegates.glTextureStorage2D(texture, levels, internalFormat, width, height);
        public static void TexStorage2DMultisample(TextureTarget target, int samples, PixelInternalFormat internalFormat, int width, int height, bool fixedsamplelocations) => Delegates.glTexStorage2DMultisample(target, samples, internalFormat, width, height, fixedsamplelocations);
        public static void TextureStorage2DMultisample(uint texture, int samples, PixelInternalFormat internalFormat, int width, int height, bool fixedsamplelocations) => Delegates.glTextureStorage2DMultisample(texture, samples, internalFormat, width, height, fixedsamplelocations);
        public static void TexStorage3D(TextureTarget target, int levels, PixelInternalFormat internalFormat, int width, int height, int depth) => Delegates.glTexStorage3D(target, levels, internalFormat, width, height, depth);
        public static void TextureStorage3D(uint texture, int levels, PixelInternalFormat internalFormat, int width, int height, int depth) => Delegates.glTextureStorage3D(texture, levels, internalFormat, width, height, depth);
        public static void TexStorage3DMultisample(TextureTarget target, int samples, PixelInternalFormat internalFormat, int width, int height, int depth, bool fixedsamplelocations) => Delegates.glTexStorage3DMultisample(target, samples, internalFormat, width, height, depth, fixedsamplelocations);
        public static void TextureStorage3DMultisample(uint texture, int samples, PixelInternalFormat internalFormat, int width, int height, int depth, bool fixedsamplelocations) => Delegates.glTextureStorage3DMultisample(texture, samples, internalFormat, width, height, depth, fixedsamplelocations);

        public static void TexSubImage1D(TextureTarget target, int level, int xoffset, int width, PixelFormat format, PixelType type, IntPtr pixels) => Delegates.glTexSubImage1D(target, level, xoffset, width, format, type, pixels);
        public static void TextureSubImage1D(uint texture, int level, int xoffset, int width, PixelFormat format, PixelType type, IntPtr pixels) => Delegates.glTextureSubImage1D(texture, level, xoffset, width, format, type, pixels);
        public static void TexSubImage2D(TextureTarget target, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, IntPtr pixels) => Delegates.glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
        public static void TextureSubImage2D(uint texture, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, IntPtr pixels) => Delegates.glTextureSubImage2D(texture, level, xoffset, yoffset, width, height, format, type, pixels);
        public static void TexSubImage3D(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, IntPtr pixels) => Delegates.glTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);
        public static void TextureSubImage3D(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, IntPtr pixels) => Delegates.glTextureSubImage3D(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels);

        public static void TexSubImage1D<T>(TextureTarget target, int level, int xoffset, int width, PixelFormat format, PixelType type, T pixels) where T : unmanaged => Delegates.glTexSubImage1Dgeneric(target, level, xoffset, width, format, type, &pixels);
        public static void TextureSubImage1D<T>(uint texture, int level, int xoffset, int width, PixelFormat format, PixelType type, T pixels) where T : unmanaged => Delegates.glTextureSubImage1Dgeneric(texture, level, xoffset, width, format, type, &pixels);
        public static void TexSubImage2D<T>(TextureTarget target, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, T pixels) where T : unmanaged => Delegates.glTexSubImage2Dgeneric(target, level, xoffset, yoffset, width, height, format, type, &pixels);
        public static void TextureSubImage2D<T>(uint texture, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, T pixels) where T : unmanaged => Delegates.glTextureSubImage2Dgeneric(texture, level, xoffset, yoffset, width, height, format, type, &pixels);
        public static void TexSubImage3D<T>(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, T pixels) where T : unmanaged => Delegates.glTexSubImage3Dgeneric(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, &pixels);
        public static void TextureSubImage3D<T>(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, T pixels) where T : unmanaged => Delegates.glTextureSubImage3Dgeneric(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, &pixels);

        public static void TexSubImage1D<T>(TextureTarget target, int level, int xoffset, int width, PixelFormat format, PixelType type, Span<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage1DVoidPtr(target, level, xoffset, width, format, type, ptr);
        }
        public static void TextureSubImage1D<T>(uint texture, int level, int xoffset, int width, PixelFormat format, PixelType type, Span<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage1DVoidPtr(texture, level, xoffset, width, format, type, ptr);
        }
        public static void TexSubImage2D<T>(TextureTarget target, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, Span<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage2DVoidPtr(target, level, xoffset, yoffset, width, height, format, type, ptr);
        }
        public static void TextureSubImage2D<T>(uint texture, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, Span<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage2DVoidPtr(texture, level, xoffset, yoffset, width, height, format, type, ptr);
        }
        public static void TexSubImage3D<T>(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, Span<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage3DVoidPtr(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, ptr);
        }
        public static void TextureSubImage3D<T>(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, Span<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage3DVoidPtr(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, ptr);
        }
        public static void TexSubImage1D<T>(TextureTarget target, int level, int xoffset, int width, PixelFormat format, PixelType type, ReadOnlySpan<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage1DVoidPtr(target, level, xoffset, width, format, type, ptr);
        }
        public static void TextureSubImage1D<T>(uint texture, int level, int xoffset, int width, PixelFormat format, PixelType type, ReadOnlySpan<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage1DVoidPtr(texture, level, xoffset, width, format, type, ptr);
        }
        public static void TexSubImage2D<T>(TextureTarget target, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, ReadOnlySpan<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage2DVoidPtr(target, level, xoffset, yoffset, width, height, format, type, ptr);
        }
        public static void TextureSubImage2D<T>(uint texture, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, ReadOnlySpan<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage2DVoidPtr(texture, level, xoffset, yoffset, width, height, format, type, ptr);
        }
        public static void TexSubImage3D<T>(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, ReadOnlySpan<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage3DVoidPtr(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, ptr);
        }
        public static void TextureSubImage3D<T>(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, ReadOnlySpan<T> pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage3DVoidPtr(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, ptr);
        }

        public static void TexSubImage1D<T>(TextureTarget target, int level, int xoffset, int width, PixelFormat format, PixelType type, T[] pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage1DVoidPtr(target, level, xoffset, width, format, type, ptr);
        }
        public static void TextureSubImage1D<T>(uint texture, int level, int xoffset, int width, PixelFormat format, PixelType type, T[] pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage1DVoidPtr(texture, level, xoffset, width, format, type, ptr);
        }
        public static void TexSubImage2D<T>(TextureTarget target, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, T[] pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage2DVoidPtr(target, level, xoffset, yoffset, width, height, format, type, ptr);
        }
        public static void TextureSubImage2D<T>(uint texture, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, T[] pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage2DVoidPtr(texture, level, xoffset, yoffset, width, height, format, type, ptr);
        }
        public static void TexSubImage3D<T>(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, T[] pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTexSubImage3DVoidPtr(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, ptr);
        }
        public static void TextureSubImage3D<T>(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, T[] pixels) where T : unmanaged
        {
            fixed (T* ptr = pixels)
                Delegates.glTextureSubImage3DVoidPtr(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, ptr);
        }

        public static void TextureBarrier() => Delegates.glTextureBarrier();
        public static void TextureView(uint texture, TextureTarget target, uint origtexture, PixelInternalFormat internalFormat, uint minlevel, uint numlevels, uint minlayer, uint numlayers) => Delegates.glTextureView(texture, target, origtexture, internalFormat, minlevel, numlevels, minlayer, numlayers);
        public static void TransformFeedbackBufferBase(uint xfb, uint index, uint buffer) => Delegates.glTransformFeedbackBufferBase(xfb, index, buffer);
        public static void TransformFeedbackBufferRange(uint xfb, uint index, uint buffer, IntPtr offset, int size) => Delegates.glTransformFeedbackBufferRange(xfb, index, buffer, offset, size);
        public static void TransformFeedbackVaryings(uint program, int count, string[] varyings, TransformFeedbackMode bufferMode)
        {
            var varyingsPtr = stackalloc byte*[varyings.Length];
            for (int i = 0; i < varyings.Length; i++)
            {
                fixed (byte* ptr = Encoding.UTF8.GetBytes(varyings[i]))
                    varyingsPtr[i] = ptr;
            }

            Delegates.glTransformFeedbackVaryings(program, count, varyingsPtr, bufferMode);
        }
        public static void Uniform1f(int location, float v0) => Delegates.glUniform1f(location, v0);
        public static void Uniform2f(int location, float v0, float v1) => Delegates.glUniform2f(location, v0, v1);
        public static void Uniform3f(int location, float v0, float v1, float v2) => Delegates.glUniform3f(location, v0, v1, v2);
        public static void Uniform4f(int location, float v0, float v1, float v2, float v3) => Delegates.glUniform4f(location, v0, v1, v2, v3);
        public static void Uniform1i(int location, int v0) => Delegates.glUniform1i(location, v0);
        public static void Uniform2i(int location, int v0, int v1) => Delegates.glUniform2i(location, v0, v1);
        public static void Uniform3i(int location, int v0, int v1, int v2) => Delegates.glUniform3i(location, v0, v1, v2);
        public static void Uniform4i(int location, int v0, int v1, int v2, int v3) => Delegates.glUniform4i(location, v0, v1, v2, v3);
        public static void Uniform1ui(int location, uint v0) => Delegates.glUniform1ui(location, v0);
        public static void Uniform2ui(int location, uint v0, uint v1) => Delegates.glUniform2ui(location, v0, v1);
        public static void Uniform3ui(int location, uint v0, uint v1, uint v2) => Delegates.glUniform3ui(location, v0, v1, v2);
        public static void Uniform4ui(int location, uint v0, uint v1, uint v2, uint v3) => Delegates.glUniform4ui(location, v0, v1, v2, v3);
        public static void Uniform1fv(int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniform1fv(location, count, valuePtr);
        }
        public static void Uniform2fv(int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniform2fv(location, count, valuePtr);
        }
        public static void Uniform3fv(int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniform3fv(location, count, valuePtr);
        }
        public static void Uniform4fv(int location, int count, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniform4fv(location, count, valuePtr);
        }
        public static void Uniform1iv(int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glUniform1iv(location, count, valuePtr);
        }
        public static void Uniform2iv(int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glUniform2iv(location, count, valuePtr);
        }
        public static void Uniform3iv(int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glUniform3iv(location, count, valuePtr);
        }
        public static void Uniform4iv(int location, int count, int[] value)
        {
            fixed (int* valuePtr = value)
                Delegates.glUniform4iv(location, count, valuePtr);
        }
        public static void Uniform1uiv(int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glUniform1uiv(location, count, valuePtr);
        }
        public static void Uniform2uiv(int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glUniform2uiv(location, count, valuePtr);
        }
        public static void Uniform3uiv(int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glUniform3uiv(location, count, valuePtr);
        }
        public static void Uniform4uiv(int location, int count, uint[] value)
        {
            fixed (uint* valuePtr = value)
                Delegates.glUniform4uiv(location, count, valuePtr);
        }
        public static void UniformMatrix2fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix2fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix2fv(int location, int count, bool transpose, ref float matrixFirstValue)
        {
            fixed (float* matrixPtr = &matrixFirstValue)
                Delegates.glUniformMatrix2fvStruct(location, count, transpose, matrixPtr);
        }
        public static void UniformMatrix3fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix3fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix3fv(int location, int count, bool transpose, ref float matrixFirstValue)
        {
            fixed (float* matrixPtr = &matrixFirstValue)
                Delegates.glUniformMatrix3fvStruct(location, count, transpose, matrixPtr);
        }
        public static void UniformMatrix4fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix4fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix4fv(int location, int count, bool transpose, ref float matrixFirstValue)
        {
            fixed (float* matrixPtr = &matrixFirstValue)
                Delegates.glUniformMatrix4fvStruct(location, count, transpose, matrixPtr);
        }
        public static void UniformMatrix2x3fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix2x3fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix3x2fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix3x2fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix2x4fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix2x4fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix4x2fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix4x2fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix3x4fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix3x4fv(location, count, transpose, valuePtr);
        }
        public static void UniformMatrix4x3fv(int location, int count, bool transpose, float[] value)
        {
            fixed (float* valuePtr = value)
                Delegates.glUniformMatrix4x3fv(location, count, transpose, valuePtr);
        }
        public static void UniformBlockBinding(uint program, uint uniformBlockIndex, uint uniformBlockBinding) => Delegates.glUniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding);
        public static void UniformSubroutinesuiv(ShaderType shadertype, int count, uint[] indices)
        {
            fixed (uint* indicesPtr = indices)
                Delegates.glUniformSubroutinesuiv(shadertype, count, indicesPtr);
        }
        public static bool UnmapBuffer(BufferTarget target) => Delegates.glUnmapBuffer(target);
        public static bool UnmapNamedBuffer(uint buffer) => Delegates.glUnmapNamedBuffer(buffer);
        public static void UseProgram(uint program) => Delegates.glUseProgram(program);
        public static void UseProgramStages(uint pipeline, uint stages, uint program) => Delegates.glUseProgramStages(pipeline, stages, program);
        public static void ValidateProgram(uint program) => Delegates.glValidateProgram(program);
        public static void ValidateProgramPipeline(uint pipeline) => Delegates.glValidateProgramPipeline(pipeline);
        public static void VertexArrayElementBuffer(uint vaobj, uint buffer) => Delegates.glVertexArrayElementBuffer(vaobj, buffer);
        public static void VertexAttrib1f(uint index, float v0) => Delegates.glVertexAttrib1f(index, v0);
        public static void VertexAttrib1s(uint index, Int16 v0) => Delegates.glVertexAttrib1s(index, v0);
        public static void VertexAttrib1d(uint index, double v0) => Delegates.glVertexAttrib1d(index, v0);
        public static void VertexAttribI1i(uint index, int v0) => Delegates.glVertexAttribI1i(index, v0);
        public static void VertexAttribI1ui(uint index, uint v0) => Delegates.glVertexAttribI1ui(index, v0);
        public static void VertexAttrib2f(uint index, float v0, float v1) => Delegates.glVertexAttrib2f(index, v0, v1);
        public static void VertexAttrib2s(uint index, Int16 v0, Int16 v1) => Delegates.glVertexAttrib2s(index, v0, v1);
        public static void VertexAttrib2d(uint index, double v0, double v1) => Delegates.glVertexAttrib2d(index, v0, v1);
        public static void VertexAttribI2i(uint index, int v0, int v1) => Delegates.glVertexAttribI2i(index, v0, v1);
        public static void VertexAttribI2ui(uint index, uint v0, uint v1) => Delegates.glVertexAttribI2ui(index, v0, v1);
        public static void VertexAttrib3f(uint index, float v0, float v1, float v2) => Delegates.glVertexAttrib3f(index, v0, v1, v2);
        public static void VertexAttrib3s(uint index, Int16 v0, Int16 v1, Int16 v2) => Delegates.glVertexAttrib3s(index, v0, v1, v2);
        public static void VertexAttrib3d(uint index, double v0, double v1, double v2) => Delegates.glVertexAttrib3d(index, v0, v1, v2);
        public static void VertexAttribI3i(uint index, int v0, int v1, int v2) => Delegates.glVertexAttribI3i(index, v0, v1, v2);
        public static void VertexAttribI3ui(uint index, uint v0, uint v1, uint v2) => Delegates.glVertexAttribI3ui(index, v0, v1, v2);
        public static void VertexAttrib4f(uint index, float v0, float v1, float v2, float v3) => Delegates.glVertexAttrib4f(index, v0, v1, v2, v3);
        public static void VertexAttrib4s(uint index, Int16 v0, Int16 v1, Int16 v2, Int16 v3) => Delegates.glVertexAttrib4s(index, v0, v1, v2, v3);
        public static void VertexAttrib4d(uint index, double v0, double v1, double v2, double v3) => Delegates.glVertexAttrib4d(index, v0, v1, v2, v3);
        public static void VertexAttrib4Nub(uint index, Byte v0, Byte v1, Byte v2, Byte v3) => Delegates.glVertexAttrib4Nub(index, v0, v1, v2, v3);
        public static void VertexAttribI4i(uint index, int v0, int v1, int v2, int v3) => Delegates.glVertexAttribI4i(index, v0, v1, v2, v3);
        public static void VertexAttribI4ui(uint index, uint v0, uint v1, uint v2, uint v3) => Delegates.glVertexAttribI4ui(index, v0, v1, v2, v3);
        public static void VertexAttribL1d(uint index, double v0) => Delegates.glVertexAttribL1d(index, v0);
        public static void VertexAttribL2d(uint index, double v0, double v1) => Delegates.glVertexAttribL2d(index, v0, v1);
        public static void VertexAttribL3d(uint index, double v0, double v1, double v2) => Delegates.glVertexAttribL3d(index, v0, v1, v2);
        public static void VertexAttribL4d(uint index, double v0, double v1, double v2, double v3) => Delegates.glVertexAttribL4d(index, v0, v1, v2, v3);
        public static void VertexAttrib1fv(uint index, float[] v)
        {
            fixed (float* vPtr = v)
                Delegates.glVertexAttrib1fv(index, vPtr);
        }
        public static void VertexAttrib1sv(uint index, Int16[] v)
        {
            fixed (Int16* vPtr = v)
                Delegates.glVertexAttrib1sv(index, vPtr);
        }
        public static void VertexAttrib1dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttrib1dv(index, vPtr);
        }
        public static void VertexAttribI1iv(uint index, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glVertexAttribI1iv(index, vPtr);
        }
        public static void VertexAttribI1uiv(uint index, uint[] v)
        {
            fixed (uint* vPtr = v)
                Delegates.glVertexAttribI1uiv(index, vPtr);
        }
        public static void VertexAttrib2fv(uint index, float[] v)
        {
            fixed (float* vPtr = v)
                Delegates.glVertexAttrib2fv(index, vPtr);
        }
        public static void VertexAttrib2sv(uint index, Int16[] v)
        {
            fixed (Int16* vPtr = v)
                Delegates.glVertexAttrib2sv(index, vPtr);
        }
        public static void VertexAttrib2dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttrib2dv(index, vPtr);
        }
        public static void VertexAttribI2iv(uint index, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glVertexAttribI2iv(index, vPtr);
        }
        public static void VertexAttribI2uiv(uint index, uint[] v)
        {
            fixed (uint* vPtr = v)
                Delegates.glVertexAttribI2uiv(index, vPtr);
        }
        public static void VertexAttrib3fv(uint index, float[] v)
        {
            fixed (float* vPtr = v)
                Delegates.glVertexAttrib3fv(index, vPtr);
        }
        public static void VertexAttrib3sv(uint index, Int16[] v)
        {
            fixed (Int16* vPtr = v)
                Delegates.glVertexAttrib3sv(index, vPtr);
        }
        public static void VertexAttrib3dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttrib3dv(index, vPtr);
        }
        public static void VertexAttribI3iv(uint index, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glVertexAttribI3iv(index, vPtr);
        }
        public static void VertexAttribI3uiv(uint index, uint[] v)
        {
            fixed (uint* vPtr = v)
                Delegates.glVertexAttribI3uiv(index, vPtr);
        }
        public static void VertexAttrib4fv(uint index, float[] v)
        {
            fixed (float* vPtr = v)
                Delegates.glVertexAttrib4fv(index, vPtr);
        }
        public static void VertexAttrib4sv(uint index, Int16[] v)
        {
            fixed (Int16* vPtr = v)
                Delegates.glVertexAttrib4sv(index, vPtr);
        }
        public static void VertexAttrib4dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttrib4dv(index, vPtr);
        }
        public static void VertexAttrib4iv(uint index, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glVertexAttrib4iv(index, vPtr);
        }
        public static void VertexAttrib4bv(uint index, SByte[] v)
        {
            fixed (SByte* vPtr = v)
                Delegates.glVertexAttrib4bv(index, vPtr);
        }
        public static void VertexAttrib4ubv(uint index, Byte[] v)
        {
            fixed (Byte* vPtr = v)
                Delegates.glVertexAttrib4ubv(index, vPtr);
        }
        public static void VertexAttrib4usv(uint index, UInt16[] v)
        {
            fixed (UInt16* vPtr = v)
                Delegates.glVertexAttrib4usv(index, vPtr);
        }
        public static void VertexAttrib4uiv(uint index, uint[] v)
        {
            fixed (uint* vPtr = v)
                Delegates.glVertexAttrib4uiv(index, vPtr);
        }
        public static void VertexAttrib4Nbv(uint index, SByte[] v)
        {
            fixed (SByte* vPtr = v)
                Delegates.glVertexAttrib4Nbv(index, vPtr);
        }
        public static void VertexAttrib4Nsv(uint index, Int16[] v)
        {
            fixed (Int16* vPtr = v)
                Delegates.glVertexAttrib4Nsv(index, vPtr);
        }
        public static void VertexAttrib4Niv(uint index, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glVertexAttrib4Niv(index, vPtr);
        }
        public static void VertexAttrib4Nubv(uint index, Byte[] v)
        {
            fixed (Byte* vPtr = v)
                Delegates.glVertexAttrib4Nubv(index, vPtr);
        }
        public static void VertexAttrib4Nusv(uint index, UInt16[] v)
        {
            fixed (UInt16* vPtr = v)
                Delegates.glVertexAttrib4Nusv(index, vPtr);
        }
        public static void VertexAttrib4Nuiv(uint index, uint[] v)
        {
            fixed (uint* vPtr = v)
                Delegates.glVertexAttrib4Nuiv(index, vPtr);
        }
        public static void VertexAttribI4bv(uint index, SByte[] v)
        {
            fixed (SByte* vPtr = v)
                Delegates.glVertexAttribI4bv(index, vPtr);
        }
        public static void VertexAttribI4ubv(uint index, Byte[] v)
        {
            fixed (Byte* vPtr = v)
                Delegates.glVertexAttribI4ubv(index, vPtr);
        }
        public static void VertexAttribI4sv(uint index, Int16[] v)
        {
            fixed (Int16* vPtr = v)
                Delegates.glVertexAttribI4sv(index, vPtr);
        }
        public static void VertexAttribI4usv(uint index, UInt16[] v)
        {
            fixed (UInt16* vPtr = v)
                Delegates.glVertexAttribI4usv(index, vPtr);
        }
        public static void VertexAttribI4iv(uint index, int[] v)
        {
            fixed (int* vPtr = v)
                Delegates.glVertexAttribI4iv(index, vPtr);
        }
        public static void VertexAttribI4uiv(uint index, uint[] v)
        {
            fixed (uint* vPtr = v)
                Delegates.glVertexAttribI4uiv(index, vPtr);
        }
        public static void VertexAttribL1dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttribL1dv(index, vPtr);
        }
        public static void VertexAttribL2dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttribL2dv(index, vPtr);
        }
        public static void VertexAttribL3dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttribL3dv(index, vPtr);
        }
        public static void VertexAttribL4dv(uint index, double[] v)
        {
            fixed (double* vPtr = v)
                Delegates.glVertexAttribL4dv(index, vPtr);
        }
        public static void VertexAttribP1ui(uint index, VertexAttribPType type, bool normalized, uint value) => Delegates.glVertexAttribP1ui(index, type, normalized, value);
        public static void VertexAttribP2ui(uint index, VertexAttribPType type, bool normalized, uint value) => Delegates.glVertexAttribP2ui(index, type, normalized, value);
        public static void VertexAttribP3ui(uint index, VertexAttribPType type, bool normalized, uint value) => Delegates.glVertexAttribP3ui(index, type, normalized, value);
        public static void VertexAttribP4ui(uint index, VertexAttribPType type, bool normalized, uint value) => Delegates.glVertexAttribP4ui(index, type, normalized, value);
        public static void VertexAttribBinding(uint attribindex, uint bindingindex) => Delegates.glVertexAttribBinding(attribindex, bindingindex);
        public static void VertexArrayAttribBinding(uint vaobj, uint attribindex, uint bindingindex) => Delegates.glVertexArrayAttribBinding(vaobj, attribindex, bindingindex);
        public static void VertexAttribDivisor(uint index, uint divisor) => Delegates.glVertexAttribDivisor(index, divisor);
        public static void VertexAttribFormat(uint attribindex, int size, VertexAttribFormatType type, bool normalized, uint relativeoffset) => Delegates.glVertexAttribFormat(attribindex, size, type, normalized, relativeoffset);
        public static void VertexAttribIFormat(uint attribindex, int size, VertexAttribFormatType type, uint relativeoffset) => Delegates.glVertexAttribIFormat(attribindex, size, type, relativeoffset);
        public static void VertexAttribLFormat(uint attribindex, int size, VertexAttribFormatType type, uint relativeoffset) => Delegates.glVertexAttribLFormat(attribindex, size, type, relativeoffset);
        public static void VertexArrayAttribFormat(uint vaobj, uint attribindex, int size, VertexAttribFormatType type, bool normalized, uint relativeoffset) => Delegates.glVertexArrayAttribFormat(vaobj, attribindex, size, type, normalized, relativeoffset);
        public static void VertexArrayAttribIFormat(uint vaobj, uint attribindex, int size, VertexAttribFormatType type, uint relativeoffset) => Delegates.glVertexArrayAttribIFormat(vaobj, attribindex, size, type, relativeoffset);
        public static void VertexArrayAttribLFormat(uint vaobj, uint attribindex, int size, VertexAttribFormatType type, uint relativeoffset) => Delegates.glVertexArrayAttribLFormat(vaobj, attribindex, size, type, relativeoffset);
        public static void VertexAttribPointer(uint index, int size, VertexAttribPointerType type, bool normalized, int stride, int pointer) => Delegates.glVertexAttribPointer(index, size, type, normalized, stride, (void*)pointer);
        public static void VertexAttribIPointer(uint index, int size, VertexAttribPointerType type, int stride, int pointer) => Delegates.glVertexAttribIPointer(index, size, type, stride, (void*)pointer);
        public static void VertexAttribLPointer(uint index, int size, VertexAttribPointerType type, int stride, int pointer) => Delegates.glVertexAttribLPointer(index, size, type, stride, (void*)pointer);
        public static void VertexBindingDivisor(uint bindingindex, uint divisor) => Delegates.glVertexBindingDivisor(bindingindex, divisor);
        public static void VertexArrayBindingDivisor(uint vaobj, uint bindingindex, uint divisor) => Delegates.glVertexArrayBindingDivisor(vaobj, bindingindex, divisor);
        public static void Viewport(int x, int y, int width, int height) => Delegates.glViewport(x, y, width, height);
        public static void ViewportArrayv(uint first, int count, float[] v)
        {
            fixed (float* vPtr = v)
                Delegates.glViewportArrayv(first, count, vPtr);
        }
        public static void ViewportIndexedf(uint index, float x, float y, float w, float h) => Delegates.glViewportIndexedf(index, x, y, w, h);
        public static void ViewportIndexedfv(uint index, float[] v)
        {
            fixed (float* vPtr = v)
                Delegates.glViewportIndexedfv(index, vPtr);
        }
        public static void WaitSync(IntPtr sync, uint flags, ulong timeout) => Delegates.glWaitSync(sync, flags, timeout);
    }
}
