## Magma.OpenGL

This library tries to stay as close to the unmanaged function declarations as possible with a few variations and overrides.
You should be able to follow any OpenGL tutorial without issue.

### Example Code:

```c#
using System;
using Magma.OpenGL;
using Magma.GLFW;

namespace Sandbox
{
    class Program
    {
        private static string vertexShaderSource = "#version 450 core\nlayout (location = 0) in vec3 aPos;\nvoid main()\n{\n   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n}\0";
        private static string fragmentShaderSource = "#version 450 core\nout vec4 FragColor;\nvoid main()\n{\n   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n}\n\0";

        static void Main(string[] args)
        {
            Glfw.Init();

            Glfw.WindowHint(WindowHint.ContextVersionMajor, 4);
            Glfw.WindowHint(WindowHint.ContextVersionMinor, 5);
            Glfw.WindowHint(WindowHint.OpenGLProfile, (int) OpenGLProfile.Core);

            IntPtr window = Glfw.CreateWindow(800, 600, "Game", IntPtr.Zero, IntPtr.Zero);
            if(window == IntPtr.Zero)
            {
                Glfw.Terminate();
                Environment.Exit(-1);
            }

            Glfw.MakeContextCurrent(window);

            uint vshader = Gl.CreateShader(ShaderType.VertexShader);
            uint fshader = Gl.CreateShader(ShaderType.FragmentShader);

            Gl.ShaderSource(vshader, vertexShaderSource, null);
            Gl.CompileShader(vshader);

            Gl.ShaderSource(fshader, fragmentShaderSource, null);
            Gl.CompileShader(fshader);

            uint program = Gl.CreateProgram();
            Gl.AttachShader(program, vshader);
            Gl.AttachShader(program, fshader);
            Gl.LinkProgram(program);

            Gl.DeleteShader(vshader);
            Gl.DeleteShader(fshader);

            float[] vertices = {
                 0.5f,  0.5f, 0.0f,
                 0.5f, -0.5f, 0.0f,
                -0.5f, -0.5f, 0.0f,
                -0.5f,  0.5f, 0.0f
            };
            uint[] indices = {
                0, 1, 3,
                1, 2, 3
            };

            uint[] vbo;
            uint[] vao;
            uint[] ebo;
            Gl.GenVertexArrays(1, out vao);
            Gl.GenBuffers(1, out vbo);
            Gl.GenBuffers(1, out ebo);

            Gl.BindVertexArray(vao[0]);

            Gl.BindBuffer(BufferTarget.ArrayBuffer, vbo[0]);
            Gl.BufferData<float>(BufferTarget.ArrayBuffer, sizeof(float) * vertices.Length, vertices, BufferUsageHint.StaticDraw);

            Gl.BindBuffer(BufferTarget.ElementArrayBuffer, ebo[0]);
            Gl.BufferData<uint>(BufferTarget.ElementArrayBuffer, sizeof(uint) * indices.Length, indices, BufferUsageHint.StaticDraw);

            Gl.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);
            Gl.EnableVertexAttribArray(0);

            Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);

            Gl.BindVertexArray(0);

            int frames = 0;
            double time = Glfw.GetTime();
            while(!Glfw.WindowShouldClose(window))
            {
                frames++;
                Glfw.PollEvents();

                if(Glfw.GetTime() - time >= 1)
                {
                    Console.WriteLine($"FPS: {frames}");
                    Console.WriteLine($"TPS: {1000 / frames}");
                    frames = 0;
                    time = Glfw.GetTime();
                }

                Gl.Clear(ClearBufferMask.ColorBufferBit);
                Gl.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);

                Gl.UseProgram(program);
                Gl.BindVertexArray(vao[0]);
                Gl.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, IntPtr.Zero);

                Glfw.SwapBuffers(window);
            }

            Gl.DeleteVertexArrays(1, vao);
            Gl.DeleteBuffers(1, vbo);
            Gl.DeleteBuffers(1, ebo);
            Gl.DeleteProgram(program);

            Glfw.Terminate();
        }
    }
}
```

## Example Image

![HelloRectangle](ExampleImage/HelloRectangle.png "HelloRectangle.png")
