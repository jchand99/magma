using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkPipelineCache : DeviceBoundObject
    {
        public VkPipelineCache(VkDevice parent, PipelineCacheCreateInfo pipelineCacheCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreatePipelineCache(pipelineCacheCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Pipline Cache: ", result);
        }


        #region Core Methods
        private unsafe VkResult _CreatePipelineCache(PipelineCacheCreateInfo pipelineCacheCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr pipelineCache)
        {
            fixed (byte* ptr = pipelineCacheCreateInfo.InitialData)
            fixed (IntPtr* pipelineCachePtr = &pipelineCache)
            {
                pipelineCacheCreateInfo.ToNative(out var native, ptr);
                if (allocationCallbacks.HasValue) return vkCreatePipelineCache(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, pipelineCachePtr);
                else return vkCreatePipelineCache(LogicalDevice._Handle, &native, null, pipelineCachePtr);
            }
        }

        public unsafe VkResult GetData(out byte[] data)
        {
            int size;
            VkResult result = vkGetPipelineCacheData(LogicalDevice._Handle, _Handle, &size, null);

            data = new byte[size];
            fixed (byte* ptr = data) result = vkGetPipelineCacheData(LogicalDevice._Handle, _Handle, &size, ptr);

            return result;
        }

        public unsafe VkResult Merge(params VkPipelineCache[] sourceCaches)
        {
            var ptrs = stackalloc IntPtr[sourceCaches?.Length ?? 0];
            for (int i = 0; i < sourceCaches.Length; i++)
                ptrs[i] = sourceCaches[i]._Handle;

            return vkMergePipelineCaches(LogicalDevice._Handle, _Handle, (uint)(sourceCaches?.Length ?? 0), ptrs);
        }

        public unsafe VkResult Merge(VkPipelineCache sourceCache)
        {
            fixed (IntPtr* ptr = &sourceCache._Handle)
                return vkMergePipelineCaches(LogicalDevice._Handle, _Handle, 1, ptr);
        }

        internal override unsafe void Destroy()
        {

            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyPipelineCache(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyPipelineCache(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PipelineCacheCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreatePipelineCache = (delegate* unmanaged[Cdecl]<IntPtr, PipelineCacheCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreatePipelineCache");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyPipelineCache = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyPipelineCache");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, int*, byte*, VkResult> vkGetPipelineCacheData = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, int*, byte*, VkResult>)Vulkan.GetStaticProcPointer("vkGetPipelineCacheData");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, VkResult> vkMergePipelineCaches = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkMergePipelineCaches");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created pipeline cache.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PipelineCacheCreateInfo
    {
        /// <summary>
        /// Previously retrieved pipeline cache data. If the pipeline cache data is incompatible with
        /// the device, the pipeline cache will be initially empty. If length is zero, <see
        /// cref="InitialData"/> is ignored.
        /// </summary>
        public byte[] InitialData;

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineCacheCreateInfo"/> structure.
        /// </summary>
        /// <param name="initialData">
        /// Previously retrieved pipeline cache data. If the pipeline cache data is incompatible with
        /// the device, the pipeline cache will be initially empty. If length is zero, <see
        /// cref="InitialData"/> is ignored.
        /// </param>
        public PipelineCacheCreateInfo(byte[] initialData)
        {
            InitialData = initialData;
        }

        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public PipelineCacheCreateFlags Flags;
            public int InitialDataSize;
            public byte* InitialData;
        }

        internal void ToNative(out Native native, byte* initialData)
        {
            native.Type = StructureType.PipelineCacheCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = 0;
            native.InitialDataSize = InitialData?.Length ?? 0;
            native.InitialData = initialData;
        }
    }

    internal enum PipelineCacheCreateFlags
    {
        None = 0
    }

    /// <summary>
    /// Encode pipeline cache version.
    /// </summary>
    public enum PipelineCacheHeaderVersion
    {
        /// <summary>
        /// Specifies version one of the pipeline cache.
        /// </summary>
        One = 1
    }
}
