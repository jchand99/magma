using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct AllocationCallbacks
    {
        public IntPtr UserData;
        public delegate* unmanaged<IntPtr, SizeT, SizeT, SystemAllocationScope, IntPtr> Allocation;
        public delegate* unmanaged<IntPtr, IntPtr, SizeT, SizeT, SystemAllocationScope, IntPtr> Reallocation;
        public delegate* unmanaged<IntPtr, IntPtr, void> Free;
        public delegate* unmanaged<IntPtr, SizeT, InternalAllocationType, SystemAllocationScope, void> InternalAllocation;
        public delegate* unmanaged<IntPtr, SizeT, InternalAllocationType, SystemAllocationScope, void> InternalFree;

        public AllocationCallbacks(delegate* unmanaged<IntPtr, SizeT, SizeT, SystemAllocationScope, IntPtr> alloc, delegate* unmanaged<IntPtr, IntPtr, SizeT, SizeT, SystemAllocationScope, IntPtr> realloc, delegate* unmanaged<IntPtr, IntPtr, void> free,
          delegate* unmanaged<IntPtr, SizeT, InternalAllocationType, SystemAllocationScope, void> internalAlloc = null, delegate* unmanaged<IntPtr, SizeT, InternalAllocationType, SystemAllocationScope, void> internalFree = null,
          IntPtr userData = default)
        {
            Allocation = alloc;
            Reallocation = realloc;
            Free = free;
            InternalAllocation = internalAlloc;
            InternalFree = internalFree;
            UserData = userData;
        }
    }

    public enum SystemAllocationScope
    {
        Command = 0,
        Object = 1,
        Cache = 2,
        Device = 3,
        Instance = 4
    }

    public enum InternalAllocationType
    {
        Executable = 0
    }
}
