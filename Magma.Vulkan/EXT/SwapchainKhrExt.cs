using System;
using Magma.Vulkan.KHR;

namespace Magma.Vulkan.EXT
{
    public static class SwapchainKhrExt
    {
        #region Methods
        public unsafe static VkResult GetSwapchainCounterExt(this VkSwapchainKhr swapchain, SurfaceCountersExt counter, out ulong counterValue)
        {
            fixed (ulong* ptr = &counterValue) return swapchain.vkGetSwapchainCounterEXT(swapchain.LogicalDevice, swapchain, counter, ptr);
        }
        #endregion

        #region C Functions
        #endregion
    }

    // Structs


}
