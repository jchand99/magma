using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure returning information about sample count specific additional multisampling capabilities.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MultisamplePropertiesExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The maximum size of the pixel grid in which sample locations can vary.
        /// </summary>
        public Extent2D MaxSampleLocationGridSize;
    }
}
