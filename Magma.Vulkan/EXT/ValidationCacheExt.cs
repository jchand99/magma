using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    public class ValidationCacheExt : DeviceBoundObject
    {
        public ValidationCacheExt(VkDevice parent, ValidationCacheCreateInfoExt validationCacheCreateInfoExt, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateValidationCacheExt(validationCacheCreateInfoExt, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create ValidationCacheExt: ", result);

            unsafe
            {
                vkCreateValidationCacheEXT = (delegate* unmanaged[Cdecl]<IntPtr, ValidationCacheCreateInfoExt*, AllocationCallbacks*, IntPtr*, VkResult>)LogicalDevice.GetProcAddr("vkCreateValidationCacheEXT");
                vkMergeValidationCachcesEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, VkResult>)LogicalDevice.GetProcAddr("vkMergeValidationCachesEXT");
                vkGetValidationCacheDataEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, int*, IntPtr*, VkResult>)LogicalDevice.GetProcAddr("vkGetValidationCacheDataEXt");
                vkDestroyValidationCacheEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)LogicalDevice.GetProcAddr("vkDestroyValidationCacheEXT");
            }
        }

        #region Methods
        private unsafe VkResult _CreateValidationCacheExt(ValidationCacheCreateInfoExt createInfo, AllocationCallbacks? allocationCallbacks, out IntPtr validationCache)
        {
            fixed (IntPtr* ptr = &validationCache)
            {
                createInfo.Prepare();
                if (allocationCallbacks.HasValue) return vkCreateValidationCacheEXT(LogicalDevice, &createInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateValidationCacheEXT(LogicalDevice, &createInfo, null, ptr);
            }
        }
        public unsafe VkResult MergeValidationCachesExt(ValidationCacheExt dstCache, ValidationCacheExt[] srcCaches)
        {
            var ptr = stackalloc IntPtr[srcCaches?.Length ?? 0];
            for (int i = 0; i < srcCaches.Length; i++)
                ptr[i] = srcCaches[i];
            return vkMergeValidationCachcesEXT(LogicalDevice, dstCache, (uint)(srcCaches?.Length ?? 0), ptr);
        }
        public unsafe void GetValidationCacheDataExt(byte[] data)
        {
            int size;
            VkResult result = vkGetValidationCacheDataEXT(LogicalDevice, _Handle, &size, null);

            data = new byte[size];
            fixed (byte* ptr = data)
            {
                result = vkGetValidationCacheDataEXT(LogicalDevice, _Handle, &size, (IntPtr*)ptr);
            }
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyValidationCacheEXT(LogicalDevice, _Handle, ptr);
            }
            else
            {
                vkDestroyValidationCacheEXT(LogicalDevice, _Handle, null);
            }
        }
        #endregion

        #region C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, ValidationCacheCreateInfoExt*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateValidationCacheEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, VkResult> vkMergeValidationCachcesEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyValidationCacheEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, int*, IntPtr*, VkResult> vkGetValidationCacheDataEXT;
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created validation cache.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ValidationCacheCreateInfoExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        internal StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Reserved for future use.
        /// </summary>
        internal ValidationCacheCreateFlags Flags;
        /// <summary>
        /// The number of bytes in <see cref="InitialData"/>. If <see cref="InitialDataSize"/> is
        /// zero, the validation cache will initially be empty.
        /// </summary>
        public SizeT InitialDataSize;
        /// <summary>
        /// Is a pointer to previously retrieved validation cache data. If the validation cache data
        /// is incompatible with the device, the validation cache will be initially empty. If <see
        /// cref="InitialDataSize"/> is zero, <see cref="InitialData"/> is ignored.
        /// </summary>
        public IntPtr InitialData;

        internal void Prepare()
        {
            Type = StructureType.ValidationCacheCreateInfoExt;
        }
    }

    // Is reserved for future use.
    internal enum ValidationCacheCreateFlags
    {
        None = 0
    }
}
