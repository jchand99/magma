using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure describing sample location limits that can be supported by an implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PhysicalDeviceSampleLocationsPropertiesExt
    {
        public StructureType Type;
        public IntPtr Next;
        public int SampleLocationSampleCounts;
        public Extent2D MaxSampleLocationGridSize;
        public fixed float SampleLocationCoordinateRange[2];
        public int SampleLocationSubPixelBits;
        public bool VariableSampleLocations;
    }
}
