using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Magma.Vulkan.EXT
{
    public struct DebugUtilsObjectNameInfoExt
    {
        public IntPtr Next;
        public DebugReportObjectTypeExt ObjectType;
        public IntPtr ObjectHandle;
        public String ObjectName;

        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct Native
        {
            internal StructureType StructureType;
            internal IntPtr Next;
            internal DebugReportObjectTypeExt ObjectType;
            internal IntPtr ObjectHandle;
            internal byte* ObjectName;
        }

        internal static unsafe void FromNative(ref Native native, out DebugUtilsObjectNameInfoExt managed)
        {
            managed.Next = native.Next;
            managed.ObjectType = native.ObjectType;
            managed.ObjectHandle = native.ObjectHandle;
            managed.ObjectName = StringExtensions.FromPointer(native.ObjectName);
        }
    }
}
