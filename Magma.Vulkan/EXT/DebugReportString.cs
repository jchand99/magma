namespace Magma.Vulkan.EXT
{
    public static class DebugReportString
    {
        public static unsafe string FromBytePtr(byte* ptr) => StringExtensions.FromPointer(ptr);
    }
}
