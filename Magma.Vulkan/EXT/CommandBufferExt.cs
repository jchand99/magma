using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    public static class CommandBufferExt
    {
        #region Methods
        public unsafe static void CmdDebugMarkerBeginExt(this VkCommandBuffer commandBuffer, DebugMarkerMarkerInfoExt markerInfoExt)
        {
            var byteCount = markerInfoExt.MarkerName.GetMaxBytes();
            var bytes = stackalloc byte[byteCount];
            markerInfoExt.MarkerName.ToBytePointer(bytes);
            markerInfoExt.ToNative(out var native, bytes);
            commandBuffer.vkCmdDebugMarkerBeginEXT(commandBuffer, &native);
        }
        public unsafe static void CmdDebugMarkerEndExt(this VkCommandBuffer commandBuffer)
        {
            commandBuffer.vkCmdDebugMarkerEndEXT(commandBuffer);
        }
        public unsafe static void CmdDebugMarkerInsertExt(this VkCommandBuffer commandBuffer, DebugMarkerMarkerInfoExt markerInfoExt)
        {
            var byteCount = markerInfoExt.MarkerName.GetMaxBytes();
            var bytes = stackalloc byte[byteCount];
            markerInfoExt.MarkerName.ToBytePointer(bytes);
            markerInfoExt.ToNative(out var native, bytes);
            commandBuffer.vkCmdDebugMarkerInstertEXT(commandBuffer, &native);
        }
        public unsafe static void CmdSetDiscardRectangleExt(this VkCommandBuffer commandBuffer, uint firstDiscardRectangle, VkRect2D[] discardRectangles)
        {
            fixed (VkRect2D* ptr = discardRectangles)
                commandBuffer.vkCmdSetDiscardRectangleEXT(commandBuffer, firstDiscardRectangle, (uint)(discardRectangles?.Length ?? 0), ptr);
        }
        public unsafe static void CmdSetSampleLocationsExt(this VkCommandBuffer commandBuffer, SampleLocationsInfoExt sampleLocationsInfoExt)
        {
            commandBuffer.vkCmdSetSampleLocationsEXT(commandBuffer, &sampleLocationsInfoExt);
        }
        #endregion
    }

    // Structs

    /// <summary>
    /// Specify parameters of a command buffer marker region.
    /// </summary>
    public unsafe struct DebugMarkerMarkerInfoExt
    {
        /// <summary>
        /// A unicode string that contains the name of the marker.
        /// </summary>
        public string MarkerName;
        /// <summary>
        /// An optional RGBA color value that can be associated with the marker. A particular
        /// implementation may choose to ignore this color value. The values contain RGBA values in
        /// order, in the range 0.0 to 1.0. If all elements in color are set to 0.0 then it is ignored.
        /// </summary>
        public ColorF4 Color;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugMarkerMarkerInfoExt"/> structure.
        /// </summary>
        /// <param name="markerName">A unicode string that contains the name of the marker.</param>
        /// <param name="color">
        /// An optional RGBA color value that can be associated with the marker. A particular
        /// implementation may choose to ignore this color value. The values contain RGBA values in
        /// order, in the range 0.0 to 1.0. If all elements in color are set to 0.0 then it is ignored.
        /// </param>
        public DebugMarkerMarkerInfoExt(string markerName, ColorF4 color = default(ColorF4))
        {
            MarkerName = markerName;
            Color = color;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public byte* MarkerName;
            public ColorF4 Color;
        }

        internal void ToNative(out Native native, byte* markerName)
        {
            native.Type = StructureType.DebugMarkerMarkerInfoExt;
            native.Next = IntPtr.Zero;
            native.MarkerName = markerName;
            native.Color = Color;
        }
    }

    /// <summary>
    /// Structure specifying a set of sample locations.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SampleLocationsInfoExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Specifies the number of sample locations per pixel.
        /// </summary>
        public SampleCounts SampleLocationsPerPixel;
        /// <summary>
        /// The size of the sample location grid to select custom sample locations for.
        /// </summary>
        public Extent2D SampleLocationGridSize;
        /// <summary>
        /// The number of sample locations in <see cref="SampleLocations"/>.
        /// </summary>
        public int SampleLocationsCount;
        /// <summary>
        /// An array of <see cref="SampleLocationExt"/> structures.
        /// </summary>
        public IntPtr SampleLocations;
    }
}
