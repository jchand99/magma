using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    public class VkDebugReportCallbackExt : InstanceBoundObject
    {

        public VkDebugReportCallbackExt(VkInstance parent, DebugReportCallbackCreateInfoExt debugReportCallbackCreateInfoExt, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            Instance = parent;

            VkResult result = _CreateDebugReportCallbackExt(debugReportCallbackCreateInfoExt, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create DeubgReportCallbackExt: ", result);

            unsafe
            {
                vkCreateDebugReportCallbackEXT = (delegate* unmanaged[Cdecl]<IntPtr, DebugReportCallbackCreateInfoExt.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Instance.GetProcAddr("vkCreateDebugReportCallbackEXT");
                vkDestroyDebugReportCallbackEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, VkResult>)Instance.GetProcAddr("vkDestroyDebugReportCallbackEXT");
            }
        }

        #region Methods
        private unsafe VkResult _CreateDebugReportCallbackExt(DebugReportCallbackCreateInfoExt createInfoExt, AllocationCallbacks? allocationCallbacks, out IntPtr callback)
        {
            if (vkCreateDebugReportCallbackEXT == (void*)0)
            {
                callback = IntPtr.Zero;
                return VkResult.ErrorUnknown;
            }

            if (createInfoExt.Callback != null)
            {
                _DebugReportCallback = (flags, objectType, @object, location, messageCode, layerPrefix, message, userData) =>
                    createInfoExt.Callback(flags, objectType, @object, location, messageCode, StringExtensions.FromPointer(layerPrefix), StringExtensions.FromPointer(message), userData);
            }
            createInfoExt.ToNative(out var native, Marshal.GetFunctionPointerForDelegate(_DebugReportCallback));

            fixed (IntPtr* ptr = &callback)
            {
                if (allocationCallbacks.HasValue) return vkCreateDebugReportCallbackEXT(Instance, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateDebugReportCallbackEXT(Instance, &native, null, ptr);
            }
        }

        internal override unsafe void Destroy()
        {
            if (vkDestroyDebugReportCallbackEXT == (void*)0) return;

            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyDebugReportCallbackEXT(Instance, _Handle, ptr);
            }
            else
            {
                vkDestroyDebugReportCallbackEXT(Instance, _Handle, null);
            }
        }
        #endregion

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal unsafe delegate bool DebugReportCallback(DebugReportFlagsExt flagsExt, DebugReportObjectTypeExt objectType, IntPtr @object, IntPtr location, int messageCode, byte* layerPrefix, byte* message, IntPtr userData);
        internal static DebugReportCallback _DebugReportCallback;

        #region C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DebugReportCallbackCreateInfoExt.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateDebugReportCallbackEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, VkResult> vkDestroyDebugReportCallbackEXT;
        #endregion

    }
}
