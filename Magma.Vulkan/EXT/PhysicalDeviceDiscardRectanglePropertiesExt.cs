using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure describing discard rectangle limits that can be supported by an implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceDiscardRectanglePropertiesExt
    {
        internal StructureType Type;

        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The maximum number of discard rectangles that can be specified.
        /// </summary>
        public int MaxDiscardRectangles;
    }
}
