using System;

namespace Magma.Vulkan.EXT
{
    [Flags]
    public enum PipelineRasterizationConservativeStateCreateFlagsExt
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0
    }
}
