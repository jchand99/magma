namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Encode validation cache version.
    /// </summary>
    public enum ValidationCacheHeaderVersionExt
    {
        /// <summary>
        /// Specifies version one of the validation cache.
        /// </summary>
        One = 1
    }
}
