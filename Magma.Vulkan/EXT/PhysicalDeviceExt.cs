using System;
using System.Runtime.InteropServices;
using Magma.Vulkan.KHR;

namespace Magma.Vulkan.EXT
{
    public static class PhysicalDeviceExt
    {
        #region Methods
        public unsafe static VkResult GetSurfaceCapabilities2Ext(this VkPhysicalDevice physicalDevice, VkSurfaceKhr surface, SurfaceCapabilities2Ext surfaceCapabilities2Ext)
        {
            return physicalDevice.vkGetPhysicalDeviceSurfaceCapabilities2EXT(physicalDevice, surface, &surfaceCapabilities2Ext);
        }
        public unsafe static VkResult GetRandROutputDisplayExt(this VkPhysicalDevice physicalDevice, IntPtr dpy, IntPtr rrOutput, DisplayKhr display)
        {
            return physicalDevice.vkGetRandROutputDisplayEXT(physicalDevice, &dpy, rrOutput, display);
        }
        public unsafe static void GetMultisamplePropertiesExt(this VkPhysicalDevice physicalDevice, SampleCounts samples, MultisamplePropertiesExt multisampleProperties)
        {
            physicalDevice.vkGetPhysicalDeviceMultisamplePropertiesEXT(physicalDevice, samples, &multisampleProperties);
        }
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure describing capabilities of a surface.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SurfaceCapabilities2Ext
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The minimum number of images the specified device supports for a swapchain created for
        /// the surface, and will be at least one.
        /// </summary>
        public int MinImageCount;
        /// <summary>
        /// The maximum number of images the specified device supports for a swapchain created for
        /// the surface, and will be either 0, or greater than or equal to <see
        /// cref="MinImageCount"/>. A value of 0 means that there is no limit on the number of
        /// images, though there may be limits related to the total amount of memory used by
        /// swapchain images.
        /// </summary>
        public int MaxImageCount;
        /// <summary>
        /// The current width and height of the surface, or the special value <see
        /// cref="Extent2D.WholeSize"/> indicating that the surface size will be determined by the
        /// extent of a swapchain targeting the surface.
        /// </summary>
        public Extent2D CurrentExtent;
        /// <summary>
        /// Contains the smallest valid swapchain extent for the surface on the specified device. The
        /// width and height of the extent will each be less than or equal to the corresponding width
        /// and height of <see cref="CurrentExtent"/>, unless <see cref="CurrentExtent"/> has the
        /// special value described above.
        /// </summary>
        public Extent2D MinImageExtent;
        /// <summary>
        /// Contains the largest valid swapchain extent for the surface on the specified device. The
        /// width and height of the extent will each be greater than or equal to the corresponding
        /// width and height of <see cref="MinImageExtent"/>. The width and height of the extent will
        /// each be greater than or equal to the corresponding width and height of <see
        /// cref="CurrentExtent"/>, unless <see cref="CurrentExtent"/> has the special value
        /// described above.
        /// </summary>
        public Extent2D MaxImageExtent;
        /// <summary>
        /// The maximum number of layers swapchain images can have for a swapchain created for this
        /// device and surface, and will be at least one.
        /// </summary>
        public int MaxImageArrayLayers;
        /// <summary>
        /// A bitmask of <see cref="SurfaceTransformsKhr"/>, describing the presentation
        /// transforms supported for the surface on the specified device, and at least one bit will
        /// be set.
        /// </summary>
        public SurfaceTransformsKhr SupportedTransforms;
        /// <summary>
        /// The surface's current transform relative to the presentation engine's natural
        /// orientation, as described by <see cref="SurfaceTransformsKhr"/>.
        /// </summary>
        public SurfaceTransformsKhr CurrentTransform;
        /// <summary>
        /// A bitmask of <see cref="CompositeAlphasKhr"/>, representing the alpha compositing
        /// modes supported by the presentation engine for the surface on the specified device, and
        /// at least one bit will be set. Opaque composition can be achieved in any alpha compositing
        /// mode by either using a swapchain image format that has no alpha component, or by ensuring
        /// that all pixels in the swapchain images have an alpha value of 1.0.
        /// </summary>
        public CompositeAlphasKhr SupportedCompositeAlpha;
        /// <summary>
        /// A bitmask of <see cref="ImageUsages"/> representing the ways the
        /// application can use the presentable images of a swapchain created for the
        /// surface on the specified device. <see cref="ImageUsages.ColorAttachment"/>
        /// must be included in the set but implementations may support additional usages.
        /// </summary>
        public ImageUsages SupportedUsageFlags;
        /// <summary>
        /// A bitfield containing one bit set for each surface counter type supported.
        /// </summary>
        public SurfaceCountersExt SupportedSurfaceCounters;
    }

    /// <summary>
    /// Surface-relative counter types.
    /// </summary>
    [Flags]
    public enum SurfaceCountersExt
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Indicates a counter incrementing once every time a vertical blanking period occurs on the
        /// display associated with the surface.
        /// </summary>
        VBlank = 1 << 0
    }
}
