namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Specify the discard rectangle mode.
    /// </summary>
    public enum DiscardRectangleModeExt
    {
        /// <summary>
        /// Specifies that a fragment within any discard rectangle satisfies the test.
        /// </summary>
        Inclusive = 0,
        /// <summary>
        /// Specifies that a fragment not within any of the discard rectangles satisfies
        /// the test.
        /// </summary>
        Exclusive = 1
    }
}
