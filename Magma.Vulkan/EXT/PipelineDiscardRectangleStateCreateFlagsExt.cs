using System;

namespace Magma.Vulkan.EXT
{
    [Flags]
    public enum PipelineDiscardRectangleStateCreateFlagsExt
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0
    }
}
