using System;
using System.Runtime.InteropServices;
using Magma.Vulkan.KHR;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Import memory from a host pointer.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImportMemoryHostPointerInfoExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Specifies the handle type.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleType;
        /// <summary>
        /// The host pointer to import from.
        /// </summary>
        public IntPtr HostPointer;
    }
}
