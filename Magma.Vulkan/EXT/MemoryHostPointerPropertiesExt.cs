using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Roperties of external memory host pointer.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryHostPointerPropertiesExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask containing one bit set for every memory type which the specified host pointer
        /// can be imported as.
        /// </summary>
        public int MemoryTypeBits;
    }
}
