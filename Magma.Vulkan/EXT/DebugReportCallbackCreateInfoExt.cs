using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying parameters of a newly created debug report callback.
    /// <para>
    /// For each <see cref="DebugReportCallbackCreateInfoExt"/> that is created the flags determine
    /// when that function is called.
    /// </para>
    /// <para>
    /// A callback will be made for issues that match any bit set in its flags. The callback will
    /// come directly from the component that detected the event, unless some other layer intercepts
    /// the calls for its own purposes (filter them in different way, log to system error log, etc.)
    /// An application may receive multiple callbacks if multiple <see
    /// cref="DebugReportCallbackCreateInfoExt"/> objects were created.
    /// </para>
    /// <para>A callback will always be executed in the same thread as the originating Vulkan call.</para>
    /// <para>
    /// A callback may be called from multiple threads simultaneously (if the application is making
    /// Vulkan calls from multiple threads).
    /// </para>
    /// </summary>
    public unsafe struct DebugReportCallbackCreateInfoExt
    {
        /// <summary>
        /// A bitmask specifying which event(s) will cause this callback to be called. Flags are
        /// interpreted as bitmasks and multiple can be set.
        /// </summary>
        public DebugReportFlagsExt Flags;
        /// <summary>
        /// The application callback function to call.
        /// </summary>
        public Func<DebugReportFlagsExt, DebugReportObjectTypeExt, IntPtr, IntPtr, int, string, string, IntPtr, bool> Callback;
        /// <summary>
        /// User data to be passed to the callback.
        /// </summary>
        public IntPtr UserData;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugReportCallbackCreateInfoExt"/> structure.
        /// </summary>
        /// <param name="flags">
        /// A bitmask specifying which event(s) will cause this callback to be called. Flags are
        /// interpreted as bitmasks and multiple can be set.
        /// </param>
        /// <param name="callback">The application callback function to call.</param>
        /// <param name="userData">User data to be passed to the callback.</param>
        public DebugReportCallbackCreateInfoExt(
            DebugReportFlagsExt flags,
            Func<DebugReportFlagsExt, DebugReportObjectTypeExt, IntPtr, IntPtr, int, string, string, IntPtr, bool> callback,
            IntPtr userData = default)
        {
            Flags = flags;
            Callback = callback;
            UserData = userData;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DebugReportFlagsExt Flags;
            public IntPtr Callback;
            public IntPtr UserData;
        }

        internal void ToNative(out Native native, IntPtr callback)
        {
            native.Type = StructureType.DebugReportCallbackCreateInfoExt;
            native.Next = IntPtr.Zero;
            native.Flags = Flags;
            native.Callback = callback;
            native.UserData = UserData;
        }
    }
}
