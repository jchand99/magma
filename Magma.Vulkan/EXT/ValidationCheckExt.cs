namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Specify validation checks to disable.
    /// </summary>
    public enum ValidationCheckExt
    {
        /// <summary>
        /// Specifies that all validation checks are disabled.
        /// </summary>
        All = 0,
        /// <summary>
        /// Specifies that shader validation is disabled.
        /// </summary>
        Shaders = 1
    }
}
