using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Specify validation checks to disable for a Vulkan instance.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ValidationFlagsExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The number of checks to disable.
        /// </summary>
        public int DisabledValidationCheckCount;
        /// <summary>
        /// A pointer to an array of <see cref="ValidationCheckExt"/> values specifying the
        /// validation checks to be disabled.
        /// </summary>
        public IntPtr DisabledValidationChecks;
    }
}
