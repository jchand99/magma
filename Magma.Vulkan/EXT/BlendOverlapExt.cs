namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Enumerant specifying the blend overlap parameter.
    /// </summary>
    public enum BlendOverlapExt
    {
        /// <summary>
        /// Specifies that there is no correlation between the source and destination
        /// coverage.
        /// </summary>
        Uncorrelated = 0,
        /// <summary>
        /// Specifies that the source and destination coverage are considered to have
        /// minimal overlap.
        /// </summary>
        Disjoint = 1,
        /// <summary>
        /// Specifies that the source and destination coverage are considered to have
        /// maximal overlap.
        /// </summary>
        Conjoint = 2
    }
}
