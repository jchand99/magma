using System;
using System.Text;

namespace Magma.Vulkan.EXT
{
    public static class InstanceExt
    {
        #region Methods
        public unsafe static void DebugReportMessageExt(this VkInstance instance, DebugReportFlagsExt flags, DebugReportObjectTypeExt objectType, ulong @object, SizeT location, int messageCode, string layerPrefix, string message)
        {
            fixed (byte* ptr = Encoding.UTF8.GetBytes(layerPrefix))
            fixed (byte* mptr = Encoding.UTF8.GetBytes(message))
            {
                instance.vkDebugReportMessageEXT(instance, flags, objectType, @object, location, messageCode, ptr, mptr);
            }
        }
        #endregion
    }

    // Structs

    /// <summary>
    /// Bitmask specifying events which cause a debug report callback.
    /// </summary>
    [Flags]
    public enum DebugReportFlagsExt
    {
        /// <summary>
        /// Specifies an informational message such as resource details that may be handy when
        /// debugging an application.
        /// </summary>
        Information = 1 << 0,
        /// <summary>
        /// Specifies use of Vulkan that may expose an app bug. Such cases may not be immediately
        /// harmful, such as a fragment shader outputting to a location with no attachment. Other
        /// cases may point to behavior that is almost certainly bad when unintended such as using an
        /// image whose memory has not been filled. In general if you see a warning but you know that
        /// the behavior is intended/desired, then simply ignore the warning.
        /// </summary>
        Warning = 1 << 1,
        /// <summary>
        /// Specifies a potentially non-optimal use of Vulkan. E.g. using <see
        /// cref="VkCommandBuffer.CmdClearColorImage"/> when a <see cref="VkRenderPass"/> load op would
        /// have worked.
        /// </summary>
        PerformanceWarning = 1 << 2,
        /// <summary>
        /// Specifies that an error that may cause undefined results, including an application crash.
        /// </summary>
        Error = 1 << 3,
        /// <summary>
        /// Specifies diagnostic information from the loader and layers.
        /// </summary>
        Debug = 1 << 4,
        /// <summary>
        /// All flags.
        /// </summary>
        All = Information | Warning | PerformanceWarning | Error | Debug
    }
}
