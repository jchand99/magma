namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Specify the discard rectangle mode.
    /// </summary>
    public enum CoverageModulationModeNV
    {
        /// <summary>
        /// Specifies that no components are multiplied by the modulation factor.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that the red, green, and blue components are multiplied by the modulation factor.
        /// </summary>
        Rgb = 1,
        /// <summary>
        /// Specifies that the alpha component is multiplied by the modulation factor.
        /// </summary>
        Alpha = 2,
        /// <summary>
        /// Specifies that all components are multiplied by the modulation factor.
        /// </summary>
        Rgba = 3
    }
}
