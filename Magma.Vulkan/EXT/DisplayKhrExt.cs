using System;
using Magma.Vulkan.KHR;

namespace Magma.Vulkan.EXT
{
    public static class DisplayKhrExt
    {
        #region Methods
        public unsafe static void AcquireXlibDisplayExt(this DisplayKhr display, IntPtr dpy)
        {
            display.vkAcquireXlibDisplayEXT(display.PhysicalDevice, &dpy, display);
        }
        public unsafe static void ReleaseDisplayExt(this DisplayKhr display)
        {
            display.vkReleaseDisplayEXT(display.PhysicalDevice, display);
        }
        #endregion

        #region C Functions
        #endregion
    }

    // Structs


}
