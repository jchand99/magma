using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying parameters that affect advanced blend operations.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PipelineColorBlendAdvancedStateCreateInfoExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Specifies whether the source color of the blend operation is treated as premultiplied.
        /// </summary>
        public bool SrcPremultiplied;
        /// <summary>
        /// Specifies whether the destination color of the blend operation is treated as premultiplied.
        /// </summary>
        public bool DstPremultiplied;
        /// <summary>
        /// Specifies how the source and destination sample's coverage is correlated.
        /// </summary>
        public BlendOverlapExt BlendOverlap;
    }
}
