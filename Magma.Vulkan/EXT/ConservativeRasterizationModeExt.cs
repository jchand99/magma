namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Specify the conservative rasterization mode.
    /// </summary>
    public enum ConservativeRasterizationModeExt
    {
        /// <summary>
        /// Specifies that conservative rasterization is disabled and rasterization proceeds as normal.
        /// </summary>
        Disabled = 0,
        /// <summary>
        /// Specifies that conservative rasterization is enabled in overestimation mode.
        /// </summary>
        Overestimate = 1,
        /// <summary>
        /// Specifies that conservative rasterization is enabled in underestimation mode.
        /// </summary>
        Underestimate = 2
    }
}
