using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    public class VkDebugUtilsMessengerExt : InstanceBoundObject
    {
        public VkDebugUtilsMessengerExt(VkInstance parent, DebugUtilsMessengerCreateInfoExt debugUtilsMessengerCreateInfoExt, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            Instance = parent;

            unsafe
            {
                vkCreateDebugUtilsMessengerEXT = (delegate* unmanaged[Cdecl]<IntPtr, DebugUtilsMessengerCreateInfoExt.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Instance.GetProcAddr("vkCreateDebugUtilsMessengerEXT");
                vkDestroyDebugUtilsMessengerEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Instance.GetProcAddr("vkDestroyDebugUtilsMessengerEXT");
            }

            VkResult result = _CreateDebugUtilsMessengerExt(debugUtilsMessengerCreateInfoExt, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create DebugUtilsMessengerExt: ", result);
        }

        #region Methods
        private unsafe VkResult _CreateDebugUtilsMessengerExt(DebugUtilsMessengerCreateInfoExt createInfo, AllocationCallbacks? allocationCallbacks, out IntPtr messenger)
        {
            if (vkCreateDebugUtilsMessengerEXT == (void*)0)
            {
                messenger = IntPtr.Zero;
                return VkResult.ErrorUnknown;
            }

            if (createInfo.UserCallback != null)
            {
                _DebugUtilsMessengerCallback = (messageSeverity, messageTypesExt, callbackData, userData) =>
                {
                    DebugUtilsMessengerCallbackDataExt.FromNative(callbackData, out var managed);
                    return createInfo.UserCallback(messageSeverity, messageTypesExt, managed, userData);
                };
            }
            createInfo.ToNative(out var native, Marshal.GetFunctionPointerForDelegate(_DebugUtilsMessengerCallback));
            fixed (IntPtr* ptr = &messenger)
            {
                if (allocationCallbacks.HasValue) return vkCreateDebugUtilsMessengerEXT(Instance, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateDebugUtilsMessengerEXT(Instance, &native, null, ptr);
            }
        }

        internal override unsafe void Destroy()
        {
            if (vkDestroyDebugUtilsMessengerEXT == (void*)0) return;
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyDebugUtilsMessengerEXT(Instance, _Handle, ptr);
            }
            else
            {
                vkDestroyDebugUtilsMessengerEXT(Instance, _Handle, null);
            }
        }
        #endregion

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        internal unsafe delegate bool DebugUtilsMessengerCallback(DebugUtilsMessageSeveritiesExt messageSeverity, DebugUtilsMessageTypesExt messageTypesExt, DebugUtilsMessengerCallbackDataExt.Native* callbackData, IntPtr userData);
        internal static DebugUtilsMessengerCallback _DebugUtilsMessengerCallback;

        #region C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DebugUtilsMessengerCreateInfoExt.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateDebugUtilsMessengerEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyDebugUtilsMessengerEXT;
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created debug messenger.
    /// </summary>
    public struct DebugUtilsMessengerCreateInfoExt
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is 0 and reserved for future use.
        /// </summary>
        private DebugUtilsMessengerCreateFlagsExt Flags;
        /// <summary>
        /// Specifies which severity of event(s) will cause this callback to be called.
        /// </summary>
        public DebugUtilsMessageSeveritiesExt MessageSeverity;
        /// <summary>
        /// Specifies which type of event(s) will cause this callback to be called.
        /// </summary>
        public DebugUtilsMessageTypesExt MessageTypes;
        /// <summary>
        /// The application callback function to call.
        /// </summary>
        public Func<DebugUtilsMessageSeveritiesExt, DebugUtilsMessageTypesExt, DebugUtilsMessengerCallbackDataExt, IntPtr, bool> UserCallback;
        /// <summary>
        /// User data to be passed to the callback.
        /// </summary>
        public IntPtr UserData;

        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DebugUtilsMessengerCreateFlagsExt Flags;
            public DebugUtilsMessageSeveritiesExt MessageSeverity;
            public DebugUtilsMessageTypesExt MessageTypes;
            public IntPtr UserCallback;
            public IntPtr UserData;
        }

        internal void ToNative(out Native native, IntPtr callback)
        {
            native.Type = StructureType.DebugUtilsMessengerCreateInfoExt;
            native.Next = IntPtr.Zero;
            native.Flags = Flags;
            native.MessageSeverity = MessageSeverity;
            native.MessageTypes = MessageTypes;
            native.UserCallback = callback;
            native.UserData = UserData;
        }
    }

    // Is reserved for future use.
    internal enum DebugUtilsMessengerCreateFlagsExt
    {
        None = 0
    }

    /// <summary>
    /// Bitmask specifying which severities of events cause a debug messenger callback.
    /// </summary>
    [Flags]
    public enum DebugUtilsMessageSeveritiesExt
    {
        /// <summary>
        /// Specifies the most verbose output indicating all diagnostic messages from the Vulkan
        /// loader, layers, and drivers should be captured.
        /// </summary>
        Verbose = 1 << 0,
        /// <summary>
        /// Specifies an informational message such as resource details that may be handy when
        /// debugging an application.
        /// </summary>
        Info = 1 << 4,
        /// <summary>
        /// Specifies use of Vulkan that may: expose an app bug. Such cases may not be immediately
        /// harmful, such as a fragment shader outputting to a location with no attachment. Other
        /// cases may: point to behavior that is almost certainly bad when unintended such as using
        /// an image whose memory has not been filled. In general if you see a warning but you know
        /// that the behavior is intended/desired, then simply ignore the warning.
        /// </summary>
        Warning = 1 << 8,
        /// <summary>
        /// Specifies that an error that may cause undefined results, including an application crash.
        /// </summary>
        Error = 1 << 12
    }

    /// <summary>
    /// Bitmask specifying which types of events cause a debug messenger callback.
    /// </summary>
    [Flags]
    public enum DebugUtilsMessageTypesExt
    {
        /// <summary>
        /// Specifies that some general event has occurred. This is typically a non-specification,
        /// non-performance event.
        /// </summary>
        General = 1 << 0,
        /// <summary>
        /// Specifies that something has occurred during validation against the Vulkan specification
        /// that may indicate invalid behavior.
        /// </summary>
        Validation = 1 << 1,
        /// <summary>
        /// Specifies a potentially non-optimal use of Vulkan, e.g. using <see
        /// cref="VkCommandBuffer.CmdClearColorImage"/> when setting <see
        /// cref="AttachmentDescription.LoadOp"/> to <see cref="AttachmentLoadOp.Clear"/> would have worked.
        /// </summary>
        Performance = 1 << 2
    }
}
