using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure describing sampler filter minmax limits that can be supported by an implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceSamplerFilterMinmaxPropertiesExt
    {
        public StructureType Type;
        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        public bool FilterMinmaxSingleComponentFormats;
        public bool FilterMinmaxImageComponentMapping;
    }
}
