using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure describing external memory host pointer limits that can be supported by an implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceExternalMemoryHostPropertiesExt
    {
        public StructureType Type;
        public IntPtr Next;
        public long MinImportedHostPointerAlignment;
    }
}
