using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Magma.Vulkan.EXT
{
    public struct DebugUtilsMessengerCallbackDataExt
    {
        public IntPtr Next;
        public String MessageIdName;
        public int MessageIdNumber;
        public string Message;
        public DebugUtilsLabelExt[] QueueLabels;
        public DebugUtilsLabelExt[] CmdBufLabels;
        public DebugUtilsObjectNameInfoExt[] Objects;

        [StructLayout(LayoutKind.Sequential)]
        public unsafe struct Native
        {
            internal StructureType StructureType;
            internal IntPtr Next;
            internal DebugUtilsMessengerCallbackDataFlagsExt Flags;
            internal byte* MessageIdName;
            internal int MessageIdNumber;
            internal byte* Message;
            internal uint QueueLabelCount;
            internal DebugUtilsLabelExt.Native* QueueLabels;
            internal uint CmdBufLabelCount;
            internal DebugUtilsLabelExt.Native* CmdBufLabels;
            internal uint ObjectCount;
            internal DebugUtilsObjectNameInfoExt.Native* Objects;
        }

        public static unsafe void FromNative(Native* native, out DebugUtilsMessengerCallbackDataExt managed)
        {
            managed.Next = native->Next;
            managed.MessageIdName = StringExtensions.FromPointer(native->MessageIdName);
            managed.MessageIdNumber = native->MessageIdNumber;
            managed.Message = StringExtensions.FromPointer(native->Message);

            managed.CmdBufLabels = new DebugUtilsLabelExt[native->CmdBufLabelCount];
            for (int i = 0; i < native->CmdBufLabelCount; i++)
                DebugUtilsLabelExt.FromNative(ref native->CmdBufLabels[i], out managed.CmdBufLabels[i]);

            managed.QueueLabels = new DebugUtilsLabelExt[native->QueueLabelCount];
            for (int i = 0; i < native->QueueLabelCount; i++)
                DebugUtilsLabelExt.FromNative(ref native->QueueLabels[i], out managed.QueueLabels[i]);

            managed.Objects = new DebugUtilsObjectNameInfoExt[native->ObjectCount];
            for (int i = 0; i < native->ObjectCount; i++)
                DebugUtilsObjectNameInfoExt.FromNative(ref native->Objects[i], out managed.Objects[i]);
        }
    }
}
