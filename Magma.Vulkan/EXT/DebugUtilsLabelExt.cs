using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Magma.Vulkan.EXT
{
    public struct DebugUtilsLabelExt
    {
        public IntPtr Next;
        public String LabelName;
        public ColorF4 Color;

        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct Native
        {
            internal StructureType StructureType;
            internal IntPtr Next;
            internal byte* LabelName;
            internal ColorF4 Color;
        }

        internal static unsafe void FromNative(ref Native native, out DebugUtilsLabelExt managed)
        {
            managed.Next = native.Next;
            managed.LabelName = StringExtensions.FromPointer(native.LabelName);
            managed.Color = native.Color;
        }
    }
}
