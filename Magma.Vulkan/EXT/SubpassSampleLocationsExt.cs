using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying the sample locations state to use for layout transitions of
    /// attachments performed after a given subpass.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SubpassSampleLocationsExt
    {
        /// <summary>
        /// Is the index of the subpass for which the sample locations state is provided.
        /// </summary>
        public int SubpassIndex;
        /// <summary>
        /// Is the sample locations state to use for the layout transition of the depth/stencil
        /// attachment away from the image layout the attachment is used with in the subpass
        /// specified in <c>SubpassIndex</c>.
        /// </summary>
        public SampleLocationsInfoExt SampleLocationsInfo;
    }
}
