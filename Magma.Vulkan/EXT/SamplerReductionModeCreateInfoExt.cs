using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying sampler reduction mode.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SamplerReductionModeCreateInfoExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Controls how texture filtering combines texel values.
        /// </summary>
        public SamplerReductionModeExt ReductionMode;
    }
}
