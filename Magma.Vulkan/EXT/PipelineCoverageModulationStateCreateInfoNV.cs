using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying parameters controlling coverage modulation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PipelineCoverageModulationStateCreateInfoNV
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is reserved for future use.
        /// </summary>
        public PipelineCoverageModulationStateCreateFlagsNV Flags;
        /// <summary>
        /// Controls which color components are modulated and is of type <see cref="CoverageModulationModeNV"/>.
        /// </summary>
        public CoverageModulationModeNV CoverageModulationMode;
        /// <summary>
        /// Controls whether the modulation factor is looked up from a table in <see cref="CoverageModulationTable"/>.
        /// </summary>
        public bool CoverageModulationTableEnable;
        /// <summary>
        /// The number of elements in <see cref="CoverageModulationTable"/>.
        /// </summary>
        public int CoverageModulationTableCount;
        /// <summary>
        /// A pointer to a table of modulation factors containing a value for each number of covered samples.
        /// </summary>
        public IntPtr CoverageModulationTable;
    }
}
