using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying the coordinates of a sample location.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SampleLocationExt
    {
        /// <summary>
        /// The horizontal coordinate of the sample's location.
        /// </summary>
        public float X;
        /// <summary>
        /// The vertical coordinate of the sample's location.
        /// </summary>
        public float Y;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleLocationExt"/> structure.
        /// </summary>
        /// <param name="x">The horizontal coordinate of the sample's location.</param>
        /// <param name="y">The vertical coordinate of the sample's location.</param>
        public SampleLocationExt(float x, float y)
        {
            X = x;
            Y = y;
        }
    }
}
