using System;

namespace Magma.Vulkan.EXT
{
    [Flags]
    public enum PipelineCoverageModulationStateCreateFlagsNV
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0
    }
}
