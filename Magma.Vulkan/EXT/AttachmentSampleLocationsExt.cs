using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying the sample locations state to use in the initial layout transition of attachments.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct AttachmentSampleLocationsExt
    {
        /// <summary>
        /// The index of the attachment for which the sample locations state is provided.
        /// </summary>
        public int AttachmentIndex;
        /// <summary>
        /// The sample locations state to use for the layout transition of the given attachment from
        /// the initial layout of the attachment to the image layout specified for the attachment in
        /// the first subpass using it.
        /// </summary>
        public SampleLocationsInfoExt SampleLocationsInfo;
    }
}
