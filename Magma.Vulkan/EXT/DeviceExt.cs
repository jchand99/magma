using System;
using System.Runtime.InteropServices;
using System.Text;
using Magma.Vulkan.KHR;

namespace Magma.Vulkan.EXT
{
    public static class DeviceExt
    {
        #region Methods
        public static unsafe VkResult DebugMarkerSetObjectNameInfoExt(this VkDevice device, DebugMarkerObjectNameInfoExt nameInfo)
        {
            fixed (byte* ptr = Encoding.UTF8.GetBytes(nameInfo.ObjectName))
            {
                nameInfo.ToNative(out var native, ptr);
                return device.vkDebugMarkerSetObjectNameEXT(device, &native);
            }
        }

        public static unsafe VkResult DebugMarkerSetObjectTagInfoExt(this VkDevice device, DebugMarkerObjectTagInfoExt tagInfo)
        {
            fixed (byte* ptr = tagInfo.Tag)
            {
                tagInfo.ToNative(out var native, ptr);
                return device.vkDebugMarkerSetObjectTagEXT(device, &native);
            }
        }

        public static unsafe VkResult DisplayPowerControlExt(this VkDevice device, DisplayKhr display, DisplayPowerInfoExt displayPowerInfo)
        {
            displayPowerInfo.Prepare();
            return device.vkDisplayPowerControlEXT(device, display, &displayPowerInfo);
        }

        public static unsafe VkResult RegisterDisplayEventExt(this VkDevice device, DisplayKhr display, DisplayEventInfoExt displayEventInfo, AllocationCallbacks? allocationCallbacks, out IntPtr fence)
        {
            fixed (IntPtr* ptr = &fence)
            {
                displayEventInfo.Prepare();
                if (allocationCallbacks.HasValue) return device.vkRegisterDisplayEventEXT(device, display, &displayEventInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return device.vkRegisterDisplayEventEXT(device, display, &displayEventInfo, null, ptr);
            }
        }

        public static unsafe VkResult RegisterDeviceEventExt(this VkDevice device, DeviceEventInfoExt deviceEventInfo, AllocationCallbacks? allocationCallbacks, out IntPtr fence)
        {
            fixed (IntPtr* ptr = &fence)
            {
                deviceEventInfo.Prepare();
                if (allocationCallbacks.HasValue) return device.vkRegisterDeviceEventEXT(device, &deviceEventInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return device.vkRegisterDeviceEventEXT(device, &deviceEventInfo, null, ptr);
            }
        }

        public static unsafe void SetHdrMetadataExt(this VkDevice device, VkSwapchainKhr[] swapchains, HdrMetadataExt[] metadata)
        {
            fixed (HdrMetadataExt* hptr = metadata)
            {
                var sptr = stackalloc IntPtr[swapchains?.Length ?? 0];
                for (int i = 0; i < swapchains.Length; i++)
                    sptr[i] = swapchains[i];

                device.vkSetHdrMetadataEXT(device, (uint)(swapchains?.Length ?? 0), sptr, hptr);
            }
        }

        public static unsafe void GetMemoryHostPointerPropertiesExt(this VkDevice device, ExternalMemoryHandleTypesKhr handleType, IntPtr hostPointer, MemoryHostPointerPropertiesExt memoryHostPointerProperties)
        {
            device.vkGetMemoryHostPointerPropertiesEXT(device, handleType, hostPointer, &memoryHostPointerProperties);
        }
        #endregion

    }

    // Structs

    /// <summary>
    /// Specify parameters of a name to give to an object.
    /// </summary>
    public unsafe struct DebugMarkerObjectNameInfoExt
    {
        /// <summary>
        /// Specifies specifying the type of the object to be named.
        /// </summary>
        public DebugReportObjectTypeExt ObjectType;
        /// <summary>
        /// The object to be named.
        /// </summary>
        public ulong Object;
        /// <summary>
        /// A unicode string specifying the name to apply to object.
        /// </summary>
        public string ObjectName;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugMarkerObjectNameInfoExt"/> structure.
        /// </summary>
        /// <param name="obj">Vulkan object to be name.</param>
        /// <param name="name">Name to set.</param>
        public DebugMarkerObjectNameInfoExt(IntPtr obj, string name)
        {
            ObjectType = GetTypeForObject(obj);
            Object = (ulong)obj.ToInt64();
            ObjectName = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugMarkerObjectNameInfoExt"/> structure.
        /// </summary>
        /// <param name="obj">Vulkan object to name.</param>
        /// <param name="name">Name to set.</param>
        public DebugMarkerObjectNameInfoExt(long obj, string name)
        {
            ObjectType = GetTypeForObject(obj);
            Object = (ulong)obj;
            ObjectName = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugMarkerObjectNameInfoExt"/> structure.
        /// </summary>
        /// <param name="obj">Vulkan object to name.</param>
        /// <param name="name">Name to set.</param>
        public DebugMarkerObjectNameInfoExt(ulong obj, string name)
        {
            ObjectType = GetTypeForObject(obj);
            Object = obj;
            ObjectName = name;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DebugReportObjectTypeExt ObjectType;
            public ulong Object;
            public byte* ObjectName;
        }

        internal void ToNative(out Native native, byte* objectName)
        {
            native.Type = StructureType.DebugMarkerObjectNameInfoExt;
            native.Next = IntPtr.Zero;
            native.ObjectType = ObjectType;
            native.Object = Object;
            native.ObjectName = objectName;
        }

        internal static DebugReportObjectTypeExt GetTypeForObject<T>(T obj)
        {
            string name = typeof(T).Name;
            bool success = Enum.TryParse<DebugReportObjectTypeExt>(name, out var type);
            return success ? type : DebugReportObjectTypeExt.Unknown;
        }
    }

    /// <summary>
    /// Specify the type of an object handle.
    /// </summary>
    public enum DebugReportObjectTypeExt
    {
        /// <summary>
        /// Specifies an unknown object.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Specifies an <see cref="Magma.Vulkan.VkInstance"/>.
        /// </summary>
        Instance = 1,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkPhysicalDevice"/>.
        /// </summary>
        PhysicalDevice = 2,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkDevice"/>.
        /// </summary>
        Device = 3,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkQueue"/>.
        /// </summary>
        Queue = 4,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkSemaphore"/>.
        /// </summary>
        Semaphore = 5,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkCommandBuffer"/>.
        /// </summary>
        CommandBuffer = 6,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkFence"/>.
        /// </summary>
        Fence = 7,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkDeviceMemory"/>.
        /// </summary>
        DeviceMemory = 8,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkBuffer"/>.
        /// </summary>
        Buffer = 9,
        /// <summary>
        /// Specifies an <see cref="Magma.Vulkan.VkImage"/>.
        /// </summary>
        Image = 10,
        /// <summary>
        /// Specifies an <see cref="Magma.Vulkan.VkEvent"/>.
        /// </summary>
        Event = 11,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkQueryPool"/>.
        /// </summary>
        QueryPool = 12,
        /// <summary>
        /// Specifies an <see cref="Magma.Vulkan.VkBufferView"/>.
        /// </summary>
        BufferView = 13,
        /// <summary>
        /// Specifies an <see cref="Magma.Vulkan.VkImageView"/>.
        /// </summary>
        ImageView = 14,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkShaderModule"/>.
        /// </summary>
        ShaderModule = 15,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkPipelineCache"/>.
        /// </summary>
        PipelineCache = 16,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkPipelineLayout"/>.
        /// </summary>
        PipelineLayout = 17,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkRenderPass"/>.
        /// </summary>
        RenderPass = 18,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkPipeline"/>.
        /// </summary>
        Pipeline = 19,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkDescriptorSetLayout"/>.
        /// </summary>
        DescriptorSetLayout = 20,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkSampler"/>.
        /// </summary>
        Sampler = 21,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkDescriptorPool"/>.
        /// </summary>
        DescriptorPool = 22,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkDescriptorSet"/>.
        /// </summary>
        DescriptorSet = 23,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkFramebuffer"/>.
        /// </summary>
        Framebuffer = 24,
        /// <summary>
        /// Specifies a <see cref="Magma.Vulkan.VkCommandPool"/>.
        /// </summary>
        CommandPool = 25,
        /// <summary>
        /// Specifies a <see cref="Khr.SurfaceKhr"/>.
        /// </summary>
        SurfaceKhr = 26,
        /// <summary>
        /// Specifies a <see cref="Khr.SwapchainKhr"/>.
        /// </summary>
        SwapchainKhr = 27,
        /// <summary>
        /// Specifies a <see cref="VkDebugReportCallbackExt"/>.
        /// </summary>
        DebugReportCallback = 28,
        /// <summary>
        /// Specifies a <see cref="Khr.DisplayKhr"/>.
        /// </summary>
        DisplayKhr = 29,
        /// <summary>
        /// Specifies a <see cref="Khr.DisplayModeKhr"/>.
        /// </summary>
        DisplayModeKhr = 30,
        /// <summary>
        /// Specifies a <see cref="Nvx.ObjectTableNvx"/>.
        /// </summary>
        ObjectTableNvx = 31,
        /// <summary>
        /// Specifies a <see cref="Nvx.IndirectCommandsLayoutNvx"/>.
        /// </summary>
        IndirectCommandsLayoutNvx = 32,
        /// <summary>
        /// Specifies a <see cref="ValidationCacheExt"/>.
        /// </summary>
        ValidationCache = 33,
        /// <summary>
        /// Specifies a <see cref="Khr.DescriptorUpdateTemplateKhr"/>.
        /// </summary>
        DescriptorUpdateTemplateKhrExt = 1000085000,
        SamplerYcbcrConversionKhrExt = 1000156000
    }

    /// <summary>
    /// Specify parameters of a tag to attach to an object.
    /// </summary>
    public unsafe struct DebugMarkerObjectTagInfoExt
    {
        /// <summary>
        /// Specifies the type of the object to be named.
        /// </summary>
        public DebugReportObjectTypeExt ObjectType;
        /// <summary>
        /// The object to be tagged.
        /// </summary>
        public ulong Object;
        /// <summary>
        /// A numerical identifier of the tag.
        /// </summary>
        public long TagName;
        /// <summary>
        /// Bytes containing the data to be associated with the object.
        /// </summary>
        public byte[] Tag;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugMarkerObjectTagInfoExt"/> structure.
        /// </summary>
        /// <param name="obj">Vulkan object to be tagged.</param>
        /// <param name="tagName">A numerical identifier of the tag.</param>
        /// <param name="tag">Bytes containing the data to be associated with the object.</param>
        public DebugMarkerObjectTagInfoExt(IntPtr obj, long tagName, byte[] tag)
        {
            ObjectType = GetTypeForObject(obj);
            Object = (ulong)obj.ToInt64();
            TagName = tagName;
            Tag = tag;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugMarkerObjectTagInfoExt"/> structure.
        /// </summary>
        /// <param name="obj">Vulkan object to be tagged.</param>
        /// <param name="tagName">A numerical identifier of the tag.</param>
        /// <param name="tag">Bytes containing the data to be associated with the object.</param>
        public DebugMarkerObjectTagInfoExt(long obj, long tagName, byte[] tag)
        {
            ObjectType = GetTypeForObject(obj);
            Object = (ulong)obj;
            TagName = tagName;
            Tag = tag;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DebugReportObjectTypeExt ObjectType;
            public ulong Object;
            public long TagName;
            public SizeT TagSize;
            public byte* Tag;
        }

        internal void ToNative(out Native native, byte* tag)
        {
            native.Type = StructureType.DebugMarkerObjectTagInfoExt;
            native.Next = IntPtr.Zero;
            native.ObjectType = ObjectType;
            native.Object = Object;
            native.TagName = TagName;
            native.TagSize = Tag?.Length ?? 0;
            native.Tag = tag;
        }

        internal static DebugReportObjectTypeExt GetTypeForObject<T>(T obj)
        {
            string name = typeof(T).Name;
            bool success = Enum.TryParse<DebugReportObjectTypeExt>(name, out var type);
            return success ? type : DebugReportObjectTypeExt.Unknown;
        }
    }

    /// <summary>
    /// Describe the power state of a display.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DisplayPowerInfoExt
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// The new power state of the display.
        /// </summary>
        public DisplayPowerStateExt PowerState;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayPowerInfoExt"/> structure.
        /// </summary>
        /// <param name="powerState">The new power state of the display.</param>
        public DisplayPowerInfoExt(DisplayPowerStateExt powerState)
        {
            Type = StructureType.DisplayPowerInfoExt;
            Next = IntPtr.Zero;
            PowerState = powerState;
        }

        internal void Prepare()
        {
            Type = StructureType.DisplayPowerInfoExt;
        }
    }

    /// <summary>
    /// Possible power states for a display.
    /// </summary>
    public enum DisplayPowerStateExt
    {
        /// <summary>
        /// Specifies that the display is powered down.
        /// </summary>
        Off = 0,
        /// <summary>
        /// Specifies that the display is in a low power mode, but may be able to transition back to
        /// <see cref="On"/> more quickly than if it were in <see cref="Off"/>.
        /// <para>This state may be the same as <see cref="Off"/>.</para>
        /// </summary>
        Suspend = 1,
        /// <summary>
        /// Specifies that the display is powered on.
        /// </summary>
        On = 2
    }

    /// <summary>
    /// Describe a display event to create.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DisplayEventInfoExt
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// Specifies when the fence will be signaled.
        /// </summary>
        public DisplayEventTypeExt DisplayEvent;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayEventInfoExt"/> structure.
        /// </summary>
        /// <param name="displayEvent">Specifies when the fence will be signaled.</param>
        public DisplayEventInfoExt(DisplayEventTypeExt displayEvent)
        {
            Type = StructureType.DisplayEventInfoExt;
            Next = IntPtr.Zero;
            DisplayEvent = displayEvent;
        }

        internal void Prepare()
        {
            Type = StructureType.DisplayEventInfoExt;
        }
    }

    /// <summary>
    /// Events that can occur on a display object.
    /// </summary>
    public enum DisplayEventTypeExt
    {
        /// <summary>
        /// Specifies that the fence is signaled when the first pixel of the next display refresh
        /// cycle leaves the display engine for the display.
        /// </summary>
        FirstPixelOut = 0
    }

    /// <summary>
    /// Describe a device event to create.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DeviceEventInfoExt
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// Specifies when the fence will be signaled.
        /// </summary>
        public DeviceEventTypeExt DeviceEvent;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceEventInfoExt"/> structure.
        /// </summary>
        /// <param name="deviceEvent">Specifies when the fence will be signaled.</param>
        public DeviceEventInfoExt(DeviceEventTypeExt deviceEvent)
        {
            Type = StructureType.DeviceEventInfoExt;
            Next = IntPtr.Zero;
            DeviceEvent = deviceEvent;
        }

        internal void Prepare()
        {
            Type = StructureType.DeviceEventInfoExt;
        }
    }

    /// <summary>
    /// Events that can occur on a device object.
    /// </summary>
    public enum DeviceEventTypeExt
    {
        /// <summary>
        /// Specifies that the fence is signaled when a display is plugged into or unplugged from the
        /// specified device.
        /// <para>
        /// Applications can use this notification to determine when they need to re-enumerate the
        /// available displays on a device.
        /// </para>
        /// </summary>
        DisplayHotplug = 0
    }

    /// <summary>
    /// Structure to specify HDR metadata.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct HdrMetadataExt
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The mastering display's red primary in chromaticity coordinates.
        /// </summary>
        public XYColorExt DisplayPrimaryRed;
        /// <summary>
        /// The mastering display's green primary in chromaticity coordinates.
        /// </summary>
        public XYColorExt DisplayPrimaryGreen;
        /// <summary>
        /// The mastering display's blue primary in chromaticity coordinates.
        /// </summary>
        public XYColorExt DisplayPrimaryBlue;
        /// <summary>
        /// The mastering display's white-point in chromaticity coordinates.
        /// </summary>
        public XYColorExt WhitePoint;
        /// <summary>
        /// The maximum luminance of the mastering display in nits.
        /// </summary>
        public float MaxLuminance;
        /// <summary>
        /// The minimum luminance of the mastering display in nits.
        /// </summary>
        public float MinLuminance;
        /// <summary>
        /// Content's maximum luminance in nits.
        /// </summary>
        public float MaxContentLightLevel;
        /// <summary>
        /// The maximum frame average light level in nits.
        /// </summary>
        public float MaxFrameAverageLightLevel;

        internal void Prepare()
        {
            Type = StructureType.HdrMetadataExt;
        }
    }

    /// <summary>
    /// Structure to specify X,Y chromaticity coordinates.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct XYColorExt
    {
        /// <summary>
        /// The X coordinate of chromaticity limited to between 0 and 1.
        /// </summary>
        public float X;
        /// <summary>
        /// The Y coordinate of chromaticity limited to between 0 and 1.
        /// </summary>
        public float Y;
    }
}
