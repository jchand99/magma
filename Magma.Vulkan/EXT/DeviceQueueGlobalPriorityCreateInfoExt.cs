using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Specify a system wide priority.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DeviceQueueGlobalPriorityCreateInfoExt
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The system-wide priority associated to this queue as specified by <see
        /// cref="QueueGlobalPriorityExt"/>.
        /// </summary>
        public QueueGlobalPriorityExt GlobalPriority;
    }
}
