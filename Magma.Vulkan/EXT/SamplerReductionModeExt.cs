namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Specify reduction mode for texture filtering.
    /// </summary>
    public enum SamplerReductionModeExt
    {
        /// <summary>
        /// Indicates that texel values are combined by computing a weighted average of values in the
        /// footprint, using weights.
        /// </summary>
        WeightedAverage = 0,
        /// <summary>
        /// Indicates that texel values are combined by taking the component-wise minimum of values
        /// in the footprint with non-zero weights.
        /// </summary>
        Min = 1,
        /// <summary>
        /// Indicates that texel values are combined by taking the component-wise maximum of values
        /// in the footprint with non-zero weights.
        /// </summary>
        Max = 2
    }
}
