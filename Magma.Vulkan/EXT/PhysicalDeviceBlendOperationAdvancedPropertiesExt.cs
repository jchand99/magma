using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure describing advanced blending limits that can be supported by an implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceBlendOperationAdvancedPropertiesExt
    {
        internal StructureType Type;

        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        public int AdvancedBlendMaxColorAttachments;
        public bool AdvancedBlendIndependentBlend;
        public bool AdvancedBlendNonPremultipliedSrcColor;
        public bool AdvancedBlendNonPremultipliedDstColor;
        public bool AdvancedBlendCorrelatedOverlap;
        public bool AdvancedBlendAllOperations;
    }
}
