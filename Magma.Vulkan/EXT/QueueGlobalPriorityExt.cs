namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Values specifying a system-wide queue priority.
    /// </summary>
    public enum QueueGlobalPriorityExt
    {
        /// <summary>
        /// Below the system default. Useful for non-interactive tasks.
        /// </summary>
        Low = 128,
        /// <summary>
        /// The system default priority.
        /// </summary>
        Medium = 256,
        /// <summary>
        /// Above the system default.
        /// </summary>
        High = 512,
        /// <summary>
        /// The highest priority. Useful for critical tasks.
        /// </summary>
        Realtime = 1024
    }
}
