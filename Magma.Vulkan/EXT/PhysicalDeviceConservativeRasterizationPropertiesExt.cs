using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure describing conservative raster properties that can be supported by an
    /// implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceConservativeRasterizationPropertiesExt
    {
        public StructureType Type;
        public IntPtr Next;
        public float PrimitiveOverestimationSize;
        public float MaxExtraPrimitiveOverestimationSize;
        public float ExtraPrimitiveOverestimationSizeGranularity;
        public bool PrimitiveUnderestimation;
        public bool ConservativePointAndLineRasterization;
        public bool DegenerateTrianglesRasterized;
        public bool DegenerateLinesRasterized;
        public bool FullyCoveredFragmentShaderInputVariable;
        public bool ConservativeRasterizationPostDepthCoverage;
    }
}
