using System;

namespace Magma.Vulkan.EXT
{
    /// <summary>
    /// Structure specifying arguments for a debug report callback function.
    /// </summary>

    public struct DebugReportCallbackInfo
    {
        /// <summary>
        /// The <see cref="DebugReportFlagsExt"/> that triggered this callback.
        /// </summary>
        public DebugReportFlagsExt Flags;
        /// <summary>
        /// The <see cref="DebugReportObjectTypeExt"/> specifying the type of object being used or
        /// created at the time the event was triggered.
        /// </summary>
        public DebugReportObjectTypeExt ObjectType;
        /// <summary>
        /// The object where the issue was detected. <see cref="Object"/> may be 0 if there is no
        /// object associated with the event.
        /// </summary>
        public IntPtr Object;
        /// <summary>
        /// The component (layer, driver, loader) defined value that indicates the location of the
        /// trigger. This is an optional value.
        /// </summary>
        public IntPtr Location;
        /// <summary>
        /// The layer-defined value indicating what test triggered this callback.
        /// </summary>
        public int MessageCode;
        /// <summary>
        /// The abbreviation of the component making the callback.
        /// </summary>
        public string LayerPrefix;
        /// <summary>
        /// The string detailing the trigger conditions.
        /// </summary>
        public string Message;
        /// <summary>
        /// The user data given when the <see cref="DebugReportCallbackCreateInfoExt"/> was created.
        /// </summary>
        public IntPtr UserData;
    }
}
