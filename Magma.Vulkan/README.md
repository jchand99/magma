## Magma.Vulkan

This library tries to stay as close to the unmanaged function declarations as possible with a few variations and overrides to make the code managed.
You should be able to follow any Vulkan tutorial without issue.

### Example Code:

```c#
using System;
using Magma.Vulkan;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            ApplicationInfo appInfo = new ApplicationInfo
            {
                ApiVersion = new VkVersion(1, 2, 176),
                ApplicationName = "MyApp",
                ApplicationVersion = new VkVersion(1, 0, 0),
                EngineName = "MyEngine",
                EngineVersion = new VkVersion(1, 0, 0),
            };

            InstanceCreateInfo info = new InstanceCreateInfo
            {
                ApplicationInfo = appInfo,
            };

            VkInstance instance = new VkInstance(info);

            instance.EnumeratePhysicalDevices(out VkPhysicalDevice[] physicalDevices);

            foreach (var device in physicalDevices)
            {
                device.GetProperties(out PhysicalDeviceProperties properties);
                Console.WriteLine(properties.DeviceName);
            }

            instance.Dispose();
        }
    }
}
```
