using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Offset2D
    {
        public readonly int X;
        public readonly int Y;

        public Offset2D(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Offset3D
    {
        public readonly int X;
        public readonly int Y;
        public readonly int Z;

        public Offset3D(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
