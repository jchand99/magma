using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkCommandPool : DeviceBoundObject
    {
        public VkCommandPool(VkDevice parent, CommandPoolCreateInfo commandPoolCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateCommandPool(commandPoolCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create CommandPool: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateCommandPool(CommandPoolCreateInfo commandPoolCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr commandPool)
        {
            commandPoolCreateInfo.Prepare();
            fixed (IntPtr* ptr = &commandPool)
            {
                if (allocationCallbacks.HasValue) return vkCreateCommandPool(LogicalDevice._Handle, &commandPoolCreateInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateCommandPool(LogicalDevice._Handle, &commandPoolCreateInfo, null, ptr);
            }
        }

        public unsafe void ResetCommandPool(CommandPoolResetFlags flags) => vkResetCommandPool(LogicalDevice._Handle, _Handle, flags);

        public unsafe VkResult AllocateBuffers(CommandBufferAllocateInfo commandBufferAllocateInfo, out VkCommandBuffer[] commandBuffers)
        {
            return VkCommandBuffer.AllocateBuffers(this, commandBufferAllocateInfo, out commandBuffers);
        }

        public unsafe void FreeBuffers(params VkCommandBuffer[] commandBuffers)
        {
            VkCommandBuffer.FreeBuffers(this, commandBuffers);
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyCommandPool(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyCommandPool(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, CommandPoolCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateCommandPool = (delegate* unmanaged[Cdecl]<IntPtr, CommandPoolCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateCommandPool");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyCommandPool = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyCommandPool");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, CommandPoolResetFlags, VkResult> vkResetCommandPool = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, CommandPoolResetFlags, VkResult>)Vulkan.GetStaticProcPointer("vkResetCommandPool");
        #endregion

    }

    // Structs

    /// <summary>
    /// Bitmask controlling behavior of a command pool reset.
    /// </summary>
    [Flags]
    public enum CommandPoolResetFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that resetting a command pool recycles all of the resources from the command
        /// pool back to the system.
        /// </summary>
        ReleaseResources = 1 << 0
    }

    /// <summary>
    /// Structure specifying parameters of a newly created command pool.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CommandPoolCreateInfo
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// A bitmask indicating usage behavior for the pool and command buffers allocated from it.
        /// </summary>
        public CommandPoolCreateFlags Flags;
        /// <summary>
        /// Designates a queue family.
        /// <para>
        /// All command buffers allocated from this command pool must be submitted on queues from the
        /// same queue family.
        /// </para>
        /// </summary>
        public uint QueueFamilyIndex;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandPoolCreateInfo"/> structure.
        /// </summary>
        /// <param name="queueFamilyIndex">
        /// Designates a queue family.
        /// <para>
        /// All command buffers allocated from this command pool must be submitted on queues from the
        /// same queue family.
        /// </para>
        /// </param>
        /// <param name="flags">
        /// A bitmask indicating usage behavior for the pool and command buffers allocated from it.
        /// </param>
        public CommandPoolCreateInfo(uint queueFamilyIndex, CommandPoolCreateFlags flags = 0)
        {
            Type = StructureType.CommandPoolCreateInfo;
            Next = IntPtr.Zero;
            Flags = flags;
            QueueFamilyIndex = queueFamilyIndex;
        }

        internal void Prepare()
        {
            Type = StructureType.CommandPoolCreateInfo;
        }
    }

    /// <summary>
    /// Bitmask specifying usage behavior for a command pool.
    /// </summary>
    [Flags]
    public enum CommandPoolCreateFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Indicates that command buffers allocated from the pool will be short-lived,
        /// meaning that they will be reset or freed in a relatively short timeframe.
        /// <para>This
        /// flag may be used by the implementation to control memory allocation behavior
        /// within the pool.</para>
        /// </summary>
        Transient = 1 << 0,
        /// <summary>
        /// Allows any command buffer allocated from a pool to be individually reset to the initial
        /// state either by calling <see cref="VkCommandBuffer.Reset"/>, or via the implicit reset when
        /// calling <see cref="VkCommandBuffer.Begin"/>.
        /// <para>
        /// If this flag is not set on a pool, then <see cref="VkCommandBuffer.Reset"/> must not be
        /// called for any command buffer allocated from that pool.
        /// </para>
        /// </summary>
        ResetCommandBuffer = 1 << 1
    }


}
