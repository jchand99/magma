using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkImageView : DeviceBoundObject
    {
        public VkImageView(VkDevice parent, ImageViewCreateInfo imageViewCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateImageView(imageViewCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create ImageView: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateImageView(ImageViewCreateInfo imageViewCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr imageView)
        {
            fixed (IntPtr* ptr = &imageView)
            {
                imageViewCreateInfo.ToNative(out var native);
                if (allocationCallbacks.HasValue) return vkCreateImageView(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateImageView(LogicalDevice._Handle, &native, null, ptr);
            }
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyImageView(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyImageView(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, ImageViewCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateImageView = (delegate* unmanaged[Cdecl]<IntPtr, ImageViewCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateImageView");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyImageView = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyImageView");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created image view.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageViewCreateInfo
    {
        internal StructureType Type;
        internal IntPtr Next;
        internal ImageViewCreateFlags Flags;

        public VkImage Image;
        /// <summary>
        /// Specifies the type of the image view.
        /// </summary>
        public ImageViewType ViewType;
        /// <summary>
        /// A <see cref="Format"/> describing the format and type used to interpret data elements in
        /// the image.
        /// </summary>
        public Format Format;
        /// <summary>
        /// Specifies a remapping of color components (or of depth or stencil components after they
        /// have been converted into color components).
        /// </summary>
        public ComponentMapping Components;
        /// <summary>
        /// A range selecting the set of mipmap levels and array layers to be accessible to the view.
        /// <para>
        /// Must be a valid image subresource range for image. If image was created with the <see
        /// cref="ImageCreateFlags.MutableFormat"/> flag, format must be compatible with the format
        /// used to create image. If image was not created with the <see
        /// cref="ImageCreateFlags.MutableFormat"/> flag, format must be identical to the format used
        /// to create image. If image is non-sparse then it must be bound completely and contiguously
        /// to a single <see cref="VkDeviceMemory"/> object.
        /// </para>
        /// </summary>
        public ImageSubresourceRange SubresourceRange;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageViewCreateInfo"/> structure.
        /// </summary>
        /// <param name="format">
        /// A <see cref="Format"/> describing the format and type used to interpret data elements in
        /// the image.
        /// </param>
        /// <param name="subresourceRange">
        /// A range selecting the set of mipmap levels and array layers to be accessible to the view.
        /// </param>
        /// <param name="viewType">Specifies the type of the image view.</param>
        /// <param name="components">
        /// Specifies a remapping of color components (or of depth or stencil components after they
        /// have been converted into color components).
        /// </param>
        public ImageViewCreateInfo(
            Format format,
            ImageSubresourceRange subresourceRange, VkImage image,
            ImageViewType viewType = ImageViewType.Image2D,
            ComponentMapping components = default(ComponentMapping))
        {
            Type = StructureType.ImageViewCreateInfo;
            Next = IntPtr.Zero;
            Flags = 0;
            Image = image;
            Format = format;
            SubresourceRange = subresourceRange;
            ViewType = viewType;
            Components = components;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            internal StructureType Type;
            internal IntPtr Next;
            internal ImageViewCreateFlags Flags;
            internal IntPtr Image;
            internal ImageViewType ViewType;
            internal Format Format;
            internal ComponentMapping Components;
            internal ImageSubresourceRange SubresourceRange;
        }

        internal void ToNative(out Native native)
        {
            native.Type = StructureType.ImageViewCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = 0;
            native.Image = Image._Handle;
            native.ViewType = ViewType;
            native.Format = Format;
            native.Components = Components;
            native.SubresourceRange = SubresourceRange;
        }
    }

    // Reserved for future use.
    [Flags]
    internal enum ImageViewCreateFlags
    {
        None = 0,
    }

    /// <summary>
    /// Structure specifying a color component mapping.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ComponentMapping
    {
        /// <summary>
        /// Specifies the component value placed in the R component of the output vector.
        /// </summary>
        public ComponentSwizzle R;
        /// <summary>
        /// Specifies the component value placed in the G component of the output vector.
        /// </summary>
        public ComponentSwizzle G;
        /// <summary>
        /// Specifies the component value placed in the B component of the output vector.
        /// </summary>
        public ComponentSwizzle B;
        /// <summary>
        /// Specifies the component value placed in the A component of the output vector.
        /// </summary>
        public ComponentSwizzle A;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentMapping"/> structure.
        /// </summary>
        /// <param name="r">
        /// Specifies the component value placed in the R component of the output vector.
        /// </param>
        /// <param name="g">
        /// Specifies the component value placed in the G component of the output vector.
        /// </param>
        /// <param name="b">
        /// Specifies the component value placed in the B component of the output vector.
        /// </param>
        /// <param name="a">
        /// Specifies the component value placed in the A component of the output vector.
        /// </param>
        public ComponentMapping(ComponentSwizzle r, ComponentSwizzle g, ComponentSwizzle b, ComponentSwizzle a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }
    }

    /// <summary>
    /// Image view types.
    /// </summary>
    public enum ImageViewType
    {
        Image1D = 0,
        Image2D = 1,
        Image3D = 2,
        ImageCube = 3,
        Image1DArray = 4,
        Image2DArray = 5,
        ImageCubeArray = 6
    }

    /// <summary>
    /// Specify how a component is swizzled.
    /// </summary>
    public enum ComponentSwizzle
    {
        /// <summary>
        /// Specifies that the component is set to the identity swizzle.
        /// </summary>
        Identity = 0,
        /// <summary>
        /// Specifies that the component is set to zero.
        /// </summary>
        Zero = 1,
        /// <summary>
        /// Specifies that the component is set to either 1 or 1.0, depending on whether the type of
        /// the image view format is integer or floating-point respectively.
        /// </summary>
        One = 2,
        /// <summary>
        /// Specifies that the component is set to the value of the R component of the image.
        /// </summary>
        R = 3,
        /// <summary>
        /// Specifies that the component is set to the value of the G component of the image.
        /// </summary>
        G = 4,
        /// <summary>
        /// Specifies that the component is set to the value of the B component of the image.
        /// </summary>
        B = 5,
        /// <summary>
        /// Specifies that the component is set to the value of the A component of the image.
        /// </summary>
        A = 6
    }
}
