using System;
using System.Runtime.InteropServices;
using static Magma.Vulkan.Constants;

namespace Magma.Vulkan
{
    public class VkDescriptorSet : IDisposable
    {
        internal IntPtr _Handle;
        public VkDescriptorPool DescriptorPool { get; internal set; }

        public static implicit operator IntPtr(VkDescriptorSet descriptorSet) => descriptorSet == null ? IntPtr.Zero : descriptorSet._Handle;

        private VkDescriptorSet(VkDescriptorPool descriptorPool, IntPtr handle)
        {
            _Handle = handle;
            DescriptorPool = descriptorPool;
        }

        #region Core Methods
        internal unsafe static VkResult AllocateDescriptorSets(VkDescriptorPool parent, DescriptorSetAllocateInfo descriptorSetAllocateInfo, out VkDescriptorSet[] descriptorSets)
        {
            uint count = descriptorSetAllocateInfo.DescriptorSetCount;
            var ptr = stackalloc IntPtr[(int)count];

            int layoutCount = descriptorSetAllocateInfo.SetLayouts?.Length ?? 0;
            var setLayouts = new IntPtr[layoutCount];
            for (int i = 0; i < layoutCount; i++)
                setLayouts[i] = descriptorSetAllocateInfo.SetLayouts[i];

            fixed (IntPtr* setLayoutsPtr = setLayouts)
            {
                descriptorSetAllocateInfo.ToNative(out var native, parent, setLayoutsPtr);
                VkResult result = vkAllocateDescriptorSets(parent.LogicalDevice, &native, ptr);

                descriptorSets = new VkDescriptorSet[count];
                for (int i = 0; i < count; i++)
                    descriptorSets[i] = new VkDescriptorSet(parent, ptr[i]);

                return result;
            }
        }

        internal unsafe static void UpdateDescriptorSets(VkDescriptorPool parent, WriteDescriptorSet[] descriptorWrites, CopyDescriptorSet[] descriptorCopies)
        {
            int descriptorWriteCount = descriptorWrites?.Length ?? 0;
            var nativeDescriptorWrites = stackalloc WriteDescriptorSet.Native[descriptorWriteCount];
            for (int i = 0; i < descriptorWriteCount; i++)
                descriptorWrites?[i].ToNative(&nativeDescriptorWrites[i]);

            int descriptorCopiesCount = descriptorCopies?.Length ?? 0;
            for (int i = 0; i < descriptorCopiesCount; i++)
                descriptorCopies?[i].Prepare();

            fixed (CopyDescriptorSet* ptr = descriptorCopies) vkUpdateDescriptorSets(parent.LogicalDevice, (uint)descriptorWriteCount, nativeDescriptorWrites, (uint)descriptorCopiesCount, ptr);
        }

        internal unsafe static VkResult FreeDescriptorSets(VkDescriptorPool parent, VkDevice logicalDevice, VkDescriptorSet[] descriptorSets)
        {
            int count = descriptorSets?.Length ?? 0;
            var ptr = stackalloc IntPtr[count];
            for (int i = 0; i < count; i++)
                ptr[i] = descriptorSets[i];

            return vkFreeDescriptorSets(logicalDevice._Handle, parent._Handle, (uint)(count), ptr);
        }

        internal unsafe VkResult Free()
        {
            fixed (IntPtr* ptr = &_Handle)
                return vkFreeDescriptorSets(DescriptorPool.LogicalDevice._Handle, DescriptorPool._Handle, 1, ptr);
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, DescriptorSetAllocateInfo.Native*, IntPtr*, VkResult> vkAllocateDescriptorSets = (delegate* unmanaged[Cdecl]<IntPtr, DescriptorSetAllocateInfo.Native*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkAllocateDescriptorSets");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, VkResult> vkFreeDescriptorSets = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkFreeDescriptorSets");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, WriteDescriptorSet.Native*, uint, CopyDescriptorSet*, void> vkUpdateDescriptorSets = (delegate* unmanaged[Cdecl]<IntPtr, uint, WriteDescriptorSet.Native*, uint, CopyDescriptorSet*, void>)Vulkan.GetStaticProcPointer("vkUpdateDescriptorSets");
        #endregion

        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                Free();
                _Handle = IntPtr.Zero;

                disposedValue = true;
            }
        }

        // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~VkDescriptorSet()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    // Structs

    /// <summary>
    /// Structure specifying a copy descriptor set operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CopyDescriptorSet
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// Source <see cref="VkDescriptorSet"/>.
        /// </summary>
        public IntPtr SrcSet;
        /// <summary>
        /// Source binding.
        /// <para>
        /// Must be a valid binding within <see cref="SrcSet"/>. The sum of <see
        /// cref="SrcArrayElement"/> and <see cref="DescriptorCount"/> must be less than or equal to
        /// the number of array elements in the descriptor set binding specified by <see
        /// cref="SrcBinding"/>, and all applicable consecutive bindings.
        /// </para>
        /// </summary>
        public int SrcBinding;
        /// <summary>
        /// Array element within the source binding to copy from.
        /// </summary>
        public int SrcArrayElement;
        /// <summary>
        /// Destination <see cref="VkDescriptorSet"/>.
        /// </summary>
        public IntPtr DstSet;
        /// <summary>
        /// Destination binding.
        /// <para>
        /// Must be a valid binding within <see cref="DstSet"/>. The sum of <see
        /// cref="DstArrayElement"/> and <see cref="DescriptorCount"/> must be less than or equal to
        /// the number of array elements in the descriptor set binding specified by <see
        /// cref="DstBinding"/>, and all applicable consecutive bindings. If <see cref="SrcSet"/> is
        /// equal to <see cref="DstSet"/>, then the source and destination ranges of descriptors must
        /// not overlap, where the ranges may include array elements from consecutive bindings.
        /// </para>
        /// </summary>
        public int DstBinding;
        /// <summary>
        /// Array element within the destination binding to copy to.
        /// </summary>
        public int DstArrayElement;
        /// <summary>
        /// The number of descriptors to copy from the source to destination.
        /// <para>
        /// If <see cref="DescriptorCount"/> is greater than the number of remaining array elements
        /// in the source or destination binding, those affect consecutive bindings in a manner
        /// similar to <see cref="WriteDescriptorSet"/>.
        /// </para>
        /// </summary>
        public int DescriptorCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="CopyDescriptorSet"/> structure.
        /// </summary>
        /// <param name="srcSet">Source descriptor set.</param>
        /// <param name="srcBinding">Source binding.</param>
        /// <param name="srcArrayElement">Array element within the source binding to copy from.</param>
        /// <param name="dstSet">Destination descriptor set.</param>
        /// <param name="dstBinding">Destination binding.</param>
        /// <param name="dstArrayElement">
        /// Array element within the destination binding to copy to.
        /// </param>
        /// <param name="descriptorCount">
        /// The number of descriptors to copy from the source to destination.
        /// </param>
        public CopyDescriptorSet(
            IntPtr srcSet, int srcBinding, int srcArrayElement,
            IntPtr dstSet, int dstBinding, int dstArrayElement,
            int descriptorCount)
        {
            Type = StructureType.CopyDescriptorSet;
            Next = IntPtr.Zero;
            SrcSet = srcSet;
            SrcBinding = srcBinding;
            SrcArrayElement = srcArrayElement;
            DstSet = dstSet;
            DstBinding = dstBinding;
            DstArrayElement = dstArrayElement;
            DescriptorCount = descriptorCount;
        }

        internal void Prepare()
        {
            Type = StructureType.CopyDescriptorSet;
        }
    }

    /// <summary>
    /// Structure specifying the parameters of a descriptor set write operation.
    /// </summary>
    public unsafe struct WriteDescriptorSet
    {
        /// <summary>
        /// The destination descriptor set to update.
        /// </summary>
        public IntPtr DstSet;
        /// <summary>
        /// The descriptor binding within that set.
        /// </summary>
        public int DstBinding;
        /// <summary>
        /// The starting element in that array.
        /// </summary>
        public int DstArrayElement;
        /// <summary>
        /// The number of descriptors to update (the number of elements in <see cref="ImageInfo"/>,
        /// <see cref="BufferInfo"/>, or <see cref="TexelBufferView"/>).
        /// </summary>
        public int DescriptorCount;
        /// <summary>
        /// Specifies the type of each descriptor in <see cref="ImageInfo"/>, <see
        /// cref="BufferInfo"/>, or <see cref="TexelBufferView"/>, as described below. It must be the
        /// same type as that specified in <see cref="DescriptorSetLayoutBinding"/> for <see
        /// cref="DstSet"/> at <see cref="DstBinding"/>. The type of the descriptor also controls
        /// which array the descriptors are taken from.
        /// </summary>
        public DescriptorType DescriptorType;
        /// <summary>
        /// An array of <see cref="DescriptorImageInfo"/> structures or is ignored.
        /// </summary>
        public DescriptorImageInfo[] ImageInfo;
        /// <summary>
        /// An array of <see cref="DescriptorBufferInfo"/> structures or is ignored.
        /// </summary>
        public DescriptorBufferInfo[] BufferInfo;
        /// <summary>
        /// An array of <see cref="VkBufferView"/> handles or is ignored.
        /// </summary>
        public IntPtr[] TexelBufferView;

        /// <summary>
        /// Initializes a new instance of the <see cref="WriteDescriptorSet"/> structure.
        /// </summary>
        /// <param name="dstSet">The destination descriptor set to update.</param>
        /// <param name="dstBinding">The descriptor binding within that set.</param>
        /// <param name="dstArrayElement">The starting element in that array.</param>
        /// <param name="descriptorCount">
        /// The number of descriptors to update (the number of elements in <see cref="ImageInfo"/>,
        /// <see cref="BufferInfo"/>, or <see cref="TexelBufferView"/>).
        /// </param>
        /// <param name="descriptorType">
        /// Specifies the type of each descriptor in <see cref="ImageInfo"/>, <see
        /// cref="BufferInfo"/>, or <see cref="TexelBufferView"/>, as described below. It must be the
        /// same type as that specified in <see cref="DescriptorSetLayoutBinding"/> for <see
        /// cref="DstSet"/> at <see cref="DstBinding"/>. The type of the descriptor also controls
        /// which array the descriptors are taken from.
        /// </param>
        /// <param name="imageInfo">
        /// An array of <see cref="DescriptorImageInfo"/> structures or is ignored.
        /// </param>
        /// <param name="bufferInfo">
        /// An array of <see cref="DescriptorBufferInfo"/> structures or is ignored.
        /// </param>
        /// <param name="texelBufferView">An array of <see cref="VkBufferView"/> handles or is ignored.</param>
        public WriteDescriptorSet(IntPtr dstSet, int dstBinding, int dstArrayElement, int descriptorCount,
            DescriptorType descriptorType, DescriptorImageInfo[] imageInfo = null, DescriptorBufferInfo[] bufferInfo = null,
            IntPtr[] texelBufferView = null)
        {
            DstSet = dstSet;
            DstBinding = dstBinding;
            DstArrayElement = dstArrayElement;
            DescriptorCount = descriptorCount;
            DescriptorType = descriptorType;
            ImageInfo = imageInfo;
            BufferInfo = bufferInfo;
            TexelBufferView = texelBufferView;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public IntPtr DstSet;
            public int DstBinding;
            public int DstArrayElement;
            public int DescriptorCount;
            public DescriptorType DescriptorType;
            public DescriptorImageInfo* ImageInfo;
            public DescriptorBufferInfo* BufferInfo;
            public IntPtr* TexelBufferView;
        }

        internal void ToNative(Native* native)
        {
            native->Type = StructureType.WriteDescriptorSet;
            native->DstSet = DstSet;
            native->DstBinding = DstBinding;
            native->DescriptorCount = DescriptorCount;
            native->DescriptorType = DescriptorType;
            fixed (DescriptorImageInfo* ptr = ImageInfo)
                native->ImageInfo = ptr;
            fixed (DescriptorBufferInfo* ptr = BufferInfo)
                native->BufferInfo = ptr;
            fixed (IntPtr* ptr = TexelBufferView)
                native->TexelBufferView = ptr;
        }
    }

    /// <summary>
    /// Structure specifying descriptor buffer info.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DescriptorBufferInfo
    {
        /// <summary>
        /// The <see cref="Buffer"/> resource.
        /// </summary>
        public IntPtr Buffer;
        /// <summary>
        /// The offset in bytes from the start of buffer. Access to buffer memory via this descriptor
        /// uses addressing that is relative to this starting offset.
        /// </summary>
        public long Offset;
        /// <summary>
        /// The size in bytes that is used for this descriptor update, or <see cref="WholeSize"/> to
        /// use the range from <see cref="Offset"/> to the end of the buffer.
        /// </summary>
        public long Range;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorBufferInfo"/> structure.
        /// </summary>
        /// <param name="buffer">The buffer resource.</param>
        /// <param name="offset">
        /// The offset in bytes from the start of buffer. Access to buffer memory via this descriptor
        /// uses addressing that is relative to this starting offset.
        /// </param>
        /// <param name="range">
        /// The size in bytes that is used for this descriptor update, or <see cref="WholeSize"/> to
        /// use the range from <see cref="Offset"/> to the end of the buffer.
        /// </param>
        public DescriptorBufferInfo(IntPtr buffer, long offset = 0, long range = WholeSize)
        {
            Buffer = buffer;
            Offset = offset;
            Range = range;
        }
    }

    /// <summary>
    /// Structure specifying descriptor image info.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DescriptorImageInfo
    {
        /// <summary>
        /// A sampler handle, and is used in descriptor updates for types <see
        /// cref="DescriptorType.Sampler"/> and <see cref="DescriptorType.CombinedImageSampler"/> if
        /// the binding being updated does not use immutable samplers.
        /// </summary>
        public IntPtr Sampler;
        /// <summary>
        /// An <see cref="ImageView"/> handle, and is used in descriptor updates for types <see
        /// cref="DescriptorType.SampledImage"/>, <see cref="DescriptorType.StorageImage"/>, <see
        /// cref="DescriptorType.CombinedImageSampler"/>, and <see cref="DescriptorType.InputAttachment"/>.
        /// </summary>
        public IntPtr ImageView;
        /// <summary>
        /// The layout that the image subresources accessible from <see cref="ImageView"/> will be in
        /// at the time this descriptor is accessed. Is used in descriptor updates for types <see
        /// cref="DescriptorType.SampledImage"/>, <see cref="DescriptorType.StorageImage"/>, <see
        /// cref="DescriptorType.CombinedImageSampler"/>, and <see cref="DescriptorType.InputAttachment"/>.
        /// </summary>
        public ImageLayout ImageLayout;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorImageInfo"/> structure.
        /// </summary>
        /// <param name="sampler">
        /// A sampler handle, and is used in descriptor updates for types <see
        /// cref="DescriptorType.Sampler"/> and <see cref="DescriptorType.CombinedImageSampler"/> if
        /// the binding being updated does not use immutable samplers.
        /// </param>
        /// <param name="imageView">
        /// An image view handle, and is used in descriptor updates for types <see
        /// cref="DescriptorType.SampledImage"/>, <see cref="DescriptorType.StorageImage"/>, <see
        /// cref="DescriptorType.CombinedImageSampler"/>, and <see cref="DescriptorType.InputAttachment"/>.
        /// </param>
        /// <param name="imageLayout">
        /// The layout that the image will be in at the time this descriptor is accessed. Is used in
        /// descriptor updates for types <see cref="DescriptorType.SampledImage"/>, <see
        /// cref="DescriptorType.StorageImage"/>, <see cref="DescriptorType.CombinedImageSampler"/>,
        /// and <see cref="DescriptorType.InputAttachment"/>.
        /// </param>
        public DescriptorImageInfo(IntPtr sampler, IntPtr imageView, ImageLayout imageLayout)
        {
            Sampler = sampler;
            ImageView = imageView;
            ImageLayout = imageLayout;
        }
    }

    /// <summary>
    /// Structure specifying the allocation parameters for descriptor sets.
    /// </summary>
    public unsafe struct DescriptorSetAllocateInfo
    {
        /// <summary>
        /// Determines the number of descriptor sets to be allocated from the pool.
        /// </summary>
        public uint DescriptorSetCount;
        /// <summary>
        /// An array of <see cref="VkDescriptorSetLayout"/>, with each member specifying how the
        /// corresponding descriptor set is allocated.
        /// <para>Array length must be greater than 0.</para>
        /// </summary>
        public VkDescriptorSetLayout[] SetLayouts;

        public VkDescriptorPool DescriptorPool;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorSetAllocateInfo"/> structure.
        /// </summary>
        /// <param name="descriptorSetCount">
        /// Determines the number of descriptor sets to be allocated from the pool.
        /// </param>
        /// <param name="setLayouts">
        /// An array of descriptor set layouts, with each member specifying how the corresponding
        /// descriptor set is allocated.
        /// </param>
        public DescriptorSetAllocateInfo(uint descriptorSetCount, VkDescriptorPool descriptorPool, params VkDescriptorSetLayout[] setLayouts)
        {
            DescriptorSetCount = descriptorSetCount;
            SetLayouts = setLayouts;
            DescriptorPool = descriptorPool;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public IntPtr DescriptorPool;
            public uint DescriptorSetCount;
            public IntPtr* SetLayouts;
        }

        internal void ToNative(out Native native, IntPtr pool, IntPtr* setLayouts)
        {
            native.Type = StructureType.DescriptorSetAllocateInfo;
            native.Next = IntPtr.Zero;
            native.DescriptorPool = pool;
            native.DescriptorSetCount = DescriptorSetCount;
            native.SetLayouts = setLayouts;
        }
    }
}
