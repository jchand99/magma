using System;
using System.Runtime.InteropServices;
using Magma.Vulkan.KHR;

namespace Magma.Vulkan
{
    public class VkSemaphore : DeviceBoundObject
    {
        public VkSemaphore(VkDevice parent, SemaphoreCreateInfo semaphoreCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateSemaphore(semaphoreCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Semaphore: ", result);

            unsafe
            {
                vkGetSemaphoreWin32HandleKHR = (delegate* unmanaged[Cdecl]<IntPtr, SemaphoreGetWin32HandleInfoKhr*, IntPtr*, VkResult>)LogicalDevice.GetProcAddr("vkGetSemaphoreWin32HandleKHR");
                vkGetSemaphoreFdKHR = (delegate* unmanaged[Cdecl]<IntPtr, SemaphoreGetFdInfoKhr*, int*, VkResult>)LogicalDevice.GetProcAddr("vkGetSemaphoreFdKHR");
            }
        }

        internal VkSemaphore(VkDevice parent, IntPtr handle)
        {
            LogicalDevice = parent;
            _Handle = handle;
            _AllocationCallbacks = null;
        }

        internal void SetHandle(IntPtr handle)
        {
            _Handle = handle;
        }

        #region Core Methods
        private unsafe VkResult _CreateSemaphore(SemaphoreCreateInfo semaphoreCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr semaphore)
        {
            semaphoreCreateInfo.Prepare();
            fixed (IntPtr* ptr = &semaphore)
            {
                if (allocationCallbacks.HasValue) return vkCreateSemaphore(LogicalDevice._Handle, &semaphoreCreateInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateSemaphore(LogicalDevice._Handle, &semaphoreCreateInfo, null, ptr);
            }
        }
        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroySemaphore(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroySemaphore(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, SemaphoreCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateSemaphore = (delegate* unmanaged[Cdecl]<IntPtr, SemaphoreCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateSemaphore");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroySemaphore = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroySemaphore");
        #endregion

        #region Khr C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, SemaphoreGetWin32HandleInfoKhr*, IntPtr*, VkResult> vkGetSemaphoreWin32HandleKHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, SemaphoreGetFdInfoKhr*, int*, VkResult> vkGetSemaphoreFdKHR;
        #endregion

    }

    // Structs
    /// <summary>
    /// Structure specifying parameters of a newly created semaphore.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SemaphoreCreateInfo
    {
        internal StructureType Type;
        public IntPtr Next;
        public SemaphoreCreateFlags Flags;

        public void Prepare()
        {
            Type = StructureType.SemaphoreCreateInfo;
        }
    }

    // Is reserved for future use.
    [Flags]
    public enum SemaphoreCreateFlags
    {
        None = 0
    }
}
