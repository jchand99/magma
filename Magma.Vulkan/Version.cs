using System;

namespace Magma.Vulkan
{
    public struct VkVersion : IEquatable<VkVersion>, IComparable<VkVersion>
    {
        private readonly int _Version;

        public VkVersion(int major, int minor, int patch)
        {
            _Version = major << 22 | minor << 12 | patch;
        }

        public int Major => _Version >> 22;
        public int Minor => (_Version >> 12) & 0x3ff;
        public int Patch => _Version & 0xfff;

        public int CompareTo(VkVersion other)
        {
            return _Version.CompareTo(other._Version);
        }

        public bool Equals(VkVersion other)
        {
            return _Version.Equals(other._Version);
        }

        public override string ToString() => $"{Major}.{Minor}.{Patch}";

        public static implicit operator int(VkVersion version)
        {
            return version._Version;
        }
    }
}
