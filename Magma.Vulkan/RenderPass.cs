using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkRenderPass : DeviceBoundObject
    {
        public VkRenderPass(VkDevice parent, RenderPassCreateInfo renderPassCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateRenderPass(renderPassCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create RenderPass: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateRenderPass(RenderPassCreateInfo renderPassCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr renderPass)
        {
            fixed (AttachmentDescription* attPtr = renderPassCreateInfo.Attachments)
            fixed (SubpassDependency* subPtr = renderPassCreateInfo.Dependencies)
            fixed (IntPtr* ptr = &renderPass)
            {
                renderPassCreateInfo.ToNative(out var native, attPtr, subPtr);
                if (allocationCallbacks.HasValue) return vkCreateRenderPass(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateRenderPass(LogicalDevice._Handle, &native, null, ptr);
            }
        }

        public unsafe void GetRenderAreaGranularity(Extent2D extent) => vkGetRenderAreaGranularity(LogicalDevice._Handle, _Handle, &extent);

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyRenderPass(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyRenderPass(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, RenderPassCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateRenderPass = (delegate* unmanaged[Cdecl]<IntPtr, RenderPassCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateRenderPass");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, Extent2D*, void> vkGetRenderAreaGranularity = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, Extent2D*, void>)Vulkan.GetStaticProcPointer("vkGetRenderAreaGranularity");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyRenderPass = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyRenderPass");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created render pass.
    /// </summary>
    public unsafe struct RenderPassCreateInfo
    {
        /// <summary>
        /// Structures describing properties of the attachments, or <c>null</c>.
        /// </summary>
        public AttachmentDescription[] Attachments;
        /// <summary>
        /// Structures describing properties of the subpasses.
        /// </summary>
        public SubpassDescription[] Subpasses;
        /// <summary>
        /// Structures describing dependencies between pairs of subpasses, or <c>null</c>.
        /// </summary>
        public SubpassDependency[] Dependencies;

        /// <summary>
        /// Initializes a new instnace of the <see cref="RenderPassCreateInfo"/> structure.
        /// </summary>
        /// <param name="subpasses">Structures describing properties of the subpasses.</param>
        /// <param name="attachments">Structures describing properties of the attachments, or <c>null</c>.</param>
        /// <param name="dependencies">
        /// Structures describing dependencies between pairs of subpasses, or <c>null</c>.
        /// </param>
        public RenderPassCreateInfo(
            SubpassDescription[] subpasses,
            AttachmentDescription[] attachments = null,
            SubpassDependency[] dependencies = null)
        {
            Attachments = attachments;
            Subpasses = subpasses;
            Dependencies = dependencies;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public RenderPassCreateFlags Flags;
            public int AttachmentCount;
            public AttachmentDescription* Attachments;
            public int SubpassCount;
            public SubpassDescription.Native* Subpasses;
            public int DependencyCount;
            public SubpassDependency* Dependencies;

            public void Free()
            {
                for (int i = 0; i < SubpassCount; i++)
                    Subpasses[i].Free();
                Interop.Free(Subpasses);
            }
        }

        internal void ToNative(out Native native, AttachmentDescription* attachments, SubpassDependency* dependencies)
        {
            int subpassCount = Subpasses?.Length ?? 0;
            var subpasses = (SubpassDescription.Native*)Interop.Alloc<SubpassDescription.Native>(subpassCount);
            for (int i = 0; i < subpassCount; i++)
                Subpasses[i].ToNative(out subpasses[i]);

            native.Type = StructureType.RenderPassCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = 0;
            native.AttachmentCount = Attachments?.Length ?? 0;
            native.Attachments = attachments;
            native.SubpassCount = subpassCount;
            native.Subpasses = subpasses;
            native.DependencyCount = Dependencies?.Length ?? 0;
            native.Dependencies = dependencies;
        }
    }

    [Flags]
    // Is reserved for future use.
    internal enum RenderPassCreateFlags
    {
        None = 0
    }

    /// <summary>
    /// Structure specifying an attachment description.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct AttachmentDescription
    {
        /// <summary>
        /// A bitmask specifying additional properties of the attachment.
        /// </summary>
        public AttachmentDescriptions Flags;
        /// <summary>
        /// Specifies the format of the image that will be used for the attachment.
        /// </summary>
        public Format Format;
        /// <summary>
        /// The number of samples of the image.
        /// </summary>
        public SampleCounts Samples;
        /// <summary>
        /// Specifies how the contents of color and depth components of the attachment are treated at
        /// the beginning of the subpass where it is first used.
        /// </summary>
        public AttachmentLoadOp LoadOp;
        /// <summary>
        /// Specifies how the contents of color and depth components of the attachment are treated at
        /// the end of the subpass where it is last used.
        /// </summary>
        public AttachmentStoreOp StoreOp;
        /// <summary>
        /// Specifies how the contents of stencil components of the attachment are treated at the
        /// beginning of the subpass where it is first used, and must be one of the same values
        /// allowed for <see cref="LoadOp"/> above.
        /// </summary>
        public AttachmentLoadOp StencilLoadOp;
        /// <summary>
        /// Specifies how the contents of stencil components of the attachment are treated at the end
        /// of the last subpass where it is used, and must be one of the same values allowed for <see
        /// cref="StoreOp"/> above.
        /// </summary>
        public AttachmentStoreOp StencilStoreOp;
        /// <summary>
        /// The layout the attachment image subresource will be in when a render pass instance begins.
        /// </summary>
        public ImageLayout InitialLayout;
        /// <summary>
        /// The layout the attachment image subresource will be transitioned to when a render pass
        /// instance ends. During a render pass instance, an attachment can use a different layout in
        /// each subpass, if desired.
        /// </summary>
        public ImageLayout FinalLayout;
    }

    /// <summary>
    /// Specify how contents of an attachment are treated at the end of a subpass.
    /// </summary>
    public enum AttachmentStoreOp
    {
        /// <summary>
        /// Specifies the contents generated during the render pass and within the render area are
        /// written to memory. For attachments with a depth/stencil format, this uses the access type
        /// <see cref="Accesses.DepthStencilAttachmentWrite"/>. For attachments with a color format,
        /// this uses the access type <see cref="Accesses.ColorAttachmentWrite"/>.
        /// </summary>
        Store = 0,
        /// <summary>
        /// Specifies the contents within the render area are not needed after rendering, and may be
        /// discarded; the contents of the attachment will be undefined inside the render area. For
        /// attachments with a depth/stencil format, this uses the access type <see
        /// cref="Accesses.DepthStencilAttachmentWrite"/>. For attachments with a color format, this
        /// uses the access type <see cref="Accesses.ColorAttachmentWrite"/>.
        /// </summary>
        DontCare = 1
    }

    /// <summary>
    /// Specify how contents of an attachment are treated at the beginning of a subpass.
    /// </summary>
    public enum AttachmentLoadOp
    {
        /// <summary>
        /// Specifies that the previous contents of the image within the render area will be
        /// preserved. For attachments with a depth/stencil format, this uses the access type <see
        /// cref="Accesses.DepthStencilAttachmentRead"/>. For attachments with a color format, this
        /// uses the access type <see cref="Accesses.ColorAttachmentRead"/>.
        /// </summary>
        Load = 0,
        /// <summary>
        /// Specifies that the contents within the render area will be cleared to a uniform value,
        /// which is specified when a render pass instance is begun. For attachments with a
        /// depth/stencil format, this uses the access type <see
        /// cref="Accesses.DepthStencilAttachmentWrite"/>. For attachments with a color format, this
        /// uses the access type <see cref="Accesses.ColorAttachmentWrite"/>.
        /// </summary>
        Clear = 1,
        /// <summary>
        /// Specifies that the previous contents within the area need not be preserved; the contents
        /// of the attachment will be undefined inside the render area. For attachments with a
        /// depth/stencil format, this uses the access type <see
        /// cref="Accesses.DepthStencilAttachmentWrite"/>. For attachments with a color format, this
        /// uses the access type <see cref="Accesses.ColorAttachmentWrite"/>.
        /// </summary>
        DontCare = 2
    }

    /// <summary>
    /// Bitmask specifying additional properties of an attachment.
    /// </summary>
    [Flags]
    public enum AttachmentDescriptions
    {
        /// <summary>
        /// Specifies that the attachment aliases the same device memory as other attachments.
        /// </summary>
        MayAlias = 1 << 0
    }

    /// <summary>
    /// Structure specifying a subpass description.
    /// </summary>
    public unsafe struct SubpassDescription
    {
        /// <summary>
        /// A bitmask specifying usage of the subpass.
        /// </summary>
        public SubpassDescriptionFlags Flags;
        /// <summary>
        /// Structures that lists which of the render pass's attachments will be used as color
        /// attachments in the subpass, and what layout each attachment will be in during the subpass.
        /// <para>
        /// Each element of the array corresponds to a fragment shader output location, i.e. if the
        /// shader declared an output variable <c>layout(location=X)</c> then it uses the attachment
        /// provided in <c>ColorAttachments[X]</c>.
        /// </para>
        /// </summary>
        public AttachmentReference[] ColorAttachments;
        /// <summary>
        /// Structures that lists which of the render pass's attachments can be read in the fragment
        /// shader stage during the subpass, and what layout each attachment will be in during the
        /// subpass. Each element of the array corresponds to an input attachment unit number in the
        /// shader, i.e. if the shader declares an input variable <c>layout(inputAttachmentIndex=X,
        /// set=Y, binding=Z</c> then it uses the attachment provided in <c>InputAttachments[X]</c>.
        /// Input attachments must also be bound to the pipeline with a descriptor set, with the
        /// input attachment descriptor written in the location (set=Y, binding=Z). Fragment shaders
        /// can use subpass input variables to access the contents of an input attachment at the
        /// fragment's (x, y, layer) framebuffer coordinates.
        /// </summary>
        public AttachmentReference[] InputAttachments;
        /// <summary>
        /// Is <c>null</c> or an array of structures that lists which of the render pass's
        /// attachments are resolved to at the end of the subpass, and what layout each attachment
        /// will be in during the multisample resolve operation. If <see cref="ResolveAttachments"/>
        /// is not <c>null</c>, each of its elements corresponds to a color attachment (the element
        /// in <see cref="ColorAttachments"/> at the same index), and a multisample resolve operation
        /// is defined for each attachment. At the end of each subpass, multisample resolve
        /// operations read the subpass's color attachments, and resolve the samples for each pixel
        /// to the same pixel location in the corresponding resolve attachments, unless the resolve
        /// attachment index is <see cref="Constant.AttachmentUnused"/>. If the first use of an
        /// attachment in a render pass is as a resolve attachment, then the <see
        /// cref="AttachmentLoadOp"/> is effectively ignored as the resolve is guaranteed to
        /// overwrite all pixels in the render area.
        /// </summary>
        public AttachmentReference[] ResolveAttachments;
        /// <summary>
        /// Specifies which attachment will be used for depth/stencil data and the layout it will be
        /// in during the subpass. Setting the attachment index to <see
        /// cref="Constant.AttachmentUnused"/> or leaving this as <c>null</c> indicates that no
        /// depth/stencil attachment will be used in the subpass.
        /// </summary>
        public AttachmentReference? DepthStencilAttachment;
        /// <summary>
        /// Render pass attachment indices describing the attachments that are not used by a subpass,
        /// but whose contents must be preserved throughout the subpass.
        /// </summary>
        public int[] PreserveAttachments;

        /// <summary>
        /// Initializes a new instance of the <see cref="SubpassDescription"/> structure.
        /// </summary>
        /// <param name="flags">A bitmask indicating usage of the subpass.</param>
        /// <param name="colorAttachments">
        /// Structures that lists which of the render pass’s attachments will be used as color
        /// attachments in the subpass, and what layout each attachment will be in during the
        /// subpass. Each element of the array corresponds to a fragment shader output location, i.e.
        /// if the shader declared an output variable <c>layout(location=X)</c> then it uses the
        /// attachment provided in <c>ColorAttachments[X]</c>.
        /// </param>
        /// <param name="inputAttachments">
        /// Structures that lists which of the render pass's attachments can be read in the shader
        /// during the subpass, and what layout each attachment will be in during the subpass. Each
        /// element of the array corresponds to an input attachment unit number in the shader, i.e.
        /// if the shader declares an input variable <c>layout(inputAttachmentIndex=X, set=Y,
        /// binding=Z)</c> then it uses the attachment provided in <c>InputAttachments[X]</c>. Input
        /// attachments must also be bound to the pipeline with a descriptor set, with the input
        /// attachment descriptor written in the location (set=Y, binding=Z).
        /// </param>
        /// <param name="resolveAttachments">
        /// Is <c>null</c> or an array of structures that lists which of the render pass's
        /// attachments are resolved to at the end of the subpass, and what layout each attachment
        /// will be in during the multisample resolve operation. If <see cref="ResolveAttachments"/>
        /// is not <c>null</c>, each of its elements corresponds to a color attachment (the element
        /// in <see cref="ColorAttachments"/> at the same index), and a multisample resolve operation
        /// is defined for each attachment. At the end of each subpass, multisample resolve
        /// operations read the subpass's color attachments, and resolve the samples for each pixel
        /// to the same pixel location in the corresponding resolve attachments, unless the resolve
        /// attachment index is <see cref="Constant.AttachmentUnused"/>. If the first use of an
        /// attachment in a render pass is as a resolve attachment, then the <see
        /// cref="AttachmentLoadOp"/> is effectively ignored as the resolve is guaranteed to
        /// overwrite all pixels in the render area.
        /// </param>
        /// <param name="depthStencilAttachment">
        /// Specifies which attachment will be used for depth/stencil data and the layout it will be
        /// in during the subpass. Setting the attachment index to <see
        /// cref="Constant.AttachmentUnused"/> or leaving this as <c>null</c> indicates that no
        /// depth/stencil attachment will be used in the subpass.
        /// </param>
        /// <param name="preserveAttachments">
        /// Render pass attachment indices describing the attachments that are not used by a subpass,
        /// but whose contents must be preserved throughout the subpass.
        /// </param>
        public SubpassDescription(
            SubpassDescriptionFlags flags = 0,
            AttachmentReference[] colorAttachments = null,
            AttachmentReference[] inputAttachments = null,
            AttachmentReference[] resolveAttachments = null,
            AttachmentReference depthStencilAttachment = default,
            int[] preserveAttachments = null)
        {
            Flags = flags;
            ColorAttachments = colorAttachments;
            InputAttachments = inputAttachments;
            ResolveAttachments = resolveAttachments;
            DepthStencilAttachment = depthStencilAttachment;
            PreserveAttachments = preserveAttachments;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SubpassDescription"/> structure.
        /// </summary>
        /// <param name="colorAttachments">
        /// Structures that lists which of the render pass’s attachments will be used as color
        /// attachments in the subpass, and what layout each attachment will be in during the
        /// subpass. Each element of the array corresponds to a fragment shader output location, i.e.
        /// if the shader declared an output variable layout(location=X) then it uses the attachment
        /// provided in <see cref="ColorAttachments"/>[X].
        /// </param>
        /// <param name="depthStencilAttachment">
        /// Specifies which attachment will be used for depth/stencil data and the layout it will be
        /// in during the subpass. Setting the attachment index to <see
        /// cref="Constant.AttachmentUnused"/> or leaving this as <c>null</c> indicates that no
        /// depth/stencil attachment will be used in the subpass.
        /// </param>
        public SubpassDescription(
            AttachmentReference[] colorAttachments,
            AttachmentReference? depthStencilAttachment = null)
        {
            Flags = 0;
            ColorAttachments = colorAttachments;
            InputAttachments = null;
            ResolveAttachments = null;
            DepthStencilAttachment = depthStencilAttachment;
            PreserveAttachments = null;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public SubpassDescriptionFlags Flags;
            public PipelineBindPoint PipelineBindPoint;
            public int InputAttachmentCount;
            public AttachmentReference* InputAttachments;
            public int ColorAttachmentCount;
            public AttachmentReference* ColorAttachments;
            public AttachmentReference* ResolveAttachments;
            public IntPtr DepthStencilAttachment;
            public int PreserveAttachmentCount;
            public int* PreserveAttachments;

            public void Free()
            {
                Interop.Free(DepthStencilAttachment);
            }
        }

        internal void ToNative(out Native native)
        {
            // Only graphics subpasses are supported.
            native.Flags = Flags;
            native.PipelineBindPoint = PipelineBindPoint.Graphics;
            native.InputAttachmentCount = InputAttachments?.Length ?? 0;
            fixed (AttachmentReference* ptr = InputAttachments)
                native.InputAttachments = ptr;
            native.ColorAttachmentCount = ColorAttachments?.Length ?? 0;
            fixed (AttachmentReference* ptr = ColorAttachments)
                native.ColorAttachments = ptr;
            fixed (AttachmentReference* ptr = ResolveAttachments)
                native.ResolveAttachments = ptr;
            native.DepthStencilAttachment = Interop.AllocToPointer<AttachmentReference>(ref DepthStencilAttachment);
            native.PreserveAttachmentCount = PreserveAttachments?.Length ?? 0;
            fixed (int* ptr = PreserveAttachments)
                native.PreserveAttachments = ptr;
        }
    }

    /// <summary>
    /// Structure specifying a subpass dependency.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SubpassDependency
    {
        /// <summary>
        /// The subpass index of the first subpass in the dependency, or <see cref="Constant.SubpassExternal"/>.
        /// </summary>
        public int SrcSubpass;
        /// <summary>
        /// The subpass index of the second subpass in the dependency, or <see cref="Constant.SubpassExternal"/>.
        /// </summary>
        public int DstSubpass;
        /// <summary>
        /// Specifies a source stage mask.
        /// </summary>
        public PipelineStages SrcStageMask;
        /// <summary>
        /// Specifies a destination stage mask.
        /// </summary>
        public PipelineStages DstStageMask;
        /// <summary>
        /// Specifies a source access mask.
        /// </summary>
        public Accesses SrcAccessMask;
        /// <summary>
        /// Specifies a destination access mask.
        /// </summary>
        public Accesses DstAccessMask;
        /// <summary>
        /// A bitmask of <see cref="Dependencies"/>.
        /// </summary>
        public Dependencies DependencyFlags;
    }

    /// <summary>
    /// Structure specifying an attachment reference.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct AttachmentReference
    {
        /// <summary>
        /// The index of the attachment of the render pass, and corresponds to the index of the
        /// corresponding element in the <see cref="RenderPassCreateInfo.Attachments"/> array. If any
        /// color or depth/stencil attachments are <see cref="Constant.AttachmentUnused"/>, then no
        /// writes occur for those attachments.
        /// </summary>
        public int Attachment;
        /// <summary>
        /// Specifies the layout the attachment uses during the subpass.
        /// <para>Must not be <see cref="ImageLayout.Undefined"/> or <see cref="ImageLayout.Preinitialized"/>.</para>
        /// </summary>
        public ImageLayout Layout;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttachmentReference"/> structure.
        /// </summary>
        /// <param name="attachment">
        /// The index of the attachment of the render pass, and corresponds to the index of the
        /// corresponding element in the <see cref="RenderPassCreateInfo.Attachments"/> array. If any
        /// color or depth/stencil attachments are <see cref="Constant.AttachmentUnused"/>, then no
        /// writes occur for those attachments.
        /// </param>
        /// <param name="layout">Specifies the layout the attachment uses during the subpass.</param>
        public AttachmentReference(int attachment, ImageLayout layout)
        {
            Attachment = attachment;
            Layout = layout;
        }
    }

    /// <summary>
    /// Bitmask specifying usage of a subpass.
    /// </summary>
    [Flags]
    public enum SubpassDescriptionFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that shaders compiled for this subpass write the attributes for all views in a
        /// single invocation of each vertex processing stage. All pipelines compiled against a
        /// subpass that includes this bit must write per-view attributes to the <c>*PerViewNV[]</c>
        /// shader outputs, in addition to the non-per-view (e.g. <c>Position</c>) outputs.
        /// </summary>
        PerViewAttributesNvx = 1 << 0,
        /// <summary>
        /// Specifies that shaders compiled for this subpass use per-view positions which only differ
        /// in value in the x component. Per-view viewport mask can also be used.
        /// </summary>
        PerViewPositionXOnlyNvx = 1 << 1
    }
}
