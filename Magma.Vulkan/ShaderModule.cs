using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkShaderModule : DeviceBoundObject
    {
        public VkShaderModule(VkDevice parent, ShaderModuleCreateInfo shaderModuleCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateShaderModule(shaderModuleCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Failed to create Shader Module: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateShaderModule(ShaderModuleCreateInfo shaderModuleCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr shaderModule)
        {
            fixed (byte* ptr = shaderModuleCreateInfo.Code)
            fixed (IntPtr* shaderModulePtr = &shaderModule)
            {
                shaderModuleCreateInfo.ToNative(out var native, ptr);
                if (allocationCallbacks.HasValue) return vkCreateShaderModule(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, shaderModulePtr);
                else return vkCreateShaderModule(LogicalDevice._Handle, &native, null, shaderModulePtr);
            }
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyShaderModule(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyShaderModule(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, ShaderModuleCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateShaderModule = (delegate* unmanaged[Cdecl]<IntPtr, ShaderModuleCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateShaderModule");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyShaderModule = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyShaderModule");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created shader module.
    /// </summary>
    public unsafe struct ShaderModuleCreateInfo
    {
        /// <summary>
        /// The code that is used to create the shader module. The type and format of the code is
        /// determined from the content of the code.
        /// </summary>
        public byte[] Code;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShaderModuleCreateInfo"/> structure.
        /// </summary>
        /// <param name="code">
        /// The code that is used to create the shader module. The type and format of the code is
        /// determined from the content of the code.
        /// </param>
        public ShaderModuleCreateInfo(byte[] code)
        {
            Code = code;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public ShaderModuleCreateFlags Flags;
            public SizeT CodeSize;
            public byte* Code;
        }

        internal void ToNative(out Native native, byte* code)
        {
            native.Type = StructureType.ShaderModuleCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = 0;
            native.CodeSize = Code?.Length ?? 0;
            native.Code = code;
        }
    }

    internal enum ShaderModuleCreateFlags
    {
        None = 0
    }
}
