using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkBuffer : DeviceBoundObject
    {
        public VkBuffer(VkDevice parent, BufferCreateInfo bufferCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateBuffer(bufferCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Buffer: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateBuffer(BufferCreateInfo bufferCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr buffer)
        {
            fixed (int* ptr = bufferCreateInfo.QueueFamilyIndices)
            fixed (IntPtr* bufferPtr = &buffer)
            {
                bufferCreateInfo.ToNative(out var native, ptr);
                if (allocationCallbacks.HasValue) return vkCreateBuffer(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, bufferPtr);
                else return vkCreateBuffer(LogicalDevice._Handle, &native, null, bufferPtr);
            }
        }

        public unsafe VkResult BindMemory(VkDeviceMemory memory, long memoryOffset)
        {
            return vkBindBufferMemory(LogicalDevice._Handle, _Handle, memory, memoryOffset);
        }

        public unsafe void GetMemoryRequirements(out MemoryRequirements memoryRequirements)
        {
            fixed (MemoryRequirements* ptr = &memoryRequirements) vkGetBufferMemoryRequirements(LogicalDevice, _Handle, ptr);
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks != null)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyBuffer(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyBuffer(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, BufferCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateBuffer = (delegate* unmanaged[Cdecl]<IntPtr, BufferCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyBuffer = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, long, VkResult> vkBindBufferMemory = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, long, VkResult>)Vulkan.GetStaticProcPointer("vkBindBufferMemory");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, MemoryRequirements*, void> vkGetBufferMemoryRequirements = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, MemoryRequirements*, void>)Vulkan.GetStaticProcPointer("vkGetBufferMemoryRequirements");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying the parameters of a newly created buffer object.
    /// </summary>
    public unsafe struct BufferCreateInfo
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask specifying additional parameters of the buffer.
        /// </summary>
        public BufferCreateFlags Flags;
        /// <summary>
        /// The size in bytes of the buffer to be created.
        /// </summary>
        public long Size;
        /// <summary>
        /// A bitmask specifying allowed usages of the buffer.
        /// </summary>
        public BufferUsages Usage;
        /// <summary>
        /// The sharing mode of the buffer when it will be accessed by multiple queue families.
        /// </summary>
        public SharingMode SharingMode;
        /// <summary>
        /// A list of queue families that will access this buffer (ignored if <see
        /// cref="SharingMode"/> is not <see cref="Magma.Vulkan.SharingMode.Concurrent"/>).
        /// </summary>
        public int[] QueueFamilyIndices;

        /// <summary>
        /// Initializes a new instance of the <see cref="BufferCreateInfo"/> structure.
        /// </summary>
        /// <param name="size">The size in bytes of the buffer to be created.</param>
        /// <param name="usages">The bitmask specifying allowed usages of the buffer.</param>
        /// <param name="flags">A bitmask specifying additional parameters of the buffer.</param>
        /// <param name="sharingMode">
        /// The sharing mode of the buffer when it will be accessed by multiple queue families.
        /// </param>
        /// <param name="queueFamilyIndices">
        /// A list of queue families that will access this buffer (ignored if <see
        /// cref="SharingMode"/> is not <see cref="Magma.Vulkan.SharingMode.Concurrent"/>).
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public BufferCreateInfo(
            long size,
            BufferUsages usages,
            BufferCreateFlags flags = 0,
            SharingMode sharingMode = SharingMode.Exclusive,
            int[] queueFamilyIndices = null,
            IntPtr next = default)
        {
            Next = next;
            Size = size;
            Usage = usages;
            Flags = flags;
            SharingMode = sharingMode;
            QueueFamilyIndices = queueFamilyIndices;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public BufferCreateFlags Flags;
            public long Size;
            public BufferUsages Usage;
            public SharingMode SharingMode;
            public int QueueFamilyIndexCount;
            public int* QueueFamilyIndices;
        }

        internal void ToNative(out Native native, int* queueFamilyIndices)
        {
            native.Type = StructureType.BufferCreateInfo;
            native.Next = Next;
            native.Flags = Flags;
            native.Size = Size;
            native.Usage = Usage;
            native.SharingMode = SharingMode;
            native.QueueFamilyIndexCount = QueueFamilyIndices?.Length ?? 0;
            native.QueueFamilyIndices = queueFamilyIndices;
        }
    }

    /// <summary>
    /// Bitmask specifying additional parameters of a buffer.
    /// </summary>
    [Flags]
    public enum BufferCreateFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that the buffer will be backed using sparse memory binding.
        /// </summary>
        SparseBinding = 1 << 0,
        /// <summary>
        /// Specifies that the buffer can be partially backed using sparse memory binding. Buffers
        /// created with this flag must also be created with the <see cref="SparseBinding"/> flag.
        /// </summary>
        SparseResidency = 1 << 1,
        /// <summary>
        /// Specifies that the buffer will be backed using sparse memory binding with memory ranges
        /// that might also simultaneously be backing another buffer (or another portion of the same
        /// buffer). Buffers created with this flag must also be created with the <see
        /// cref="SparseBinding"/> flag.
        /// </summary>
        SparseAliased = 1 << 2
    }

    /// <summary>
    /// Bitmask specifying allowed usages of a buffer.
    /// </summary>
    [Flags]
    public enum BufferUsages
    {
        /// <summary>
        /// Specifies that the buffer can be used as the source of a transfer command. (see the
        /// definition of <see cref="PipelineStages.Transfer"/>).
        /// </summary>
        TransferSrc = 1 << 0,
        /// <summary>
        /// Specifies that the buffer can be used as the destination of a transfer command.
        /// </summary>
        TransferDst = 1 << 1,
        /// <summary>
        /// Specifies that the buffer can be used to create a <see cref="VkBufferView"/> suitable for
        /// occupying a <see cref="VkDescriptorSet"/> slot of type <see cref="DescriptorType.UniformTexelBuffer"/>.
        /// </summary>
        UniformTexelBuffer = 1 << 2,
        /// <summary>
        /// Specifies that the buffer can be used to create a <see cref="VkBufferView"/> suitable for
        /// occupying a <see cref="VkDescriptorSet"/> slot of type <see cref="DescriptorType.StorageTexelBuffer"/>.
        /// </summary>
        StorageTexelBuffer = 1 << 3,
        /// <summary>
        /// Specifies that the buffer can be used in a <see cref="DescriptorBufferInfo"/> suitable
        /// for occupying a <see cref="VkDescriptorSet"/> slot either of type <see
        /// cref="DescriptorType.UniformBuffer"/> or <see cref="DescriptorType.UniformBufferDynamic"/>.
        /// </summary>
        UniformBuffer = 1 << 4,
        /// <summary>
        /// Specifies that the buffer can be used in a <see cref="DescriptorBufferInfo"/> suitable
        /// for occupying a <see cref="VkDescriptorSet"/> slot either of type <see
        /// cref="DescriptorType.StorageBuffer"/> or <see cref="DescriptorType.StorageBufferDynamic"/>.
        /// </summary>
        StorageBuffer = 1 << 5,
        /// <summary>
        /// Specifies that the buffer is suitable for passing as the buffer parameter to <see cref="VkCommandBuffer.CmdBindIndexBuffer"/>.
        /// </summary>
        IndexBuffer = 1 << 6,
        /// <summary>
        /// Specifies that the buffer is suitable for passing as an element of the pBuffers array to
        /// <see cref="VkCommandBuffer.CmdBindVertexBuffers"/>.
        /// </summary>
        VertexBuffer = 1 << 7,
        /// <summary>
        /// Specifies that the buffer is suitable for passing as the buffer parameter to <see
        /// cref="VkCommandBuffer.CmdDrawIndirect"/>, <see
        /// cref="VkCommandBuffer.CmdDrawIndexedIndirect"/>, or <see
        /// cref="VkCommandBuffer.CmdDispatchIndirect"/>. It is also suitable for passing as the <see
        /// cref="Nvx.IndirectCommandsTokenNvx.Buffer"/> member, or <see
        /// cref="Nvx.CmdProcessCommandsInfoNvx.SequencesCountBuffer"/> or <see
        /// cref="Nvx.CmdProcessCommandsInfoNvx.SequencesIndexBuffer"/> member.
        /// </summary>
        IndirectBuffer = 1 << 8
    }

    /// <summary>
    /// Buffer and image sharing modes.
    /// </summary>
    public enum SharingMode
    {
        /// <summary>
        /// Specifies that access to any range or image subresource of the object will be
        /// exclusive to a single queue family at a time.
        /// </summary>
        Exclusive = 0,
        /// <summary>
        /// Specifies that concurrent access to any range or image subresource of the
        /// object from multiple queue families is supported.
        /// </summary>
        Concurrent = 1
    }
}
