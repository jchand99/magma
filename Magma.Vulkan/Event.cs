using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkEvent : DeviceBoundObject
    {
        public VkEvent(VkDevice parent, EventCreateInfo eventCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateEvent(eventCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Event: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateEvent(EventCreateInfo eventCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr @event)
        {
            fixed (IntPtr* ptr = &@event)
            {
                if (allocationCallbacks.HasValue) return vkCreateEvent(LogicalDevice._Handle, &eventCreateInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateEvent(LogicalDevice._Handle, &eventCreateInfo, null, ptr);
            }
        }

        public unsafe VkResult GetStatus() => vkGetEventStatus(LogicalDevice._Handle, _Handle);

        public unsafe VkResult Set() => vkSetEvent(LogicalDevice._Handle, _Handle);

        public unsafe VkResult Reset() => vkResetEvent(LogicalDevice._Handle, _Handle);

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyEvent(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyEvent(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, EventCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateEvent = (delegate* unmanaged[Cdecl]<IntPtr, EventCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateEvent");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult> vkGetEventStatus = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkGetEventStatus");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult> vkSetEvent = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkSetEvent");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult> vkResetEvent = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkResetEvent");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyEvent = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyEvent");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created event.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct EventCreateInfo
    {
        public StructureType Type;
        public IntPtr Next;
        public EventCreateFlags Flags;

        public void Prepare()
        {
            Type = StructureType.EventCreateInfo;
        }
    }

    // Is reserved for future use.
    [Flags]
    public enum EventCreateFlags
    {
        None = 0
    }
}
