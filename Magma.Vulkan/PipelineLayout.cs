using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkPipelineLayout : DeviceBoundObject
    {

        public VkPipelineLayout(VkDevice parent, PipelineLayoutCreateInfo pipelineLayoutCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreatePipelineLayout(pipelineLayoutCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Pipeline Layout: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreatePipelineLayout(PipelineLayoutCreateInfo pipelineLayoutCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr pipelineLayout)
        {
            fixed (PushConstantRange* pushConstantRangePtr = pipelineLayoutCreateInfo.PushConstantRanges)
            fixed (IntPtr* pipelineLayoutPtr = &pipelineLayout)
            {
                var ptr = stackalloc IntPtr[pipelineLayoutCreateInfo.SetLayouts.Length];
                for (int i = 0; i < pipelineLayoutCreateInfo.SetLayouts.Length; i++)
                    ptr[i] = pipelineLayoutCreateInfo.SetLayouts[i];

                pipelineLayoutCreateInfo.ToNative(out var native, ptr, pushConstantRangePtr);
                if (allocationCallbacks.HasValue) return vkCreatePipelineLayout(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, pipelineLayoutPtr);
                else return vkCreatePipelineLayout(LogicalDevice._Handle, &native, null, pipelineLayoutPtr);

            }
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyPipelineLayout(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyPipelineLayout(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PipelineLayoutCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreatePipelineLayout = (delegate* unmanaged[Cdecl]<IntPtr, PipelineLayoutCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreatePipelineLayout");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyPipelineLayout = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyPipelineLayout");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying the parameters of a newly created pipeline layout object.
    /// </summary>
    public unsafe struct PipelineLayoutCreateInfo
    {
        /// <summary>
        /// An array of <see cref="VkDescriptorSetLayout"/> objects.
        /// </summary>
        public VkDescriptorSetLayout[] SetLayouts;
        /// <summary>
        /// Structures defining a set of push constant ranges for use in a single pipeline layout. In
        /// addition to descriptor set layouts, a pipeline layout also describes how many push
        /// constants can be accessed by each stage of the pipeline. Push constants represent a high
        /// speed path to modify constant data in pipelines that is expected to outperform
        /// memory-backed resource updates.
        /// </summary>
        public PushConstantRange[] PushConstantRanges;

        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineLayoutCreateInfo"/> structure.
        /// </summary>
        /// <param name="setLayouts">An array of <see cref="VkDescriptorSetLayout"/> objects.</param>
        /// <param name="pushConstantRanges">
        /// Structures defining a set of push constant ranges for use in a single pipeline layout. In
        /// addition to descriptor set layouts, a pipeline layout also describes how many push
        /// constants can be accessed by each stage of the pipeline. Push constants represent a high
        /// speed path to modify constant data in pipelines that is expected to outperform
        /// memory-backed resource updates.
        /// </param>
        public PipelineLayoutCreateInfo(
            VkDescriptorSetLayout[] setLayouts = null,
            PushConstantRange[] pushConstantRanges = null)
        {
            SetLayouts = setLayouts;
            PushConstantRanges = pushConstantRanges;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public PipelineLayoutCreateFlags Flags;
            public int SetLayoutCount;
            public IntPtr* SetLayouts;
            public int PushConstantRangeCount;
            public PushConstantRange* PushConstantRanges;
        }

        internal void ToNative(out Native native, IntPtr* setLayouts, PushConstantRange* pushConstantRanges)
        {
            native.Type = StructureType.PipelineLayoutCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = 0;
            native.SetLayoutCount = SetLayouts?.Length ?? 0;
            native.SetLayouts = setLayouts;
            native.PushConstantRangeCount = PushConstantRanges?.Length ?? 0;
            native.PushConstantRanges = pushConstantRanges;
        }
    }

    // Is reserved for future use.
    [Flags]
    internal enum PipelineLayoutCreateFlags
    {
        None = 0
    }

    /// <summary>
    /// Structure specifying a push constant range.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PushConstantRange
    {
        /// <summary>
        /// A set of stage flags describing the shader stages that will access a range of push
        /// constants. If a particular stage is not included in the range, then accessing members of
        /// that range of push constants from the corresponding shader stage will result in undefined
        /// data being read.
        /// </summary>
        public ShaderStages StageFlags;
        /// <summary>
        /// The start offset consumed by the range.
        /// <para>Offset is in units of bytes and must be a multiple of 4.</para>
        /// <para>The layout of the push constant variables is specified in the shader.</para>
        /// </summary>
        public int Offset;
        /// <summary>
        /// The size consumed by the range.
        /// <para>Size is in units of bytes and must be a multiple of 4.</para>
        /// <para>The layout of the push constant variables is specified in the shader.</para>
        /// </summary>
        public int Size;

        /// <summary>
        /// Initializes a new instance of the <see cref="PushConstantRange"/> structure.
        /// </summary>
        /// <param name="stageFlags">
        /// A set of stage flags describing the shader stages that will access a range of push
        /// constants. If a particular stage is not included in the range, then accessing members of
        /// that range of push constants from the corresponding shader stage will result in undefined
        /// data being read.
        /// </param>
        /// <param name="offset">
        /// The start offset consumed by the range.
        /// <para>Offset is in units of bytes and must be a multiple of 4.</para>
        /// <para>The layout of the push constant variables is specified in the shader.</para>
        /// </param>
        /// <param name="size">
        /// The size consumed by the range.
        /// <para>Size is in units of bytes and must be a multiple of 4.</para>
        /// <para>The layout of the push constant variables is specified in the shader.</para>
        /// </param>
        public PushConstantRange(ShaderStages stageFlags, int offset, int size)
        {
            StageFlags = stageFlags;
            Offset = offset;
            Size = size;
        }
    }

    /// <summary>
    /// Bitmask specifying a pipeline stage.
    /// </summary>
    [Flags]
    public enum ShaderStages
    {
        /// <summary>
        /// Specifies the vertex stage.
        /// </summary>
        Vertex = 1 << 0,
        /// <summary>
        /// Specifies the tessellation control stage.
        /// </summary>
        TessellationControl = 1 << 1,
        /// <summary>
        /// Specifies the tessellation evaluation stage.
        /// </summary>
        TessellationEvaluation = 1 << 2,
        /// <summary>
        /// Specifies the geometry stage.
        /// </summary>
        Geometry = 1 << 3,
        /// <summary>
        /// Specifies the fragment stage.
        /// </summary>
        Fragment = 1 << 4,
        /// <summary>
        /// Specifies the compute stage.
        /// </summary>
        Compute = 1 << 5,
        /// <summary>
        /// Is a combination of bits used as shorthand to specify all graphics stages defined above
        /// (excluding the compute stage).
        /// </summary>
        AllGraphics = 0x0000001F,
        /// <summary>
        /// Is a combination of bits used as shorthand to specify all shader stages supported by the
        /// device, including all additional stages which are introduced by extensions.
        /// </summary>
        All = 0x7FFFFFFF
    }
}
