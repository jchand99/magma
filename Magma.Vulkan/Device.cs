using System;
using System.Text;
using System.Runtime.InteropServices;
using Magma.Vulkan.EXT;
using Magma.Vulkan.KHR;
using static Magma.Vulkan.Constants;

namespace Magma.Vulkan
{
    public class VkDevice : IDisposable
    {
        internal IntPtr _Handle;
        private VkInstance _Instance;
        private VkPhysicalDevice _PhysicalDevice;
        private AllocationCallbacks? _AllocationCallbacks;

        public static implicit operator IntPtr(VkDevice logicalDevice) => logicalDevice == null ? IntPtr.Zero : logicalDevice._Handle;

        public VkDevice(VkPhysicalDevice physicalDevice, DeviceCreateInfo deviceCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _Instance = physicalDevice.Instance;
            _PhysicalDevice = physicalDevice;
            _AllocationCallbacks = allocationCallbacks;

            VkResult result = _CreateDevice(_PhysicalDevice._Handle, deviceCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success || _Handle == IntPtr.Zero)
                throw new VulkanException("Failed to create a logical device from the PhysicalDevice: ", result);

            // Ext
            unsafe
            {
                vkDebugMarkerSetObjectNameEXT = (delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerObjectNameInfoExt.Native*, VkResult>)GetProcAddr("vkDebugMarkerSetObjectNameEXT");
                vkDebugMarkerSetObjectTagEXT = (delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerObjectTagInfoExt.Native*, VkResult>)GetProcAddr("vkDebugMarkerSetObjectTagEXT");
                vkDisplayPowerControlEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DisplayPowerInfoExt*, VkResult>)GetProcAddr("vkDisplayPowerControlEXT");
                vkRegisterDisplayEventEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DisplayEventInfoExt*, AllocationCallbacks*, IntPtr*, VkResult>)GetProcAddr("vkRegisterDisplayEventEXT");
                vkRegisterDeviceEventEXT = (delegate* unmanaged[Cdecl]<IntPtr, DeviceEventInfoExt*, AllocationCallbacks*, IntPtr*, VkResult>)GetProcAddr("vkRegisterDeviceEventEXT");
                vkSetHdrMetadataEXT = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, HdrMetadataExt*, void>)GetProcAddr("vkSetHdrMetadataEXT");
                vkGetMemoryHostPointerPropertiesEXT = (delegate* unmanaged[Cdecl]<IntPtr, ExternalMemoryHandleTypesKhr, IntPtr, MemoryHostPointerPropertiesExt*, VkResult>)GetProcAddr("vkGetMemoryHostPointerPropertiesEXT");
            }

            // Khr
            unsafe
            {
                vkGetMemoryWin32HandlePropertiesKHR = (delegate* unmanaged[Cdecl]<IntPtr, ExternalMemoryHandleTypesKhr, IntPtr, MemoryWin32HandlePropertiesKhr*, VkResult>)GetProcAddr("vkGetMemoryWin32HandlePropertiesKHR");
                vkGetMemoryFdPropertiesKHR = (delegate* unmanaged[Cdecl]<IntPtr, ExternalMemoryHandleTypesKhr, int, MemoryFdPropertiesKhr*, VkResult>)GetProcAddr("vkGetMemoryFdPropertiesKHR");
                vkImportSemaphoreWin32HandleKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr*, VkResult>)GetProcAddr("vkImportSemaphoreWin32HandleKHR");
                vkImportSemaphoreFdKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr*, VkResult>)GetProcAddr("vkImportSemaphoreFdKHR");
                vkGetBufferMemoryRequirements2KHR = (delegate* unmanaged[Cdecl]<IntPtr, BufferMemoryRequirementsInfo2Khr*, MemoryRequirements2Khr*, void>)GetProcAddr("vkGetBufferMemoryRequirements2KHR");
                vkGetImageMemoryRequirements2KHR = (delegate* unmanaged[Cdecl]<IntPtr, ImageMemoryRequirementsInfo2Khr*, MemoryRequirements2Khr*, void>)GetProcAddr("vkGetImageMemoryRequirements2KHR");
                vkGetImageSparseMemoryRequirements2KHR = (delegate* unmanaged[Cdecl]<IntPtr, ImageSparseMemoryRequirementsInfo2Khr*, uint, SparseImageMemoryRequirements2Khr*, void>)GetProcAddr("vkGetImageSparseMemoryRequirements2KHR");
                vkBindBufferMemory2KHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, BindBufferMemoryInfoKhr.Native*, VkResult>)GetProcAddr("vkBindBufferMemory2KHR");
                vkBindImageMemory2KHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, BindImageMemoryInfoKhr.Native*, VkResult>)GetProcAddr("vkBindImageMemory2KHR");
            }
        }

        #region Core Methods
        private unsafe VkResult _CreateDevice(IntPtr physicalDevice, DeviceCreateInfo deviceCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr device)
        {
            deviceCreateInfo.ToNative(out var native);
            fixed (IntPtr* ptr = &device)
                if (allocationCallbacks != null)
                {
                    return vkCreateDevice(physicalDevice, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                }
                else
                {
                    return vkCreateDevice(physicalDevice, &native, null, ptr);
                }
        }

        public unsafe IntPtr GetProcAddr(string name)
        {
            byte* addrName = stackalloc byte[name?.Length + 1 ?? 0];
            name.ToBytePointer(addrName);
            return vkGetDeviceProcAddr(_Handle, addrName);
        }

        public unsafe void GetQueue(uint queueFamilyIndex, uint queueIndex, out VkQueue queue)
        {
            IntPtr queuePtr;
            vkGetDeviceQueue(_Handle, queueFamilyIndex, queueIndex, &queuePtr);
            queue = new VkQueue(this, queuePtr, queueFamilyIndex, queueIndex);
        }

        public unsafe VkResult WaitIdle() => vkDeviceWaitIdle(_Handle);

        public unsafe VkResult FlushMappedMemoryRanges(MappedMemoryRange[] memoryRanges)
        {
            fixed (MappedMemoryRange* ptr = memoryRanges) return vkFlushMappedMemoryRanges(_Handle, (uint)(memoryRanges?.Length ?? 0), ptr);
        }

        public unsafe VkResult FlushMappedMemoryRange(MappedMemoryRange memoryRange) => vkFlushMappedMemoryRanges(_Handle, 1, &memoryRange);

        public unsafe VkResult InvalidateMappedMemoryRanges(MappedMemoryRange[] memoryRanges)
        {
            fixed (MappedMemoryRange* ptr = memoryRanges) return vkInvalidateMappedMemoryRanges(_Handle, (uint)(memoryRanges?.Length ?? 0), ptr);
        }

        public unsafe VkResult InvalidateMappedMemoryRange(MappedMemoryRange memoryRange) => vkInvalidateMappedMemoryRanges(_Handle, 1, &memoryRange);
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, DeviceCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateDevice = (delegate* unmanaged[Cdecl]<IntPtr, DeviceCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateDevice");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, AllocationCallbacks?*, void> vkDestroyDevice = (delegate* unmanaged[Cdecl]<IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyDevice");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, byte*, IntPtr> vkGetDeviceProcAddr = (delegate* unmanaged[Cdecl]<IntPtr, byte*, IntPtr>)Vulkan.GetStaticProcPointer("vkGetDeviceProcAddr");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint, IntPtr*, void> vkGetDeviceQueue = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, IntPtr*, void>)Vulkan.GetStaticProcPointer("vkGetDeviceQueue");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, VkResult> vkDeviceWaitIdle = (delegate* unmanaged[Cdecl]<IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkDeviceWaitIdle");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, MappedMemoryRange*, VkResult> vkFlushMappedMemoryRanges = (delegate* unmanaged[Cdecl]<IntPtr, uint, MappedMemoryRange*, VkResult>)Vulkan.GetStaticProcPointer("vkFlushMappedMemoryRanges");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, MappedMemoryRange*, VkResult> vkInvalidateMappedMemoryRanges = (delegate* unmanaged[Cdecl]<IntPtr, uint, MappedMemoryRange*, VkResult>)Vulkan.GetStaticProcPointer("vkInvalidateMappedMemoryRanges");
        #endregion

        #region Ext C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerObjectNameInfoExt.Native*, VkResult> vkDebugMarkerSetObjectNameEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerObjectTagInfoExt.Native*, VkResult> vkDebugMarkerSetObjectTagEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DisplayPowerInfoExt*, VkResult> vkDisplayPowerControlEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DisplayEventInfoExt*, AllocationCallbacks*, IntPtr*, VkResult> vkRegisterDisplayEventEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DeviceEventInfoExt*, AllocationCallbacks*, IntPtr*, VkResult> vkRegisterDeviceEventEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, HdrMetadataExt*, void> vkSetHdrMetadataEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, ExternalMemoryHandleTypesKhr, IntPtr, MemoryHostPointerPropertiesExt*, VkResult> vkGetMemoryHostPointerPropertiesEXT;
        #endregion

        #region Khr C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, ExternalMemoryHandleTypesKhr, IntPtr, MemoryWin32HandlePropertiesKhr*, VkResult> vkGetMemoryWin32HandlePropertiesKHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, ExternalMemoryHandleTypesKhr, int, MemoryFdPropertiesKhr*, VkResult> vkGetMemoryFdPropertiesKHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr*, VkResult> vkImportSemaphoreWin32HandleKHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr*, VkResult> vkImportSemaphoreFdKHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, BufferMemoryRequirementsInfo2Khr*, MemoryRequirements2Khr*, void> vkGetBufferMemoryRequirements2KHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, ImageMemoryRequirementsInfo2Khr*, MemoryRequirements2Khr*, void> vkGetImageMemoryRequirements2KHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, ImageSparseMemoryRequirementsInfo2Khr*, uint, SparseImageMemoryRequirements2Khr*, void> vkGetImageSparseMemoryRequirements2KHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, uint, BindBufferMemoryInfoKhr.Native*, VkResult> vkBindBufferMemory2KHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, uint, BindImageMemoryInfoKhr.Native*, VkResult> vkBindImageMemory2KHR;
        #endregion

        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _DestroyDevice();
                _Handle = IntPtr.Zero;
                disposedValue = true;
            }
        }

        public unsafe void _DestroyDevice()
        {
            if (_AllocationCallbacks != null)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyDevice(_Handle, ptr);
            }
            else
            {
                vkDestroyDevice(_Handle, null);
            }
        }

        // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~VkDevice()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    // Structs

    public unsafe struct DeviceCreateInfo
    {
        public IntPtr Next;
        public DeviceQueueCreateInfo[] QueueCreateInfos;
        public string[] EnabledExtensionNames;
        public PhysicalDeviceFeatures? EnabledFeatures;

        public DeviceCreateInfo(
          DeviceQueueCreateInfo[] queueCreateInfos,
          string[] enabledExtensionNames = null,
          PhysicalDeviceFeatures? enabledFeatures = null,
          IntPtr next = default)
        {
            Next = next;
            QueueCreateInfos = queueCreateInfos;
            EnabledExtensionNames = enabledExtensionNames;
            EnabledFeatures = enabledFeatures;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DeviceCreateFlags Flags;
            public int QueueCreateInfoCount;
            public DeviceQueueCreateInfo.Native* QueueCreateInfos;
            public int EnabledLayerCount;
            public IntPtr* EnabledLayerNames;
            public int EnabledExtensionCount;
            public IntPtr* EnabledExtensionNames;
            public IntPtr EnabledFeatures;
            public void Free()
            {
                for (int i = 0; i < QueueCreateInfoCount; i++)
                    QueueCreateInfos[i].Free();
                Interop.Free(QueueCreateInfos);
                Interop.Free(EnabledExtensionNames, EnabledExtensionCount);
                Interop.Free(EnabledFeatures);
            }
        }

        internal void ToNative(out Native val)
        {
            int queueCreateInfoCount = QueueCreateInfos?.Length ?? 0;
            var ptr = (DeviceQueueCreateInfo.Native*)Interop.Alloc<DeviceQueueCreateInfo.Native>(queueCreateInfoCount);
            for (var i = 0; i < queueCreateInfoCount; i++)
                QueueCreateInfos[i].ToNative(out ptr[i]);
            val.Type = StructureType.DeviceCreateInfo;
            val.Next = Next;
            val.Flags = 0;
            val.QueueCreateInfoCount = queueCreateInfoCount;
            val.QueueCreateInfos = ptr;
            val.EnabledLayerCount = 0;
            val.EnabledLayerNames = null;
            val.EnabledExtensionCount = EnabledExtensionNames?.Length ?? 0;
            val.EnabledExtensionNames = EnabledExtensionNames.AllocToPointers();
            val.EnabledFeatures = Interop.AllocToPointer(ref EnabledFeatures);
        }
    }

    public struct DeviceQueueCreateInfo
    {
        public uint QueueFamilyIndex;
        public uint QueueCount;
        public float[] QueuePriorities;

        public DeviceQueueCreateInfo(uint queueFamilyIndex, uint queueCount, params float[] queuePriorities)
        {
            QueueFamilyIndex = queueFamilyIndex;
            QueueCount = queueCount;
            QueuePriorities = queuePriorities;
        }

        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DeviceQueueCreateFlags Flags;
            public uint QueueFmailyIndex;
            public uint QueueCount;
            public IntPtr QueuePriorities;
            public void Free()
            {
                Interop.Free(QueuePriorities);
            }
        }

        internal void ToNative(out Native native)
        {
            native.Type = StructureType.DeviceQueueCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = DeviceQueueCreateFlags.None;
            native.QueueFmailyIndex = QueueFamilyIndex;
            native.QueueCount = QueueCount;
            native.QueuePriorities = Interop.AllocFloatsToPointer(QueuePriorities);
        }
    }

    [Flags]
    public enum DeviceCreateFlags
    {
        None = 0
    }

    [Flags]
    internal enum DeviceQueueCreateFlags
    {
        None = 0
    }

    /// <summary>
    /// Structure specifying a mapped memory range.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MappedMemoryRange
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// The memory object to which this range belongs.
        /// <para>Must currently be mapped.</para>
        /// </summary>
        public IntPtr Memory;
        /// <summary>
        /// The zero-based byte offset from the beginning of the memory object.
        /// <para>Must be a multiple of <see cref="PhysicalDeviceLimits.NonCoherentAtomSize"/>.</para>
        /// <para>
        /// If <see cref="Size"/> is equal to <see cref="WholeSize"/>, offset must be within the
        /// currently mapped range of memory
        /// </para>
        /// </summary>
        public long Offset;
        /// <summary>
        /// Is either the size of range, or <see cref="WholeSize"/> to affect the range from offset
        /// to the end of the current mapping of the allocation.
        /// <para>
        /// If size is not equal to <see cref="WholeSize"/>, offset and size must specify a range
        /// contained within the currently mapped range of memory.
        /// </para>
        /// <para>
        /// If size is not equal to <see cref="WholeSize"/>, size must be a multiple of <see cref="PhysicalDeviceLimits.NonCoherentAtomSize"/>
        /// </para>
        /// </summary>
        public long Size;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappedMemoryRange"/> structure.
        /// </summary>
        /// <param name="memory">The memory object to which this range belongs.</param>
        /// <param name="offset">The zero-based byte offset from the beginning of the memory object.</param>
        /// <param name="size">
        /// Is either the size of range, or <see cref="WholeSize"/> to affect the range from offset
        /// to the end of the current mapping of the allocation.
        /// </param>
        public MappedMemoryRange(IntPtr memory, long offset = 0, long size = WholeSize)
        {
            Type = StructureType.MappedMemoryRange;
            Next = IntPtr.Zero;
            Memory = memory;
            Offset = offset;
            Size = size;
        }

        internal void Prepare()
        {
            Type = StructureType.MappedMemoryRange;
        }
    }
}
