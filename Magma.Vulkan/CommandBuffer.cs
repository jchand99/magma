using System;
using System.Runtime.InteropServices;
using Magma.Vulkan.EXT;
using static Magma.Vulkan.Constants;

namespace Magma.Vulkan
{
    public class VkCommandBuffer : IDisposable
    {
        internal IntPtr _Handle;
        public VkCommandPool CommandPool { get; internal set; }

        public static implicit operator IntPtr(VkCommandBuffer commandBuffer) => commandBuffer == null ? IntPtr.Zero : commandBuffer._Handle;

        private VkCommandBuffer(VkCommandPool commandPool, IntPtr handle)
        {
            CommandPool = commandPool;
            _Handle = handle;

            unsafe
            {
                vkCmdDebugMarkerBeginEXT = (delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerMarkerInfoExt.Native*, void>)CommandPool.LogicalDevice.GetProcAddr("vkCmdDebugMarkerBeginEXT");
                vkCmdDebugMarkerEndEXT = (delegate* unmanaged[Cdecl]<IntPtr, void>)CommandPool.LogicalDevice.GetProcAddr("vkCmdDebugMarkerEndEXT");
                vkCmdDebugMarkerInstertEXT = (delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerMarkerInfoExt.Native*, void>)CommandPool.LogicalDevice.GetProcAddr("vkCmdDebugMarkerInsertEXT");
                vkCmdSetDiscardRectangleEXT = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, VkRect2D*, void>)CommandPool.LogicalDevice.GetProcAddr("vkCmdSetDiscardRectangleEXT");
                vkCmdSetSampleLocationsEXT = (delegate* unmanaged[Cdecl]<IntPtr, SampleLocationsInfoExt*, void>)CommandPool.LogicalDevice.GetProcAddr("vkCmdSetSampleLocationsEXT");
            }
        }

        #region Core Methods

        public unsafe static VkResult AllocateBuffers(VkCommandPool parent, CommandBufferAllocateInfo commandBufferAllocateInfo, out VkCommandBuffer[] commandBuffers)
        {
            int count = commandBufferAllocateInfo.CommandBufferCount;
            commandBufferAllocateInfo.Prepare(parent);

            var ptr = stackalloc IntPtr[count];
            VkResult result = vkAllocateCommandBuffers(parent.LogicalDevice, &commandBufferAllocateInfo, ptr);

            commandBuffers = new VkCommandBuffer[count];
            for (int i = 0; i < count; i++)
            {
                commandBuffers[i] = new VkCommandBuffer(parent, ptr[i]);
            }

            return result;
        }

        public unsafe static void FreeBuffers(VkCommandPool parent, params VkCommandBuffer[] commandBuffers)
        {
            int count = commandBuffers?.Length ?? 0;

            var ptr = stackalloc IntPtr[count];
            for (int i = 0; i < commandBuffers.Length; i++)
            {
                ptr[i] = commandBuffers[i];
            }

            vkFreeCommandBuffers(parent.LogicalDevice._Handle, parent, (uint)(count), ptr);
        }

        public unsafe void Free()
        {
            fixed (IntPtr* ptr = &_Handle)
                vkFreeCommandBuffers(CommandPool.LogicalDevice._Handle, CommandPool, 1, ptr);
        }

        public unsafe VkResult BeginCommandBuffer(CommandBufferBeginInfo commandBufferBeginInfo)
        {
            commandBufferBeginInfo.ToNative(out var native);
            return vkBeginCommandBuffer(_Handle, &native);
        }

        public unsafe VkResult EndCommandBuffer() => vkEndCommandBuffer(_Handle);

        public unsafe VkResult ResetCommandBuffer(CommandBufferResetFlags flags) => vkResetCommandBuffer(_Handle, flags);

        public unsafe void CmdBindPipeline(PipelineBindPoint pipelineBindPoint, VkPipeline pipeline) => vkCmdBindPipeline(_Handle, pipelineBindPoint, pipeline);

        public unsafe void CmdSetViewport(VkViewport viewport) => vkCmdSetViewport(_Handle, 0, 1, &viewport);
        public unsafe void CmdSetViewports(uint firstViewport, VkViewport[] viewports)
        {
            fixed (VkViewport* ptr = viewports) vkCmdSetViewport(_Handle, firstViewport, (uint)(viewports?.Length ?? 0), ptr);
        }

        public unsafe void CmdSetScissor(VkRect2D scissor) => vkCmdSetScissor(_Handle, 0, 1, &scissor);
        public unsafe void CmdSetScissors(uint firstScissor, VkRect2D[] scissors)
        {
            fixed (VkRect2D* ptr = scissors) vkCmdSetScissor(_Handle, firstScissor, (uint)(scissors?.Length ?? 0), ptr);
        }

        public unsafe void CmdSetLineWidth(float lineWidth) => vkCmdSetLineWidth(_Handle, lineWidth);

        public unsafe void CmdSetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor) => vkCmdSetDepthBias(_Handle, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);

        public unsafe void CmdSetBlendConstants(ColorF4 blendConstants) => vkCmdSetBlendConstants(_Handle, blendConstants);

        public unsafe void CmdSetDepthBounds(float minDepthBounds, float maxDepthBounds) => vkCmdSetDepthBounds(_Handle, minDepthBounds, maxDepthBounds);

        public unsafe void CmdSetStencilCompareMask(StencilFaces faceMask, uint compareMask) => vkCmdSetStencilCompareMask(_Handle, faceMask, compareMask);

        public unsafe void CmdSetStencilWriteMask(StencilFaces faceMask, uint compareMask) => vkCmdSetStencilWriteMask(_Handle, faceMask, compareMask);

        public unsafe void CmdSetStencilReference(StencilFaces faceMask, uint reference) => vkCmdSetStencilReference(_Handle, faceMask, reference);

        public unsafe void CmdBindDescriptorSets(PipelineBindPoint pipelineBindPoint, VkPipelineLayout pipelineLayout, uint firstSet, VkDescriptorSet[] descriptorSets, uint[] dynamicOffsets)
        {
            fixed (uint* offsetsPtr = dynamicOffsets)
            {
                int count = descriptorSets?.Length ?? 0;
                var setsPtr = stackalloc IntPtr[count];
                for (int i = 0; i < count; i++)
                    setsPtr[i] = descriptorSets[i];

                vkCmdBindDescriptorSets(_Handle, pipelineBindPoint, pipelineLayout, firstSet, (uint)(count), setsPtr, (uint)dynamicOffsets.Length, offsetsPtr);
            }
        }

        public unsafe void CmdBindDescriptorSet(PipelineBindPoint pipelineBindPoint, VkPipelineLayout pipelineLayout, VkDescriptorSet descriptorSet, uint dynamicOffset = 0)
        {
            fixed (IntPtr* ptr = &descriptorSet._Handle)
                vkCmdBindDescriptorSets(_Handle, pipelineBindPoint, pipelineLayout, 0, 1, ptr, (uint)(dynamicOffset == 0 ? 0 : 1), dynamicOffset == 0 ? null : &dynamicOffset);
        }

        public unsafe void CmdBindIndexBuffer(VkBuffer buffer, long offset, IndexType indexType = IndexType.UInt32)
        {
            vkCmdBindIndexBuffer(_Handle, buffer, offset, indexType);
        }

        public unsafe void CmdBindVertexBuffers(uint firstBinding, uint bindingCount, VkBuffer[] buffers, long[] offsets)
        {
            int count = buffers?.Length ?? 0;
            fixed (long* offPtr = offsets)
            {
                var bufPtr = stackalloc IntPtr[count];
                for (int i = 0; i < count; i++)
                    bufPtr[i] = buffers[i];

                vkCmdBindVertexBuffers(_Handle, firstBinding, bindingCount, bufPtr, offPtr);
            }
        }

        public unsafe void CmdBindVertexBuffer(uint firstBinding, uint bindingCount, VkBuffer buffer, long offset = 0)
        {
            fixed (IntPtr* ptr = &buffer._Handle)
                vkCmdBindVertexBuffers(_Handle, 0, 1, ptr, &offset);
        }

        public unsafe void CmdDraw(uint vertexCount, uint instanceCount, uint firstVertex, uint firstInstance) => vkCmdDraw(_Handle, vertexCount, instanceCount, firstVertex, firstInstance);

        public unsafe void CmdDrawIndexed(uint indexCount, uint instanceCount, uint firstIndex, int vertexOffset, uint firstInstance) => vkCmdDrawIndexed(_Handle, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);

        public unsafe void CmdDispatch(uint groupCountX, uint groupCountY, uint groupCountZ) => vkCmdDispatch(_Handle, groupCountX, groupCountY, groupCountZ);

        public unsafe void CmdClearAttachments(ClearAttachment[] attachments, ClearRect[] clearRects)
        {
            fixed (ClearAttachment* attPtr = attachments)
            fixed (ClearRect* recPtr = clearRects)
            {
                vkCmdClearAttachments(_Handle, (uint)(attachments?.Length ?? 0), attPtr, (uint)(clearRects?.Length ?? 0), recPtr);
            }
        }

        public unsafe void CmdWaitEvents(VkEvent[] events, PipelineStages srcStageMask, PipelineStages dstStageMask, MemoryBarrier[] memoryBarriers, BufferMemoryBarrier[] bufferMemoryBarriers, ImageMemoryBarrier[] imageMemoryBarriers)
        {
            fixed (MemoryBarrier* memPtr = memoryBarriers)
            fixed (BufferMemoryBarrier* bmemPtr = bufferMemoryBarriers)
            fixed (ImageMemoryBarrier* imemPtr = imageMemoryBarriers)
            {
                int count = events?.Length ?? 0;
                var evPtr = stackalloc IntPtr[count];
                for (int i = 0; i < count; i++)
                    evPtr[i] = events[i];

                vkCmdWaitEvents(_Handle, (uint)(count), evPtr, srcStageMask, dstStageMask, (uint)(memoryBarriers?.Length ?? 0), memPtr, (uint)(bufferMemoryBarriers?.Length ?? 0), bmemPtr, (uint)(imageMemoryBarriers?.Length ?? 0), imemPtr);
            }
        }

        public unsafe void CmdPipelineBarrier(PipelineStages srcStageMask, PipelineStages dstStageMask, Dependencies dependencies, MemoryBarrier[] memoryBarriers, BufferMemoryBarrier[] bufferMemoryBarriers, ImageMemoryBarrier[] imageMemoryBarriers)
        {
            fixed (MemoryBarrier* memPtr = memoryBarriers)
            fixed (BufferMemoryBarrier* bmemPtr = bufferMemoryBarriers)
            fixed (ImageMemoryBarrier* imemPtr = imageMemoryBarriers)
            {
                vkCmdPipelineBarrier(_Handle, srcStageMask, dstStageMask, dependencies, (uint)(memoryBarriers?.Length ?? 0), memPtr, (uint)(bufferMemoryBarriers?.Length ?? 0), bmemPtr, (uint)(imageMemoryBarriers?.Length ?? 0), imemPtr);
            }
        }

        public unsafe void CmdWriteTimestamp(PipelineStages pipelineStage, VkQueryPool queryPool, uint query) => vkCmdWriteTimestamp(_Handle, pipelineStage, queryPool, query);

        public unsafe void CmdBeginRenderPass(RenderPassBeginInfo renderPassBeginInfo, SubpassContents contents)
        {
            fixed (ClearValue* ptr = renderPassBeginInfo.ClearValues)
            {
                renderPassBeginInfo.ToNative(out var native, ptr);
                vkCmdBeginRenderPass(_Handle, &native, contents);
            }
        }

        public unsafe void CmdNextSubpass(SubpassContents contents) => vkCmdNextSubpass(_Handle, contents);

        public unsafe void CmdEndRenderPass() => vkCmdEndRenderPass(_Handle);

        public unsafe void CmdExecuteCommand(VkCommandBuffer commandBuffer)
        {
            fixed (IntPtr* ptr = &commandBuffer._Handle)
                vkCmdExecuteCommands(_Handle, 1, ptr);
        }
        public unsafe void CmdExecuteCommands(VkCommandBuffer[] commandBuffers)
        {
            int count = commandBuffers?.Length ?? 0;
            var ptr = stackalloc IntPtr[count];
            for (int i = 0; i < count; i++)
                ptr[i] = commandBuffers[i];

            vkCmdExecuteCommands(_Handle, (uint)(commandBuffers?.Length ?? 0), ptr);
        }

        public unsafe void CmdDrawIndirect(VkBuffer buffer, long offset, uint drawCount, uint stride) => vkCmdDrawIndirect(_Handle, buffer, offset, drawCount, stride);

        public unsafe void CmdDrawIndexedIndirect(VkBuffer buffer, long offset, uint drawCount, uint stride) => vkCmdDrawIndexedIndirect(_Handle, buffer, offset, drawCount, stride);

        public unsafe void CmdDispatchIndirect(VkBuffer buffer, long offset) => vkCmdDispatchIndirect(_Handle, buffer, offset);

        public unsafe void CmdCopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, BufferCopy[] regions)
        {
            fixed (BufferCopy* ptr = regions) vkCmdCopyBuffer(_Handle, srcBuffer, dstBuffer, (uint)(regions?.Length ?? 0), ptr);
        }

        public unsafe void CmdUpdateBuffer(VkBuffer dstBuffer, long dstOffset, long dataSize, IntPtr data) => vkCmdUpdateBuffer(_Handle, dstBuffer, dstOffset, dataSize, data);

        public unsafe void CmdFillBuffer(VkBuffer dstBuffer, long dstOffset, long dataSize, uint data) => vkCmdFillBuffer(_Handle, dstBuffer, dstOffset, dataSize, data);

        public unsafe void CmdCopyImage(VkImage srcImage, ImageLayout srcImageLayout, VkImage dstImage, ImageLayout dstImageLayout, ImageCopy[] regions)
        {
            fixed (ImageCopy* ptr = regions) vkCmdCopyImage(_Handle, srcImage, srcImageLayout, dstImage, dstImageLayout, (uint)(regions?.Length ?? 0), ptr);
        }

        public unsafe void CmdBlitImage(VkImage srcImage, ImageLayout srcImageLayout, VkImage dstImage, ImageLayout dstImageLayout, ImageBlit[] regions, Filter filter)
        {
            fixed (ImageBlit* ptr = regions) vkCmdBlitImage(_Handle, srcImage, srcImageLayout, dstImage, dstImageLayout, (uint)(regions?.Length ?? 0), ptr, filter);
        }

        public unsafe void CmdCopyImageToBuffer(VkImage srcImage, ImageLayout srcImageLayout, VkBuffer dstBuffer, BufferImageCopy[] regions)
        {
            fixed (BufferImageCopy* ptr = regions) vkCmdCopyImageToBuffer(_Handle, srcImage, srcImageLayout, dstBuffer, (uint)(regions?.Length ?? 0), ptr);
        }

        public unsafe void CmdResolveImage(VkImage srcImage, ImageLayout srcImageLayout, VkImage dstImage, ImageLayout dstImageLayout, ImageResolve[] regions)
        {
            fixed (ImageResolve* ptr = regions) vkCmdResolveImage(_Handle, srcImage, srcImageLayout, dstImage, dstImageLayout, (uint)(regions?.Length ?? 0), ptr);
        }

        public unsafe void CmdCopyBufferToImage(VkBuffer srcBuffer, VkImage dstImage, ImageLayout dstImageLayout, BufferImageCopy[] regions)
        {
            fixed (BufferImageCopy* ptr = regions) vkCmdCopyBufferToImage(_Handle, srcBuffer, dstImage, dstImageLayout, (uint)(regions?.Length ?? 0), ptr);
        }

        public unsafe void CmdPushConstants(VkPipelineLayout pipelineLayout, ShaderStages stageFlags, uint offset, uint size, IntPtr values) => vkCmdPushConstants(_Handle, pipelineLayout, stageFlags, offset, size, values);

        public unsafe void CmdClearColorImage(VkImage image, ImageLayout imageLayout, ClearColorValue clearColorValue, ImageSubresourceRange[] ranges)
        {
            fixed (ImageSubresourceRange* ptr = ranges) vkCmdClearColorImage(_Handle, image, imageLayout, &clearColorValue, (uint)(ranges?.Length ?? 0), ptr);
        }

        public unsafe void CmdClearDepthStencilImage(VkImage image, ImageLayout imageLayout, ClearDepthStencilValue clearDepthStencilValue, ImageSubresourceRange[] ranges)
        {
            fixed (ImageSubresourceRange* ptr = ranges) vkCmdClearDepthStencilImage(_Handle, image, imageLayout, &clearDepthStencilValue, (uint)(ranges?.Length ?? 0), ptr);
        }

        public unsafe void CmdSetEvent(VkEvent @event, PipelineStages stageMask) => vkCmdSetEvent(_Handle, @event, stageMask);

        public unsafe void CmdResetEvent(VkEvent @event, PipelineStages stageMask) => vkCmdResetEvent(_Handle, @event, stageMask);

        public unsafe void CmdBeginQuery(VkQueryPool queryPool, uint query, QueryControlFlags flags) => vkCmdBeginQuery(_Handle, queryPool, query, flags);

        public unsafe void CmdEndQuery(VkQueryPool queryPool, uint query) => vkCmdEndQuery(_Handle, queryPool, query);

        public unsafe void CmdResetQueryPool(VkQueryPool queryPool, uint firstQuery, uint queryCount) => vkCmdResetQueryPool(_Handle, queryPool, firstQuery, queryCount);

        public unsafe void CmdCopyQueryPoolResults(VkQueryPool queryPool, uint firstQuery, uint queryCount, VkBuffer dstBuffer, long dstOffset, long stride, QueryResults flags) => vkCmdCopyQueryPoolResults(_Handle, queryPool, firstQuery, queryCount, dstBuffer, dstOffset, stride, flags);
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, CommandBufferAllocateInfo*, IntPtr*, VkResult> vkAllocateCommandBuffers = (delegate* unmanaged[Cdecl]<IntPtr, CommandBufferAllocateInfo*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkAllocateCommandBuffers");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, void> vkFreeCommandBuffers = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, IntPtr*, void>)Vulkan.GetStaticProcPointer("vkFreeCommandBuffers");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, CommandBufferBeginInfo.Native*, VkResult> vkBeginCommandBuffer = (delegate* unmanaged[Cdecl]<IntPtr, CommandBufferBeginInfo.Native*, VkResult>)Vulkan.GetStaticProcPointer("vkBeginCommandBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, VkResult> vkEndCommandBuffer = (delegate* unmanaged[Cdecl]<IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkEndCommandBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, CommandBufferResetFlags, VkResult> vkResetCommandBuffer = (delegate* unmanaged[Cdecl]<IntPtr, CommandBufferResetFlags, VkResult>)Vulkan.GetStaticProcPointer("vkResetCommandBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PipelineBindPoint, IntPtr, void> vkCmdBindPipeline = (delegate* unmanaged[Cdecl]<IntPtr, PipelineBindPoint, IntPtr, void>)Vulkan.GetStaticProcPointer("vkCmdBindPipeline");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint, VkViewport*, void> vkCmdSetViewport = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, VkViewport*, void>)Vulkan.GetStaticProcPointer("vkCmdSetViewport");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint, VkRect2D*, void> vkCmdSetScissor = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, VkRect2D*, void>)Vulkan.GetStaticProcPointer("vkCmdSetScissor");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, float, void> vkCmdSetLineWidth = (delegate* unmanaged[Cdecl]<IntPtr, float, void>)Vulkan.GetStaticProcPointer("vkCmdSetLineWidth");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, float, float, float, void> vkCmdSetDepthBias = (delegate* unmanaged[Cdecl]<IntPtr, float, float, float, void>)Vulkan.GetStaticProcPointer("vkCmdSetDepthBias");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, ColorF4, void> vkCmdSetBlendConstants = (delegate* unmanaged[Cdecl]<IntPtr, ColorF4, void>)Vulkan.GetStaticProcPointer("vkCmdSetBlendConstants");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, float, float, void> vkCmdSetDepthBounds = (delegate* unmanaged[Cdecl]<IntPtr, float, float, void>)Vulkan.GetStaticProcPointer("vkCmdSetDepthBounds");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, StencilFaces, uint, void> vkCmdSetStencilCompareMask = (delegate* unmanaged[Cdecl]<IntPtr, StencilFaces, uint, void>)Vulkan.GetStaticProcPointer("vkCmdSetStencilCompareMask");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, StencilFaces, uint, void> vkCmdSetStencilWriteMask = (delegate* unmanaged[Cdecl]<IntPtr, StencilFaces, uint, void>)Vulkan.GetStaticProcPointer("vkCmdSetStencilWriteMask");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, StencilFaces, uint, void> vkCmdSetStencilReference = (delegate* unmanaged[Cdecl]<IntPtr, StencilFaces, uint, void>)Vulkan.GetStaticProcPointer("vkCmdSetStencilReference");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PipelineBindPoint, IntPtr, uint, uint, IntPtr*, uint, uint*, void> vkCmdBindDescriptorSets = (delegate* unmanaged[Cdecl]<IntPtr, PipelineBindPoint, IntPtr, uint, uint, IntPtr*, uint, uint*, void>)Vulkan.GetStaticProcPointer("vkCmdBindDescriptorSets");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, IndexType, void> vkCmdBindIndexBuffer = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, IndexType, void>)Vulkan.GetStaticProcPointer("vkCmdBindIndexBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint, IntPtr*, long*, void> vkCmdBindVertexBuffers = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, IntPtr*, long*, void>)Vulkan.GetStaticProcPointer("vkCmdBindVertexBuffers");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint, uint, uint, void> vkCmdDraw = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, uint, uint, void>)Vulkan.GetStaticProcPointer("vkCmdDraw");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint, uint, int, uint, void> vkCmdDrawIndexed = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, uint, int, uint, void>)Vulkan.GetStaticProcPointer("vkCmdDrawIndexed");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint, uint, void> vkCmdDispatch = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint, uint, void>)Vulkan.GetStaticProcPointer("vkCmdDispatch");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, ClearAttachment*, uint, ClearRect*, void> vkCmdClearAttachments = (delegate* unmanaged[Cdecl]<IntPtr, uint, ClearAttachment*, uint, ClearRect*, void>)Vulkan.GetStaticProcPointer("vkCmdClearAttachments");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, PipelineStages, PipelineStages, uint, MemoryBarrier*, uint, BufferMemoryBarrier*, uint, ImageMemoryBarrier*, void> vkCmdWaitEvents = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, PipelineStages, PipelineStages, uint, MemoryBarrier*, uint, BufferMemoryBarrier*, uint, ImageMemoryBarrier*, void>)Vulkan.GetStaticProcPointer("vkCmdWaitEvents");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PipelineStages, PipelineStages, Dependencies, uint, MemoryBarrier*, uint, BufferMemoryBarrier*, uint, ImageMemoryBarrier*, void> vkCmdPipelineBarrier = (delegate* unmanaged[Cdecl]<IntPtr, PipelineStages, PipelineStages, Dependencies, uint, MemoryBarrier*, uint, BufferMemoryBarrier*, uint, ImageMemoryBarrier*, void>)Vulkan.GetStaticProcPointer("vkCmdPipelineBarrier");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PipelineStages, IntPtr, uint, void> vkCmdWriteTimestamp = (delegate* unmanaged[Cdecl]<IntPtr, PipelineStages, IntPtr, uint, void>)Vulkan.GetStaticProcPointer("vkCmdWriteTimestamp");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, RenderPassBeginInfo.Native*, SubpassContents, void> vkCmdBeginRenderPass = (delegate* unmanaged[Cdecl]<IntPtr, RenderPassBeginInfo.Native*, SubpassContents, void>)Vulkan.GetStaticProcPointer("vkCmdBeginRenderPass");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, SubpassContents, void> vkCmdNextSubpass = (delegate* unmanaged[Cdecl]<IntPtr, SubpassContents, void>)Vulkan.GetStaticProcPointer("vkCmdNextSubpass");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, void> vkCmdEndRenderPass = (delegate* unmanaged[Cdecl]<IntPtr, void>)Vulkan.GetStaticProcPointer("vkCmdEndRenderPass");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, void> vkCmdExecuteCommands = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, void>)Vulkan.GetStaticProcPointer("vkCmdExecuteCommands");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, uint, uint, void> vkCmdDrawIndirect = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, uint, uint, void>)Vulkan.GetStaticProcPointer("vkCmdDrawIndirect");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, uint, uint, void> vkCmdDrawIndexedIndirect = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, uint, uint, void>)Vulkan.GetStaticProcPointer("vkCmdDrawIndexedIndirect");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, void> vkCmdDispatchIndirect = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, void>)Vulkan.GetStaticProcPointer("vkCmdDispatchIndirect");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, uint, BufferCopy*, void> vkCmdCopyBuffer = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, uint, BufferCopy*, void>)Vulkan.GetStaticProcPointer("vkCmdCopyBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, long, IntPtr, void> vkCmdUpdateBuffer = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, long, IntPtr, void>)Vulkan.GetStaticProcPointer("vkCmdUpdateBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, long, uint, void> vkCmdFillBuffer = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, long, uint, void>)Vulkan.GetStaticProcPointer("vkCmdFillBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, ImageLayout, uint, ImageCopy*, void> vkCmdCopyImage = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, ImageLayout, uint, ImageCopy*, void>)Vulkan.GetStaticProcPointer("vkCmdCopyImage");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, ImageLayout, uint, ImageBlit*, Filter, void> vkCmdBlitImage = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, ImageLayout, uint, ImageBlit*, Filter, void>)Vulkan.GetStaticProcPointer("vkCmdBlitImage");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, uint, BufferImageCopy*, void> vkCmdCopyImageToBuffer = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, uint, BufferImageCopy*, void>)Vulkan.GetStaticProcPointer("vkCmdCopImageToBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, ImageLayout, uint, ImageResolve*, void> vkCmdResolveImage = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, IntPtr, ImageLayout, uint, ImageResolve*, void>)Vulkan.GetStaticProcPointer("vkCmdResolveImage");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, ImageLayout, uint, BufferImageCopy*, void> vkCmdCopyBufferToImage = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, ImageLayout, uint, BufferImageCopy*, void>)Vulkan.GetStaticProcPointer("vkCmdCopyBufferToImage");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ShaderStages, uint, uint, IntPtr, void> vkCmdPushConstants = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ShaderStages, uint, uint, IntPtr, void>)Vulkan.GetStaticProcPointer("vkCmdPushConstants");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, ClearColorValue*, uint, ImageSubresourceRange*, void> vkCmdClearColorImage = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, ClearColorValue*, uint, ImageSubresourceRange*, void>)Vulkan.GetStaticProcPointer("vkCmdClearColorImage");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, ClearDepthStencilValue*, uint, ImageSubresourceRange*, void> vkCmdClearDepthStencilImage = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ImageLayout, ClearDepthStencilValue*, uint, ImageSubresourceRange*, void>)Vulkan.GetStaticProcPointer("vkCmdClearDepthStencilImage");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, PipelineStages, void> vkCmdSetEvent = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, PipelineStages, void>)Vulkan.GetStaticProcPointer("vkCmdSetEvent");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, PipelineStages, void> vkCmdResetEvent = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, PipelineStages, void>)Vulkan.GetStaticProcPointer("vkCmdResetEvent");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, QueryControlFlags, void> vkCmdBeginQuery = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, QueryControlFlags, void>)Vulkan.GetStaticProcPointer("vkCmdBeginQuery");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, void> vkCmdEndQuery = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, void>)Vulkan.GetStaticProcPointer("vkCmdEndQuery");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, uint, void> vkCmdResetQueryPool = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, uint, void>)Vulkan.GetStaticProcPointer("vkCmdResetQueryPool");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, uint, IntPtr, long, long, QueryResults, void> vkCmdCopyQueryPoolResults = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, uint, IntPtr, long, long, QueryResults, void>)Vulkan.GetStaticProcPointer("vkCmdCopyQueryPoolResults");
        #endregion

        #region Ext C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerMarkerInfoExt.Native*, void> vkCmdDebugMarkerBeginEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, void> vkCmdDebugMarkerEndEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DebugMarkerMarkerInfoExt.Native*, void> vkCmdDebugMarkerInstertEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, uint, uint, VkRect2D*, void> vkCmdSetDiscardRectangleEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, SampleLocationsInfoExt*, void> vkCmdSetSampleLocationsEXT;
        #endregion

        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                Free();
                _Handle = IntPtr.Zero;

                disposedValue = true;
            }
        }

        // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~VkCommandBuffer()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }

    // Structs

    /// <summary>
    /// Structure specifying command buffer inheritance info.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CommandBufferInheritanceInfo
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// A <see cref="Magma.Vulkan.VkRenderPass"/> object defining which render passes the <see
        /// cref="VkCommandBuffer"/> will be compatible with and can be executed within. If the <see
        /// cref="VkCommandBuffer"/> will not be executed within a render pass instance, <see
        /// cref="RenderPass"/> is ignored.
        /// </summary>
        public long RenderPass;
        /// <summary>
        /// The index of the subpass within the render pass instance that the <see
        /// cref="VkCommandBuffer"/> will be executed within. If the <see cref="VkCommandBuffer"/> will
        /// not be executed within a render pass instance, subpass is ignored.
        /// </summary>
        public int Subpass;
        /// <summary>
        /// Optionally refers to the <see cref="Magma.Vulkan.VkFramebuffer"/> object that the <see
        /// cref="VkCommandBuffer"/> will be rendering to if it is executed within a render pass
        /// instance. It can be 0 if the framebuffer is not known, or if the <see
        /// cref="VkCommandBuffer"/> will not be executed within a render pass instance.
        /// <para>
        /// Specifying the exact framebuffer that the secondary command buffer will be executed with
        /// may result in better performance at command buffer execution time.
        /// </para>
        /// </summary>
        public long Framebuffer;
        /// <summary>
        /// Indicates whether the command buffer can be executed while an occlusion query is active
        /// in the primary command buffer. If this is <c>true</c>, then this command buffer can be
        /// executed whether the primary command buffer has an occlusion query active or not. If this
        /// is <c>false</c>, then the primary command buffer must not have an occlusion query active.
        /// </summary>
        public bool OcclusionQueryEnable;
        /// <summary>
        /// Indicates the query flags that can be used by an active occlusion query in the primary
        /// command buffer when this secondary command buffer is executed. If this value includes the
        /// <see cref="QueryControlFlags.Precise"/> bit, then the active query can return boolean
        /// results or actual sample counts. If this bit is not set, then the active query must not
        /// use the <see cref="QueryControlFlags.Precise"/> bit.
        /// </summary>
        public QueryControlFlags QueryFlags;
        /// <summary>
        /// Specifies the set of pipeline statistics that can be counted by an active query in the
        /// primary command buffer when this secondary command buffer is executed. If this value
        /// includes a given bit, then this command buffer can be executed whether the primary
        /// command buffer has a pipeline statistics query active that includes this bit or not. If
        /// this value excludes a given bit, then the active pipeline statistics query must not be
        /// from a query pool that counts that statistic.
        /// </summary>
        public QueryPipelineStatistics PipelineStatistics;

        internal void Prepare()
        {
            Type = StructureType.CommandBufferInheritanceInfo;
        }
    }

    /// <summary>
    /// Bitmask controlling behavior of a command buffer reset.
    /// </summary>
    [Flags]
    public enum CommandBufferResetFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that most or all memory resources currently owned by the command buffer should
        /// be returned to the parent command pool.
        /// <para>
        /// If this flag is not set, then the command buffer may hold onto memory resources and reuse
        /// them when recording commands.
        /// </para>
        /// <para>Command buffer is moved to the initial state.</para>
        /// </summary>
        ReleaseResources = 1 << 0
    }

    /// <summary>
    /// Structure specifying a command buffer begin operation.
    /// </summary>
    public unsafe struct CommandBufferBeginInfo
    {
        /// <summary>
        /// A bitmask specifying usage behavior for the command buffer.
        /// </summary>
        public CommandBufferUsages Flags;
        /// <summary>
        /// Structure, which is used if command buffer is a secondary command buffer.
        /// <para>If it is a primary command buffer, then this value is ignored.</para>
        /// </summary>
        public CommandBufferInheritanceInfo? InheritanceInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandBufferBeginInfo"/> structure.
        /// </summary>
        /// <param name="flags">A bitmask specifying usage behavior for the command buffer.</param>
        /// <param name="inheritanceInfo">
        /// Structure, which is used if command buffer is a secondary command buffer.
        /// <para>If it is a primary command buffer, then this value is ignored.</para>
        /// </param>
        public CommandBufferBeginInfo(CommandBufferUsages flags = 0,
            CommandBufferInheritanceInfo? inheritanceInfo = null)
        {
            Flags = flags;
            InheritanceInfo = inheritanceInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public CommandBufferUsages Flags;
            public CommandBufferInheritanceInfo* InheritanceInfo;

            public void Free()
            {
                Interop.Free(InheritanceInfo);
            }
        }

        internal void ToNative(out Native native)
        {
            var inheritanceInfo = (CommandBufferInheritanceInfo*)Interop.Alloc<CommandBufferInheritanceInfo>();
            if (InheritanceInfo.HasValue)
                inheritanceInfo->Prepare();

            native.Type = StructureType.CommandBufferBeginInfo;
            native.Next = IntPtr.Zero;
            native.Flags = Flags;
            native.InheritanceInfo = inheritanceInfo;
        }
    }

    /// <summary>
    /// Bitmask specifying usage behavior for command buffer.
    /// </summary>
    [Flags]
    public enum CommandBufferUsages
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that each recording of the command buffer will only be submitted once, and the
        /// command buffer will be reset and recorded again between each submission.
        /// </summary>
        OneTimeSubmit = 1 << 0,
        /// <summary>
        /// Specifies that a secondary command buffer is considered to be entirely inside a render pass.
        /// <para>If this is a primary command buffer, then this bit is ignored.</para>
        /// </summary>
        RenderPassContinue = 1 << 1,
        /// <summary>
        /// Specifies that a command buffer can be resubmitted to a queue while it is in the pending
        /// State, and recorded into multiple primary command buffers.
        /// </summary>
        SimultaneousUse = 1 << 2
    }

    /// <summary>
    /// Structure specifying the allocation parameters for command buffer object.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CommandBufferAllocateInfo
    {
        internal StructureType Type;
        internal IntPtr Next;
        internal IntPtr CommandPool;

        /// <summary>
        /// Specifies the command buffer level.
        /// </summary>
        public CommandBufferLevel Level;
        /// <summary>
        /// The number of command buffers to allocate from the pool.
        /// </summary>
        public int CommandBufferCount;

        /// <summary>
        /// Inititializes a new instance of the <see cref="VkCommandBuffer"/> structure.
        /// </summary>
        /// <param name="level">Specifies the command buffer level.</param>
        /// <param name="count">The number of command buffers to allocate from the pool.</param>
        public CommandBufferAllocateInfo(CommandBufferLevel level, int count)
        {
            Type = StructureType.CommandBufferAllocateInfo;
            Next = IntPtr.Zero;
            CommandPool = IntPtr.Zero;
            Level = level;
            CommandBufferCount = count;
        }

        internal void Prepare(IntPtr pool)
        {
            Type = StructureType.CommandBufferAllocateInfo;
            CommandPool = pool;
        }
    }

    /// <summary>
    /// Enumerant specifying a command buffer level.
    /// </summary>
    public enum CommandBufferLevel
    {
        /// <summary>
        /// Specifies a primary command buffer.
        /// </summary>
        Primary = 0,
        /// <summary>
        /// Specifies a secondary command buffer.
        /// </summary>
        Secondary = 1
    }

    /// <summary>
    /// Structure specifying color value when the format of the image or attachment is one of the
    /// formats other than signed integer or unsigned integer. Floating point values are
    /// automatically converted to the format of the image.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ColorF4
    {
        /// <summary>
        /// Gets a <see cref="ColorF4"/> with all of its components set to zero.
        /// </summary>
        public static ColorF4 Zero => new ColorF4();

        /// <summary>
        /// The red component of the color.
        /// </summary>
        public float R;
        /// <summary>
        /// The green component of the color.
        /// </summary>
        public float G;
        /// <summary>
        /// The blue component of the color.
        /// </summary>
        public float B;
        /// <summary>
        /// The alpha component of the color.
        /// </summary>
        public float A;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorF4"/> structure.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public ColorF4(float r, float g, float b, float a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }
    }

    /// <summary>
    /// Structure specifying color value when the format of the image or attachment is signed
    /// integer. Signed integer values are converted to the format of the image by casting to the
    /// smaller type (with negative 32-bit values mapping to negative values in the smaller type). If
    /// the integer value is not representable in the target type (e.g. would overflow in conversion
    /// to that type), the value is undefined.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ColorI4
    {
        /// <summary>
        /// Gets a <see cref="ColorI4"/> with all of its components set to zero.
        /// </summary>
        public static ColorI4 Zero => new ColorI4();

        /// <summary>
        /// The red component of the color.
        /// </summary>
        public int R;
        /// <summary>
        /// The green component of the color.
        /// </summary>
        public int G;
        /// <summary>
        /// The blue component of the color.
        /// </summary>
        public int B;
        /// <summary>
        /// The alpha component of the color.
        /// </summary>
        public int A;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorI4"/> structure.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public ColorI4(int r, int g, int b, int a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }
    }

    /// <summary>
    /// Structure specifying color value when the format of the image or attachment is unsigned
    /// integer. Unsigned integer values are converted to the format of the image by casting to the
    /// integer type with fewer bits.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ColorU4
    {
        /// <summary>
        /// Gets a <see cref="ColorU4"/> with all of its components set to zero.
        /// </summary>
        public static ColorU4 Zero => new ColorU4();

        /// <summary>
        /// The red component of the color.
        /// </summary>
        public uint R;
        /// <summary>
        /// The green component of the color.
        /// </summary>
        public uint G;
        /// <summary>
        /// The blue component of the color.
        /// </summary>
        public uint B;
        /// <summary>
        /// The alpha component of the color.
        /// </summary>
        public uint A;

        /// <summary>
        /// Initializes a new instance of the <see cref="ColorU4"/> structure.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public ColorU4(uint r, uint g, uint b, uint a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }
    }

    /// <summary>
    /// Bitmask specifying sets of stencil state for which to update the compare mask.
    /// </summary>
    [Flags]
    public enum StencilFaces
    {
        /// <summary>
        /// Specifies that only the front set of stencil state is updated.
        /// </summary>
        Front = 1 << 0,
        /// <summary>
        /// Specifies that only the back set of stencil state is updated.
        /// </summary>
        Back = 1 << 1,
        /// <summary>
        /// Is the combination of <see cref="Front"/> and <see cref="Back"/>, and specifies that both
        /// sets of stencil state are updated.
        /// </summary>
        StencilFrontAndBack = 0x00000003
    }

    /// <summary>
    /// Specify the bind point of a pipeline object to a command buffer.
    /// <para>
    /// There are separate bind points for each of graphics and compute, so binding one does not
    /// disturb the other.
    /// </para>
    /// </summary>
    public enum PipelineBindPoint
    {
        /// <summary>
        /// Specifies binding as a graphics pipeline.
        /// </summary>
        Graphics = 0,
        /// <summary>
        /// Specifies binding as a compute pipeline.
        /// </summary>
        Compute = 1
    }

    /// <summary>
    /// Type of index buffer indices.
    /// </summary>
    public enum IndexType
    {
        /// <summary>
        /// Specifies that indices are 16-bit unsigned integer values.
        /// </summary>
        UInt16 = 0,
        /// <summary>
        /// Specifies that indices are 32-bit unsigned integer values.
        /// </summary>
        UInt32 = 1
    }

    /// <summary>
    /// Structure specifying a clear attachment.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ClearAttachment
    {
        /// <summary>
        /// A mask selecting the color, depth and/or stencil aspects of the attachment to be cleared.
        /// <see cref="AspectMask"/> can include <see cref="ImageAspects.Color"/> for color
        /// attachments, <see cref="ImageAspects.Depth"/> for depth/stencil attachments with a
        /// depth component, and <see cref="ImageAspects.Stencil"/> for depth/stencil attachments
        /// with a stencil component. If the subpass's depth/stencil attachment is <see
        /// cref="AttachmentUnused"/>, then the clear has no effect.
        /// <para>Must not include <see cref="ImageAspects.Metadata"/>.</para>
        /// </summary>
        public ImageAspects AspectMask;
        /// <summary>
        /// Is only meaningful if <see cref="ImageAspects.Color"/> is set in <see
        /// cref="AspectMask"/>, in which case it is an index to the <see
        /// cref="SubpassDescription.ColorAttachments"/> array in the of the current subpass which
        /// selects the color attachment to clear. If <see cref="ColorAttachment"/> is <see
        /// cref="AttachmentUnused"/> then the clear has no effect.
        /// </summary>
        public int ColorAttachment;
        /// <summary>
        /// The color or depth/stencil value to clear the attachment to.
        /// </summary>
        public ClearValue ClearValue;
    }

    /// <summary>
    /// Structure specifying a clear value.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct ClearValue
    {
        /// <summary>
        /// Specifies the color image clear values to use when clearing a color image or attachment.
        /// </summary>
        [FieldOffset(0)] public ClearColorValue Color;
        /// <summary>
        /// Specifies the depth and stencil clear values to use when clearing a depth/stencil image
        /// or attachment.
        /// </summary>
        [FieldOffset(0)] public ClearDepthStencilValue DepthStencil;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearValue"/> structure.
        /// </summary>
        /// <param name="color">
        /// Specifies the color image clear values to use when clearing a color image or attachment.
        /// </param>
        public ClearValue(ClearColorValue color) : this()
        {
            Color = color;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearValue"/> structure.
        /// </summary>
        /// <param name="depthStencil">
        /// Specifies the depth and stencil clear values to use when clearing a depth/stencil image
        /// or attachment.
        /// </param>
        public ClearValue(ClearDepthStencilValue depthStencil) : this()
        {
            DepthStencil = depthStencil;
        }

        /// <summary>
        /// Implicitly converts an instance of <see cref="ClearColorValue"/> to an instance of <see cref="ClearValue"/>.
        /// </summary>
        /// <param name="value">Instance to convert.</param>
        public static implicit operator ClearValue(ClearColorValue value) => new ClearValue(value);

        /// <summary>
        /// Implicitly converts an instance of <see cref="ClearDepthStencilValue"/> to an instance of
        /// <see cref="ClearValue"/>.
        /// </summary>
        /// <param name="value">Instance to convert.</param>
        public static implicit operator ClearValue(ClearDepthStencilValue value) => new ClearValue(value);
    }

    /// <summary>
    /// Structure specifying a clear color value.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct ClearColorValue
    {
        /// <summary>
        /// Are the color clear values when the format of the image or attachment is one of the
        /// formats other than signed integer or unsigned integer. Floating point values are
        /// automatically converted to the format of the image, with the clear value being treated as
        /// linear if the image is sRGB.
        /// </summary>
        [FieldOffset(0)] public ColorF4 Float4;
        /// <summary>
        /// Are the color clear values when the format of the image or attachment is signed integer.
        /// Signed integer values are converted to the format of the image by casting to the smaller
        /// type (with negative 32-bit values mapping to negative values in the smaller type). If the
        /// integer clear value is not representable in the target type (e.g. would overflow in
        /// conversion to that type), the clear value is undefined.
        /// </summary>
        [FieldOffset(0)] public ColorI4 Int4;
        /// <summary>
        /// Are the color clear values when the format of the image or attachment is unsigned
        /// integer. Unsigned integer values are converted to the format of the image by casting to
        /// the integer type with fewer bits.
        /// </summary>
        [FieldOffset(0)] public ColorU4 UInt4;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearColorValue"/> structure.
        /// </summary>
        /// <param name="value">
        /// Are the color clear values when the format of the image or attachment is one of the
        /// formats other than signed integer or unsigned integer. Floating point values are
        /// automatically converted to the format of the image, with the clear value being treated as
        /// linear if the image is sRGB.
        /// </param>
        public ClearColorValue(ColorF4 value) : this()
        {
            Float4 = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearColorValue"/> structure.
        /// </summary>
        /// <param name="value">
        /// Are the color clear values when the format of the image or attachment is signed integer.
        /// Signed integer values are converted to the format of the image by casting to the smaller
        /// type (with negative 32-bit values mapping to negative values in the smaller type). If the
        /// integer clear value is not representable in the target type (e.g. would overflow in
        /// conversion to that type), the clear value is undefined.
        /// </param>
        public ClearColorValue(ColorI4 value) : this()
        {
            Int4 = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearColorValue"/> structure.
        /// </summary>
        /// <param name="value">
        /// Are the color clear values when the format of the image or attachment is unsigned
        /// integer. Unsigned integer values are converted to the format of the image by casting to
        /// the integer type with fewer bits.
        /// </param>
        public ClearColorValue(ColorU4 value) : this()
        {
            UInt4 = value;
        }
    }

    /// <summary>
    /// Structure specifying a clear depth stencil value.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ClearDepthStencilValue
    {
        /// <summary>
        /// The clear value for the depth aspect of the depth/stencil attachment. It is a
        /// floating-point value which is automatically converted to the attachment’s format.
        /// <para>Must be between 0.0 and 1.0, inclusive.</para>
        /// </summary>
        public float Depth;
        /// <summary>
        /// The clear value for the stencil aspect of the depth/stencil attachment. It is a 32-bit
        /// integer value which is converted to the attachment's format by taking the appropriate
        /// number of LSBs.
        /// </summary>
        public int Stencil;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearDepthStencilValue"/> structure.
        /// </summary>
        /// <param name="depth">
        /// The clear value for the depth aspect of the depth/stencil attachment. It is a
        /// floating-point value which is automatically converted to the attachment’s format.
        /// </param>
        /// <param name="stencil">
        /// The clear value for the stencil aspect of the depth/stencil attachment. It is a 32-bit
        /// integer value which is converted to the attachment's format by taking the appropriate
        /// number of LSBs.
        /// </param>
        public ClearDepthStencilValue(float depth, int stencil)
        {
            Depth = depth;
            Stencil = stencil;
        }
    }

    /// <summary>
    /// Structure specifying a clear rectangle.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ClearRect
    {
        /// <summary>
        /// The two-dimensional region to be cleared.
        /// </summary>
        public VkRect2D Rect;
        /// <summary>
        /// The first layer to be cleared.
        /// </summary>
        public int BaseArrayLayer;
        /// <summary>
        /// The number of layers to clear.
        /// </summary>
        public int LayerCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClearRect"/> structure.
        /// </summary>
        /// <param name="rect">The two-dimensional region to be cleared.</param>
        /// <param name="baseArrayLayer">The first layer to be cleared.</param>
        /// <param name="layerCount">The number of layers to clear.</param>
        public ClearRect(VkRect2D rect, int baseArrayLayer, int layerCount)
        {
            Rect = rect;
            BaseArrayLayer = baseArrayLayer;
            LayerCount = layerCount;
        }
    }

    /// <summary>
    /// Bitmask specifying pipeline stages.
    /// </summary>
    [Flags]
    public enum PipelineStages
    {
        /// <summary>
        /// Specifies the stage of the pipeline where any commands are initially received by the queue.
        /// </summary>
        TopOfPipe = 1 << 0,
        /// <summary>
        /// Specifies the stage of the pipeline where Draw/DispatchIndirect data structures are consumed.
        /// <para>This stage also includes reading commands written by <see cref="Nvx.CommandBufferExtensions.CmdProcessCommandsNvx"/>.</para>
        /// </summary>
        DrawIndirect = 1 << 1,
        /// <summary>
        /// Specifies the stage of the pipeline where vertex and index buffers are consumed.
        /// </summary>
        VertexInput = 1 << 2,
        /// <summary>
        /// Specifies the vertex shader stage.
        /// </summary>
        VertexShader = 1 << 3,
        /// <summary>
        /// Specifies the tessellation control shader stage.
        /// </summary>
        TessellationControlShader = 1 << 4,
        /// <summary>
        /// Specifies the tessellation evaluation shader stage.
        /// </summary>
        TessellationEvaluationShader = 1 << 5,
        /// <summary>
        /// Specifies the geometry shader stage.
        /// </summary>
        GeometryShader = 1 << 6,
        /// <summary>
        /// Specifies the fragment shader stage.
        /// </summary>
        FragmentShader = 1 << 7,
        /// <summary>
        /// Specifies the stage of the pipeline where early fragment tests (depth and stencil tests
        /// before fragment
        /// shading) are performed. This stage also includes subpass load operations for framebuffer
        /// attachments with a depth/stencil format.
        /// </summary>
        EarlyFragmentTests = 1 << 8,
        /// <summary>
        /// Specifies that the stage of the pipeline where late fragment tests (depth and stencil
        /// tests after fragment shading) are performed.
        /// <para>
        /// This stage also includes subpass store operations for framebuffer attachments with a
        /// depth/stencil format.
        /// </para>
        /// </summary>
        LateFragmentTests = 1 << 9,
        /// <summary>
        /// Specifies that the stage of the pipeline after blending where the final color values are
        /// output from the pipeline.
        /// <para>
        /// This stage also includes subpass load and store operations and multisample resolve
        /// operations for framebuffer attachments with a color format.
        /// </para>
        /// </summary>
        ColorAttachmentOutput = 1 << 10,
        /// <summary>
        /// Specifies the execution of a compute shader.
        /// </summary>
        ComputeShader = 1 << 11,
        /// <summary>
        /// Transfer/copy operations.
        /// </summary>
        Transfer = 1 << 12,
        /// <summary>
        /// Specifies the final stage in the pipeline where operations generated by all commands
        /// complete execution.
        /// </summary>
        BottomOfPipe = 1 << 13,
        /// <summary>
        /// Specifies a pseudo-stage indicating execution on the host of reads/writes of device
        /// memory.
        /// <para>This stage is not invoked by any commands recorded in a command buffer.</para>
        /// </summary>
        Host = 1 << 14,
        /// <summary>
        /// Specifies the execution of all graphics pipeline stages, and is equivalent to the logical
        /// OR of:
        /// <para>
        /// <see cref="TopOfPipe"/>, <see cref="DrawIndirect"/>, <see cref="VertexInput"/>, <see
        /// cref="VertexShader"/>, <see cref="TessellationControlShader"/>, <see
        /// cref="TessellationEvaluationShader"/>, <see cref="GeometryShader"/>, <see
        /// cref="FragmentShader"/>, <see cref="EarlyFragmentTests"/>, <see
        /// cref="LateFragmentTests"/>, <see cref="ColorAttachmentOutput"/>, <see cref="BottomOfPipe"/>.
        /// </para>
        /// </summary>
        AllGraphics = 1 << 15,
        /// <summary>
        /// Equivalent to the logical OR of every other pipeline stage flag that is supported on the
        /// queue it is used with.
        /// </summary>
        AllCommands = 1 << 16,
        /// <summary>
        /// Specifies the stage of the pipeline where device-side generation of commands via <see
        /// cref="Nvx.CommandBufferExtensions.CmdProcessCommandsNvx"/> is handled.
        /// </summary>
        CommandProcessNvx = 1 << 17
    }

    /// <summary>
    /// Structure specifying a global memory barrier.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryBarrier
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// Specifies a source access mask.
        /// </summary>
        public Accesses SrcAccessMask;
        /// <summary>
        /// Specifies a destination access mask.
        /// </summary>
        public Accesses DstAccessMask;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryBarrier"/> structure.
        /// </summary>
        /// <param name="srcAccessMask">Specifies a source access mask.</param>
        /// <param name="dstAccessMask">Specifies a destination access mask.</param>
        public MemoryBarrier(Accesses srcAccessMask, Accesses dstAccessMask)
        {
            Type = StructureType.MemoryBarrier;
            Next = IntPtr.Zero;
            SrcAccessMask = srcAccessMask;
            DstAccessMask = dstAccessMask;
        }

        internal void Prepare()
        {
            Type = StructureType.MemoryBarrier;
        }
    }

    /// <summary>
    /// Bitmask specifying memory access types that will participate in a memory dependency.
    /// </summary>
    [Flags]
    public enum Accesses
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies read access to an indirect command structure read as part of an indirect
        /// drawing or dispatch command.
        /// </summary>
        IndirectCommandRead = 1 << 0,
        /// <summary>
        /// Specifies read access to an index buffer as part of an indexed drawing command, bound by
        /// <see cref="VkCommandBuffer.CmdBindIndexBuffer"/>.
        /// </summary>
        IndexRead = 1 << 1,
        /// <summary>
        /// Specifies read access to a vertex buffer as part of a drawing command, bound by <see cref="VkCommandBuffer.CmdBindVertexBuffers"/>.
        /// </summary>
        VertexAttributeRead = 1 << 2,
        /// <summary>
        /// Specifies read access to a uniform buffer.
        /// </summary>
        UniformRead = 1 << 3,
        /// <summary>
        /// Specifies read access to an input attachment within a render pass during fragment shading.
        /// </summary>
        InputAttachmentRead = 1 << 4,
        /// <summary>
        /// Specifies read access to a storage buffer, uniform texel buffer, storage texel buffer,
        /// sampled image or storage image.
        /// </summary>
        ShaderRead = 1 << 5,
        /// <summary>
        /// Specifies write access to a storage buffer, storage texel buffer or storage image.
        /// </summary>
        ShaderWrite = 1 << 6,
        /// <summary>
        /// Specifies read access to a color attachment, such as via blending, logic operations or
        /// via certain subpass load operations.
        /// </summary>
        ColorAttachmentRead = 1 << 7,
        /// <summary>
        /// Specifies write access to a color or resolve attachment during a render pass or via
        /// certain subpass load and store operations.
        /// </summary>
        ColorAttachmentWrite = 1 << 8,
        /// <summary>
        /// Specifies read access to a depth/stencil attachment via depth or stencil operations or
        /// via certain subpass load operations.
        /// </summary>
        DepthStencilAttachmentRead = 1 << 9,
        /// <summary>
        /// Specifies write access to a depth/stencil attachment via depth or stencil operations or
        /// via certain subpass load and store operations.
        /// </summary>
        DepthStencilAttachmentWrite = 1 << 10,
        /// <summary>
        /// Specifies read access to an image or buffer in a copy operation.
        /// </summary>
        TransferRead = 1 << 11,
        /// <summary>
        /// Specifies write access to an image or buffer in a clear or copy operation.
        /// </summary>
        TransferWrite = 1 << 12,
        /// <summary>
        /// Specifies read access by a host operation. Accesses of this type are not performed
        /// through a resource, but directly on memory.
        /// </summary>
        HostRead = 1 << 13,
        /// <summary>
        /// Specifies write access by a host operation. Accesses of this type are not performed
        /// through a resource, but directly on memory.
        /// </summary>
        HostWrite = 1 << 14,
        /// <summary>
        /// Specifies read access via non-specific entities. These entities include the Vulkan device
        /// and host, but may also include entities external to the Vulkan device or otherwise not
        /// part of the core Vulkan pipeline. When included in a destination access mask, makes all
        /// available writes visible to all future read accesses on entities known to the Vulkan device.
        /// </summary>
        MemoryRead = 1 << 15,
        /// <summary>
        /// Specifies write access via non-specific entities. These entities include the Vulkan
        /// device and host, but may also include entities external to the Vulkan device or otherwise
        /// not part of the core Vulkan pipeline. When included in a source access mask, all writes
        /// that are performed by entities known to the Vulkan device are made available. When
        /// included in a destination access mask, makes all available writes visible to all future
        /// write accesses on entities known to the Vulkan device.
        /// </summary>
        MemoryWrite = 1 << 16,
        /// <summary>
        /// Specifies reads from <see cref="VkBuffer"/> inputs to <see cref="Nvx.CommandBufferExtensions.CmdProcessCommandsNvx"/>.
        /// </summary>
        CommandProcessReadNvx = 1 << 17,
        /// <summary>
        /// Specifies writes to the target command buffer in <see cref="Nvx.CommandBufferExtensions.CmdProcessCommandsNvx"/>.
        /// </summary>
        CommandProcessWriteNvx = 1 << 18,
        /// <summary>
        /// Is similar to <see cref="ColorAttachmentRead"/>, but also includes advanced blend operations.
        /// </summary>
        ColorAttachmentReadNoncoherentExt = 1 << 19
    }

    /// <summary>
    /// Structure specifying a buffer memory barrier.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BufferMemoryBarrier
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// Specifies a source access mask.
        /// </summary>
        public Accesses SrcAccessMask;
        /// <summary>
        /// Specifies a destination access mask.
        /// </summary>
        public Accesses DstAccessMask;
        /// <summary>
        /// The source queue family for a queue family ownership transfer.
        /// </summary>
        public int SrcQueueFamilyIndex;
        /// <summary>
        /// The destination queue family for a queue family ownership transfer.
        /// </summary>
        public int DstQueueFamilyIndex;
        /// <summary>
        /// A <see cref="Magma.Vulkan.VkBuffer"/> handle to the buffer whose backing memory is affected by the barrier.
        /// </summary>
        public IntPtr Buffer;
        /// <summary>
        /// An offset in bytes into the backing memory for buffer; this is relative to the base
        /// offset as bound to the buffer.
        /// </summary>
        public long Offset;
        /// <summary>
        /// A size in bytes of the affected area of backing memory for buffer, or <see
        /// cref="WholeSize"/> to use the range from offset to the end of the buffer.
        /// </summary>
        public long Size;

        /// <summary>
        /// Initializes a new instance of the <see cref="BufferMemoryBarrier"/> structure.
        /// </summary>
        /// <param name="buffer">
        /// A <see cref="Magma.Vulkan.VkBuffer"/> handle to the buffer whose backing memory is affected
        /// by the barrier.
        /// </param>
        /// <param name="srcAccessMask">Specifies a source access mask.</param>
        /// <param name="dstAccessMask">Specifies a destination access mask.</param>
        /// <param name="offset">
        /// An offset in bytes into the backing memory for buffer; this is relative to the base
        /// offset as bound to the buffer.
        /// </param>
        /// <param name="size">
        /// A size in bytes of the affected area of backing memory for buffer, or <see
        /// cref="WholeSize"/> to use the range from offset to the end of the buffer.
        /// </param>
        public BufferMemoryBarrier(VkBuffer buffer, Accesses srcAccessMask, Accesses dstAccessMask, long offset = 0, long size = WholeSize)
            : this(buffer, srcAccessMask, dstAccessMask, QueueFamilyIgnored, QueueFamilyIgnored, offset, size)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BufferMemoryBarrier"/> structure.
        /// </summary>
        /// <param name="buffer">
        /// A <see cref="Magma.Vulkan.VkBuffer"/> handle to the buffer whose backing memory is affected
        /// by the barrier.
        /// </param>
        /// <param name="srcAccessMask">Specifies a source access mask.</param>
        /// <param name="dstAccessMask">Specifies a destination access mask.</param>
        /// <param name="srcQueueFamilyIndex">
        /// The source queue family for a queue family ownership transfer.
        /// </param>
        /// <param name="dstQueueFamilyIndex">
        /// The destination queue family for a queue family ownership transfer.
        /// </param>
        /// <param name="offset">
        /// An offset in bytes into the backing memory for buffer; this is relative to the base
        /// offset as bound to the buffer.
        /// </param>
        /// <param name="size">
        /// A size in bytes of the affected area of backing memory for buffer, or <see
        /// cref="WholeSize"/> to use the range from offset to the end of the buffer.
        /// </param>
        public BufferMemoryBarrier(VkBuffer buffer, Accesses srcAccessMask, Accesses dstAccessMask,
            int srcQueueFamilyIndex, int dstQueueFamilyIndex, long offset = 0, long size = WholeSize)
        {
            Type = StructureType.BufferMemoryBarrier;
            Next = IntPtr.Zero;
            Buffer = buffer;
            Offset = offset;
            Size = size;
            SrcAccessMask = srcAccessMask;
            DstAccessMask = dstAccessMask;
            SrcQueueFamilyIndex = srcQueueFamilyIndex;
            DstQueueFamilyIndex = dstQueueFamilyIndex;
        }

        // TODO: Figure out way to make public Buffer as Buffer Type instead of IntPtr
        // and same for ImageMemoryBarrier.cs
        internal struct Native
        {
            internal StructureType Type;
            internal IntPtr Next;
            internal Accesses SrcAccessMask;
            internal Accesses DstAccessMask;
            internal int SrcQueueFamilyIndex;
            internal int DstQueueFamilyIndex;
            internal IntPtr Buffer;
            internal long Offset;
            internal long Size;
        }

        internal void Prepare()
        {
            Type = StructureType.BufferMemoryBarrier;
        }
    }

    /// <summary>
    /// Structure specifying the parameters of an image memory barrier.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageMemoryBarrier
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// Specifies a source access mask.
        /// </summary>
        public Accesses SrcAccessMask;
        /// <summary>
        /// Specifies a destination access mask.
        /// </summary>
        public Accesses DstAccessMask;
        /// <summary>
        /// The old layout in an image layout transition.
        /// <para>
        /// Must be <see cref="ImageLayout.Undefined"/> or the current layout of the image
        /// subresources affected by the barrier.
        /// </para>
        /// </summary>
        public ImageLayout OldLayout;
        /// <summary>
        /// The new layout in an image layout transition.
        /// <para>Must not be <see cref="ImageLayout.Undefined"/> or <see cref="ImageLayout.Preinitialized"/></para>
        /// </summary>
        public ImageLayout NewLayout;
        /// <summary>
        /// The source queue family for a queue family ownership transfer.
        /// </summary>
        public int SrcQueueFamilyIndex;
        /// <summary>
        /// The destination queue family for a queue family ownership transfer.
        /// </summary>
        public int DstQueueFamilyIndex;
        /// <summary>
        /// A handle to the <see cref="Magma.Vulkan.VkImage"/> affected by this barrier.
        /// </summary>
        public IntPtr Image;
        /// <summary>
        /// Describes an area of the backing memory for image, as well as the set of image
        /// subresources whose image layouts are modified.
        /// </summary>
        public ImageSubresourceRange SubresourceRange;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageMemoryBarrier"/> structure.
        /// </summary>
        /// <param name="image">
        /// A handle to the <see cref="Magma.Vulkan.VkImage"/> affected by this barrier.
        /// </param>
        /// <param name="subresourceRange">
        /// Describes an area of the backing memory for image, as well as the set of image
        /// subresources whose image layouts are modified.
        /// </param>
        /// <param name="srcAccessMask">Specifies a source access mask.</param>
        /// <param name="dstAccessMask">Specifies a destination access mask.</param>
        /// <param name="oldLayout">The old layout in an image layout transition.</param>
        /// <param name="newLayout">The new layout in an image layout transition.</param>
        /// <param name="srcQueueFamilyIndex">
        /// The source queue family for a queue family ownership transfer.
        /// </param>
        /// <param name="dstQueueFamilyIndex">
        /// The destination queue family for a queue family ownership transfer.
        /// </param>
        public ImageMemoryBarrier(IntPtr image, ImageSubresourceRange subresourceRange,
            Accesses srcAccessMask, Accesses dstAccessMask, ImageLayout oldLayout, ImageLayout newLayout,
            int srcQueueFamilyIndex = QueueFamilyIgnored, int dstQueueFamilyIndex = QueueFamilyIgnored)
        {
            Type = StructureType.ImageMemoryBarrier;
            Next = IntPtr.Zero;
            SrcAccessMask = srcAccessMask;
            DstAccessMask = dstAccessMask;
            OldLayout = oldLayout;
            NewLayout = newLayout;
            SrcQueueFamilyIndex = srcQueueFamilyIndex;
            DstQueueFamilyIndex = dstQueueFamilyIndex;
            Image = image;
            SubresourceRange = subresourceRange;
        }

        internal void Prepare()
        {
            Type = StructureType.ImageMemoryBarrier;
        }
    }

    /// <summary>
    /// Bitmask specifying how execution and memory dependencies are formed.
    /// </summary>
    [Flags]
    public enum Dependencies
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that dependencies will be framebuffer-local.
        /// </summary>
        ByRegion = 1 << 0,
        /// <summary>
        /// Specifies that a subpass has more than one view.
        /// </summary>
        ViewLocalKhx = 1 << 1,
        /// <summary>
        /// Specifies that dependencies are non-device-local.
        /// </summary>
        DeviceGroupKhx = 1 << 2
    }

    /// <summary>
    /// Structure specifying render pass begin info.
    /// </summary>
    public unsafe struct RenderPassBeginInfo
    {
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkRenderPass"/> to begin an instance of.
        /// </summary>
        public VkRenderPass RenderPass;
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkFramebuffer"/> containing the attachments that are used with
        /// the render pass.
        /// </summary>
        public VkFramebuffer Framebuffer;
        /// <summary>
        /// The render area that is affected by the render pass instance.
        /// <para>
        /// The effects of attachment load, store and multisample resolve operations are restricted
        /// to the pixels whose x and y coordinates fall within the render area on all attachments.
        /// The render area extends to all layers of framebuffer. The application must ensure (using
        /// scissor if necessary) that all rendering is contained within the render area, otherwise
        /// the pixels outside of the render area become undefined and shader side effects may occur
        /// for fragments outside the render area. The render area must be contained within the
        /// framebuffer dimensions.
        /// </para>
        /// </summary>
        public VkRect2D RenderArea;

        /// <summary>
        /// An array of <see cref="ClearValue"/> structures that contains clear values for each
        /// attachment, if the attachment uses a <see cref="AttachmentDescription.LoadOp"/> value of
        /// <see cref="AttachmentLoadOp.Clear"/> or if the attachment has a depth/stencil format and
        /// uses a <see cref="AttachmentDescription.StencilLoadOp"/> value of <see
        /// cref="AttachmentLoadOp.Clear"/>. The array is indexed by attachment number. Only elements
        /// corresponding to cleared attachments are used. Other elements of <see
        /// cref="ClearValues"/> are ignored.
        /// </summary>
        public ClearValue[] ClearValues;

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderPassBeginInfo"/> structure.
        /// </summary>
        /// <param name="framebuffer">
        /// The <see cref="Magma.Vulkan.VkFramebuffer"/> containing the attachments that are used with
        /// the render pass.
        /// </param>
        /// <param name="renderPass">The <see cref="Magma.Vulkan.VkRenderPass"/> to use.</param>
        /// <param name="renderArea">The render area that is affected by the render pass instance.</param>
        /// <param name="clearValues">
        /// An array of <see cref="ClearValue"/> structures that contains clear values for each
        /// attachment, if the attachment uses a <see cref="AttachmentDescription.LoadOp"/> value of
        /// <see cref="AttachmentLoadOp.Clear"/> or if the attachment has a depth/stencil format and
        /// uses a <see cref="AttachmentDescription.StencilLoadOp"/> value of <see
        /// cref="AttachmentLoadOp.Clear"/>. The array is indexed by attachment number. Only elements
        /// corresponding to cleared attachments are used. Other elements of <see
        /// cref="ClearValues"/> are ignored.
        /// </param>
        public RenderPassBeginInfo(VkFramebuffer framebuffer, VkRenderPass renderPass, VkRect2D renderArea, params ClearValue[] clearValues)
        {
            RenderPass = renderPass;
            Framebuffer = framebuffer;
            RenderArea = renderArea;
            ClearValues = clearValues;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public IntPtr RenderPass;
            public IntPtr Framebuffer;
            public VkRect2D RenderArea;
            public int ClearValueCount;
            public ClearValue* ClearValues;
        }

        internal void ToNative(out Native native, ClearValue* clearValues)
        {
            native.Type = StructureType.RenderPassBeginInfo;
            native.Next = IntPtr.Zero;
            native.RenderPass = RenderPass;
            native.Framebuffer = Framebuffer;
            native.RenderArea = RenderArea;
            native.ClearValueCount = ClearValues?.Length ?? 0;
            native.ClearValues = clearValues;
        }
    }

    /// <summary>
    /// Specify how commands in the first subpass of a render pass are provided.
    /// </summary>
    public enum SubpassContents
    {
        /// <summary>
        /// Specifies that the contents of the subpass will be recorded inline in the primary command
        /// buffer, and secondary command buffers must not be executed within the subpass.
        /// </summary>
        Inline = 0,
        /// <summary>
        /// Specifies that the contents are recorded in secondary command buffers that will be called
        /// from the primary command buffer, and <see cref="VkCommandBuffer.CmdExecuteCommands"/> is
        /// the only valid command on the command buffer until <see
        /// cref="VkCommandBuffer.vkCmdNextSubpass"/> or <see cref="VkCommandBuffer.CmdEndRenderPass"/>.
        /// </summary>
        SecondaryCommandBuffers = 1
    }

    /// <summary>
    /// Structure specifying a buffer copy operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BufferCopy
    {
        /// <summary>
        /// The starting offset in bytes from the start of source buffer.
        /// </summary>
        public long SrcOffset;
        /// <summary>
        /// The starting offset in bytes from the start of destination buffer.
        /// </summary>
        public long DstOffset;
        /// <summary>
        /// The number of bytes to copy.
        /// </summary>
        public long Size;

        /// <summary>
        /// Initializes a new instance of the <see cref="BufferCopy"/> structure.
        /// </summary>
        /// <param name="srcOffset">The starting offset in bytes from the start of source buffer.</param>
        /// <param name="dstOffset">The starting offset in bytes from the start of destination buffer.</param>
        /// <param name="size">The number of bytes to copy.</param>
        public BufferCopy(long size, long srcOffset = 0, long dstOffset = 0)
        {
            SrcOffset = srcOffset;
            DstOffset = dstOffset;
            Size = size;
        }
    }

    /// <summary>
    /// Structure specifying an image copy operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageCopy
    {
        /// <summary>
        /// Specifies the image subresource of the image used for the source image data.
        /// </summary>
        public ImageSubresourceLayers SrcSubresource;
        /// <summary>
        /// Selects the initial <c>X</c>, <c>Y</c>, and <c>Z</c> offsets in texels of the sub-region
        /// of the source image data.
        /// </summary>
        public Offset3D SrcOffset;
        /// <summary>
        /// Specifies the image subresource of the image used for the destination image data.
        /// </summary>
        public ImageSubresourceLayers DstSubresource;
        /// <summary>
        /// Selects the initial <c>X</c>, <c>Y</c>, and <c>Z</c> offsets in texels of the sub-region
        /// of the source image data.
        /// </summary>
        public Offset3D DstOffset;
        /// <summary>
        /// The size in texels of the image to copy in <c>Width</c>, <c>Height</c> and <c>Depth</c>.
        /// </summary>
        public Extent3D Extent;
    }

    /// <summary>
    /// Structure specifying a image subresource layers.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageSubresourceLayers
    {
        /// <summary>
        /// A combination of <see cref="ImageAspects"/> selecting the color, depth and/or stencil
        /// aspects to be copied.
        /// <para>Must not contain <see cref="ImageAspects.Metadata"/>.</para>
        /// </summary>
        public ImageAspects AspectMask;
        /// <summary>
        /// The mipmap level to copy from.
        /// <para>
        /// Must be less than the specified <see cref="ImageCreateInfo.MipLevels"/> when the image
        /// was created.
        /// </para>
        /// </summary>
        public int MipLevel;
        /// <summary>
        /// The starting layer.
        /// <para>
        /// Must be less than or equal to the arrayLayers specified in <see cref="ImageCreateInfo"/>
        /// when the image was created.
        /// </para>
        /// </summary>
        public int BaseArrayLayer;
        /// <summary>
        /// The number of layers to copy.
        /// </summary>
        public int LayerCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSubresourceLayers"/> structure.
        /// </summary>
        /// <param name="aspectMask">
        /// A combination of <see cref="ImageAspects"/> selecting the color, depth and/or stencil
        /// aspects to be copied.
        /// </param>
        /// <param name="mipLevel">The mipmap level to copy from.</param>
        /// <param name="baseArrayLayer">The starting layer.</param>
        /// <param name="layerCount">The number of layers to copy.</param>
        public ImageSubresourceLayers(ImageAspects aspectMask, int mipLevel, int baseArrayLayer, int layerCount)
        {
            AspectMask = aspectMask;
            MipLevel = mipLevel;
            BaseArrayLayer = baseArrayLayer;
            LayerCount = layerCount;
        }
    }

    /// <summary>
    /// Structure specifying an image blit operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageBlit
    {
        /// <summary>
        /// The subresource to blit from.
        /// </summary>
        public ImageSubresourceLayers SrcSubresource;
        /// <summary>
        /// Specifies the bounds of the first source region within <see cref="SrcSubresource"/>.
        /// </summary>
        public Offset3D SrcOffset1;
        /// <summary>
        /// Specifies the bounds of the second source region within <see cref="SrcSubresource"/>.
        /// </summary>
        public Offset3D SrcOffset2;
        /// <summary>
        /// The subresource to blit into.
        /// </summary>
        public ImageSubresourceLayers DstSubresource;
        /// <summary>
        /// Specifies the bounds of the first destination region within <see cref="DstSubresource"/>.
        /// </summary>
        public Offset3D DstOffset1;
        /// <summary>
        /// Specifies the bounds of the second destination region within <see cref="DstSubresource"/>.
        /// </summary>
        public Offset3D DstOffset2;
    }

    /// <summary>
    /// Structure specifying an image resolve operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageResolve
    {
        /// <summary>
        /// Specifies the image subresource of the source image data. Resolve of depth/stencil image
        /// is not supported.
        /// </summary>
        public ImageSubresourceLayers SrcSubresource;
        /// <summary>
        /// Selects the initial <c>X</c>, <c>Y</c>, and <c>Z</c> offsets in texels of the sub-region
        /// of the source image data.
        /// </summary>
        public Offset3D SrcOffset;
        /// <summary>
        /// Specifies the image subresource of the destination image data. Resolve of depth/stencil
        /// image is not supported.
        /// </summary>
        public ImageSubresourceLayers DstSubresource;
        /// <summary>
        /// Selects the initial <c>X</c>, <c>Y</c>, and <c>Z</c> offsets in texels of the sub-region
        /// of the destination image data.
        /// </summary>
        public Offset3D DstOffset;
        /// <summary>
        /// The size in texels of the source image to resolve in width, height and depth.
        /// </summary>
        public Extent3D Extent;
    }

    /// <summary>
    /// Structure specifying a image subresource range.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageSubresourceRange
    {
        /// <summary>
        /// A bitmask specifying which aspect(s) of the image are included in the view.
        /// </summary>
        public ImageAspects AspectMask;
        /// <summary>
        /// The first mipmap level accessible to the view.
        /// </summary>
        public int BaseMipLevel;
        /// <summary>
        /// The number of mipmap levels (starting from <see cref="BaseMipLevel"/>) accessible to the view.
        /// </summary>
        public int LevelCount;
        /// <summary>
        /// The first array layer accessible to the view.
        /// </summary>
        public int BaseArrayLayer;
        /// <summary>
        /// The number of array layers (starting from <see cref="BaseArrayLayer"/>) accessible to the view.
        /// </summary>
        public int LayerCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageSubresourceRange"/> structure.
        /// </summary>
        /// <param name="aspectMask">
        /// A bitmask specifying which aspect(s) of the image are included in the view.
        /// </param>
        /// <param name="baseMipLevel">The first mipmap level accessible to the view.</param>
        /// <param name="levelCount">
        /// The number of mipmap levels (starting from <see cref="BaseMipLevel"/>) accessible to the view.
        /// </param>
        /// <param name="baseArrayLayer">The first array layer accessible to the view.</param>
        /// <param name="layerCount">
        /// The number of array layers (starting from <see cref="BaseArrayLayer"/>) accessible to the view.
        /// </param>
        public ImageSubresourceRange(ImageAspects aspectMask,
            int baseMipLevel, int levelCount,
            int baseArrayLayer, int layerCount)
        {
            AspectMask = aspectMask;
            BaseMipLevel = baseMipLevel;
            LevelCount = levelCount;
            BaseArrayLayer = baseArrayLayer;
            LayerCount = layerCount;
        }
    }

    /// <summary>
    /// Bitmask specifying how and when query results are returned.
    /// </summary>
    [Flags]
    public enum QueryResults
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies the results will be written as an array of 64-bit unsigned integer values. If
        /// this bit is not set, the results will be written as an array of 32-bit unsigned integer values.
        /// </summary>
        Query64 = 1 << 0,
        /// <summary>
        /// Specifies that Vulkan will wait for each query's status to become available before
        /// retrieving its results.
        /// </summary>
        QueryWait = 1 << 1,
        /// <summary>
        /// Specifies that the availability status accompanies the results.
        /// </summary>
        QueryWithAvailability = 1 << 2,
        /// <summary>
        /// Specifies that returning partial results is acceptable.
        /// </summary>
        QueryPartial = 1 << 3
    }

    /// <summary>
    /// Structure specifying a buffer image copy operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BufferImageCopy
    {
        /// <summary>
        /// the offset in bytes from the start of the buffer object where the image data is copied
        /// from or to.
        /// <para>Must be a multiple of 4.</para>
        /// </summary>
        public long BufferOffset;
        /// <summary>
        /// Specifies the data in buffer memory as a subregion of a larger two- or three-dimensional
        /// image, and control the addressing calculations of data in buffer memory. If this value is
        /// zero, that aspect of the buffer memory is considered to be tightly packed according to
        /// the <see cref="ImageExtent"/>.
        /// <para>Must be 0, or greater than or equal to the width member.</para>
        /// of imageExtent.
        /// </summary>
        public int BufferRowLength;
        /// <summary>
        /// Specifies the data in buffer memory as a subregion of a larger two- or three-dimensional
        /// image, and control the addressing calculations of data in buffer memory. If this value is
        /// zero, that aspect of the buffer memory is considered to be tightly packed according to
        /// the <see cref="ImageExtent"/>.
        /// <para>Must be 0, or greater than or equal to the height member of <see cref="ImageExtent"/>.</para>
        /// </summary>
        public int BufferImageHeight;
        /// <summary>
        /// Used to specify the specific image subresources of the image used for the source or
        /// destination image data.
        /// </summary>
        public ImageSubresourceLayers ImageSubresource;
        /// <summary>
        /// Selects the initial <c>X</c>, <c>Y</c><c>Z</c> offsets in texels of the sub-region of the
        /// source or destination image data.
        /// </summary>
        public Offset3D ImageOffset;
        /// <summary>
        /// The size in texels of the image to copy in width, height and depth.
        /// </summary>
        public Extent3D ImageExtent;
    }

    /// <summary>
    /// Bitmask specifying constraints on a query.
    /// </summary>
    [Flags]
    public enum QueryControlFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies the precision of occlusion queries.
        /// </summary>
        Precise = 1 << 0
    }
}
