using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Extent2D
    {
        public int Width;
        public int Height;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Extent3D
    {
        public int Width;
        public int Height;
        public int Depth;
    }
}
