namespace Magma.Vulkan
{
  public class Constants
  {
    public const int MaxPhysicalDeviceNameSize = 256;
    public const int UuidSize = 16;
    public const int LuidSizeKhr = 8;
    public const int MaxExtensionNameSize = 256;
    public const int MaxDescriptionSize = 256;
    public const int MaxMemoryTypes = 32;
    public const int MaxMemoryHeaps = 16;
    public const float LodClampNone = 1000.0f;
    public const int RemainingMipLevels = ~0;
    public const int RemainingArrayLevels = ~0;
    public const long WholeSize = ~0;
    public const int AttachmentUnused = ~0;
    public const int True = 1;
    public const int False = 0;
    public const int QueueFamilyIgnored = ~0;
    public const int QueueFamilyExternalKhx = ~0 - 1;
    public const int QueueFamilyForeignExt = ~0 - 2;
    public const int SubpassExternal = ~0;
    public const int MaxDeviceGroupSizeKhx = 32;
  }
}