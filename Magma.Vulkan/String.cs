using System;
using System.Text;

namespace Magma.Vulkan
{
    internal static unsafe class StringExtensions
    {
        internal static unsafe IntPtr AllocToPointer(this string value)
        {
            if (string.IsNullOrEmpty(value)) return IntPtr.Zero;

            int maxSize = GetMaxBytes(value);

            IntPtr managedPointer = Interop.Alloc(maxSize);
            var ptr = (byte*)managedPointer;
            int numBytes;
            fixed (char* c = value)
            {
                numBytes = Encoding.UTF8.GetBytes(c, value.Length, ptr, maxSize);
            }
            ptr[numBytes] = 0;

            return managedPointer;
        }

        internal static unsafe IntPtr* AllocToPointers(this string[] array)
        {
            if (array == null || array.Length == 0) return null;

            var handles = (IntPtr*)Interop.Alloc<IntPtr>(array.Length);
            for (int i = 0; i < array.Length; i++)
            {
                handles[i] = AllocToPointer(array[i]);
            }

            return handles;
        }

        internal static unsafe string FromPointer(byte* ptr)
        {
            if (ptr == null) return null;

            byte* walk = ptr;
            while (*walk != 0) walk++;

            return Encoding.UTF8.GetString(ptr, (int)(walk - ptr));
        }

        internal static unsafe void ToBytePointer(this string value, byte* pointer)
        {
            if (value == null) return;

            int destination;
            fixed (char* c = value)
                destination = Encoding.UTF8.GetBytes(c, value.Length, pointer, value.Length + 1);
            pointer[destination] = 0;
        }

        internal static unsafe string FromPointer(IntPtr pointer)
        {
            return FromPointer((byte*)pointer);
        }

        internal static int GetMaxBytes(this string value)
        {
            return (value == null) ? 0 : Encoding.UTF8.GetMaxByteCount(value.Length + 1);
        }
    }
}
