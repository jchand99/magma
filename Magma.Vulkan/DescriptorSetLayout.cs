using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkDescriptorSetLayout : DeviceBoundObject
    {
        public VkDescriptorSetLayout(VkDevice parent, DescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateDescriptorSetLayout(descriptorSetLayoutCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create DescriptorSetLayout: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateDescriptorSetLayout(DescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr descriptorSetLayout)
        {
            fixed (IntPtr* ptr = &descriptorSetLayout)
            {
                descriptorSetLayoutCreateInfo.ToNative(out var native);
                if (allocationCallbacks.HasValue) return vkCreateDescriptorSetLayout(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateDescriptorSetLayout(LogicalDevice._Handle, &native, null, ptr);
            }
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyDescriptorSetLayout(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyDescriptorSetLayout(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, DescriptorSetLayoutCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateDescriptorSetLayout = (delegate* unmanaged[Cdecl]<IntPtr, DescriptorSetLayoutCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateDescriptorSetLayout");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyDescriptorSetLayout = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyDescriptorSetLayout");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created descriptor set layout.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct DescriptorSetLayoutCreateInfo
    {
        /// <summary>
        /// A bitmask specifying options for descriptor set layout creation.
        /// </summary>
        public DescriptorSetLayoutCreateFlags Flags;
        /// <summary>
        /// An array of <see cref="DescriptorSetLayoutBinding"/> structures.
        /// </summary>
        public DescriptorSetLayoutBinding[] Bindings;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorSetLayoutCreateInfo"/> structure.
        /// </summary>
        /// <param name="bindings">An array of <see cref="DescriptorSetLayoutBinding"/> structures.</param>
        public DescriptorSetLayoutCreateInfo(params DescriptorSetLayoutBinding[] bindings)
        {
            Flags = 0;
            Bindings = bindings;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorSetLayoutCreateInfo"/> structure.
        /// </summary>
        /// <param name="bindings">An array of <see cref="DescriptorSetLayoutBinding"/> structures.</param>
        /// <param name="flags">A bitmask specifying options for descriptor set layout creation.</param>
        public DescriptorSetLayoutCreateInfo(DescriptorSetLayoutBinding[] bindings, DescriptorSetLayoutCreateFlags flags = 0)
        {
            Flags = flags;
            Bindings = bindings;
        }

        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DescriptorSetLayoutCreateFlags Flags;
            public int BindingCount;
            public DescriptorSetLayoutBinding.Native* Bindings;
        }

        internal void ToNative(out Native native)
        {
            int bindingCount = Bindings?.Length ?? 0;

            var bindings = (DescriptorSetLayoutBinding.Native*)Interop.Alloc<DescriptorSetLayoutBinding.Native>(bindingCount);
            for (int i = 0; i < bindingCount; i++)
                Bindings[i].ToNative(&bindings[i]);

            native.Type = StructureType.DescriptorSetLayoutCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = Flags;
            native.BindingCount = bindingCount;
            native.Bindings = bindings;
        }
    }

    /// <summary>
    /// Bitmask specifying descriptor set layout properties.
    /// </summary>
    [Flags]
    public enum DescriptorSetLayoutCreateFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that descriptor sets must not be allocated using this layout, and descriptors
        /// are instead pushed by <see cref="Khr.CommandBufferExtensions.CmdPushDescriptorSetKhr"/>.
        /// </summary>
        PushDescriptorKhr = 1 << 0
    }

    /// <summary>
    /// Structure specifying a descriptor set layout binding.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct DescriptorSetLayoutBinding
    {
        /// <summary>
        /// The binding number of this entry and corresponds to a resource of the same binding number
        /// in the shader stages.
        /// </summary>
        public uint Binding;
        /// <summary>
        /// Specifies which type of resource descriptors are used for this binding.
        /// </summary>
        public DescriptorType DescriptorType;
        /// <summary>
        /// The number of descriptors contained in the binding, accessed in a shader as an array. If
        /// <see cref="DescriptorCount"/> is zero this binding entry is reserved and the resource
        /// must not be accessed from any stage via this binding within any pipeline using the set layout.
        /// </summary>
        public uint DescriptorCount;
        /// <summary>
        /// Specifies which pipeline shader stages can access a resource for this binding. <see
        /// cref="ShaderStages.All"/> is a shorthand specifying that all defined shader stages,
        /// including any additional stages defined by extensions, can access the resource.
        /// <para>
        /// If a shader stage is not included in <see cref="StageFlags"/>, then a resource must not
        /// be accessed from that stage via this binding within any pipeline using the set layout.
        /// There are no limitations on what combinations of stages can be used by a descriptor
        /// binding, and in particular a binding can be used by both graphics stages and the compute stage.
        /// </para>
        /// </summary>
        public ShaderStages StageFlags;
        /// <summary>
        /// Affects initialization of samplers. If <see cref="DescriptorType"/> specifies a <see
        /// cref="Magma.Vulkan.DescriptorType.Sampler"/> or <see
        /// cref="Magma.Vulkan.DescriptorType.CombinedImageSampler"/> type descriptor, then <see
        /// cref="ImmutableSamplers"/> can be used to initialize a set of immutable samplers.
        /// Immutable samplers are permanently bound into the set layout; later binding a sampler
        /// into an immutable sampler slot in a descriptor set is not allowed. If <see
        /// cref="ImmutableSamplers"/> is not <c>null</c>, then it is considered to be an array of
        /// sampler handles that will be consumed by the set layout and used for the corresponding
        /// binding. If <see cref="ImmutableSamplers"/> is <c>null</c>, then the sampler slots are
        /// dynamic and sampler handles must be bound into descriptor sets using this layout. If <see
        /// cref="DescriptorType"/> is not one of these descriptor types, then <see
        /// cref="ImmutableSamplers"/> is ignored.
        /// </summary>
        public IntPtr[] ImmutableSamplers;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorSetLayoutBinding"/> structure.
        /// </summary>
        /// <param name="binding">
        /// The binding number of this entry and corresponds to a resource of the same binding number
        /// in the shader stages.
        /// </param>
        /// <param name="descriptorType">
        /// Specifies which type of resource descriptors are used for this binding.
        /// </param>
        /// <param name="descriptorCount">
        /// The number of descriptors contained in the binding, accessed in a shader as an array. If
        /// <see cref="DescriptorCount"/> is zero this binding entry is reserved and the resource
        /// must not be accessed from any stage via this binding within any pipeline using the set layout.
        /// </param>
        /// <param name="stageFlags">
        /// Specifies which pipeline shader stages can access a resource for this binding. <see
        /// cref="ShaderStages.All"/> is a shorthand specifying that all defined shader stages,
        /// including any additional stages defined by extensions, can access the resource.
        /// </param>
        /// <param name="immutableSamplers">
        /// Affects initialization of samplers. If <see cref="DescriptorType"/> specifies a <see
        /// cref="Magma.Vulkan.DescriptorType.Sampler"/> or <see
        /// cref="Magma.Vulkan.DescriptorType.CombinedImageSampler"/> type descriptor, then <see
        /// cref="ImmutableSamplers"/> can be used to initialize a set of immutable samplers.
        /// Immutable samplers are permanently bound into the set layout; later binding a sampler
        /// into an immutable sampler slot in a descriptor set is not allowed. If <see
        /// cref="ImmutableSamplers"/> is not <c>null</c>, then it is considered to be an array of
        /// sampler handles that will be consumed by the set layout and used for the corresponding
        /// binding. If <see cref="ImmutableSamplers"/> is <c>null</c>, then the sampler slots are
        /// dynamic and sampler handles must be bound into descriptor sets using this layout. If <see
        /// cref="DescriptorType"/> is not one of these descriptor types, then <see
        /// cref="ImmutableSamplers"/> is ignored.
        /// </param>
        public DescriptorSetLayoutBinding(uint binding, DescriptorType descriptorType, uint descriptorCount,
            ShaderStages stageFlags = ShaderStages.All, IntPtr[] immutableSamplers = null)
        {
            Binding = binding;
            DescriptorType = descriptorType;
            DescriptorCount = descriptorCount;
            StageFlags = stageFlags;
            ImmutableSamplers = immutableSamplers;
        }

        internal struct Native
        {
            public uint Binding;
            public DescriptorType DescriptorType;
            public uint DescriptorCount;
            public ShaderStages StageFlags;
            public IntPtr* ImmutableSamplers;
        }

        internal void ToNative(Native* native)
        {
            native->Binding = Binding;
            native->DescriptorType = DescriptorType;
            native->DescriptorCount = DescriptorCount;
            native->StageFlags = StageFlags;
            fixed (IntPtr* ptr = ImmutableSamplers)
                native->ImmutableSamplers = ptr;
        }
    }

    /// <summary>
    /// Specifies the type of a descriptor in a descriptor set.
    /// </summary>
    public enum DescriptorType
    {
        /// <summary>
        /// Specifies a sampler descriptor.
        /// </summary>
        Sampler = 0,
        /// <summary>
        /// Specifies a combined image sampler descriptor.
        /// </summary>
        CombinedImageSampler = 1,
        /// <summary>
        /// Specifies a storage image descriptor.
        /// </summary>
        SampledImage = 2,
        /// <summary>
        /// Specifies a sampled image descriptor.
        /// </summary>
        StorageImage = 3,
        /// <summary>
        /// Specifies a uniform texel buffer descriptor.
        /// </summary>
        UniformTexelBuffer = 4,
        /// <summary>
        /// Specifies a storage texel buffer descriptor.
        /// </summary>
        StorageTexelBuffer = 5,
        /// <summary>
        /// Specifies a uniform buffer descriptor.
        /// </summary>
        UniformBuffer = 6,
        /// <summary>
        /// Specifies a storage buffer descriptor.
        /// </summary>
        StorageBuffer = 7,
        /// <summary>
        /// Specifies a dynamic uniform buffer descriptor.
        /// </summary>
        UniformBufferDynamic = 8,
        /// <summary>
        /// Specifies a dynamic storage buffer descriptor.
        /// </summary>
        StorageBufferDynamic = 9,
        /// <summary>
        /// Specifies a input attachment descriptor.
        /// </summary>
        InputAttachment = 10
    }
}
