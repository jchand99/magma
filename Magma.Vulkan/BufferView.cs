using System;
using System.Runtime.InteropServices;
using static Magma.Vulkan.Constants;

namespace Magma.Vulkan
{
    public class VkBufferView : DeviceBoundObject
    {
        public VkBufferView(VkDevice parent, BufferViewCreateInfo bufferViewCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateBufferView(bufferViewCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create BufferView: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateBufferView(BufferViewCreateInfo bufferViewCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr bufferView)
        {
            fixed (IntPtr* ptr = &bufferView)
            {
                bufferViewCreateInfo.Prepare(bufferViewCreateInfo.Buffer);
                if (allocationCallbacks.HasValue) return vkCreateBufferView(LogicalDevice._Handle, &bufferViewCreateInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateBufferView(LogicalDevice._Handle, &bufferViewCreateInfo, null, ptr);
            }
        }
        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyBufferView(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyBufferView(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyBufferView = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyBufferView");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, BufferViewCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateBufferView = (delegate* unmanaged[Cdecl]<IntPtr, BufferViewCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateBufferView");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created buffer view.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BufferViewCreateInfo
    {
        internal StructureType Type;
        internal IntPtr Next;
        internal BufferViewCreateFlags Flags;
        internal IntPtr Buffer;

        /// <summary>
        /// The format of the data elements in the buffer.
        /// </summary>
        public Format Format;
        /// <summary>
        /// An offset in bytes from the base address of the buffer. Accesses to the buffer view from
        /// shaders use addressing that is relative to this starting offset.
        /// <para>Must be less than the size of buffer.</para>
        /// <para>Must be a multiple of <see cref="PhysicalDeviceLimits.MinTexelBufferOffsetAlignment"/>.</para>
        /// </summary>
        public long Offset;
        /// <summary>
        /// A size in bytes of the buffer view. If range is equal to <see
        /// cref="WholeSize"/>, the range from offset to the end of the buffer is used. If
        /// <see cref="WholeSize"/> is used and the remaining size of the buffer is not a
        /// multiple of the element size of format, then the nearest smaller multiple is used.
        /// </summary>
        public long Range;

        /// <summary>
        /// Initializes a new instance of the <see cref="BufferViewCreateInfo"/> structure.
        /// </summary>
        /// <param name="format">The format of the data elements in the buffer.</param>
        /// <param name="offset">
        /// An offset in bytes from the base address of the buffer. Accesses to the buffer view from
        /// shaders use addressing that is relative to this starting offset.
        /// <para>Must be less than the size of buffer.</para>
        /// <para>Must be a multiple of <see cref="PhysicalDeviceLimits.MinTexelBufferOffsetAlignment"/>.</para>
        /// </param>
        /// <param name="range">
        /// A size in bytes of the buffer view. If range is equal to <see
        /// cref="Constant.WholeSize"/>, the range from offset to the end of the buffer is used. If
        /// <see cref="Constant.WholeSize"/> is used and the remaining size of the buffer is not a
        /// multiple of the element size of format, then the nearest smaller multiple is used.
        /// </param>
        public BufferViewCreateInfo(Format format, long offset = 0, long range = WholeSize)
        {
            Type = StructureType.BufferViewCreateInfo;
            Next = IntPtr.Zero;
            Flags = 0;
            Buffer = IntPtr.Zero;
            Format = format;
            Offset = offset;
            Range = range;
        }

        internal void Prepare(IntPtr buffer)
        {
            Type = StructureType.BufferViewCreateInfo;
            Buffer = buffer;
        }
    }

    // Is reserved for future use.
    [Flags]
    internal enum BufferViewCreateFlags
    {
        None = 0,
    }
}
