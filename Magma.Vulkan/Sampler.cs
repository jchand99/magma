using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkSampler : DeviceBoundObject
    {

        public VkSampler(VkDevice parent, SamplerCreateInfo samplerCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateSampler(samplerCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Sampler: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateSampler(SamplerCreateInfo samplerCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr sampler)
        {
            fixed (IntPtr* ptr = &sampler)
            {
                if (allocationCallbacks.HasValue) return vkCreateSampler(LogicalDevice._Handle, &samplerCreateInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateSampler(LogicalDevice._Handle, &samplerCreateInfo, null, ptr);
            }
        }
        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroySampler(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroySampler(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, SamplerCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateSampler = (delegate* unmanaged[Cdecl]<IntPtr, SamplerCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateSampler");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroySampler = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroySampler");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created sampler.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SamplerCreateInfo
    {
        internal StructureType Type;
        internal IntPtr Next;
        internal SamplerCreateFlags Flags;

        /// <summary>
        /// Specifies the magnification filter to apply to lookups.
        /// </summary>
        public Filter MagFilter;
        /// <summary>
        /// Specifies the minification filter to apply to lookups.
        /// </summary>
        public Filter MinFilter;
        /// <summary>
        /// Specifies the mipmap filter to apply to lookups.
        /// </summary>
        public SamplerMipmapMode MipmapMode;
        /// <summary>
        /// Specifies the addressing mode for outside [0..1] range for U coordinate.
        /// </summary>
        public SamplerAddressMode AddressModeU;
        /// <summary>
        /// Specifies the addressing mode for outside [0..1] range for V coordinate.
        /// </summary>
        public SamplerAddressMode AddressModeV;
        /// <summary>
        /// Specifies the addressing mode for outside [0..1] range for W coordinate.
        /// </summary>
        public SamplerAddressMode AddressModeW;
        /// <summary>
        /// The bias to be added to mipmap LOD calculation and bias provided by image sampling
        /// functions in SPIR-V.
        /// </summary>
        public float MipLodBias;
        /// <summary>
        /// Is <c>true</c> to enable anisotropic filtering, or <c>false</c> otherwise.
        /// </summary>
        public bool AnisotropyEnable;
        /// <summary>
        /// The anisotropy value clamp used by the sampler when <see cref="AnisotropyEnable"/> is <c>true</c>.
        /// <para>
        /// If <see cref="AnisotropyEnable"/> is <c>false</c>, <see cref="MaxAnisotropy"/> is ignored.
        /// </para>
        /// </summary>
        public float MaxAnisotropy;
        /// <summary>
        /// Is <c>true</c> to enable comparison against a reference value during lookups, or
        /// <c>false</c> otherwise.
        /// </summary>
        public bool CompareEnable;
        /// <summary>
        /// Specifies the comparison function to apply to fetched data before filtering.
        /// </summary>
        public CompareOp CompareOp;
        /// <summary>
        /// The value used to clamp the computed LOD value.
        /// <para>Must be less than or equal to <see cref="MaxLod"/>.</para>
        /// </summary>
        public float MinLod;
        /// <summary>
        /// The value used to clamp the computed LOD value.
        /// <para>Must be greater than or equal to <see cref="MinLod"/>.</para>
        /// </summary>
        public float MaxLod;
        /// <summary>
        /// Specifies the predefined border color to use.
        /// </summary>
        public BorderColor BorderColor;
        /// <summary>
        /// Controls whether to use unnormalized or normalized texel coordinates to address texels of
        /// the image. When set to <c>true</c>, the range of the image coordinates used to lookup the
        /// texel is in the range of zero to the image dimensions for x, y and z. When set to
        /// <c>false</c> the range of image coordinates is zero to one.
        /// </summary>
        public bool UnnormalizedCoordinates;

        internal void Prepare()
        {
            Type = StructureType.SamplerCreateInfo;
        }
    }

    // Is reserved for future use.
    [Flags]
    internal enum SamplerCreateFlags
    {
        None = 0
    }

    /// <summary>
    /// Specify mipmap mode used for texture lookups.
    /// </summary>
    public enum SamplerMipmapMode
    {
        /// <summary>
        /// Specifies nearest filtering.
        /// </summary>
        Nearest = 0,
        /// <summary>
        /// Specifies linear filtering.
        /// </summary>
        Linear = 1
    }

    /// <summary>
    /// Specify behavior of sampling with texture coordinates outside an image.
    /// </summary>
    public enum SamplerAddressMode
    {
        /// <summary>
        /// Specifies that the repeat wrap mode will be used.
        /// </summary>
        Repeat = 0,
        /// <summary>
        /// Specifies that the mirrored repeat wrap mode will be used.
        /// </summary>
        MirroredRepeat = 1,
        /// <summary>
        /// Specifies that the clamp to edge wrap mode will be used.
        /// </summary>
        ClampToEdge = 2,
        /// <summary>
        /// Specifies that the clamp to border wrap mode will be used.
        /// </summary>
        ClampToBorder = 3,
        /// <summary>
        /// Specifies that the mirror clamp to edge wrap mode will be used. This is only valid if the
        /// "VK_KHR_mirror_clamp_to_edge" extension is enabled.
        /// </summary>
        MirrorClampToEdge = 4
    }

    /// <summary>
    /// Specify border color used for texture lookups.
    /// </summary>
    public enum BorderColor
    {
        /// <summary>
        /// Specifies a transparent, floating-point format, black color.
        /// </summary>
        FloatTransparentBlack = 0,
        /// <summary>
        /// Specifies a transparent, integer format, black color.
        /// </summary>
        IntTransparentBlack = 1,
        /// <summary>
        /// Specifies an opaque, floating-point format, black color.
        /// </summary>
        FloatOpaqueBlack = 2,
        /// <summary>
        /// Specifies an opaque, integer format, black color.
        /// </summary>
        IntOpaqueBlack = 3,
        /// <summary>
        /// Specifies an opaque, floating-point format, white color.
        /// </summary>
        FloatOpaqueWhite = 4,
        /// <summary>
        /// Specifies an opaque, integer format, white color.
        /// </summary>
        IntOpaqueWhite = 5
    }
}
