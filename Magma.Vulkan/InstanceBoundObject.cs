using System;

namespace Magma.Vulkan
{
    public abstract class InstanceBoundObject
    {
        internal AllocationCallbacks? _AllocationCallbacks;
        public VkInstance Instance { get; internal set; }
        internal protected IntPtr _Handle;

        internal InstanceBoundObject() { }

        internal abstract void Destroy();

        public static implicit operator IntPtr(InstanceBoundObject instanceBoundObject) => instanceBoundObject == null ? IntPtr.Zero : instanceBoundObject._Handle;


        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                if (_Handle != IntPtr.Zero) Destroy();
                _Handle = IntPtr.Zero;
                disposedValue = true;
            }
        }

        // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~InstanceBoundObject()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

