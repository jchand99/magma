using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    internal static unsafe class Interop
    {
        internal static TDelegate GetDelegateForFunctionPointer<TDelegate>(IntPtr handle) where TDelegate : class => Marshal.GetDelegateForFunctionPointer<TDelegate>(handle);
        internal static IntPtr GetFunctionPointerForDelegate(object obj) => Marshal.GetFunctionPointerForDelegate(obj);

        internal static IntPtr Alloc(int size)
        {
            if (size == 0) return IntPtr.Zero;

            return Marshal.AllocHGlobal(size);
        }

        internal static IntPtr Alloc<T>(int count = 1)
        {
            if (count <= 0) return IntPtr.Zero;

            return Alloc(SizeOf<T>() * count);
        }

        internal unsafe static IntPtr AllocFloatsToPointer(float[] values)
        {
            if (values == null || values.Length == 0) return IntPtr.Zero;

            int structSize = SizeOf<float>();
            int totalSize = values.Length * structSize;
            IntPtr ptr = Alloc(totalSize);

            var walk = (byte*)ptr;
            for (int i = 0; i < values.Length; i++)
            {
                fixed (void* p = &values[i])
                    System.Buffer.MemoryCopy(p, walk, structSize, structSize);
                walk += structSize;
            }

            return ptr;
        }

        internal unsafe static IntPtr AllocToPointer<T>(ref T? value) where T : struct
        {
            if (!value.HasValue) return IntPtr.Zero;

            IntPtr ptr = Alloc<T>();
            Marshal.StructureToPtr(value.Value, ptr, true);

            return ptr;
        }

        internal unsafe static IntPtr AllocToPointer<T>(ref T value) where T : struct
        {
            IntPtr ptr = Alloc<T>();
            Marshal.StructureToPtr(value, ptr, true);

            return ptr;
        }

        internal unsafe static IntPtr AllocToPointer<T>(T value) where T : struct
        {
            IntPtr ptr = Alloc<T>();
            Marshal.StructureToPtr(value, ptr, true);

            return ptr;
        }

        internal static IntPtr ReAlloc(IntPtr orig, SizeT size)
        {
            if (size == 0) return orig;

            return Marshal.ReAllocHGlobal(orig, size);
        }

        internal static void Free(IntPtr ptr)
        {
            if (ptr == IntPtr.Zero) return;

            Marshal.FreeHGlobal(ptr);
            ptr = IntPtr.Zero;
        }

        internal static void Free(void* ptr)
        {
            Free(new IntPtr(ptr));
        }

        internal static void Free(IntPtr* ptrs, int count)
        {
            if (ptrs == null) return;

            for (int i = 0; i < count; i++)
            {
                Free(ptrs[i]);
            }

            Free(ptrs);
        }

#nullable enable
        internal static T? PtrToStructure<T>(IntPtr ptr) => Marshal.PtrToStructure<T>(ptr);
#nullable disable

        internal static int SizeOf<T>() => Marshal.SizeOf<T>();
    }
}
