using System;
using System.Runtime.InteropServices;
using Magma.Vulkan.KHR;

namespace Magma.Vulkan
{
    public class VkDeviceMemory : DeviceBoundObject
    {
        public VkDeviceMemory(VkDevice parent, MemoryAllocateInfo memoryAllocateInfo, AllocationCallbacks? AllocationCallbacks = null)
        {
            _AllocationCallbacks = AllocationCallbacks;
            LogicalDevice = parent;

            memoryAllocateInfo.Prepare();
            VkResult result = _Allocate(memoryAllocateInfo, AllocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Unable to allocate memory on the device: ", result);

            unsafe
            {
                vkGetMemoryWin32HandleKHR = (delegate* unmanaged[Cdecl]<IntPtr, MemoryGetWin32HandleInfoKhr*, IntPtr*, VkResult>)LogicalDevice.GetProcAddr("vkGetMemoryWin32HandleKHR");
                vkGetMemoryFdKHR = (delegate* unmanaged[Cdecl]<IntPtr, MemoryGetFdInfoKhr*, IntPtr*, VkResult>)LogicalDevice.GetProcAddr("vkGetMemoryFdKHR");
            }
        }

        #region Core Methods
        private unsafe VkResult _Allocate(MemoryAllocateInfo memoryAllocateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr memory)
        {
            fixed (IntPtr* memoryPtr = &memory)
            {
                if (allocationCallbacks.HasValue) return vkAllocateMemory(LogicalDevice._Handle, &memoryAllocateInfo, (AllocationCallbacks*)&allocationCallbacks, memoryPtr);
                else return vkAllocateMemory(LogicalDevice._Handle, &memoryAllocateInfo, null, memoryPtr);
            }
        }

        public unsafe VkResult Map(long offset, long size, MemoryMapFlags flags, out IntPtr data)
        {
            fixed (IntPtr* ptr = &data) return vkMapMemory(LogicalDevice._Handle, _Handle, offset, size, flags, ptr);
        }

        public unsafe void Unmap() => vkUnmapMemory(LogicalDevice._Handle, _Handle);

        public unsafe void GetCommitment(out long committedMemoryInBytes)
        {
            fixed (long* ptr = &committedMemoryInBytes) vkGetDeviceMemoryCommitment(LogicalDevice._Handle, _Handle, ptr);
        }

        public unsafe void Free()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkFreeMemory(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkFreeMemory(LogicalDevice._Handle, _Handle, null);
            }
        }

        internal override void Destroy()
        {
            Free();
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, MemoryAllocateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkAllocateMemory = (delegate* unmanaged[Cdecl]<IntPtr, MemoryAllocateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkAllocateMemory");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, long, MemoryMapFlags, IntPtr*, VkResult> vkMapMemory = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long, long, MemoryMapFlags, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkMapMemory");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, void> vkUnmapMemory = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, void>)Vulkan.GetStaticProcPointer("vkUnmapMemory");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long*, void> vkGetDeviceMemoryCommitment = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, long*, void>)Vulkan.GetStaticProcPointer("vkGetDeviceMemoryCommitment");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkFreeMemory = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkFreeMemory");
        #endregion

        #region Khr C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, MemoryGetWin32HandleInfoKhr*, IntPtr*, VkResult> vkGetMemoryWin32HandleKHR;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, MemoryGetFdInfoKhr*, IntPtr*, VkResult> vkGetMemoryFdKHR;
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure containing parameters of a memory allocation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryAllocateInfo
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The size of the allocation in bytes. Must be greater than 0.
        /// </summary>
        public long AllocationSize;
        /// <summary>
        /// An index identifying a memory type from the <see
        /// cref="PhysicalDeviceMemoryProperties.MemoryTypes"/> array.
        /// </summary>
        public uint MemoryTypeIndex;

        /// <summary>
        /// Initializes a new instance of <see cref="MemoryAllocateInfo"/> structure.
        /// </summary>
        /// <param name="allocationSize">
        /// The size of the allocation in bytes. Must be greater than 0.
        /// </param>
        /// <param name="memoryTypeIndex">
        /// The memory type index, which selects the properties of the memory to be allocated, as
        /// well as the heap the memory will come from.
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public MemoryAllocateInfo(long allocationSize, uint memoryTypeIndex, IntPtr next = default)
        {
            Type = StructureType.MemoryAllocateInfo;
            Next = next;
            AllocationSize = allocationSize;
            MemoryTypeIndex = memoryTypeIndex;
        }

        internal void Prepare()
        {
            Type = StructureType.MemoryAllocateInfo;
        }
    }

    // Is reserved for future use.
    [Flags]
    public enum MemoryMapFlags
    {
        None = 0,
    }

    /// <summary>
    /// Structure specifying memory requirements.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryRequirements
    {
        /// <summary>
        /// The size, in bytes, of the memory allocation required for the resource.
        /// </summary>
        public long Size;
        /// <summary>
        /// The alignment, in bytes, of the offset within the allocation required for the resource.
        /// </summary>
        public long Alignment;
        /// <summary>
        /// A bitmask that contains one bit set for every supported memory type for the resource. Bit
        /// `i` is set if and only if the memory type `i` in the <see
        /// cref="PhysicalDeviceMemoryProperties"/> structure for the physical device is supported
        /// for the resource.
        /// </summary>
        public uint MemoryTypeBits;
    }
}
