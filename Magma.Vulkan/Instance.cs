using System;
using System.Text;
using System.Runtime.InteropServices;
using Magma.Vulkan.EXT;
using static Magma.Vulkan.Constants;

namespace Magma.Vulkan
{
    public class VkInstance : IDisposable
    {
        public InstanceCreateInfo InstanceCreateInfo { get; }

        internal IntPtr _Handle;
        private AllocationCallbacks? _AllocationCallbacks;

        public static implicit operator IntPtr(VkInstance instance) => instance == null ? IntPtr.Zero : instance._Handle;

        public VkInstance(InstanceCreateInfo instanceCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            VkResult result = _CreateInstance(instanceCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success || _Handle == IntPtr.Zero)
                throw new VulkanException("Failed to create vulkan instance: ", result);

            unsafe
            {
                vkDebugReportMessageEXT = (delegate* unmanaged[Cdecl]<IntPtr, DebugReportFlagsExt, DebugReportObjectTypeExt, ulong, SizeT, int, byte*, byte*, void>)GetProcAddr("vkDebugReportMessageEXT");
            }
        }

        #region Core Methods
        private unsafe VkResult _CreateInstance(InstanceCreateInfo info, AllocationCallbacks? callbacks, out IntPtr value)
        {
            info.ToNative(out var native);
            fixed (IntPtr* ptr = &value)
                if (callbacks != null)
                {
                    VkResult result = vkCreateInstance(&native, (AllocationCallbacks*)&callbacks, ptr);
                    native.Free();
                    return result;
                }
                else
                {
                    VkResult result = vkCreateInstance(&native, null, ptr);
                    native.Free();
                    return result;
                }
        }

        public unsafe IntPtr GetProcAddr(string name)
        {
            if (_Handle == IntPtr.Zero) return IntPtr.Zero;
            fixed (byte* namePtr = Encoding.UTF8.GetBytes(name)) return vkGetInstanceProcAddr(_Handle, namePtr);
        }

        public unsafe VkResult EnumerateVersion(out VkVersion version)
        {
            fixed (VkVersion* ptr = &version) return vkEnumerateInstanceVersion(ptr);
        }

#nullable enable
        public unsafe VkResult EnumerateExtensionProperties(out ExtensionProperties[] instanceExtensionProperties, string? name = null)
        {
            int count;
            VkResult result;
            var nameBytes = name != null ? Encoding.UTF8.GetBytes(name) : null;
            fixed (byte* ptr = nameBytes)
            {
                result = vkEnumerateInstanceExtensionProperties(ptr, &count, null);

                var unmanagedProperties = stackalloc ExtensionProperties.Native[count];
                result = vkEnumerateInstanceExtensionProperties(ptr, &count, unmanagedProperties);

                instanceExtensionProperties = new ExtensionProperties[count];
                for (int i = 0; i < count; i++)
                    ExtensionProperties.FromNative(ref unmanagedProperties[i], out instanceExtensionProperties[i]);

                return result;
            }
        }
#nullable disable

        public unsafe VkResult EnumerateLayerProperties(out LayerProperties[] instanceLayerProperties)
        {
            int count;
            VkResult result;
            result = vkEnumerateInstanceLayerProperties(&count, null);

            var propertiesPointer = stackalloc LayerProperties.Native[count];
            result = vkEnumerateInstanceLayerProperties(&count, propertiesPointer);

            instanceLayerProperties = new LayerProperties[count];
            for (int i = 0; i < count; i++)
                instanceLayerProperties[i] = LayerProperties.FromNative(ref propertiesPointer[i]);
            return result;
        }

        public unsafe VkResult EnumeratePhysicalDevices(out VkPhysicalDevice[] physicalDevices)
        {
            int count;
            VkResult result;
            result = vkEnumeratePhysicalDevices(_Handle, &count, null);

            var physicalDevicePointers = stackalloc IntPtr[count];
            result = vkEnumeratePhysicalDevices(_Handle, &count, physicalDevicePointers);
            physicalDevices = new VkPhysicalDevice[count];
            for (int i = 0; i < count; i++)
                physicalDevices[i] = new VkPhysicalDevice(this, physicalDevicePointers[i]);
            return result;
        }

        public unsafe VkResult EnumeratePhysicalDeviceGroups(out PhysicalDeviceGroupProperties[] physicalDeviceGroupProperties)
        {
            int count;
            VkResult result;
            result = vkEnumeratePhysicalDeviceGroups(_Handle, &count, null);

            var nativeProperties = stackalloc PhysicalDeviceGroupProperties.Native[count];
            result = vkEnumeratePhysicalDeviceGroups(_Handle, &count, &nativeProperties);

            physicalDeviceGroupProperties = new PhysicalDeviceGroupProperties[count];
            for (int i = 0; i < count; i++)
                PhysicalDeviceGroupProperties.FromNative(ref nativeProperties[i], _Handle, out physicalDeviceGroupProperties[i]);
            return result;
        }
        #endregion

        #region C Core Functions
        internal unsafe static delegate* unmanaged[Cdecl]<InstanceCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateInstance = (delegate* unmanaged[Cdecl]<InstanceCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateInstance");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, AllocationCallbacks?*, void> vkDestroyInstance = (delegate* unmanaged[Cdecl]<IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyInstance");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, byte*, IntPtr> vkGetInstanceProcAddr = (delegate* unmanaged[Cdecl]<IntPtr, byte*, IntPtr>)Vulkan.GetStaticProcPointer("vkGetInstanceProcAddr");
        internal unsafe static delegate* unmanaged[Cdecl]<VkVersion*, VkResult> vkEnumerateInstanceVersion = (delegate* unmanaged[Cdecl]<VkVersion*, VkResult>)Vulkan.GetStaticProcPointer("vkEnumerateInstanceVersion");
        internal unsafe static delegate* unmanaged[Cdecl]<byte*, int*, ExtensionProperties.Native*, VkResult> vkEnumerateInstanceExtensionProperties = (delegate* unmanaged[Cdecl]<byte*, int*, ExtensionProperties.Native*, VkResult>)Vulkan.GetStaticProcPointer("vkEnumerateInstanceExtensionProperties");
        internal unsafe static delegate* unmanaged[Cdecl]<int*, LayerProperties.Native*, VkResult> vkEnumerateInstanceLayerProperties = (delegate* unmanaged[Cdecl]<int*, LayerProperties.Native*, VkResult>)Vulkan.GetStaticProcPointer("vkEnumerateInstanceLayerProperties");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, int*, IntPtr*, VkResult> vkEnumeratePhysicalDevices = (delegate* unmanaged[Cdecl]<IntPtr, int*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkEnumeratePhysicalDevices");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, int*, PhysicalDeviceGroupProperties.Native**, VkResult> vkEnumeratePhysicalDeviceGroups = (delegate* unmanaged[Cdecl]<IntPtr, int*, PhysicalDeviceGroupProperties.Native**, VkResult>)Vulkan.GetStaticProcPointer("vkEnumeratePhysicalDeviceGroups");
        #endregion

        #region C Ext Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, DebugReportFlagsExt, DebugReportObjectTypeExt, ulong, SizeT, int, byte*, byte*, void> vkDebugReportMessageEXT;
        #endregion

        private bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                if (_Handle != IntPtr.Zero) _DestroyInstance();
                disposedValue = true;
            }
        }

        private unsafe void _DestroyInstance()
        {
            if (_AllocationCallbacks != null)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyInstance(_Handle, ptr);
            }
            else
            {
                vkDestroyInstance(_Handle, null);
            }

            _Handle = IntPtr.Zero;
            _AllocationCallbacks = null;
        }

        // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~VkInstance()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }

    // Structs

    public unsafe struct ApplicationInfo
    {
        public string ApplicationName;
        public VkVersion ApplicationVersion;
        public string EngineName;
        public VkVersion EngineVersion;
        public VkVersion ApiVersion;

        public ApplicationInfo(
          string applicationName = null, VkVersion applicationVersion = default,
          string engineName = null, VkVersion engineVersion = default,
          VkVersion apiVersion = default
        )
        {
            ApplicationName = applicationName;
            ApplicationVersion = applicationVersion;
            EngineName = engineName;
            EngineVersion = engineVersion;
            ApiVersion = apiVersion;
        }

        internal void ToNative(Native* val)
        {
            val->StructureType = StructureType.ApplicationInfo;
            val->Next = IntPtr.Zero;
            val->ApplicationName = ApplicationName.AllocToPointer();
            val->ApplicationVersion = ApplicationVersion;
            val->EngineName = EngineName.AllocToPointer();
            val->EngineVersion = EngineVersion;
            val->ApiVersion = ApiVersion;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType StructureType;
            public IntPtr Next;
            public IntPtr ApplicationName;
            public int ApplicationVersion;
            public IntPtr EngineName;
            public int EngineVersion;
            public int ApiVersion;

            public void Free()
            {
                Interop.Free(ApplicationName);
                Interop.Free(EngineName);
            }
        }
    }

    public unsafe struct InstanceCreateInfo
    {
        public IntPtr Next;
        public ApplicationInfo? ApplicationInfo;
        public string[] EnabledLayerNames;
        public string[] EnabledExtensionNames;

        public InstanceCreateInfo(
          IntPtr next = default, ApplicationInfo? applicationInfo = null,
          string[] enabledLayerNames = null, string[] enabledExtensionNames = null
        )
        {
            Next = next;
            ApplicationInfo = applicationInfo;
            EnabledExtensionNames = enabledExtensionNames;
            EnabledLayerNames = enabledLayerNames;
        }

        internal void ToNative(out Native val)
        {
            val = default;
            val.StructureType = StructureType.InstanceCreateInfo;
            val.Next = Next;
            val.Flags = InstanceCreateFlags.None;
            if (ApplicationInfo.HasValue)
            {
                var appInfoNative = (ApplicationInfo.Native*)Interop.Alloc<ApplicationInfo.Native>();
                ApplicationInfo.Value.ToNative(appInfoNative);
                val.ApplicationInfo = appInfoNative;
            }
            else ApplicationInfo = null;
            val.EnabledLayerCount = (int)(EnabledLayerNames?.Length ?? 0);
            val.EnabledLayerNames = EnabledLayerNames.AllocToPointers();
            val.EnabledExtensionCount = (int)(EnabledExtensionNames?.Length ?? 0);
            val.EnabledExtensionNames = EnabledExtensionNames.AllocToPointers();

        }

        internal struct Native
        {
            public StructureType StructureType;
            public IntPtr Next;
            public InstanceCreateFlags Flags;
            public ApplicationInfo.Native* ApplicationInfo;
            public int EnabledLayerCount;
            public IntPtr* EnabledLayerNames;
            public int EnabledExtensionCount;
            public IntPtr* EnabledExtensionNames;

            public void Free()
            {
                if (ApplicationInfo != null)
                {
                    ApplicationInfo->Free();
                    Interop.Free(ApplicationInfo);
                }

                Interop.Free(EnabledLayerNames, (int)EnabledLayerCount);
                Interop.Free(EnabledExtensionNames, (int)EnabledExtensionCount);
            }
        }
    }

    [Flags]
    internal enum InstanceCreateFlags
    {
        None = 0
    }

    public unsafe struct ExtensionProperties
    {
        public string ExtensionName;
        public int SpecificationVersion;

        public override string ToString() => $"{ExtensionName} v{SpecificationVersion}";

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public fixed byte ExtensionName[MaxExtensionNameSize];
            public int SpecificationVersion;
        }

        internal static void FromNative(ref Native native, out ExtensionProperties managed)
        {
            fixed (byte* extensionNamePointer = native.ExtensionName)
            {
                managed = new ExtensionProperties
                {
                    ExtensionName = StringExtensions.FromPointer(extensionNamePointer),
                    SpecificationVersion = native.SpecificationVersion
                };
            }
        }
    }

    public unsafe struct LayerProperties
    {
        public string LayerName;
        public VkVersion SpecificationVersion;
        public int ImplementationVersion;
        public string Description;

        public override string ToString() => $"{LayerName} v{SpecificationVersion}";

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public fixed byte LayerName[MaxExtensionNameSize];
            public VkVersion SpecVersion;
            public int ImplementationVersion;
            public fixed byte Description[MaxDescriptionSize];
        }

        internal static LayerProperties FromNative(ref Native native)
        {
            fixed (byte* layerNamePointer = native.LayerName)
            fixed (byte* descriptionPointer = native.Description)
            {
                return new LayerProperties
                {
                    LayerName = StringExtensions.FromPointer(layerNamePointer),
                    SpecificationVersion = native.SpecVersion,
                    ImplementationVersion = native.ImplementationVersion,
                    Description = StringExtensions.FromPointer(descriptionPointer)
                };
            }
        }
    }

    public unsafe struct PhysicalDeviceGroupProperties
    {
        public IntPtr Next;
        public IntPtr[] PhysicalDevices;
        public bool SubsetAllocation;

        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public int PhysicalDeviceCount;
            public IntPtr* PhysicalDevices;
            public bool SubsetAllocation;
        }

        internal static void FromNative(ref Native native, IntPtr instance, out PhysicalDeviceGroupProperties managed)
        {
            managed.Next = native.Next;
            managed.PhysicalDevices = new IntPtr[native.PhysicalDeviceCount];
            for (int i = 0; i < native.PhysicalDeviceCount; i++)
                managed.PhysicalDevices[i] = native.PhysicalDevices[i];
            managed.SubsetAllocation = native.SubsetAllocation;
        }
    }
}
