using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkFramebuffer : DeviceBoundObject
    {
        public VkFramebuffer(VkDevice parent, FramebufferCreateInfo framebufferCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateFramebuffer(framebufferCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Framebuffer: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateFramebuffer(FramebufferCreateInfo framebufferCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr framebuffer)
        {
            fixed (IntPtr* ptr = &framebuffer)
            {
                var att = stackalloc IntPtr[framebufferCreateInfo.Attachments?.Length ?? 0];
                for (int i = 0; i < framebufferCreateInfo.Attachments.Length; i++)
                    att[i] = framebufferCreateInfo.Attachments[i];

                framebufferCreateInfo.ToNative(out var native, att, framebufferCreateInfo.RenderPass);
                if (allocationCallbacks.HasValue) return vkCreateFramebuffer(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateFramebuffer(LogicalDevice._Handle, &native, null, ptr);
            }
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyFramebuffer(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyFramebuffer(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, FramebufferCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateFramebuffer = (delegate* unmanaged[Cdecl]<IntPtr, FramebufferCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateFramebuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyFramebuffer = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyFramebuffer");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created framebuffer.
    /// <para>
    /// Image subresources used as attachments must not be used via any non-attachment usage for the
    /// duration of a render pass instance. This restriction means that the render pass has full
    /// knowledge of all uses of all of the attachments, so that the implementation is able to make
    /// correct decisions about when and how to perform layout transitions, when to overlap execution
    /// of subpasses, etc.
    /// </para>
    /// <para>
    /// It is legal for a subpass to use no color or depth/stencil attachments, and rather use shader
    /// side effects such as image stores and atomics to produce an output. In this case, the subpass
    /// continues to use the width, height, and layers of the framebuffer to define the dimensions of
    /// the rendering area, and the rasterizationSamples from each pipeline’s <see
    /// cref="PipelineMultisampleStateCreateInfo"/> to define the number of samples used in
    /// rasterization; however, if <see cref="PhysicalDeviceFeatures.VariableMultisampleRate"/> is
    /// <c>false</c>, then all pipelines to be bound with a given zero-attachment subpass must have
    /// the same value for <see cref="PipelineMultisampleStateCreateInfo.RasterizationSamples"/>.
    /// </para>
    /// </summary>
    public unsafe struct FramebufferCreateInfo
    {
        /// <summary>
        /// An array of <see cref="VkImageView"/> handles, each of which will be used as the
        /// corresponding attachment in a render pass instance.
        /// </summary>
        public VkImageView[] Attachments;
        /// <summary>
        /// Dimension of the framebuffer.
        /// <para>Must be less than or equal to <see cref="PhysicalDeviceLimits.MaxFramebufferWidth"/>.</para>
        /// </summary>
        public int Width;
        /// <summary>
        /// Dimension of the framebuffer.
        /// <para>Must be less than or equal to <see cref="PhysicalDeviceLimits.MaxFramebufferHeight"/>.</para>
        /// </summary>
        public int Height;
        /// <summary>
        /// Dimension of the framebuffer.
        /// <para>Must be less than or equal to <see cref="PhysicalDeviceLimits.MaxFramebufferLayers"/>.</para>
        /// </summary>
        public int Layers;

        public VkRenderPass RenderPass;

        /// <summary>
        /// Initializes a new instance of the <see cref="FramebufferCreateInfo"/> structure.
        /// </summary>
        /// <param name="attachments">
        /// An array of <see cref="VkImageView"/> handles, each of which will be used as the
        /// corresponding attachment in a render pass instance.
        /// </param>
        /// <param name="width">Dimension of the framebuffer.</param>
        /// <param name="height">Dimension of the framebuffer.</param>
        /// <param name="layers">Dimension of the framebuffer.</param>
        public FramebufferCreateInfo(VkImageView[] attachments, int width, int height, VkRenderPass renderPass, int layers = 1)
        {
            Attachments = attachments;
            Width = width;
            Height = height;
            Layers = layers;
            RenderPass = renderPass;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public FramebufferCreateFlags Flags;
            public IntPtr RenderPass;
            public int AttachmentCount;
            public IntPtr* Attachments;
            public int Width;
            public int Height;
            public int Layers;
        }

        internal void ToNative(out Native native, IntPtr* attachments, IntPtr renderPass)
        {
            native.Type = StructureType.FramebufferCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = 0;
            native.RenderPass = renderPass;
            native.AttachmentCount = Attachments?.Length ?? 0;
            native.Attachments = attachments;
            native.Width = Width;
            native.Height = Height;
            native.Layers = Layers;
        }
    }

    // Is reserved for future use.
    [Flags]
    internal enum FramebufferCreateFlags
    {
        None = 0
    }
}
