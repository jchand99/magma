using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkQueryPool : DeviceBoundObject
    {
        public VkQueryPool(VkDevice parent, QueryPoolCreateInfo queryPoolCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateQueryPool(queryPoolCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create Query Pool: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateQueryPool(QueryPoolCreateInfo queryPoolCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr queryPool)
        {
            fixed (IntPtr* ptr = &queryPool)
            {
                if (allocationCallbacks.HasValue) return vkCreateQueryPool(LogicalDevice._Handle, &queryPoolCreateInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateQueryPool(LogicalDevice._Handle, &queryPoolCreateInfo, null, ptr);
            }
        }

        public unsafe VkResult GetResults(uint firstQuery, uint queryCount, int dataSize, IntPtr data, long stride, QueryResults flags) => vkGetQueryPoolResults(LogicalDevice._Handle, _Handle, firstQuery, queryCount, dataSize, data, stride, flags);

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyQueryPool(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyQueryPool(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, QueryPoolCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateQueryPool = (delegate* unmanaged[Cdecl]<IntPtr, QueryPoolCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateQueryPool");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyQueryPool = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyQueryPool");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, uint, int, IntPtr, long, QueryResults, VkResult> vkGetQueryPoolResults = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, uint, int, IntPtr, long, QueryResults, VkResult>)Vulkan.GetStaticProcPointer("vkGetQueryPoolResults");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created query pool.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct QueryPoolCreateInfo
    {
        internal StructureType Type;
        internal IntPtr Next;
        internal QueryPoolCreateFlags Flags;

        /// <summary>
        /// Specifies the type of queries managed by the pool.
        /// </summary>
        public QueryType QueryType;
        /// <summary>
        /// The number of queries managed by the pool.
        /// </summary>
        public int QueryCount;
        /// <summary>
        /// A bitmask specifying which counters will be returned in queries on the new pool.
        /// <para>Ignored if <see cref="QueryType"/> is not <see cref="Magma.Vulkan.QueryType.PipelineStatistics"/>.</para>
        /// </summary>
        public QueryPipelineStatistics PipelineStatistics;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryPoolCreateInfo"/> structure.
        /// </summary>
        /// <param name="queryType">Specifies the type of queries managed by the pool.</param>
        /// <param name="queryCount">The number of queries managed by the pool.</param>
        /// <param name="pipelineStatistics">
        /// A bitmask specifying which counters will be returned in queries on the new pool.
        /// <para>Ignored if <see cref="QueryType"/> is not <see cref="Magma.Vulkan.QueryType.PipelineStatistics"/>.</para>
        /// </param>
        public QueryPoolCreateInfo(QueryType queryType, int queryCount,
            QueryPipelineStatistics pipelineStatistics = QueryPipelineStatistics.None)
        {
            Type = StructureType.QueryPoolCreateInfo;
            Next = IntPtr.Zero;
            Flags = 0;
            QueryType = queryType;
            QueryCount = queryCount;
            PipelineStatistics = pipelineStatistics;
        }

        internal void Prepare()
        {
            Type = StructureType.QueryPoolCreateInfo;
        }
    }

    // Is reserved for future use.
    [Flags]
    internal enum QueryPoolCreateFlags
    {
        None = 0
    }

    /// <summary>
    /// Specify the type of queries managed by a query pool.
    /// </summary>
    public enum QueryType
    {
        /// <summary>
        /// Specifies an occlusion query.
        /// </summary>
        Occlusion = 0,
        /// <summary>
        /// Specifies a pipeline statistics query.
        /// </summary>
        PipelineStatistics = 1,
        /// <summary>
        /// Specifies a timestamp query.
        /// </summary>
        Timestamp = 2
    }

    /// <summary>
    /// Bitmask specifying queried pipeline statistics.
    /// </summary>
    [Flags]
    public enum QueryPipelineStatistics
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of vertices processed by
        /// the input assembly stage.
        /// <para>Vertices corresponding to incomplete primitives may contribute to the count.</para>
        /// </summary>
        InputAssemblyVertices = 1 << 0,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of primitives processed
        /// by the input assembly stage.
        /// <para>
        /// If primitive restart is enabled, restarting the primitive topology has no effect on the count.
        /// </para>
        /// <para>Incomplete primitives may be counted.</para>
        /// </summary>
        InputAssemblyPrimitives = 1 << 1,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of vertex shader invocations.
        /// <para>This counter's value is incremented each time a vertex shader is invoked.</para>
        /// </summary>
        VertexShaderInvocations = 1 << 2,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of geometry shader invocations.
        /// <para>This counter's value is incremented each time a geometry shader is invoked.</para>
        /// <para>
        /// In the case of instanced geometry shaders, the geometry shader invocations count is
        /// incremented for each separate instanced invocation.
        /// </para>
        /// </summary>
        GeometryShaderInvocations = 1 << 3,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of primitives generated
        /// by geometry shader invocations.
        /// <para>The counter's value is incremented each time the geometry shader emits a primitive.</para>
        /// <para>
        /// Restarting primitive topology using the SPIR-V instructions <c>OpEndPrimitive</c> or
        /// <c>OpEndStreamPrimitive</c> has no effect on the geometry shader output primitives count.
        /// </para>
        /// </summary>
        GeometryShaderPrimitives = 1 << 4,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of primitives processed
        /// by the primitive clipping stage of the pipeline.
        /// <para>
        /// The counter's value is incremented each time a primitive reaches the primitive clipping stage.
        /// </para>
        /// </summary>
        ClippingInvocations = 1 << 5,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of primitives output by
        /// the primitive clipping stage of the pipeline.
        /// <para>
        /// The counter's value is incremented each time a primitive passes the primitive clipping stage.
        /// </para>
        /// <para>
        /// The actual number of primitives output by the primitive clipping stage for a particular
        /// input primitive is implementation-dependent but must satisfy the following conditions:
        /// </para>
        /// <para>
        /// ** If at least one vertex of the input primitive lies inside the clipping volume, the
        /// counter is incremented by one or more.
        /// </para>
        /// <para>** Otherwise, the counter is incremented by zero or more.</para>
        /// </summary>
        ClippingPrimitives = 1 << 6,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of fragment shader invocations.
        /// <para>The counter's value is incremented each time the fragment shader is invoked.</para>
        /// </summary>
        FragmentShaderInvocations = 1 << 7,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of patches processed by
        /// the tessellation control shader.
        /// <para>
        /// The counter's value is incremented once for each patch for which a tessellation control
        /// shader is invoked.
        /// </para>
        /// </summary>
        TessellationControlShaderPatches = 1 << 8,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of invocations of the
        /// tessellation evaluation shader.
        /// <para>
        /// The counter's value is incremented each time the tessellation evaluation shader is invoked.
        /// </para>
        /// </summary>
        TessellationEvaluationShaderInvocations = 1 << 9,
        /// <summary>
        /// Specifies that queries managed by the pool will count the number of compute shader invocations.
        /// <para>The counter's value is incremented every time the compute shader is invoked.</para>
        /// <para>
        /// Implementations may skip the execution of certain compute shader invocations or execute
        /// additional compute shader invocations for implementation-dependent reasons as long as the
        /// results of rendering otherwise remain unchanged.
        /// </para>
        /// </summary>
        ComputeShaderInvocations = 1 << 10
    }
}
