using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkDescriptorPool : DeviceBoundObject
    {
        public VkDescriptorPool(VkDevice parent, DescriptorPoolCreateInfo descriptorPoolCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateDescriptorPool(descriptorPoolCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create DescriptorPool: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateDescriptorPool(DescriptorPoolCreateInfo descriptorPoolCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr descriptorPool)
        {
            fixed (DescriptorPoolSize* sizePtr = descriptorPoolCreateInfo.PoolSizes)
            fixed (IntPtr* ptr = &descriptorPool)
            {
                descriptorPoolCreateInfo.ToNative(out var native, sizePtr);
                if (allocationCallbacks.HasValue) return vkCreateDescriptorPool(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateDescriptorPool(LogicalDevice._Handle, &native, null, ptr);
            }
        }

        public unsafe void ResetDescriptorPool(DescriptorPoolResetFlags flags) => vkResetDescriptorPool(LogicalDevice._Handle, _Handle, flags);

        public VkResult AllocateSets(DescriptorSetAllocateInfo descriptorSetAllocateInfo, out VkDescriptorSet[] descriptorSets)
        {
            return VkDescriptorSet.AllocateDescriptorSets(this, descriptorSetAllocateInfo, out descriptorSets);
        }

        public void UpdateSets(WriteDescriptorSet[] descriptorWrites = null, CopyDescriptorSet[] descriptorCopies = null)
        {
            VkDescriptorSet.UpdateDescriptorSets(this, descriptorWrites, descriptorCopies);
        }

        public VkResult FreeSets(VkDescriptorSet[] descriptorSets)
        {
            return VkDescriptorSet.FreeDescriptorSets(this, LogicalDevice, descriptorSets);
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyDescriptorPool(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyDescriptorPool(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, DescriptorPoolCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateDescriptorPool = (delegate* unmanaged[Cdecl]<IntPtr, DescriptorPoolCreateInfo.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateDescriptorPool");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyDescriptorPool = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyDescriptorPool");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DescriptorPoolResetFlags, VkResult> vkResetDescriptorPool = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DescriptorPoolResetFlags, VkResult>)Vulkan.GetStaticProcPointer("vkResetDescriptorPool");
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created descriptor pool.
    /// </summary>
    public unsafe struct DescriptorPoolCreateInfo
    {
        /// <summary>
        /// A bitmask specifying certain supported operations on the pool.
        /// </summary>
        public DescriptorPoolCreateFlags Flags;
        /// <summary>
        /// The maximum number of descriptor sets that can be allocated from the pool.
        /// </summary>
        public uint MaxSets;
        /// <summary>
        /// Structures, each containing a descriptor type and number of descriptors of that type to
        /// be allocated in the pool.
        /// </summary>
        public DescriptorPoolSize[] PoolSizes;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorPoolCreateInfo"/> structure.
        /// </summary>
        /// <param name="maxSets">
        /// The maximum number of descriptor sets that can be allocated from the pool.
        /// </param>
        /// <param name="poolSizes">
        /// Structures, each containing a descriptor type and number of descriptors of that type to
        /// be allocated in the pool.
        /// </param>
        /// <param name="flags">A bitmask specifying certain supported operations on the pool.</param>
        public DescriptorPoolCreateInfo(
            uint maxSets,
            DescriptorPoolSize[] poolSizes,
            DescriptorPoolCreateFlags flags = 0)
        {
            MaxSets = maxSets;
            PoolSizes = poolSizes;
            Flags = flags;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public DescriptorPoolCreateFlags Flags;
            public uint MaxSets;
            public int PoolSizeCount;
            public DescriptorPoolSize* PoolSizes;
        }

        internal void ToNative(out Native native, DescriptorPoolSize* poolSizes)
        {
            native.Type = StructureType.DescriptorPoolCreateInfo;
            native.Next = IntPtr.Zero;
            native.Flags = Flags;
            native.MaxSets = MaxSets;
            native.PoolSizeCount = PoolSizes?.Length ?? 0;
            native.PoolSizes = poolSizes;
        }
    }

    // Is reserved for future use.
    [Flags]
    public enum DescriptorPoolResetFlags
    {
        None = 0
    }

    /// <summary>
    /// Bitmask specifying certain supported operations on a descriptor pool.
    /// </summary>
    [Flags]
    public enum DescriptorPoolCreateFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that descriptor sets can return their individual allocations to the pool, i.e.
        /// all of <see cref="VkDescriptorPool.AllocateSets"/>, <see cref="VkDescriptorPool.FreeSets"/>,
        /// and <see cref="VkDescriptorPool.Reset"/> are allowed.
        /// <para>
        /// Otherwise, descriptor sets allocated from the pool must not be individually freed back to
        /// the pool, i.e. only <see cref="VkDescriptorPool.AllocateSets"/> and <see
        /// cref="VkDescriptorPool.Reset"/> are allowed.
        /// </para>
        /// </summary>
        FreeDescriptorSet = 1 << 0
    }

    /// <summary>
    /// Structure specifying descriptor pool size.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DescriptorPoolSize
    {
        /// <summary>
        /// The type of descriptor.
        /// </summary>
        public DescriptorType Type;
        /// <summary>
        /// The number of descriptors of that type to allocate.
        /// <para>Must be greater than 0.</para>
        /// </summary>
        public uint DescriptorCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptorPoolSize"/> structure.
        /// </summary>
        /// <param name="type">The type of descriptor.</param>
        /// <param name="descriptorCount">The number of descriptors of that type to allocate.</param>
        public DescriptorPoolSize(DescriptorType type, uint descriptorCount)
        {
            Type = type;
            DescriptorCount = descriptorCount;
        }
    }
}
