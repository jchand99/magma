using System;
using System.Runtime.Serialization;

namespace Magma.Vulkan
{
    public class VulkanException : Exception
    {
        internal VulkanException() { }

        internal VulkanException(VkResult result) : base(result.ToString()) { }

        internal VulkanException(string message) : base(message) { }

        internal VulkanException(string message, VkResult result) : base($"{message} Result: {result.ToString()}") { }

        internal VulkanException(string message, Exception innerException, VkResult result) : base($"{message} Result: {result.ToString()}", innerException) { }
    }
}
