using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkQueue
    {
        public VkDevice LogicalDevice { get; }
        public uint FamilyIndex { get; }
        public uint Index { get; }

        internal IntPtr _Handle;

        public static implicit operator IntPtr(VkQueue queue) => queue == null ? IntPtr.Zero : queue._Handle;

        internal VkQueue(VkDevice parent, IntPtr handle, uint queueFamilyIndex, uint queueIndex)
        {
            _Handle = handle;
            LogicalDevice = parent;
            FamilyIndex = queueFamilyIndex;
            Index = queueIndex;
        }

#nullable enable
        #region Core Methods
        public unsafe VkResult Submit(DeviceQueueSubmitInfo[] submitInfos, VkFence fence)
        {
            int count = submitInfos?.Length ?? 0;
            var nativeSubmits = stackalloc DeviceQueueSubmitInfo.Native[count];
            for (int i = 0; i < count; i++)
                submitInfos?[i].ToNative(out nativeSubmits[i]);

            if (fence == null) return vkQueueSubmit(_Handle, (uint)count, nativeSubmits, IntPtr.Zero);
            else return vkQueueSubmit(_Handle, (uint)count, nativeSubmits, fence);
        }

        public unsafe VkResult Submit(DeviceQueueSubmitInfo submitInfo, VkFence fence)
        {
            submitInfo.ToNative(out var native);
            if (fence == null) return vkQueueSubmit(_Handle, 1, &native, IntPtr.Zero);
            else return vkQueueSubmit(_Handle, 1, &native, fence._Handle);
        }

        public unsafe VkResult WaitIdle() => vkQueueWaitIdle(_Handle);

        public unsafe VkResult BindSparse(BindSparseInfo[] sparseInfos, VkFence? fence)
        {
            int count = sparseInfos?.Length ?? 0;
            var nativeInfos = stackalloc BindSparseInfo.Native[count];
            for (int i = 0; i < count; i++)
                sparseInfos?[i].ToNative(out nativeInfos[i]);

            return vkQueueBindSparse(_Handle, (uint)count, nativeInfos, fence?._Handle);
        }

        public unsafe VkResult BindSparse(BindSparseInfo sparseInfo, VkFence? fence)
        {
            sparseInfo.ToNative(out var native);
            return vkQueueBindSparse(_Handle, 1, &native, fence?._Handle);
        }
        #endregion
#nullable disable

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, DeviceQueueSubmitInfo.Native*, IntPtr, VkResult> vkQueueSubmit = (delegate* unmanaged[Cdecl]<IntPtr, uint, DeviceQueueSubmitInfo.Native*, IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkQueueSubmit");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, VkResult> vkQueueWaitIdle = (delegate* unmanaged[Cdecl]<IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkQueueWaitIdle");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, BindSparseInfo.Native*, IntPtr?, VkResult> vkQueueBindSparse = (delegate* unmanaged[Cdecl]<IntPtr, uint, BindSparseInfo.Native*, IntPtr?, VkResult>)Vulkan.GetStaticProcPointer("vkQueueBindSparse");
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure specifying a queue submit operation.
    /// </summary>
    public unsafe struct DeviceQueueSubmitInfo
    {
        /// <summary>
        /// Semaphores upon which to wait before the command buffers for this batch begin execution.
        /// If semaphores to wait on are provided, they define a semaphore wait operation.
        /// </summary>
        public VkSemaphore[] WaitSemaphores;
        /// <summary>
        /// Pipeline stages at which each corresponding semaphore wait will occur.
        /// </summary>
        public PipelineStages[] WaitDstStageMask;
        /// <summary>
        /// Command buffers to execute in the batch.
        /// </summary>
        public VkCommandBuffer[] CommandBuffers;
        /// <summary>
        /// Semaphores which will be signaled when the command buffers for this batch have completed
        /// execution. If semaphores to be signaled are provided, they define a semaphore signal operation.
        /// </summary>
        public VkSemaphore[] SignalSemaphores;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceQueueSubmitInfo"/> structure.
        /// </summary>
        /// <param name="waitSemaphores">
        /// Semaphores upon which to wait before the command buffers for this batch begin execution.
        /// If semaphores to wait on are provided, they define a semaphore wait operation.
        /// </param>
        /// <param name="waitDstStageMask">
        /// Pipeline stages at which each corresponding semaphore wait will occur.
        /// </param>
        /// <param name="commandBuffers">
        /// Command buffers to execute in the batch. The command buffers submitted in a batch begin
        /// execution in the order they appear in <paramref name="commandBuffers"/>, but may complete
        /// out of order.
        /// </param>
        /// <param name="signalSemaphores">
        /// Semaphores which will be signaled when the command buffers for this batch have completed
        /// execution. If semaphores to be signaled are provided, they define a semaphore signal operation.
        /// </param>
        public DeviceQueueSubmitInfo(VkSemaphore[] waitSemaphores = null, PipelineStages[] waitDstStageMask = null,
            VkCommandBuffer[] commandBuffers = null, VkSemaphore[] signalSemaphores = null)
        {
            WaitSemaphores = waitSemaphores;
            WaitDstStageMask = waitDstStageMask;
            CommandBuffers = commandBuffers;
            SignalSemaphores = signalSemaphores;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public int WaitSemaphoreCount;
            public IntPtr* WaitSemaphores;
            public PipelineStages* WaitDstStageMask;
            public int CommandBufferCount;
            public IntPtr* CommandBuffers;
            public int SignalSemaphoreCount;
            public IntPtr* SignalSemaphores;
        }

        internal void ToNative(out Native native)
        {
            native.Type = StructureType.SubmitInfo;
            native.Next = IntPtr.Zero;
            native.WaitSemaphoreCount = WaitSemaphores?.Length ?? 0;

            var waitArray = new IntPtr[native.WaitSemaphoreCount];
            for (int i = 0; i < native.WaitSemaphoreCount; i++)
                waitArray[i] = WaitSemaphores[i];

            fixed (IntPtr* ptr = waitArray)
                native.WaitSemaphores = ptr;

            fixed (PipelineStages* ptr = WaitDstStageMask)
                native.WaitDstStageMask = ptr;
            native.CommandBufferCount = CommandBuffers?.Length ?? 0;

            var commArray = new IntPtr[native.CommandBufferCount];
            for (int i = 0; i < native.CommandBufferCount; i++)
                commArray[i] = CommandBuffers[i];

            fixed (IntPtr* ptr = commArray)
                native.CommandBuffers = ptr;

            native.SignalSemaphoreCount = SignalSemaphores?.Length ?? 0;

            var sigArray = new IntPtr[native.SignalSemaphoreCount];
            for (int i = 0; i < native.SignalSemaphoreCount; i++)
                sigArray[i] = SignalSemaphores[i];

            fixed (IntPtr* ptr = sigArray)
                native.SignalSemaphores = ptr;
        }
    }

    /// <summary>
    /// Structure specifying a sparse binding operation.
    /// </summary>
    public unsafe struct BindSparseInfo
    {
        /// <summary>
        /// Semaphores upon which to wait on before the sparse binding operations for this batch
        /// begin execution. If semaphores to wait on are provided, they define a semaphore wait operation.
        /// </summary>
        public IntPtr[] WaitSemaphores;
        /// <summary>
        /// An array of <see cref="SparseBufferMemoryBindInfo"/> structures.
        /// </summary>
        public SparseBufferMemoryBindInfo[] BufferBinds;
        /// <summary>
        /// An array of <see cref="SparseImageOpaqueMemoryBindInfo"/> structures, indicating opaque
        /// sparse image bindings to perform.
        /// </summary>
        public SparseImageOpaqueMemoryBindInfo[] ImageOpaqueBinds;
        /// <summary>
        /// An array of <see cref="SparseImageMemoryBindInfo"/> structures, indicating sparse image
        /// bindings to perform.
        /// </summary>
        public SparseImageMemoryBindInfo[] ImageBinds;
        /// <summary>
        /// Semaphores which will be signaled when the sparse binding operations for this batch have
        /// completed execution. If semaphores to be signaled are provided, they define a semaphore
        /// signal operation.
        /// </summary>
        public IntPtr[] SignalSemaphores;

        /// <summary>
        /// Initializes a new instance of the <see cref="BindSparseInfo"/> structure.
        /// </summary>
        /// <param name="waitSemaphores">
        /// Semaphores upon which to wait on before the sparse binding operations for this batch
        /// begin execution. If semaphores to wait on are provided, they define a semaphore wait operation.
        /// </param>
        /// <param name="bufferBinds">An array of <see cref="SparseBufferMemoryBindInfo"/> structures.</param>
        /// <param name="imageOpaqueBinds">
        /// An array of <see cref="SparseImageOpaqueMemoryBindInfo"/> structures, indicating opaque
        /// sparse image bindings to perform.
        /// </param>
        /// <param name="imageBinds">
        /// An array of <see cref="SparseImageMemoryBindInfo"/> structures, indicating sparse image
        /// bindings to perform.
        /// </param>
        /// <param name="signalSemaphores">
        /// Semaphores which will be signaled when the sparse binding operations for this batch have
        /// completed execution. If semaphores to be signaled are provided, they define a semaphore
        /// signal operation.
        /// </param>
        public BindSparseInfo(IntPtr[] waitSemaphores, SparseBufferMemoryBindInfo[] bufferBinds,
            SparseImageOpaqueMemoryBindInfo[] imageOpaqueBinds, SparseImageMemoryBindInfo[] imageBinds,
            IntPtr[] signalSemaphores)
        {
            WaitSemaphores = waitSemaphores;
            BufferBinds = bufferBinds;
            ImageOpaqueBinds = imageOpaqueBinds;
            ImageBinds = imageBinds;
            SignalSemaphores = signalSemaphores;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public int WaitSemaphoreCount;
            public IntPtr* WaitSemaphores;
            public int BufferBindCount;
            public SparseBufferMemoryBindInfo.Native* BufferBinds;
            public int ImageOpaqueBindCount;
            public SparseImageOpaqueMemoryBindInfo.Native* ImageOpaqueBinds;
            public int ImageBindCount;
            public SparseImageMemoryBindInfo.Native* ImageBinds;
            public int SignalSemaphoreCount;
            public IntPtr* SignalSemaphores;

            public void Free()
            {
                Interop.Free(BufferBinds);
                Interop.Free(ImageOpaqueBinds);
                Interop.Free(ImageBinds);
            }
        }

        internal void ToNative(out Native native)
        {
            int bufferBindCount = BufferBinds?.Length ?? 0;
            int imageOpaqueBindCount = ImageOpaqueBinds?.Length ?? 0;
            int imageBindCount = ImageBinds?.Length ?? 0;

            var bufferBinds = (SparseBufferMemoryBindInfo.Native*)
                Interop.Alloc<SparseBufferMemoryBindInfo.Native>(bufferBindCount);
            for (int i = 0; i < bufferBindCount; i++)
                BufferBinds[i].ToNative(&bufferBinds[i]);
            var imageOpaqueBinds = (SparseImageOpaqueMemoryBindInfo.Native*)
                Interop.Alloc<SparseImageOpaqueMemoryBindInfo.Native>(bufferBindCount);
            for (int i = 0; i < imageOpaqueBindCount; i++)
                ImageOpaqueBinds[i].ToNative(&imageOpaqueBinds[i]);
            var imageBinds = (SparseImageMemoryBindInfo.Native*)
                Interop.Alloc<SparseImageMemoryBindInfo.Native>(bufferBindCount);
            for (int i = 0; i < imageBindCount; i++)
                ImageBinds[i].ToNative(&imageBinds[i]);

            native.Type = StructureType.BindSparseInfo;
            native.Next = IntPtr.Zero;
            native.WaitSemaphoreCount = WaitSemaphores?.Length ?? 0;
            fixed (IntPtr* ptr = WaitSemaphores)
                native.WaitSemaphores = ptr;
            native.BufferBindCount = bufferBindCount;
            native.BufferBinds = bufferBinds;
            native.ImageOpaqueBindCount = imageOpaqueBindCount;
            native.ImageOpaqueBinds = imageOpaqueBinds;
            native.ImageBindCount = imageBindCount;
            native.ImageBinds = imageBinds;
            native.SignalSemaphoreCount = SignalSemaphores?.Length ?? 0;
            fixed (IntPtr* ptr = SignalSemaphores)
                native.SignalSemaphores = ptr;
        }
    }

    /// <summary>
    /// Structure specifying a sparse buffer memory bind operation.
    /// </summary>
    public unsafe struct SparseBufferMemoryBindInfo
    {
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkBuffer"/> object to be bound.
        /// </summary>
        public IntPtr Buffer;
        /// <summary>
        /// An array of <see cref="SparseMemoryBind"/> structures.
        /// </summary>
        public SparseMemoryBind[] Binds;

        /// <summary>
        /// Initializes a new instance of the <see cref="SparseBufferMemoryBindInfo"/> structure.
        /// </summary>
        /// <param name="buffer">The <see cref="Magma.Vulkan.VkBuffer"/> object to be bound.</param>
        /// <param name="binds">An array of <see cref="SparseMemoryBind"/> structures.</param>
        public SparseBufferMemoryBindInfo(IntPtr buffer, params SparseMemoryBind[] binds)
        {
            Buffer = buffer;
            Binds = binds;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public IntPtr Buffer;
            public int BindCount;
            public SparseMemoryBind* Binds;
        }

        internal void ToNative(Native* native)
        {
            native->Buffer = Buffer;
            native->BindCount = Binds?.Length ?? 0;
            fixed (SparseMemoryBind* ptr = Binds)
                native->Binds = ptr;
        }
    }

    /// <summary>
    /// Structure specifying sparse image memory bind info.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct SparseImageMemoryBindInfo
    {
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkImage"/> object to be bound.
        /// </summary>
        public IntPtr Image;
        /// <summary>
        /// An array of <see cref="SparseImageMemoryBind"/> structures.
        /// <para>Length must be greater than 0.</para>
        /// </summary>
        public SparseImageMemoryBind[] Binds;

        /// <summary>
        /// Initializes a new instance of the <see cref="SparseImageMemoryBind"/> structure.
        /// </summary>
        /// <param name="image">The <see cref="Magma.Vulkan.VkImage"/> object to be bound.</param>
        /// <param name="binds">An array of <see cref="SparseImageMemoryBind"/> structures.</param>
        public SparseImageMemoryBindInfo(IntPtr image, params SparseImageMemoryBind[] binds)
        {
            Image = image;
            Binds = binds;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public IntPtr Image;
            public int BindCount;
            public SparseImageMemoryBind* Binds;
        }

        internal void ToNative(Native* native)
        {
            native->Image = Image;
            native->BindCount = Binds?.Length ?? 0;
            fixed (SparseImageMemoryBind* ptr = Binds)
                native->Binds = ptr;
        }
    }

    /// <summary>
    /// Structure specifying sparse image memory bind.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SparseImageMemoryBind
    {
        /// <summary>
        /// The aspect mask and region of interest in the image.
        /// <para>Must be a valid subresource for <see cref="VkImage"/>.</para>
        /// </summary>
        public ImageSubresource Subresource;
        /// <summary>
        /// The coordinates of the first texel within the image subresource to bind.
        /// </summary>
        public Offset3D Offset;
        /// <summary>
        /// The size in texels of the region within the image subresource to bind. The extent must be
        /// a multiple of the sparse image block dimensions, except when binding sparse image blocks
        /// along the edge of an image subresource it can instead be such that any coordinate of <see
        /// cref="Offset"/> + <see cref="Extent"/> equals the corresponding dimensions of the image subresource.
        /// </summary>
        public Extent3D Extent;
        /// <summary>
        /// The <see cref="VkDeviceMemory"/> object that the sparse image blocks of the image are bound
        /// to. If memory is 0, the sparse image blocks are unbound.
        /// <para>Must match the memory requirements of the calling command's <see cref="VkImage"/>.</para>
        /// </summary>
        public long Memory;
        /// <summary>
        /// An offset into <see cref="VkDeviceMemory"/> object. If memory is 0, this value is ignored.
        /// <para>Must match the memory requirements of the calling command's <see cref="VkImage"/>.</para>
        /// </summary>
        public long MemoryOffset;
        /// <summary>
        /// Sparse memory binding flags.
        /// </summary>
        public SparseMemoryBindFlags Flags;
    }

    /// <summary>
    /// Structure specifying sparse image opaque memory bind info.
    /// </summary>
    public unsafe struct SparseImageOpaqueMemoryBindInfo
    {
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkImage"/> object to be bound.
        /// </summary>
        public IntPtr Image;
        /// <summary>
        /// An array of <see cref="SparseMemoryBind"/> structures.
        /// <para>Length must be greater than 0.</para>
        /// </summary>
        public SparseMemoryBind[] Binds;

        /// <summary>
        /// Initializes a new instance of the <see cref="SparseImageOpaqueMemoryBindInfo"/> structure.
        /// </summary>
        /// <param name="image">The <see cref="Magma.Vulkan.VkImage"/> object to be bound.</param>
        /// <param name="binds">An array of <see cref="SparseMemoryBind"/> structures.</param>
        public SparseImageOpaqueMemoryBindInfo(IntPtr image, params SparseMemoryBind[] binds)
        {
            Image = image;
            Binds = binds;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public IntPtr Image;
            public int BindCount;
            public SparseMemoryBind* Binds;
        }

        internal void ToNative(Native* native)
        {
            native->Image = Image;
            native->BindCount = Binds?.Length ?? 0;
            fixed (SparseMemoryBind* ptr = Binds)
                native->Binds = ptr;
        }
    }

    /// <summary>
    /// Structure specifying a sparse memory bind operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SparseMemoryBind
    {
        /// <summary>
        /// The offset into the resource.
        /// </summary>
        public long ResourceOffset;
        /// <summary>
        /// The size of the memory region to be bound.
        /// </summary>
        public long Size;
        /// <summary>
        /// The <see cref="VkDeviceMemory"/> object that the range of the resource is bound to. If
        /// memory 0, the range is unbound.
        /// </summary>
        public IntPtr Memory;
        /// <summary>
        /// The offset into the <see cref="VkDeviceMemory"/> object to bind the resource range to. If
        /// memory is 0, this value is ignored.
        /// </summary>
        public long MemoryOffset;
        /// <summary>
        /// A bitmask specifying usage of the binding operation.
        /// </summary>
        public SparseMemoryBindFlags Flags;

        /// <summary>
        /// Initializes a new instance of the <see cref="SparseMemoryBind"/> structure.
        /// </summary>
        /// <param name="resourceOffset">The offset into the resource.</param>
        /// <param name="size">The size of the memory region to be bound.</param>
        /// <param name="memory">
        /// The <see cref="VkDeviceMemory"/> object that the range of the resource is bound to. If
        /// memory 0, the range is unbound.
        /// </param>
        /// <param name="memoryOffset">
        /// The offset into the <see cref="VkDeviceMemory"/> object to bind the resource range to. If
        /// memory is 0, this value is ignored.
        /// </param>
        /// <param name="flags">A bitmask specifying usage of the binding operation.</param>
        public SparseMemoryBind(long resourceOffset, long size, IntPtr memory = default,
            long memoryOffset = 0, SparseMemoryBindFlags flags = 0)
        {
            ResourceOffset = resourceOffset;
            Size = size;
            Memory = memory;
            MemoryOffset = memoryOffset;
            Flags = flags;
        }
    }

    /// <summary>
    /// Bitmask specifying usage of a sparse memory binding operation.
    /// </summary>
    [Flags]
    public enum SparseMemoryBindFlags
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that the memory being bound is only for the metadata aspect.
        /// </summary>
        Metadata = 1 << 0
    }
}
