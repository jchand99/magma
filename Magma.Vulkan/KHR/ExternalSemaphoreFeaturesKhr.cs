using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Bitfield describing features of an external semaphore handle type.
    /// </summary>
    [Flags]
    public enum ExternalSemaphoreFeaturesKhr
    {
        /// <summary>
        /// Specifies that handles of this type can be exported from Vulkan semaphore objects.
        /// </summary>
        Exportable = 1 << 0,
        /// <summary>
        /// Specifies that handles of this type can be imported as Vulkan semaphore objects.
        /// </summary>
        Importable = 1 << 1
    }
}
