using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying Y'CbCr conversion to a sampler or image view.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SamplerYcbcrConversionInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A <see cref="SamplerYcbcrConversionKhr"/> handle created with <see cref="DeviceExtensions.CreateSamplerYcbcrConversionKhr"/>.
        /// </summary>
        public long Conversion;
    }
}
