using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    public static class QueueKhr
    {
        public unsafe static VkResult QueuePresentKhr(this VkQueue queue, PresentInfoKhr presentInfo)
        {
            fixed (uint* imageIndicesPtr = presentInfo.ImageIndices)
            fixed (VkResult* resultsPtr = presentInfo.Results)
            {
                int wcount = presentInfo.WaitSemaphores?.Length ?? 0;
                int swcount = presentInfo.Swapchains?.Length ?? 0;

                var swapchainsPtr = stackalloc IntPtr[swcount];
                var waitSemaphoresPtr = stackalloc IntPtr[wcount];

                for (int i = 0; i < swcount; i++)
                    swapchainsPtr[i] = presentInfo.Swapchains[i];

                for (int i = 0; i < wcount; i++)
                    waitSemaphoresPtr[i] = presentInfo.WaitSemaphores[i];

                presentInfo.ToNative(out PresentInfoKhr.Native nativePresentInfo,
                    waitSemaphoresPtr,
                    swapchainsPtr,
                    imageIndicesPtr,
                    resultsPtr);
                return vkQueuePresentKHR(queue, &nativePresentInfo);
            }
        }

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PresentInfoKhr.Native*, VkResult> vkQueuePresentKHR = (delegate* unmanaged[Cdecl]<IntPtr, PresentInfoKhr.Native*, VkResult>)Vulkan.GetStaticProcPointer("vkQueuePresentKHR");
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure describing parameters of a queue presentation.
    /// </summary>
    public unsafe struct PresentInfoKhr
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Semaphores to wait for before presenting.
        /// </summary>
        public VkSemaphore[] WaitSemaphores;
        /// <summary>
        /// Valid <see cref="VkSwapchainKhr"/> handles.
        /// </summary>
        public VkSwapchainKhr[] Swapchains;
        /// <summary>
        /// Indices into the array of each swapchain's presentable images, with swapchain count entries.
        /// <para>
        /// Each entry in this array identifies the image to present on the corresponding entry in
        /// the <see cref="Swapchains"/> array.
        /// </para>
        /// </summary>
        public uint[] ImageIndices;
        /// <summary>
        /// <see cref="VkResult"/> typed elements with swapchain count entries.
        /// <para>
        /// Applications that do not need per-swapchain results can use <c>null</c> for <see cref="Results"/>.
        /// </para>
        /// <para>
        /// If not <c>null</c>, each entry in <see cref="Results"/> will be set to the <see
        /// cref="VkResult"/> for presenting the swapchain corresponding to the same index in <see cref="Swapchains"/>.
        /// </para>
        /// </summary>
        public VkResult[] Results;

        /// <summary>
        /// Initializes a new instance of the <see cref="PresentInfoKhr"/> structure.
        /// </summary>
        /// <param name="waitSemaphores">Semaphores to wait for before presenting.</param>
        /// <param name="swapchains">Valid <see cref="VkSwapchainKhr"/> handles.</param>
        /// <param name="imageIndices">
        /// Indices into the array of each swapchain’s presentable images, with swapchain count entries.
        /// <para>
        /// Each entry in this array identifies the image to present on the corresponding entry in
        /// the <see cref="Swapchains"/> array.
        /// </para>
        /// </param>
        /// <param name="results">
        /// <see cref="VkResult"/> typed elements with swapchain count entries.
        /// <para>
        /// Applications that do not need per-swapchain results can use <c>null</c> for <see cref="Results"/>.
        /// </para>
        /// <para>
        /// If not <c>null</c>, each entry in <see cref="Results"/> will be set to the <see
        /// cref="VkResult"/> for presenting the swapchain corresponding to the same index in <see cref="Swapchains"/>.
        /// </para>
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public PresentInfoKhr(VkSemaphore[] waitSemaphores, VkSwapchainKhr[] swapchains, uint[] imageIndices,
            VkResult[] results = null, IntPtr next = default(IntPtr))
        {
            Next = next;
            WaitSemaphores = waitSemaphores;
            Swapchains = swapchains;
            ImageIndices = imageIndices;
            Results = results;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public int WaitSemaphoreCount;
            public IntPtr* WaitSemaphores;
            public int SwapchainCount;
            public IntPtr* Swapchains;
            public uint* ImageIndices;
            public VkResult* Results;
        }

        internal void ToNative(out Native native,
            IntPtr* waitSemaphores, IntPtr* swapchains, uint* imageIndices, VkResult* results)
        {
            native.Type = StructureType.PresentInfoKhr;
            native.Next = Next;
            native.WaitSemaphoreCount = WaitSemaphores?.Length ?? 0;
            native.WaitSemaphores = waitSemaphores;
            native.SwapchainCount = Swapchains?.Length ?? 0;
            native.Swapchains = swapchains;
            native.ImageIndices = imageIndices;
            native.Results = results;
        }
    }
}
