using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Import Win32 memory created on the same physical device.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImportMemoryWin32HandleInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Specifies the type of handle or name.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleType;
        /// <summary>
        /// The external handle to import, or <c>null</c>.
        /// </summary>
        public IntPtr Handle;
        /// <summary>
        /// A NULL-terminated UTF-16 string naming the underlying memory resource to import, or <c>null</c>.
        /// </summary>
        public IntPtr Name;
    }
}
