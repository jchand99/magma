using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying physical device properties.
    /// </summary>
    public struct PhysicalDeviceProperties2Khr
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A structure describing the properties of the physical device. This structure is written
        /// with the same values as if it were written by <see cref="VkPhysicalDevice.GetProperties"/>.
        /// </summary>
        public PhysicalDeviceProperties Properties;

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public PhysicalDeviceProperties.Native Properties;
        }

        internal static void FromNative(ref Native native, out PhysicalDeviceProperties2Khr properties)
        {
            properties = new PhysicalDeviceProperties2Khr
            {
                Next = native.Next
            };
            PhysicalDeviceProperties.FromNative(native.Properties, out properties.Properties);
        }
    }
}
