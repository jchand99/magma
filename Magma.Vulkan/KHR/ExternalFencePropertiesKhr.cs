using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing supported external fence handle features.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExternalFencePropertiesKhr
    {
        public StructureType Type;
        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is a bitmask of <see cref="ExternalFenceHandleTypeFlagsKhr"/> indicating which types of
        /// imported handle handleType can be exported from.
        /// </summary>
        public ExternalFenceHandleTypeFlagsKhr ExportFromImportedHandleTypes;
        /// <summary>
        /// Is a bitmask of <see cref="ExternalFenceHandleTypeFlagsKhr"/> specifying handle types
        /// which can be specified at the same time as handleType when creating a fence.
        /// </summary>
        public ExternalFenceHandleTypeFlagsKhr CompatibleHandleTypes;
        /// <summary>
        /// Is a bitmask of <see cref="ExternalFenceFeatureFlagsKhr"/> indicating the features of handleType.
        /// </summary>
        public ExternalFenceFeatureFlagsKhr ExternalFenceFeatures;
    }
}
