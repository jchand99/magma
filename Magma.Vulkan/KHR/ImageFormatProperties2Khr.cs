using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying a image format properties.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageFormatProperties2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure. The <see
        /// cref="Next"/> chain is used to allow the specification of additional capabilities to be
        /// returned from <see cref="PhysicalDeviceExtensions.GetImageFormatProperties2Khr"/>.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A structure in which capabilities are returned.
        /// </summary>
        public ImageFormatProperties ImageFormatProperties;
    }
}
