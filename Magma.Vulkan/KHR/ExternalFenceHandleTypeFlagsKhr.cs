using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Bitmask of valid external fence handle types.
    /// </summary>
    [Flags]
    public enum ExternalFenceHandleTypeFlagsKhr
    {
        /// <summary>
        /// Indicates a POSIX file descriptor handle that has only limited valid usage outside of
        /// Vulkan and other compatible APIs. It must be compatible with the POSIX system calls
        /// <c>dup</c>, <c>dup2</c>, <c>close</c>, and the non-standard system call <c>dup3</c>.
        /// Additionally, it must be transportable over a socket using an <c>SCMRIGHTS</c> control
        /// message. It owns a reference to the underlying synchronization primitive represented by
        /// its Vulkan fence object.
        /// </summary>
        OpaqueFd = 1 << 0,
        /// <summary>
        /// Indicates an NT handle that has only limited valid usage outside of Vulkan and other
        /// compatible APIs. It must be compatible with the functions <c>DuplicateHandle</c>,
        /// <c>CloseHandle</c>, <c>CompareObjectHandles</c>, <c>GetHandleInformation</c>, and
        /// <c>SetHandleInformation</c>. It owns a reference to the underlying synchronization
        /// primitive represented by its Vulkan fence object.
        /// </summary>
        OpaqueWin32 = 1 << 1,
        /// <summary>
        /// Indicates a global share handle that has only limited valid usage outside of Vulkan and
        /// other compatible APIs. It is not compatible with any native APIs. It does not own a
        /// reference to the underlying synchronization primitive represented by its Vulkan fence
        /// object, and will therefore become invalid when all Vulkan fence objects associated with
        /// it are destroyed.
        /// </summary>
        OpaqueWin32Kmt = 1 << 2,
        /// <summary>
        /// Indicates a POSIX file descriptor handle to a Linux Sync File or Android Fence. It can be
        /// used with any native API accepting a valid sync file or fence as input. It owns a
        /// reference to the underlying synchronization primitive associated with the file
        /// descriptor. Implementations which support importing this handle type must accept any type
        /// of sync or fence FD supported by the native system they are running on.
        /// </summary>
        SyncFd = 1 << 3
    }
}
