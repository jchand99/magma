using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Bitmask specifying additional parameters of fence payload import.
    /// </summary>
    [Flags]
    public enum FenceImportFlagsKhr
    {
        /// <summary>
        /// Specifies that the fence payload will be imported only temporarily, regardless of the
        /// permanence of handle type.
        /// </summary>
        Temporary = 1 << 0
    }
}
