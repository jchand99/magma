using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying image creation parameters.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceImageFormatInfo2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure. The <see
        /// cref="Next"/> chain is used to provide additional image parameters to <see cref="PhysicalDeviceExtensions.GetImageFormatProperties2Khr"/>.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The image format, corresponding to <see cref="ImageCreateInfo.Format"/>.
        /// </summary>
        public Format Format;
        /// <summary>
        /// The image type, corresponding to <see cref="ImageCreateInfo.ImageType"/>.
        /// </summary>
        public ImageType ImageType;
        /// <summary>
        /// The image tiling, corresponding to <see cref="ImageCreateInfo.Tiling"/>.
        /// </summary>
        public ImageTiling Tiling;
        /// <summary>
        /// The intended usage of the image, corresponding to <see cref="ImageCreateInfo.Usage"/>.
        /// </summary>
        public ImageUsages Usage;
        /// <summary>
        /// A bitmask describing additional parameters of the image, corresponding to <see cref="ImageCreateInfo.Flags"/>.
        /// </summary>
        public ImageCreateFlags Flags;
    }
}
