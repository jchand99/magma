using System;

namespace Magma.Vulkan.KHR
{
    public static class CommandBufferKhr
    {
        #region Methods
        public unsafe static void CmdPushDescriptorSetKhr(this VkCommandBuffer commandBuffer, PipelineBindPoint pipelineBindPoint, VkPipelineLayout layout, uint set, WriteDescriptorSet[] descriptorWrites)
        {
            int count = descriptorWrites?.Length ?? 0;
            var nativeDescriptorWrites = stackalloc WriteDescriptorSet.Native[count];
            for (int i = 0; i < count; i++)
                descriptorWrites[i].ToNative(&nativeDescriptorWrites[i]);

            vkCmdPushDescriptorSetKHR(commandBuffer, pipelineBindPoint, layout, set, (uint)count, nativeDescriptorWrites);
        }
        public unsafe static void CmdPushDescriptorSetWithTemplateKhr(this VkCommandBuffer commandBuffer, IntPtr descriptorUpdateTemplate, VkPipelineLayout layout, uint set, IntPtr data) => vkCmdPushDescriptorSetWithTemplateKHR(commandBuffer, descriptorUpdateTemplate, layout, set, data);
        #endregion

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, PipelineBindPoint, IntPtr, uint, uint, WriteDescriptorSet.Native*, void> vkCmdPushDescriptorSetKHR = (delegate* unmanaged[Cdecl]<IntPtr, PipelineBindPoint, IntPtr, uint, uint, WriteDescriptorSet.Native*, void>)Vulkan.GetStaticProcPointer("vkCmdPushDescriptorSetKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, uint, IntPtr, void> vkCmdPushDescriptorSetWithTemplateKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, uint, IntPtr, void>)Vulkan.GetStaticProcPointer("vkCmdPushDescriptorSetWithTemplateKHR");
        #endregion
    }
}
