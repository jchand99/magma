using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying a surface and related swapchain creation parameters.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceSurfaceInfo2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The <see cref="VkSurfaceKhr"/> that will be associated with the swapchain.
        /// </summary>
        public IntPtr Surface;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhysicalDeviceSurfaceInfo2Khr"/> structure.
        /// </summary>
        /// <param name="surface">
        /// The <see cref="VkSurfaceKhr"/> that will be associated with the swapchain.
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public PhysicalDeviceSurfaceInfo2Khr(IntPtr surface, IntPtr next = default(IntPtr))
        {
            Type = StructureType.PhysicalDeviceSurfaceInfo2Khr;
            Next = next;
            Surface = surface;
        }

        internal void Prepare()
        {
            Type = StructureType.PhysicalDeviceSurfaceInfo2Khr;
        }
    }
}
