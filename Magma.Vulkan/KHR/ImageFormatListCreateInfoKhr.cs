using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Specify that an image can be used with a particular set of formats.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageFormatListCreateInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The number of entries in the <see cref="ViewFormats"/> array.
        /// </summary>
        public int ViewFormatCount;
        /// <summary>
        /// An array which lists of all formats which can be used when creating views of this image.
        /// </summary>
        public IntPtr ViewFormats;
    }
}
