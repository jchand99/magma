using System;
using System.Runtime.InteropServices;
using Magma.Vulkan.EXT;

namespace Magma.Vulkan.KHR
{
    public class VkSwapchainKhr : DeviceBoundObject
    {
        public VkSwapchainKhr(VkDevice parent, SwapchainCreateInfoKhr swapchainCreateInfoKhr, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateSwapchainKhr(swapchainCreateInfoKhr, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create SwapchainKhr: ", result);

            unsafe
            {
                vkGetSwapchainStatusKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult>)LogicalDevice.GetProcAddr("vkGetSwapchainStatusKHR");
                vkGetSwapchainCounterEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, SurfaceCountersExt, ulong*, VkResult>)LogicalDevice.GetProcAddr("vkGetSwapchainCounterEXT");
            }
        }

        internal VkSwapchainKhr(VkDevice device, IntPtr handle, AllocationCallbacks? allocationCallbacks)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = device;
            _Handle = handle;
        }

        #region Methods
        private unsafe VkResult _CreateSwapchainKhr(SwapchainCreateInfoKhr swapchainCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr _Handle)
        {
            fixed (IntPtr* ptr = &_Handle)
            {
                swapchainCreateInfo.ToNative(out var native);
                if (allocationCallbacks.HasValue) return vkCreateSwapchainKHR(LogicalDevice._Handle, &native, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateSwapchainKHR(LogicalDevice._Handle, &native, null, ptr);
            }
        }

        public unsafe VkResult GetSwapchainImagesKhr(out VkImage[] images)
        {
            int imageCount;
            VkResult result = vkGetSwapchainImagesKHR(LogicalDevice._Handle, _Handle, (uint*)&imageCount, null);

            var ptr = stackalloc IntPtr[imageCount];
            result = vkGetSwapchainImagesKHR(LogicalDevice._Handle, _Handle, (uint*)&imageCount, ptr);

            images = new VkImage[imageCount];
            for (int i = 0; i < imageCount; i++)
                images[i] = new VkImage(LogicalDevice, ptr[i], _AllocationCallbacks);

            return result;
        }

        public unsafe VkResult AcquireNextImageKhr(ulong timeout, VkSemaphore semaphore, VkFence fence, out uint imageIndex)
        {
            fixed (uint* ptr = &imageIndex) return vkAcquireNextImageKHR(LogicalDevice._Handle, _Handle, timeout, semaphore, fence, ptr);
        }

        public unsafe VkResult CreateSharedSwapchainsKhr(SwapchainCreateInfoKhr[] swapchainCreateInfos, AllocationCallbacks? allocationCallbacks, out VkSwapchainKhr[] swapchains)
        {
            int count = swapchainCreateInfos?.Length ?? 0;
            var nativeCreateInfos = stackalloc SwapchainCreateInfoKhr.Native[count];
            for (int i = 0; i < count; i++)
                swapchainCreateInfos[i].ToNative(out nativeCreateInfos[i]);

            var ptr = stackalloc IntPtr[count];
            VkResult result;
            if (allocationCallbacks.HasValue) result = vkCreateSwapchainKHR(LogicalDevice._Handle, nativeCreateInfos, (AllocationCallbacks*)&allocationCallbacks, ptr);
            else result = vkCreateSwapchainKHR(LogicalDevice._Handle, nativeCreateInfos, null, ptr);

            swapchains = new VkSwapchainKhr[count];
            for (int i = 0; i < count; i++)
                swapchains[i] = new VkSwapchainKhr(LogicalDevice, ptr[i], allocationCallbacks);

            return result;
        }

        public unsafe VkResult GetSwapchainStatusKhr(IntPtr swapchain)
        {
            return vkGetSwapchainStatusKHR(LogicalDevice._Handle, swapchain);
        }

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks.HasValue)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroySwapchainKHR(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroySwapchainKHR(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, SwapchainCreateInfoKhr.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateSwapchainKHR = (delegate* unmanaged[Cdecl]<IntPtr, SwapchainCreateInfoKhr.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateSwapchainKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroySwapchainKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroySwapchainKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, IntPtr*, VkResult> vkGetSwapchainImagesKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetSwapchainImagesKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ulong, IntPtr, IntPtr, uint*, VkResult> vkAcquireNextImageKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, ulong, IntPtr, IntPtr, uint*, VkResult>)Vulkan.GetStaticProcPointer("vkAcquireNextImageKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, SwapchainCreateInfoKhr.Native*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateSharedSwapchainsKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, SwapchainCreateInfoKhr.Native*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateSharedSwapchainsKHR");
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult> vkGetSwapchainStatusKHR;
        #endregion

        #region Ext C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, SurfaceCountersExt, ulong*, VkResult> vkGetSwapchainCounterEXT;
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created swapchain object.
    /// </summary>
    public unsafe struct SwapchainCreateInfoKhr
    {
        /// <summary>
        /// A bitmask indicating parameters of the swapchain creation.
        /// </summary>
        public SwapchainCreateFlagsKhr Flags;
        /// <summary>
        /// The <see cref="VkSurfaceKhr"/> onto which the swapchain will present images. If the
        /// creation succeeds, the swapchain becomes associated with <see cref="VkSurfaceKhr"/>.
        /// </summary>
        public VkSurfaceKhr Surface;
        /// <summary>
        /// The minimum number of presentable images that the application needs.
        /// <para>
        /// The implementation will either create the swapchain with at least that many images, or it
        /// will fail to create the swapchain.
        /// </para>
        /// </summary>
        public int MinImageCount;
        /// <summary>
        /// A format value specifying the format the swapchain image(s) will be created with.
        /// </summary>
        public Format ImageFormat;
        /// <summary>
        /// Color space value specifying the way the swapchain interprets image data.
        /// </summary>
        public ColorSpaceKhr ImageColorSpace;
        /// <summary>
        /// The size (in pixels) of the swapchain image(s).
        /// <para>
        /// The behavior is platform-dependent if the image extent does not match the surface's <see
        /// cref="SurfaceCapabilitiesKhr.CurrentExtent"/> as returned by <see cref="PhysicalDeviceExtensions.GetSurfaceCapabilitiesKhr"/>.
        /// </para>
        /// </summary>
        public Extent2D ImageExtent;
        /// <summary>
        /// The number of views in a multiview/stereo surface.
        /// <para>For non-stereoscopic-3D applications, this value is 1.</para>
        /// </summary>
        public int ImageArrayLayers;
        /// <summary>
        /// A bitmask describing the intended usage of the (acquired) swapchain images.
        /// </summary>
        public ImageUsages ImageUsage;
        /// <summary>
        /// The sharing mode used for the image(s) of the swapchain.
        /// </summary>
        public SharingMode ImageSharingMode;
        /// <summary>
        /// Queue family indices having access to the image(s) of the swapchain when <see
        /// cref="ImageSharingMode"/> is <see cref="SharingMode.Concurrent"/>.
        /// </summary>
        public int[] QueueFamilyIndices;
        /// <summary>
        /// A value describing the transform, relative to the presentation engine's natural
        /// orientation, applied to the image content prior to presentation.
        /// <para>
        /// If it does not match the <see cref="SurfaceCapabilitiesKhr.CurrentTransform"/> value
        /// returned by <see cref="PhysicalDeviceExtensions.GetSurfaceCapabilitiesKhr"/>, the
        /// presentation engine will transform the image content as part of the presentation operation.
        /// </para>
        /// </summary>
        public SurfaceTransformsKhr PreTransform;
        /// <summary>
        /// A value indicating the alpha compositing mode to use when this surface is composited
        /// together with other surfaces on certain window systems.
        /// </summary>
        public CompositeAlphasKhr CompositeAlpha;
        /// <summary>
        /// The presentation mode the swapchain will use.
        /// <para>
        /// A swapchain's present mode determines how incoming present requests will be processed and
        /// queued internally.
        /// </para>
        /// </summary>
        public PresentModeKhr PresentMode;
        /// <summary>
        /// Indicates whether the Vulkan implementation is allowed to discard rendering operations
        /// that affect regions of the surface that are not visible.
        /// <para>
        /// If set to <c>true</c>, the presentable images associated with the swapchain may not own
        /// all of their pixels. Pixels in the presentable images that correspond to regions of the
        /// target surface obscured by another window on the desktop, or subject to some other
        /// clipping mechanism will have undefined content when read back. Pixel shaders may not
        /// execute for these pixels, and thus any side effects they would have had will not occur.
        /// </para>
        /// <para>
        /// <c>true</c> value does not guarantee any clipping will occur, but allows more optimal
        /// presentation methods to be used on some platforms.
        /// </para>
        /// <para>
        /// If set to <c>false</c>, presentable images associated with the swapchain will own all of
        /// the pixels they contain.
        /// </para>
        /// </summary>
        public bool Clipped;
        /// <summary>
        /// Is <c>null</c>, or the existing non-retired swapchain currently associated with <c>Surface</c>.
        /// <para>
        /// Providing a valid <see cref="OldSwapchain"/> may aid in the resource reuse, and also
        /// allows the application to still present any images that are already acquired from it.
        /// </para>
        /// </summary>
        public VkSwapchainKhr OldSwapchain;

        /// <summary>
        /// Initializes a new instance of the <see cref="SwapchainCreateInfoKhr"/> structure.
        /// </summary>
        /// <param name="surface">
        /// The <see cref="VkSurfaceKhr"/> that the swapchain will present images to.
        /// </param>
        /// <param name="imageFormat">A format that is valid for swapchains on the specified surface.</param>
        /// <param name="imageExtent">
        /// The size (in pixels) of the swapchain.
        /// <para>
        /// Behavior is platform-dependent when the image extent does not match the surface's <see
        /// cref="SurfaceCapabilitiesKhr.CurrentExtent"/> as returned by <see cref="PhysicalDeviceExtensions.GetSurfaceCapabilitiesKhr"/>.
        /// </para>
        /// </param>
        /// <param name="minImageCount">
        /// The minimum number of presentable images that the application needs. The platform will
        /// either create the swapchain with at least that many images, or will fail to create the swapchain.
        /// </param>
        /// <param name="imageColorSpace">Color space value specifying the way the swapchain interprets image data.</param>
        /// <param name="imageArrayLayers">
        /// The number of views in a multiview/stereo surface.
        /// <para>For non-stereoscopic-3D applications, this value is 1.</para>
        /// </param>
        /// <param name="imageUsage">A bitmask describing the intended usage of the (acquired) swapchain images.</param>
        /// <param name="imageSharingMode">The sharing mode used for the image(s) of the swapchain.</param>
        /// <param name="queueFamilyIndices">
        /// Queue family indices having access to the image(s) of the swapchain when <see
        /// cref="ImageSharingMode"/> is <see cref="SharingMode.Concurrent"/>.
        /// </param>
        /// <param name="preTransform">
        /// A value describing the transform, relative to the presentation engine's natural
        /// orientation, applied to the image content prior to presentation.
        /// <para>
        /// If it does not match the <see cref="SurfaceCapabilitiesKhr.CurrentTransform"/> value
        /// returned by <see cref="PhysicalDeviceExtensions.GetSurfaceCapabilitiesKhr"/>, the
        /// presentation engine will transform the image content as part of the presentation operation.
        /// </para>
        /// </param>
        /// <param name="compositeAlpha">
        /// A bitmask indicating the alpha compositing mode to use when this surface is composited
        /// together with other surfaces on certain window systems.
        /// </param>
        /// <param name="presentMode">
        /// The presentation mode the swapchain will use.
        /// <para>
        /// A swapchain's present mode determines how incoming present requests will be processed and
        /// queued internally.
        /// </para>
        /// </param>
        /// <param name="clipped">
        /// Indicates whether the Vulkan implementation is allowed to discard rendering operations
        /// that affect regions of the surface which are not visible.</param>
        /// <param name="oldSwapchain">Existing swapchain to replace, if any.</param>
        public SwapchainCreateInfoKhr(
            VkSurfaceKhr surface,
            Format imageFormat,
            Extent2D imageExtent,
            int minImageCount = 2,
            ColorSpaceKhr imageColorSpace = ColorSpaceKhr.SRgbNonlinear,
            int imageArrayLayers = 1,
            ImageUsages imageUsage = ImageUsages.ColorAttachment | ImageUsages.TransferDst,
            SharingMode imageSharingMode = SharingMode.Exclusive,
            int[] queueFamilyIndices = null,
            SurfaceTransformsKhr preTransform = SurfaceTransformsKhr.Identity,
            CompositeAlphasKhr compositeAlpha = CompositeAlphasKhr.Opaque,
            PresentModeKhr presentMode = PresentModeKhr.Fifo,
            bool clipped = true,
            VkSwapchainKhr oldSwapchain = default)
        {
            Flags = SwapchainCreateFlagsKhr.None;
            Surface = surface;
            MinImageCount = minImageCount;
            ImageFormat = imageFormat;
            ImageColorSpace = imageColorSpace;
            ImageExtent = imageExtent;
            ImageArrayLayers = imageArrayLayers;
            ImageUsage = imageUsage;
            ImageSharingMode = imageSharingMode;
            QueueFamilyIndices = queueFamilyIndices;
            PreTransform = preTransform;
            CompositeAlpha = compositeAlpha;
            PresentMode = presentMode;
            Clipped = clipped;
            OldSwapchain = oldSwapchain;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public SwapchainCreateFlagsKhr Flags;
            public IntPtr Surface;
            public int MinImageCount;
            public Format ImageFormat;
            public ColorSpaceKhr ImageColorSpace;
            public Extent2D ImageExtent;
            public int ImageArrayLayers;
            public ImageUsages ImageUsage;
            public SharingMode ImageSharingMode;
            public int QueueFamilyIndexCount;
            public int* QueueFamilyIndices;
            public SurfaceTransformsKhr PreTransform;
            public CompositeAlphasKhr CompositeAlpha;
            public PresentModeKhr PresentMode;
            public bool Clipped;
            public IntPtr OldSwapchain;
        }

        internal void ToNative(out Native native)
        {
            native.Type = StructureType.SwapchainCreateInfoKhr;
            native.Next = IntPtr.Zero;
            native.Flags = Flags;
            native.Surface = Surface;
            native.MinImageCount = MinImageCount;
            native.ImageFormat = ImageFormat;
            native.ImageColorSpace = ImageColorSpace;
            native.ImageExtent = ImageExtent;
            native.ImageArrayLayers = ImageArrayLayers;
            native.ImageUsage = ImageUsage;
            native.ImageSharingMode = ImageSharingMode;
            native.QueueFamilyIndexCount = QueueFamilyIndices?.Length ?? 0;
            fixed (int* ptr = QueueFamilyIndices)
                native.QueueFamilyIndices = ptr;
            native.PreTransform = PreTransform;
            native.CompositeAlpha = CompositeAlpha;
            native.PresentMode = PresentMode;
            native.Clipped = Clipped;
            native.OldSwapchain = OldSwapchain;
        }
    }

    /// <summary>
    /// Bitmask controlling swapchain creation.
    /// </summary>
    [Flags]
    public enum SwapchainCreateFlagsKhr
    {
        /// <summary>
        /// No flags.
        /// </summary>
        None = 0
    }

    /// <summary>
    /// Supported color space of the presentation engine.
    /// </summary>
    public enum ColorSpaceKhr
    {
        /// <summary>
        /// Indicates support for the sRGB color space.
        /// </summary>
        SRgbNonlinear = 0,
        /// <summary>
        /// Indicates support for the Display-P3 color space and applies an sRGB-like transfer function.
        /// </summary>
        DisplayP3NonlinearExt = 1000104001,
        /// <summary>
        /// Indicates support for the extended sRGB color space and applies a linear transfer function.
        /// </summary>
        ExtendedSRgbLinearExt = 1000104002,
        /// <summary>
        /// Indicates support for the DCI-P3 color space and applies a linear OETF.
        /// </summary>
        DciP3LinearExt = 1000104003,
        /// <summary>
        /// Indicates support for the DCI-P3 color space and applies the Gamma 2.6 OETF.
        /// </summary>
        DciP3NonlinearExt = 1000104004,
        /// <summary>
        /// Indicates support for the BT709 color space and applies a linear OETF.
        /// </summary>
        BT709LinearExt = 1000104005,
        /// <summary>
        /// Indicates support for the BT709 color space and applies the SMPTE 170M OETF.
        /// </summary>
        BT709NonlinearExt = 1000104006,
        /// <summary>
        /// Indicates support for the BT2020 color space and applies a linear OETF.
        /// </summary>
        BT2020LinearExt = 1000104007,
        /// <summary>
        /// Indicates support for HDR10 (BT2020 color) space and applies the SMPTE ST2084 Perceptual
        /// Quantizer (PQ) OETF.
        /// </summary>
        Hdr10ST2084Ext = 1000104008,
        /// <summary>
        /// Indicates support for Dolby Vision (BT2020 color space), proprietary encoding, and
        /// applies the SMPTE ST2084 OETF.
        /// </summary>
        DolbyVisionExt = 1000104009,
        /// <summary>
        /// Indicates support for HDR10 (BT2020 color space) and applies the Hybrid Log Gamma (HLG) OETF.
        /// </summary>
        Hdr10HlgExt = 1000104010,
        /// <summary>
        /// Indicates support for the AdobeRGB color space and applies a linear OETF.
        /// </summary>
        AdobeRgbLinearExt = 1000104011,
        /// <summary>
        /// Indicates support for the AdobeRGB color space and applies the Gamma 2.2 OETF.
        /// </summary>
        AdobeRgbNonlinearExt = 1000104012,
        /// <summary>
        /// Indicates that color components are used "as is". This is intended to allow application
        /// to supply data for color spaces not described here.
        /// </summary>
        PassThroughExt = 1000104013,
        /// <summary>
        /// Indicates support for the extended sRGB color space and applies an sRGB transfer function.
        /// </summary>
        ExtendedSRgbNonlinearExt = 1000104014
    }

    /// <summary>
    /// Presentation mode supported for a surface.
    /// </summary>
    public enum PresentModeKhr
    {
        /// <summary>
        /// Indicates that the presentation engine does not wait for a vertical blanking period to
        /// update the current image, meaning this mode may result in visible tearing. No internal
        /// queuing of presentation requests is needed, as the requests are applied immediately.
        /// </summary>
        Immediate = 0,
        /// <summary>
        /// Indicates that the presentation engine waits for the next vertical blanking period to
        /// update the current image. Tearing cannot be observed. An internal single-entry queue is
        /// used to hold pending presentation requests. If the queue is full when a new presentation
        /// request is received, the new request replaces the existing entry, and any images
        /// associated with the prior entry become available for re-use by the application. One
        /// request is removed from the queue and processed during each vertical blanking period in
        /// which the queue is non-empty.
        /// </summary>
        Mailbox = 1,
        /// <summary>
        /// Indicates that the presentation engine waits for the next vertical blanking period to
        /// update the current image. Tearing cannot be observed. An internal queue is used to hold
        /// pending presentation requests. New requests are appended to the end of the queue, and one
        /// request is removed from the beginning of the queue and processed during each vertical
        /// blanking period in which the queue is non-empty. This is the only value of presentMode
        /// that is required: to be supported.
        /// </summary>
        Fifo = 2,
        /// <summary>
        /// Indicates that the presentation engine generally waits for the next vertical blanking
        /// period to update the current image. If a vertical blanking period has already passed
        /// since the last update of the current image then the presentation engine does not wait for
        /// another vertical blanking period for the update, meaning this mode may result in visible
        /// tearing in this case. This mode is useful for reducing visual stutter with an application
        /// that will mostly present a new image before the next vertical blanking period, but may
        /// occasionally be late, and present a new image just after the next vertical blanking
        /// period. An internal queue is used to hold pending presentation requests. New requests are
        /// appended to the end of the queue, and one request is removed from the beginning of the
        /// queue and processed during or after each vertical blanking period in which the queue is non-empty.
        /// </summary>
        FifoRelaxed = 3,
        /// <summary>
        /// Indicates that the presentation engine and application have concurrent access to a single
        /// image, which is referred to as a shared presentable image. The presentation engine is
        /// only required to update the current image after a new presentation request is received.
        /// Therefore the application must make a presentation request whenever an update is
        /// required. However, the presentation engine may update the current image at any point,
        /// meaning this mode may result in visible tearing.
        /// </summary>
        SharedDemandRefreshKhr = 1000111000,
        /// <summary>
        /// Indicates that the presentation engine and application have concurrent access to a single
        /// image, which is referred to as a shared presentable image. The presentation engine
        /// periodically updates the current image on its regular refresh cycle. The application is
        /// only required to make one initial presentation request, after which the presentation
        /// engine must update the current image without any need for further presentation requests.
        /// The application can indicate the image contents have been updated by making a
        /// presentation request, but this does not guarantee the timing of when it will be updated.
        /// This mode may result in visible tearing if rendering to the image is not timed correctly.
        /// </summary>
        SharedContinuousRefreshKhr = 1000111001
    }

    /// <summary>
    /// Alpha compositing modes supported on a device.
    /// </summary>
    [Flags]
    public enum CompositeAlphasKhr
    {
        /// <summary>
        /// The alpha channel, if it exists, of the images is ignored in the compositing process.
        /// Instead, the image is treated as if it has a constant alpha of 1.0.
        /// </summary>
        Opaque = 1 << 0,
        /// <summary>
        /// The alpha channel, if it exists, of the images is respected in the compositing process.
        /// The non-alpha channels of the image are expected to already be multiplied by the alpha
        /// channel by the application.
        /// </summary>
        PreMultiplied = 1 << 1,
        /// <summary>
        /// The alpha channel, if it exists, of the images is respected in the compositing process.
        /// The non-alpha channels of the image are not expected to already be multiplied by the
        /// alpha channel by the application; instead, the compositor will multiply the non-alpha
        /// channels of the image by the alpha channel during compositing.
        /// </summary>
        PostMultiplied = 1 << 2,
        /// <summary>
        /// The way in which the presentation engine treats the alpha channel in the images is
        /// unknown to the Vulkan API. Instead, the application is responsible for setting the
        /// composite alpha blending mode using native window system commands. If the application
        /// does not set the blending mode using native window system commands, then a
        /// platform-specific default will be used.
        /// </summary>
        Inherit = 1 << 3
    }
}
