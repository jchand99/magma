using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Bitmask specifying features of an external memory handle type.
    /// </summary>
    [Flags]
    public enum ExternalMemoryFeaturesKhr
    {
        /// <summary>
        /// Specifies that images or buffers created with the specified parameters and handle type
        /// must use the mechanisms defined in the "VK_NV_dedicated_allocation" extension to to
        /// create (or import) a dedicated allocation for the image or buffer.
        /// </summary>
        DedicatedOnly = 1 << 0,
        /// <summary>
        /// Specifies that handles of this type can be exported from Vulkan memory objects.
        /// </summary>
        Exportable = 1 << 1,
        /// <summary>
        /// Specifies that handles of this type can be imported as Vulkan memory objects.
        /// </summary>
        Importable = 1 << 2
    }
}
