using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    public static class PhysicalDeviceKhr
    {
        #region Methods
        public unsafe static VkResult GetSurfaceSupportKhr(this VkPhysicalDevice physicalDevice, uint queueFamilyIndex, VkSurfaceKhr surface, out bool supported)
        {
            fixed (bool* ptr = &supported) return vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, ptr);
        }
        public unsafe static bool GetWaylandPresentationSupportKhr(this VkPhysicalDevice physicalDevice, uint queueFamilyIndex, DisplayKhr display) => vkGetPhysicalDeviceWaylandPresentationSupportKHR(physicalDevice, queueFamilyIndex, display);
        public unsafe static bool GetWin32PresentationSupportKhr(this VkPhysicalDevice physicalDevice, uint queueFamilyIndex) => vkGetPhysicalDeviceWin32PresentationSupportKHR(physicalDevice, queueFamilyIndex);
        public unsafe static bool GetXlibPresentationSupportKhr(this VkPhysicalDevice physicalDevice, uint queueFamilyIndex, IntPtr dpy, IntPtr visualID) => vkGetPhysicalDeviceXlibPresentationSupportKHR(physicalDevice, queueFamilyIndex, &dpy, visualID);
        public unsafe static bool GetXcbPresentationSupportKhr(this VkPhysicalDevice physicalDevice, uint queueFamilyIndex, IntPtr connection, IntPtr visual_id) => vkGetPhysicalDeviceXcbPresentationSupportKHR(physicalDevice, queueFamilyIndex, &connection, visual_id);
        public unsafe static VkResult GetSurfaceCapabilitiesKhr(this VkPhysicalDevice physicalDevice, VkSurfaceKhr surface, out SurfaceCapabilitiesKhr surfaceCapabilities)
        {
            var ptr = Interop.Alloc<SurfaceCapabilitiesKhr>();
            VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, (SurfaceCapabilitiesKhr*)ptr);
            surfaceCapabilities = Interop.PtrToStructure<SurfaceCapabilitiesKhr>(ptr);
            Interop.Free(ptr);
            return result;
        }
        public unsafe static VkResult GetSurfaceFormatsKhr(this VkPhysicalDevice physicalDevice, VkSurfaceKhr surface, out SurfaceFormatKhr[] surfaceFormats)
        {
            uint count;
            VkResult result = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &count, null);

            surfaceFormats = new SurfaceFormatKhr[count];
            fixed (SurfaceFormatKhr* ptr = surfaceFormats)
                result = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &count, ptr);
            return result;
        }
        public unsafe static VkResult GetPresentModesKhr(this VkPhysicalDevice physicalDevice, VkSurfaceKhr surface, out PresentModeKhr[] presentModes)
        {
            uint count;
            VkResult result = vkGetSurfacePresentModesKHR(physicalDevice, surface, &count, null);

            presentModes = new PresentModeKhr[count];
            fixed (PresentModeKhr* ptr = presentModes)
                result = vkGetSurfacePresentModesKHR(physicalDevice, surface, &count, ptr);
            return result;
        }
        public unsafe static VkResult GetDisplayPropertiesKhr(this VkPhysicalDevice physicalDevice, out DisplayPropertiesKhr[] surfaceCapabilities)
        {
            uint count;
            VkResult result = vkGetPhysicalDeviceDisplayPropertiesKHR(physicalDevice, &count, null);

            var natives = stackalloc DisplayPropertiesKhr.Native[(int)count];
            result = vkGetPhysicalDeviceDisplayPropertiesKHR(physicalDevice, &count, natives);

            surfaceCapabilities = new DisplayPropertiesKhr[count];
            for (uint i = 0; i < count; i++)
                DisplayPropertiesKhr.FromNative(&natives[i], out surfaceCapabilities[i]);
            return result;
        }
        public unsafe static VkResult GetDisplayPlanePropertiesKhr(this VkPhysicalDevice physicalDevice, out DisplayPlanePropertiesKhr[] properties)
        {
            uint count;
            VkResult result = vkGetPhysicalDeviceDisplayPlanePropertiesKHR(physicalDevice, &count, null);

            properties = new DisplayPlanePropertiesKhr[count];
            fixed (DisplayPlanePropertiesKhr* ptr = properties)
                result = vkGetPhysicalDeviceDisplayPlanePropertiesKHR(physicalDevice, &count, ptr);
            return result;
        }
        public unsafe static VkResult GetDisplayPlaneSupportedDisplaysKhr(this VkPhysicalDevice physicalDevice, uint planeIndex, out DisplayKhr[] displays)
        {
            uint count;
            VkResult result = vkGetDisplayPlaneSupportedDisplaysKHR(physicalDevice, planeIndex, &count, null);

            var ptr = stackalloc IntPtr[(int)count];
            result = vkGetDisplayPlaneSupportedDisplaysKHR(physicalDevice, planeIndex, &count, ptr);

            displays = new DisplayKhr[count];
            for (int i = 0; i < count; i++)
                displays[i] = new DisplayKhr(physicalDevice, ptr[i]);

            return result;
        }
        public unsafe static void GetFeatures2Khr(this VkPhysicalDevice physicalDevice, out PhysicalDeviceFeatures2Khr features)
        {
            var ptr = Interop.Alloc<PhysicalDeviceFeatures2Khr>();
            physicalDevice.vkGetPhysicalDeviceFeatures2KHR(physicalDevice, (PhysicalDeviceFeatures2Khr*)ptr);
            features = Interop.PtrToStructure<PhysicalDeviceFeatures2Khr>(ptr);
            Interop.Free(ptr);
        }
        public unsafe static void GetProperties2Khr(this VkPhysicalDevice physicalDevice, out PhysicalDeviceProperties2Khr features)
        {
            var ptr = Interop.Alloc<PhysicalDeviceProperties2Khr.Native>();
            physicalDevice.vkGetPhysicalDeviceProperties2KHR(physicalDevice, (PhysicalDeviceProperties2Khr.Native*)ptr);
            var native = Interop.PtrToStructure<PhysicalDeviceProperties2Khr.Native>(ptr);
            PhysicalDeviceProperties2Khr.FromNative(ref native, out features);
            Interop.Free(ptr);
        }
        public unsafe static void GetFormatProperties2Khr(this VkPhysicalDevice physicalDevice, Format format, out FormatProperties2Khr properties)
        {
            var ptr = Interop.Alloc<FormatProperties2Khr>();
            physicalDevice.vkGetPhysicalDeviceFormatProperties2KHR(physicalDevice, format, (FormatProperties2Khr*)ptr);
            properties = Interop.PtrToStructure<FormatProperties2Khr>(ptr);
            Interop.Free(ptr);
        }
        public unsafe static void GetImageFormatProperties2Khr(this VkPhysicalDevice physicalDevice, PhysicalDeviceImageFormatInfo2Khr imageFormatInfo, out ImageFormatProperties2Khr imageFormatProperties)
        {
            var ptr = Interop.Alloc<ImageFormatProperties2Khr>();
            physicalDevice.vkGetPhysicalDeviceImageFormatProperties2KHR(physicalDevice, &imageFormatInfo, (ImageFormatProperties2Khr*)ptr);
            imageFormatProperties = Interop.PtrToStructure<ImageFormatProperties2Khr>(ptr);
            Interop.Free(ptr);
        }
        public unsafe static void GetQueueFamilyPropertiesKhr(this VkPhysicalDevice physicalDevice, out QueueFamilyProperties2Khr[] queueFamilyProperties)
        {
            uint count;
            physicalDevice.vkGetPhysicalDeviceQueueFamilyProperties2KHR(physicalDevice, &count, null);

            queueFamilyProperties = new QueueFamilyProperties2Khr[count];
            fixed (QueueFamilyProperties2Khr* ptr = queueFamilyProperties)
                physicalDevice.vkGetPhysicalDeviceQueueFamilyProperties2KHR(physicalDevice, &count, ptr);
        }
        public unsafe static void GetMemoryProperties2Khr(this VkPhysicalDevice physicalDevice, out PhysicalDeviceMemoryProperties2Khr memoryProperties)
        {
            PhysicalDeviceMemoryProperties2Khr.Native nativeProperties = default;
            physicalDevice.vkGetPhysicalDeviceMemoryProperties2KHR(physicalDevice, ref nativeProperties);
            PhysicalDeviceMemoryProperties2Khr.FromNative(ref nativeProperties, out memoryProperties);
        }
        public unsafe static void GetSparseImageFormatProperties2Khr(this VkPhysicalDevice physicalDevice, PhysicalDeviceSparseImageFormatInfo2Khr formatInfo, out SparseImageFormatProperties2Khr[] properties)
        {
            uint count;
            physicalDevice.vkGetPhysicalDeviceSparseImageFormatProperties2KHR(physicalDevice, &formatInfo, &count, null);

            properties = new SparseImageFormatProperties2Khr[count];
            fixed (SparseImageFormatProperties2Khr* ptr = properties)
                physicalDevice.vkGetPhysicalDeviceSparseImageFormatProperties2KHR(physicalDevice, &formatInfo, &count, ptr);
        }
        public unsafe static VkResult GetSurfaceCapabilities2Khr(this VkPhysicalDevice physicalDevice, PhysicalDeviceSurfaceInfo2Khr surfaceInfo, SurfaceCapabilities2Khr surfaceCapabilities)
        {
            var ptr = Interop.Alloc<SurfaceCapabilities2Khr>();
            VkResult result = physicalDevice.vkGetPhysicalDeviceSurfaceCapabilities2KHR(physicalDevice, &surfaceInfo, (SurfaceCapabilities2Khr*)ptr);
            surfaceCapabilities = Interop.PtrToStructure<SurfaceCapabilities2Khr>(ptr);
            Interop.Free(ptr);
            return result;
        }
        public unsafe static VkResult GetSurfaceFormats2Khr(this VkPhysicalDevice physicalDevice, PhysicalDeviceSurfaceInfo2Khr surfaceInfo, SurfaceFormat2Khr surfaceFormats)
        {
            var ptr = Interop.Alloc<SurfaceFormat2Khr>();
            VkResult result = physicalDevice.vkGetPhysicalDeviceSurfaceFormats2KHR(physicalDevice, &surfaceInfo, (SurfaceFormat2Khr*)ptr);
            surfaceFormats = Interop.PtrToStructure<SurfaceFormat2Khr>(ptr);
            Interop.Free(ptr);
            return result;
        }
        public unsafe static VkResult GetExternalBufferPropertiesKhr(this VkPhysicalDevice physicalDevice, PhysicalDeviceExternalBufferInfoKhr surfaceInfo, ExternalBufferPropertiesKhr externalBufferProperties)
        {
            var ptr = Interop.Alloc<ExternalBufferPropertiesKhr>();
            VkResult result = physicalDevice.vkGetPhysicalDeviceExternalBufferPropertiesKHR(physicalDevice, &surfaceInfo, (ExternalBufferPropertiesKhr*)ptr);
            externalBufferProperties = Interop.PtrToStructure<ExternalBufferPropertiesKhr>(ptr);
            Interop.Free(ptr);
            return result;
        }
        public unsafe static VkResult GetExternalSemaphorePropertiesKhr(this VkPhysicalDevice physicalDevice, PhysicalDeviceExternalSemaphoreInfoKhr surfaceInfo, ExternalSemaphoreFeaturesKhr externalBufferProperties)
        {
            var ptr = Interop.Alloc<ExternalSemaphoreFeaturesKhr>();
            VkResult result = physicalDevice.vkGetPhysicalDeviceExternalSemaphorePropertiesKHR(physicalDevice, &surfaceInfo, (ExternalSemaphoreFeaturesKhr*)ptr);
            externalBufferProperties = Interop.PtrToStructure<ExternalSemaphoreFeaturesKhr>(ptr);
            Interop.Free(ptr);
            return result;
        }
        #endregion

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr, bool*, VkResult> vkGetPhysicalDeviceSurfaceSupportKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr, bool*, VkResult>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceSurfaceSupportKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr, bool> vkGetPhysicalDeviceWaylandPresentationSupportKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr, bool>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceWaylandPresentationSupportKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, bool> vkGetPhysicalDeviceWin32PresentationSupportKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, bool>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceWin32PresentationSupportKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, IntPtr, bool> vkGetPhysicalDeviceXlibPresentationSupportKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, IntPtr, bool>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceXlibPresentationSupportKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, IntPtr, bool> vkGetPhysicalDeviceXcbPresentationSupportKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, IntPtr, bool>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceXcbPresentationSupportKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, SurfaceCapabilitiesKhr*, VkResult> vkGetPhysicalDeviceSurfaceCapabilitiesKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, SurfaceCapabilitiesKhr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceSurfaceCapabilitiesKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, SurfaceFormatKhr*, VkResult> vkGetPhysicalDeviceSurfaceFormatsKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, SurfaceFormatKhr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceSurfaceFormatsKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, PresentModeKhr*, VkResult> vkGetSurfacePresentModesKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, PresentModeKhr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceSurfacePresentModesKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint*, DisplayPropertiesKhr.Native*, VkResult> vkGetPhysicalDeviceDisplayPropertiesKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint*, DisplayPropertiesKhr.Native*, VkResult>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceDisplayPropertiesKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint*, DisplayPlanePropertiesKhr*, VkResult> vkGetPhysicalDeviceDisplayPlanePropertiesKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint*, DisplayPlanePropertiesKhr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetPhysicalDeviceDisplayPlanePropertiesKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, uint*, IntPtr*, VkResult> vkGetDisplayPlaneSupportedDisplaysKHR = (delegate* unmanaged[Cdecl]<IntPtr, uint, uint*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetDisplayPlaneSupportedDisplaysKHR");
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure describing capabilities of a surface.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SurfaceCapabilitiesKhr
    {
        /// <summary>
        /// The minimum number of images the specified device supports for a swapchain created for
        /// the surface, and will be at least one.
        /// </summary>
        public int MinImageCount;
        /// <summary>
        /// The maximum number of images the specified device supports for a swapchain created for
        /// the surface, and will be either 0, or greater than or equal to <see
        /// cref="MinImageCount"/>. A value of 0 means that there is no limit on the number of
        /// images, though there may be limits related to the total amount of memory used by
        /// presentable images.
        /// </summary>
        public int MaxImageCount;
        /// <summary>
        /// The current width and height of the surface, or the special value <see
        /// cref="Extent2D.WholeSize"/> indicating that the surface size will be determined by the
        /// extent of a swapchain targeting the surface.
        /// </summary>
        public Extent2D CurrentExtent;
        /// <summary>
        /// Contains the smallest valid swapchain extent for the surface on the specified device.
        /// </summary>
        public Extent2D MinImageExtent;
        /// <summary>
        /// Contains the largest valid swapchain extent for the surface on the specified device.
        /// <para>
        /// The width and height of the extent will each be greater than or equal to the
        /// corresponding width and height of <see cref="MinImageExtent"/>.
        /// </para>
        /// <para>
        /// The width and height of the extent will each be greater than or equal to the
        /// corresponding width and height of <see cref="CurrentExtent"/>, unless <see
        /// cref="CurrentExtent"/> has the special value described above.
        /// </para>
        /// </summary>
        public Extent2D MaxImageExtent;
        /// <summary>
        /// The maximum number of layers presentable images can have for a swapchain created for this
        /// device and surface, and will be at least one.
        /// </summary>
        public int MaxImageArrayLayers;
        /// <summary>
        /// A bitmask of <see cref="SurfaceTransformsKhr"/>, indicating the presentation transforms
        /// supported for the surface on the specified device.
        /// <para>At least one bit will be set.</para>
        /// </summary>
        public SurfaceTransformsKhr SupportedTransforms;
        /// <summary>
        /// Indicates the surface's current transform relative to the presentation engine's natural orientation.
        /// </summary>
        public SurfaceTransformsKhr CurrentTransform;
        /// <summary>
        /// A bitmask of <see cref="CompositeAlphasKhr"/>, representing the alpha compositing modes
        /// supported by the presentation engine for the surface on the specified device, and at
        /// least one bit will be set. Opaque composition can be achieved in any alpha compositing
        /// mode by either using an image format that has no alpha component, or by ensuring that all
        /// pixels in the presentable images have an alpha value of 1.0.
        /// </summary>
        public CompositeAlphasKhr SupportedCompositeAlpha;
        /// <summary>
        /// A bitmask of <see cref="ImageUsages"/> representing the ways the application can use the
        /// presentable images of a swapchain created for the surface on the specified device.
        /// <para>
        /// <see cref="ImageUsages.ColorAttachment"/> must be included in the set but implementations
        /// may support additional usages.
        /// </para>
        /// </summary>
        public ImageUsages SupportedUsageFlags;
    }

    /// <summary>
    /// Structure describing a supported swapchain format-color space pair.
    /// <para>
    /// While the <see cref="Format"/> of a presentable image refers to the encoding of each pixel,
    /// the <see cref="ColorSpace"/> determines how the presentation engine interprets the pixel values.
    /// </para>
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SurfaceFormatKhr
    {
        /// <summary>
        /// A <see cref="Format"/> that is compatible with the specified surface.
        /// </summary>
        public Format Format;
        /// <summary>
        /// A presentation <see cref="ColorSpaceKhr"/> that is compatible with the surface.
        /// </summary>
        public ColorSpaceKhr ColorSpace;
    }

    /// <summary>
    /// Structure describing an available display device.
    /// </summary>
    public unsafe struct DisplayPropertiesKhr
    {
        /// <summary>
        /// A handle that is used to refer to the display described here. This handle will be valid
        /// for the lifetime of the Vulkan instance.
        /// </summary>
        public long Display;
        /// <summary>
        /// A unicode string containing the name of the display. Generally, this will be the name
        /// provided by the display's EDID. It can be <c>null</c> if no suitable name is available.
        /// If not <c>null</c>, the memory it points to must remain accessible as long as display is valid.
        /// </summary>
        public string DisplayName;
        /// <summary>
        /// Describes the physical width and height of the visible portion of the display, in millimeters.
        /// </summary>
        public Extent2D PhysicalDimensions;
        /// <summary>
        /// Describes the physical, native, or preferred resolution of the display.
        /// </summary>
        public Extent2D PhysicalResolution;
        /// <summary>
        /// Tells which transforms are supported by this display. This will contain one or more of
        /// the bits from <see cref="SurfaceTransformsKhr"/>.
        /// </summary>
        public SurfaceTransformsKhr SupportedTransforms;
        /// <summary>
        /// Tells whether the planes on this display can have their z order changed. If this is
        /// <c>true</c>, the application can re-arrange the planes on this display in any order
        /// relative to each other.
        /// </summary>
        public bool PlaneReorderPossible;
        /// <summary>
        /// Tells whether the display supports self-refresh/internal buffering. If this is
        /// <c>true</c>, the application can submit persistent present operations on swapchains
        /// created against this display.
        /// </summary>
        public bool PersistentContent;

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public long Display;
            public byte* DisplayName;
            public Extent2D PhysicalDimensions;
            public Extent2D PhysicalResolution;
            public SurfaceTransformsKhr SupportedTransforms;
            public bool PlaneReorderPossible;
            public bool PersistentContent;
        }

        internal static void FromNative(Native* native, out DisplayPropertiesKhr val)
        {
            val.Display = native->Display;

            val.DisplayName = StringExtensions.FromPointer(native->DisplayName);
            val.PhysicalDimensions = native->PhysicalDimensions;
            val.PhysicalResolution = native->PhysicalResolution;
            val.SupportedTransforms = native->SupportedTransforms;
            val.PlaneReorderPossible = native->PlaneReorderPossible;
            val.PersistentContent = native->PersistentContent;
        }
    }

    /// <summary>
    /// Structure describing display plane properties.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DisplayPlanePropertiesKhr
    {
        /// <summary>
        /// The handle of the display the plane is currently associated with. If the plane is not
        /// currently attached to any displays, this will be <see cref="IntPtr.Zero"/>.
        /// </summary>
        public long CurrentDisplay;
        /// <summary>
        /// The current z-order of the plane. This will be between 0 and the count of the elements
        /// returned by <see cref="PhysicalDeviceExtensions.GetDisplayPlanePropertiesKhr"/>.
        /// </summary>
        public int CurrentStackIndex;
    }
}
