using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    public static class SemaphoreKhr
    {
        #region Methods
        public unsafe static VkResult GetSemaphoreWin32HandleKhr(this VkSemaphore semaphore, SemaphoreGetWin32HandleInfoKhr getWin32HandleInfo, out IntPtr handle)
        {
            fixed (IntPtr* ptr = &handle) return semaphore.vkGetSemaphoreWin32HandleKHR(semaphore.LogicalDevice, &getWin32HandleInfo, ptr);
        }
        public unsafe static VkResult GetSemaphoreFdKhr(this VkSemaphore semaphore, SemaphoreGetFdInfoKhr getFdInfo, out int fd)
        {
            fixed (int* ptr = &fd) return semaphore.vkGetSemaphoreFdKHR(semaphore.LogicalDevice, &getFdInfo, ptr);
        }
        #endregion

        #region C Functions
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure describing a Win32 handle semaphore export operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SemaphoreGetWin32HandleInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The semaphore from which state will be exported.
        /// </summary>
        public long Semaphore;
        /// <summary>
        /// The type of handle requested.
        /// </summary>
        public ExternalSemaphoreHandleTypesKhr HandleType;
    }

    /// <summary>
    /// Structure describing a POSIX FD semaphore export operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SemaphoreGetFdInfoKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is the semaphore from which state will be exported.
        /// </summary>
        public long Semaphore;
        /// <summary>
        /// Is the type of handle requested.
        /// </summary>
        public ExternalSemaphoreHandleTypesKhr HandleType;
    }

    /// <summary>
    /// Bitmask of valid external semaphore handle types.
    /// </summary>
    [Flags]
    public enum ExternalSemaphoreHandleTypesKhr
    {
        /// <summary>
        /// Specifies a POSIX file descriptor handle that has only limited valid usage outside of
        /// Vulkan and other compatible APIs.
        /// <para>
        /// It must be compatible with the POSIX system calls <c>dup</c>, <c>dup2</c>, <c>close</c>,
        /// and the non-standard system call <c>dup3</c>. Additionally, it must be transportable over
        /// a socket using an <c>SCM_RIGHTS</c> control message.
        /// </para>
        /// <para>
        /// It owns a reference to the underlying synchronization primitive represented by its Vulkan
        /// semaphore object.
        /// </para>
        /// </summary>
        OpaqueFd = 1 << 0,
        /// <summary>
        /// Specifies an NT handle that has only limited valid usage outside of Vulkan and other
        /// compatible APIs.
        /// <para>
        /// It must be compatible with the functions <c>DuplicateHandle</c>, <c>CloseHandle</c>,
        /// <c>CompareObjectHandles</c>, <c>GetHandleInformation</c>, and <c>SetHandleInformation</c>.
        /// </para>
        /// <para>
        /// It owns a reference to the underlying synchronization primitive represented by its Vulkan
        /// semaphore object.
        /// </para>
        /// </summary>
        OpaqueWin32 = 1 << 1,
        /// <summary>
        /// Specifies a global share handle that has only limited valid usage outside of Vulkan and
        /// other compatible APIs.
        /// <para>It is not compatible with any native APIs.</para>
        /// <para>
        /// It does not own own a reference to the underlying synchronization primitive represented
        /// its Vulkan semaphore object, and will therefore become invalid when all Vulkan semaphore
        /// objects associated with it are destroyed.
        /// </para>
        /// </summary>
        OpaqueWin32Kmt = 1 << 2,
        /// <summary>
        /// Specifies an NT handle returned by <c>ID3D12Device::CreateSharedHandle</c> referring to a
        /// Direct3D 12 fence.
        /// <para>
        /// It owns a reference to the underlying synchronization primitive associated with the
        /// Direct3D fence.
        /// </para>
        /// </summary>
        D3D12Fence = 1 << 3,
        /// <summary>
        /// Specifies a POSIX file descriptor handle to a Linux or Android Fence object.
        /// <para>
        /// It can be used with any native API accepting a valid fence object file descriptor as input.
        /// </para>
        /// <para>
        /// It owns a reference to the underlying synchronization primitive associated with the file descriptor.
        /// </para>
        /// <para>
        /// Implementations which support importing this handle type must accept any type of fence FD
        /// supported by the native system they are running on.
        /// </para>
        /// </summary>
        SyncFd = 1 << 4
    }
}
