using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying buffer creation parameters.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceExternalBufferInfoKhr
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask describing additional parameters of the buffer, corresponding to <see cref="BufferCreateInfo.Flags"/>.
        /// </summary>
        public BufferCreateFlags Flags;
        /// <summary>
        /// A bitmask describing the intended usage of the buffer, corresponding to <see cref="BufferCreateInfo.Usage"/>.
        /// </summary>
        public BufferUsages Usage;
        /// <summary>
        /// A bit indicating a memory handle type that will be used with the memory associated with
        /// the buffer.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleType;
    }
}
