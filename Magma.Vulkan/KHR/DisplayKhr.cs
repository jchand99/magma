using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    public class DisplayKhr
    {
        public VkPhysicalDevice PhysicalDevice { get; internal set; }
        internal IntPtr _Handle;

        public static implicit operator IntPtr(DisplayKhr display) => display == null ? IntPtr.Zero : display._Handle;

        internal DisplayKhr(VkPhysicalDevice parent, IntPtr handle)
        {
            _Handle = handle;
            PhysicalDevice = parent;

            unsafe
            {
                vkAcquireXlibDisplayEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr*, IntPtr, VkResult>)PhysicalDevice.Instance.GetProcAddr("vkAcquireXlibDisplayEXT");
                vkReleaseDisplayEXT = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult>)PhysicalDevice.Instance.GetProcAddr("vkReleaseDisplayEXT");
            }
        }

        #region Methods
        public unsafe VkResult GetDisplayModePropertiesKhr(out DisplayModePropertiesKhr[] displayModeProperties)
        {
            uint count;
            VkResult result = vkGetDisplayModePropertiesKHR(PhysicalDevice, _Handle, &count, null);

            displayModeProperties = new DisplayModePropertiesKhr[count];
            fixed (DisplayModePropertiesKhr* ptr = displayModeProperties) return vkGetDisplayModePropertiesKHR(PhysicalDevice, _Handle, &count, ptr);
        }
        #endregion

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, DisplayModePropertiesKhr*, VkResult> vkGetDisplayModePropertiesKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint*, DisplayModePropertiesKhr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetDisplayModePropertiesKHR");
        #endregion

        #region Ext C Functions
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr*, IntPtr, VkResult> vkAcquireXlibDisplayEXT;
        internal unsafe delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult> vkReleaseDisplayEXT;
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure describing display mode properties.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DisplayModePropertiesKhr
    {
        /// <summary>
        /// A handle to the display mode described in this structure. This handle will be valid for
        /// the lifetime of the Vulkan instance.
        /// </summary>
        public long DisplayMode;
        /// <summary>
        /// Is a <see cref="DisplayModeParametersKhr"/> structure describing the display parameters
        /// associated with displayMode.
        /// </summary>
        public DisplayModeParametersKhr Parameters;
    }

    /// <summary>
    /// Structure describing display parameters associated with a display mode.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DisplayModeParametersKhr
    {
        /// <summary>
        /// The 2D extents of the visible region.
        /// </summary>
        public Extent2D VisibleRegion;
        /// <summary>
        /// The number of times the display is refreshed each second multiplied by 1000.
        /// </summary>
        public int RefreshRate;
    }
}
