using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    public static class DeviceMemoryKhr
    {
        #region Methods
        public unsafe static VkResult GetMemoryWin32HandleKhr(this VkDeviceMemory deviceMemory, MemoryGetWin32HandleInfoKhr getWin32HandleInfo, out IntPtr handle)
        {
            fixed (IntPtr* ptr = &handle) return deviceMemory.vkGetMemoryWin32HandleKHR(deviceMemory.LogicalDevice, &getWin32HandleInfo, ptr);
        }
        public unsafe static VkResult GetMemoryFdKhr(this VkDeviceMemory deviceMemory, MemoryGetFdInfoKhr getWin32HandleInfo, out IntPtr handle)
        {
            fixed (IntPtr* ptr = &handle) return deviceMemory.vkGetMemoryFdKHR(deviceMemory.LogicalDevice, &getWin32HandleInfo, ptr);
        }
        #endregion

    }

    // Structs

    /// <summary>
    /// Structure describing a Win32 handle semaphore export operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryGetWin32HandleInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The memory object from which the handle will be exported.
        /// </summary>
        public long Memory;
        /// <summary>
        /// The type of handle requested.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleType;
    }

    /// <summary>
    /// Structure describing a POSIX FD semaphore export operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryGetFdInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The memory object from which the handle will be exported.
        /// </summary>
        public long Memory;
        /// <summary>
        /// The type of handle requested.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleType;
    }
}
