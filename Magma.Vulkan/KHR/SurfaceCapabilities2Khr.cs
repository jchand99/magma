using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing capabilities of a surface.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SurfaceCapabilities2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Describes the capabilities of the specified surface.
        /// </summary>
        public SurfaceCapabilitiesKhr SurfaceCapabilities;

        /// <summary>
        /// Initializes a new instance of the <see cref="SurfaceCapabilities2Khr"/> structure.
        /// </summary>
        /// <param name="surfaceCapabilities">Describes the capabilities of the specified surface.</param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public SurfaceCapabilities2Khr(SurfaceCapabilitiesKhr surfaceCapabilities, IntPtr next = default(IntPtr))
        {
            Type = StructureType.SurfaceCapabilities2Khr;
            Next = next;
            SurfaceCapabilities = surfaceCapabilities;
        }

        internal void Prepare()
        {
            Type = StructureType.SurfaceCapabilities2Khr;
        }
    }
}
