using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure containing rectangular region changed by <see
    /// cref="QueueExtensions.PresentKhr(Queue, PresentInfoKhr)"/> for a given <see cref="Image"/>.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PresentRegionKhr
    {
        /// <summary>
        /// The number of rectangles in <see cref="Rectangles"/>, or zero if the entire image has
        /// changed and should be presented.
        /// </summary>
        public int RectangleCount;
        /// <summary>
        /// Is either <c>null</c> or a pointer to an array of <see cref="RectLayerKhr"/> structures.
        /// <para>
        /// The <see cref="RectLayerKhr"/> structure is the framebuffer coordinates, plus layer, of a
        /// portion of a presentable image that has changed and must be presented. If non-
        /// <c>null</c>, each entry in <see cref="Rectangles"/> is a rectangle of the given image
        /// that has changed since the last image was presented to the given swapchain.
        /// </para>
        /// </summary>
        public RectLayerKhr* Rectangles;

        /// <summary>
        /// Initializes a new instance of the <see cref="PresentRegionKhr"/> structure.
        /// </summary>
        /// <param name="rectangleCount">
        /// The number of rectangles in <see cref="Rectangles"/>, or zero if the entire image has
        /// changed and should be presented.
        /// </param>
        /// <param name="rectangles">
        /// Is either <c>null</c> or a pointer to an array of <see cref="RectLayerKhr"/> structures.
        /// <para>
        /// The <see cref="RectLayerKhr"/> structure is the framebuffer coordinates, plus layer, of a
        /// portion of a presentable image that has changed and must be presented. If non-
        /// <c>null</c>, each entry in <see cref="Rectangles"/> is a rectangle of the given image
        /// that has changed since the last image was presented to the given swapchain.
        /// </para>
        /// </param>
        public PresentRegionKhr(int rectangleCount, RectLayerKhr* rectangles)
        {
            RectangleCount = rectangleCount;
            Rectangles = rectangles;
        }
    }
}
