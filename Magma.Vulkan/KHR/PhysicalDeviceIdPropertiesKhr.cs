using System;
using System.Runtime.InteropServices;
using static Magma.Vulkan.Constants;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying IDs related to the physical device.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PhysicalDeviceIdPropertiesKhr
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// An array of size <see cref="UuidSize"/>, containing 8-bit values that represent a
        /// universally unique identifier for the device.
        /// </summary>
        public fixed byte DeviceUuid[UuidSize];
        /// <summary>
        /// An array of size <see cref="UuidSize"/>, containing 8-bit values that represent a
        /// universally unique identifier for the driver build in use by the device.
        /// </summary>
        public fixed byte DriverUuid[UuidSize];
        /// <summary>
        /// A array of size <see cref="LuidSizeKhr"/>, containing 8-bit values that represent a
        /// locally unique identifier for the device.
        /// </summary>
        public fixed byte DeviceLuid[LuidSizeKhr];
        /// <summary>
        /// A bitfield identifying the node within a linked device adapter corresponding to the device.
        /// </summary>
        public int DeviceNodeMask;
        /// <summary>
        /// A boolean value that will be <c>true</c> if <see cref="DeviceLuid"/> contains a valid
        /// LUID, and <c>false</c> if it does not.
        /// </summary>
        public bool DeviceLuidValid;
    }
}
