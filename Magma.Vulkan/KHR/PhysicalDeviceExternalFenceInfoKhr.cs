using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying fence creation parameters.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceExternalFenceInfoKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is NULL or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is a <see cref="ExternalFenceHandleTypeFlagsKhr"/> value indicating an external fence
        /// handle type for which capabilities will be returned.
        /// </summary>
        public ExternalFenceHandleTypeFlagsKhr HandleType;
    }
}
