using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing capabilities of a surface for shared presentation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SharedPresentSurfaceCapabilitiesKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask representing the ways the application can use the shared presentable image from
        /// a swapchain created with <see cref="PresentModeKhr"/> set to <see
        /// cref="PresentModeKhr.SharedDemandRefreshKhr"/> or <see
        /// cref="PresentModeKhr.SharedContinuousRefreshKhr"/> for the surface on the specified device.
        /// <para>
        /// <see cref="ImageUsages.ColorAttachment"/> must be included in the set but implementations
        /// may support additional usages.
        /// </para>
        /// </summary>
        public ImageUsages SharedPresentSupportedUsages;

        /// <summary>
        /// Initializes a new instance of the <see cref="SharedPresentSurfaceCapabilitiesKhr"/> structure.
        /// </summary>
        /// <param name="sharedPresentSupportedUsages">
        /// A bitmask representing the ways the application can use the shared presentable image from
        /// a swapchain created with <see cref="PresentModeKhr"/> set to <see
        /// cref="PresentModeKhr.SharedDemandRefreshKhr"/> or <see
        /// cref="PresentModeKhr.SharedContinuousRefreshKhr"/> for the surface on the specified device.
        /// <para>
        /// <see cref="ImageUsages.ColorAttachment"/> must be included in the set but implementations
        /// may support additional usages.
        /// </para>
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public SharedPresentSurfaceCapabilitiesKhr(ImageUsages sharedPresentSupportedUsages, IntPtr next = default(IntPtr))
        {
            Type = StructureType.SharedPresentSurfaceCapabilitiesKhr;
            Next = next;
            SharedPresentSupportedUsages = sharedPresentSupportedUsages;
        }

        internal void Prepare()
        {
            Type = StructureType.SharedPresentSurfaceCapabilitiesKhr;
        }
    }
}
