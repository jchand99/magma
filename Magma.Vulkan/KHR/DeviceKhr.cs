using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Magma.Vulkan.KHR
{
    public static class DeviceKhr
    {
        #region Methods
        public unsafe static VkResult GetMemoryWin32HandlePropertiesKhr(this VkDevice device, ExternalMemoryHandleTypesKhr handleType, IntPtr handle, out MemoryWin32HandlePropertiesKhr memoryWin32HandleProperties)
        {
            fixed (MemoryWin32HandlePropertiesKhr* ptr = &memoryWin32HandleProperties) return device.vkGetMemoryWin32HandlePropertiesKHR(device, handleType, handle, ptr);
        }
        public unsafe static VkResult GetMemoryFdPropertiesKhr(this VkDevice device, ExternalMemoryHandleTypesKhr handleTypes, int fd, out MemoryFdPropertiesKhr memoryFdProperties)
        {
            fixed (MemoryFdPropertiesKhr* ptr = &memoryFdProperties) return device.vkGetMemoryFdPropertiesKHR(device, handleTypes, fd, ptr);
        }
        public unsafe static VkResult ImportSemaphoreWin32HandleKHR(this VkDevice device, out ImportSemaphoreWin32HandleInfoKhr importSemaphoreWin32HandleInfo)
        {
            var native = Interop.Alloc<ImportSemaphoreWin32HandleInfoKhr.Native>();
            VkResult result = device.vkImportSemaphoreWin32HandleKHR(device, &native);
            var nativeS = Interop.PtrToStructure<ImportSemaphoreWin32HandleInfoKhr.Native>(native);
            ImportSemaphoreWin32HandleInfoKhr.FromNative(ref nativeS, device, nativeS.Semaphore, out importSemaphoreWin32HandleInfo);
            Interop.Free(native);
            return result;
        }
        public unsafe static VkResult ImportSemaphoreFdKHR(this VkDevice device, out ImportSemaphoreFdInfoKhr importSemaphoreWin32HandleInfo)
        {
            var native = Interop.Alloc<ImportSemaphoreFdInfoKhr.Native>();
            VkResult result = device.vkImportSemaphoreFdKHR(device, &native);
            var nativeS = Interop.PtrToStructure<ImportSemaphoreFdInfoKhr.Native>(native);
            ImportSemaphoreFdInfoKhr.FromNative(ref nativeS, device, nativeS.Semaphore, out importSemaphoreWin32HandleInfo);
            Interop.Free(native);
            return result;
        }
        public unsafe static void GetBufferMemoryRequirements2Khr(this VkDevice device, BufferMemoryRequirementsInfo2Khr info, MemoryRequirements2Khr memoryRequirements)
        {
            device.vkGetBufferMemoryRequirements2KHR(device, &info, &memoryRequirements);
        }
        public unsafe static void GetImageMemoryRequirements2Khr(this VkDevice device, ImageMemoryRequirementsInfo2Khr info, MemoryRequirements2Khr memoryRequirements)
        {
            device.vkGetImageMemoryRequirements2KHR(device, &info, &memoryRequirements);
        }
        public unsafe static void GetSparseImageMemoryRequirements2Khr(this VkDevice device, ImageSparseMemoryRequirementsInfo2Khr info, SparseImageMemoryRequirements2Khr[] sparseImageMemoryRequirements)
        {
            fixed (SparseImageMemoryRequirements2Khr* ptr = sparseImageMemoryRequirements) device.vkGetImageSparseMemoryRequirements2KHR(device, &info, (uint)(sparseImageMemoryRequirements?.Length ?? 0), ptr);
        }
        public unsafe static VkResult BindBufferMemory2Khr(this VkDevice device, BindBufferMemoryInfoKhr[] bindInfos)
        {
            int count = bindInfos?.Length ?? 0;
            var natives = stackalloc BindBufferMemoryInfoKhr.Native[count];
            for (int i = 0; i < count; i++)
                bindInfos[i].ToNative(out natives[i]);

            return device.vkBindBufferMemory2KHR(device, (uint)count, natives);
        }
        public unsafe static VkResult BindImageMemory2Khr(this VkDevice device, BindImageMemoryInfoKhr[] bindInfos)
        {
            int count = bindInfos?.Length ?? 0;
            var natives = stackalloc BindImageMemoryInfoKhr.Native[count];
            for (int i = 0; i < count; i++)
                bindInfos[i].ToNative(out natives[i]);

            return device.vkBindImageMemory2KHR(device, (uint)count, natives);
        }
        #endregion
    }

    // Structs

    /// <summary>
    /// Bitmask specifying external memory handle types.
    /// </summary>
    [Flags]
    public enum ExternalMemoryHandleTypesKhr
    {
        /// <summary>
        /// Specifies a POSIX file descriptor handle that has only limited valid usage outside of
        /// Vulkan and other compatible APIs.
        /// <para>
        /// It must be compatible with the POSIX system calls <c>dup</c>, <c>dup2</c>, <c>close</c>,
        /// and the non-standard system call <c>dup3</c>. Additionally, it must be transportable over
        /// a socket using an <c>SCM_RIGHTS</c> control message.
        /// </para>
        /// <para>
        /// It owns a reference to the underlying memory resource represented by its Vulkan memory object.
        /// </para>
        /// </summary>
        OpaqueFd = 1 << 0,
        /// <summary>
        /// Specifies an NT handle that has only limited valid usage outside of Vulkan and other
        /// compatible APIs.
        /// <para>
        /// It must: be compatible with the functions <c>DuplicateHandle</c>, <c>CloseHandle</c>,
        /// <c>CompareObjectHandles</c>, <c>GetHandleInformation</c>, and <c>SetHandleInformation</c>.
        /// </para>
        /// <para>
        /// It owns a reference to the underlying memory resource represented by its Vulkan memory object.
        /// </para>
        /// </summary>
        OpaqueWin32 = 1 << 1,
        /// <summary>
        /// Specifies a global share handle that has only limited valid usage outside of Vulkan and
        /// other compatible APIs.
        /// <para>It is not compatible with any native APIs.</para>
        /// <para>
        /// It does not own own a reference to the underlying memory resource represented its Vulkan
        /// memory object, and will therefore become invalid when all Vulkan memory objects
        /// associated with it are destroyed.
        /// </para>
        /// </summary>
        OpaqueWin32Kmt = 1 << 2,
        /// <summary>
        /// Specifies an NT handle returned by <c>IDXGIResource1::CreateSharedHandle</c> referring to
        /// a Direct3D 10 or 11 texture resource.
        /// <para>It owns a reference to the memory used by the Direct3D resource.</para>
        /// </summary>
        D3D11Texture = 1 << 3,
        /// <summary>
        /// Specifies a global share handle returned by <c>IDXGIResource::GetSharedHandle</c>
        /// referring to a Direct3D 10 or 11 texture resource.
        /// <para>
        /// It does not own own a reference to the underlying Direct3D resource, and will therefore
        /// become invalid when all Vulkan memory objects and Direct3D resources associated with it
        /// are destroyed.
        /// </para>
        /// </summary>
        D3D11TextureKmt = 1 << 4,
        /// <summary>
        /// Specifies an NT handle returned by <c>ID3D12Device::CreateSharedHandle</c> referring to a
        /// Direct3D 12 heap resource.
        /// <para>It owns a reference to the resources used by the Direct3D heap.</para>
        /// </summary>
        D3D12Heap = 1 << 5,
        /// <summary>
        /// Specifies an NT handle returned by <c>ID3D12Device::CreateSharedHandle</c> referring to a
        /// Direct3D 12 committed resource.
        /// <para>It owns a reference to the memory used by the Direct3D resource.</para>
        /// </summary>
        D3D12Resource = 1 << 6,
        /// <summary>
        /// Is a file descriptor for a Linux DmaBuf. It owns a reference to the underlying memory
        /// resource represented by its Vulkan memory object.
        /// </summary>
        DmaBufExt = 1 << 9,
        /// <summary>
        /// Specifies a host pointer returned by a host memory allocation command. It does not own a
        /// reference to the underlying memory resource, and will therefore become invalid if the
        /// host memory is freed.
        /// </summary>
        HostAllocationExt = 1 << 7,
        /// <summary>
        /// Specifies a host pointer to host mapped foreign Memory. It does not own a reference to
        /// the underlying memory resource, and will therefore become invalid if the foreign memory
        /// is unmapped or otherwise becomes no longer available.
        /// </summary>
        HostMappedForeignMemoryExt = 1 << 8
    }

    /// <summary>
    /// Properties of external memory windows handles.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryWin32HandlePropertiesKhr
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask containing one bit set for every memory type which the specified windows handle
        /// can be imported as.
        /// </summary>
        public int MemoryTypeBits;
    }

    /// <summary>
    /// Properties of external memory file descriptors.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryFdPropertiesKhr
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask containing one bit set for every memory type which the specified file
        /// descriptor can be imported as.
        /// </summary>
        public int MemoryTypeBits;
    }

    /// <summary>
    /// Structure specifying Windows handle to import to a semaphore.
    /// </summary>
    public struct ImportSemaphoreWin32HandleInfoKhr
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The semaphore into which the state will be imported.
        /// </summary>
        public VkSemaphore Semaphore;
        /// <summary>
        /// Specifies additional parameters for the semaphore payload import operation.
        /// </summary>
        public SemaphoreImportFlagsKhr Flags;
        /// <summary>
        /// Specifies the type of <see cref="Handle"/>.
        /// </summary>
        public ExternalSemaphoreHandleTypesKhr HandleType;
        /// <summary>
        /// The external handle to import, or <c>null</c>.
        /// </summary>
        public IntPtr Handle;
        /// <summary>
        /// A NULL-terminated UTF-16 string naming the underlying synchronization primitive to
        /// import, or <c>null</c>.
        /// </summary>
        public string Name;

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public IntPtr Semaphore;
            public SemaphoreImportFlagsKhr Flags;
            public ExternalSemaphoreHandleTypesKhr HandleType;
            public IntPtr Handle;
            public IntPtr Name;
        }

        internal static void FromNative(ref Native native, VkDevice device, IntPtr semaphore,
            out ImportSemaphoreWin32HandleInfoKhr managed)
        {
            managed.Next = native.Next;
            managed.Semaphore = new VkSemaphore(device, semaphore);
            managed.Flags = native.Flags;
            managed.HandleType = native.HandleType;
            managed.Handle = native.Handle;
            managed.Name = StringExtensions.FromPointer(native.Name);
        }
    }

    /// <summary>
    /// Structure specifying POSIX file descriptor to import to a semaphore.
    /// </summary>
    public struct ImportSemaphoreFdInfoKhr
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The semaphore into which the payload will be imported.
        /// </summary>
        public IntPtr Semaphore;
        /// <summary>
        /// Specifies additional parameters for the semaphore payload import operation.
        /// </summary>
        public SemaphoreImportFlagsKhr Flags;
        /// <summary>
        /// Specifies the type of <see cref="Fd"/>.
        /// </summary>
        public ExternalSemaphoreHandleTypesKhr HandleType;
        /// <summary>
        /// The external handle to import.
        /// </summary>
        public int Fd;

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public IntPtr Semaphore;
            public SemaphoreImportFlagsKhr Flags;
            public ExternalSemaphoreHandleTypesKhr HandleType;
            public int Fd;
        }

        internal static void FromNative(ref Native native, VkDevice device, IntPtr semaphore,
            out ImportSemaphoreFdInfoKhr managed)
        {
            managed.Next = native.Next;
            managed.Semaphore = new VkSemaphore(device, semaphore);
            managed.Flags = native.Flags;
            managed.HandleType = native.HandleType;
            managed.Fd = native.Fd;
        }
    }

    /// <summary>
    /// Structure specifying memory requirements.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryRequirements2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Describes the memory requirements of the resource.
        /// </summary>
        public MemoryRequirements MemoryRequirements;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct BufferMemoryRequirementsInfo2Khr
    {
        /// <summary>
        /// Type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkBuffer"/> to query.
        /// </summary>
        public long Buffer;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ImageMemoryRequirementsInfo2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkImage"/> to query.
        /// </summary>
        public long Image;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ImageSparseMemoryRequirementsInfo2Khr
    {
        public StructureType Type;
        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        public long Image;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SparseImageMemoryRequirements2Khr
    {
        public StructureType Type;
        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        public SparseImageMemoryRequirements MemoryRequirements;
    }

    /// <summary>
    /// Structure specifying how to bind a buffer to memory.
    /// </summary>
    public unsafe struct BindBufferMemoryInfoKhr
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkBuffer"/> to be attached to memory.
        /// </summary>
        public VkBuffer Buffer;
        /// <summary>
        /// A <see cref="VkDeviceMemory"/> object describing the device memory to attach.
        /// </summary>
        public VkDeviceMemory Memory;
        /// <summary>
        /// The start offset of the region of memory which is to be bound to the buffer. The number
        /// of bytes returned in the <see cref="MemoryRequirements.Size"/> member in memory, starting
        /// from <see cref="MemoryOffset"/> bytes, will be bound to the specified buffer.
        /// </summary>
        public long MemoryOffset;
        /// <summary>
        /// An array of device indices.
        /// </summary>
        public int[] DeviceIndices;

        /// <summary>
        /// Initializes a new instance of the <see cref="BindBufferMemoryInfoKhr"/> structure.
        /// </summary>
        /// <param name="buffer">The <see cref="Magma.Vulkan.VkBuffer"/> to be attached to memory.</param>
        /// <param name="memory">
        /// A <see cref="VkDeviceMemory"/> object describing the device memory to attach.
        /// </param>
        /// <param name="memoryOffset">
        /// The start offset of the region of memory which is to be bound to the buffer. The number
        /// of bytes returned in the <see cref="MemoryRequirements.Size"/> member in memory, starting
        /// from <see cref="MemoryOffset"/> bytes, will be bound to the specified buffer.
        /// </param>
        /// <param name="deviceIndices">An array of device indices.</param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public BindBufferMemoryInfoKhr(VkBuffer buffer, VkDeviceMemory memory, long memoryOffset,
            int[] deviceIndices, IntPtr next = default)
        {
            Next = next;
            Buffer = buffer;
            Memory = memory;
            MemoryOffset = memoryOffset;
            DeviceIndices = deviceIndices;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public IntPtr Buffer;
            public IntPtr Memory;
            public long MemoryOffset;
            public int DeviceIndexCount;
            public int* DeviceIndices;
        }

        internal void ToNative(out Native native)
        {
            native.Type = StructureType.BindBufferMemoryInfo;
            native.Next = Next;
            native.Buffer = Buffer;
            native.Memory = Memory;
            native.MemoryOffset = MemoryOffset;
            native.DeviceIndexCount = DeviceIndices?.Length ?? 0;
            fixed (int* ptr = DeviceIndices)
                native.DeviceIndices = ptr;
        }
    }

    /// <summary>
    /// Structure specifying how to bind an image to memory.
    /// </summary>
    public unsafe struct BindImageMemoryInfoKhr
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The <see cref="Magma.Vulkan.VkImage"/> to be attached to memory.
        /// </summary>
        public VkImage Image;
        /// <summary>
        /// A <see cref="VkDeviceMemory"/> object describing the device memory to attach.
        /// </summary>
        public VkDeviceMemory Memory;
        /// <summary>
        /// The start offset of the region of memory which is to be bound to the image. The number of
        /// bytes returned in the <see cref="MemoryRequirements.Size"/> member in memory, starting
        /// from <see cref="MemoryOffset"/> bytes, will be bound to the specified image.
        /// </summary>
        public long MemoryOffset;
        /// <summary>
        /// An array of device indices.
        /// </summary>
        public int[] DeviceIndices;
        /// <summary>
        /// An array of rectangles describing which regions of the image are attached to each
        /// instance of memory.
        /// </summary>
        public VkRect2D[] SFRRects;

        /// <summary>
        /// Initializes a new instance of the <see cref="BindImageMemoryInfoKhr"/> structure.
        /// </summary>
        /// <param name="image">The <see cref="Magma.Vulkan.VkImage"/> to be attached to memory.</param>
        /// <param name="memory">
        /// A <see cref="VkDeviceMemory"/> object describing the device memory to attach.
        /// </param>
        /// <param name="memoryOffset">
        /// The start offset of the region of memory which is to be bound to the image. If the length
        /// of <see cref="SFRRects"/> is zero, the number of bytes returned in the <see
        /// cref="MemoryRequirements.Size"/> member in memory, starting from <see
        /// cref="MemoryOffset"/> bytes, will be bound to the specified image.
        /// </param>
        /// <param name="deviceIndices">An array of device indices.</param>
        /// <param name="sfrRects">
        /// An array of rectangles describing which regions of the image are attached to each
        /// instance of memory.
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public BindImageMemoryInfoKhr(VkImage image, VkDeviceMemory memory, long memoryOffset,
            int[] deviceIndices, VkRect2D[] sfrRects = null, IntPtr next = default(IntPtr))
        {
            Next = next;
            Image = image;
            Memory = memory;
            MemoryOffset = memoryOffset;
            DeviceIndices = deviceIndices;
            SFRRects = sfrRects;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public IntPtr Image;
            public IntPtr Memory;
            public long MemoryOffset;
            public int DeviceIndexCount;
            public int* DeviceIndices;
            public int SFRRectCount;
            public VkRect2D* SFRRects;
        }

        internal void ToNative(out Native native)
        {
            native.Type = StructureType.BindBufferMemoryInfo;
            native.Next = Next;
            native.Image = Image;
            native.Memory = Memory;
            native.MemoryOffset = MemoryOffset;
            native.DeviceIndexCount = DeviceIndices?.Length ?? 0;
            fixed (int* ptr = DeviceIndices)
                native.DeviceIndices = ptr;
            native.SFRRectCount = SFRRects?.Length ?? 0;
            fixed (VkRect2D* ptr = SFRRects)
                native.SFRRects = ptr;
        }
    }
}
