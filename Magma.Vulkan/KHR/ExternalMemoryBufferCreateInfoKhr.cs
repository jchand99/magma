using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Specify that a buffer may be backed by external memory.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExternalMemoryBufferCreateInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Specifies one or more external memory handle types.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleTypes;
    }
}
