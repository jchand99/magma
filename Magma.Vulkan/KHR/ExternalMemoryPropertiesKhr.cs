using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying external memory handle type capabilities.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExternalMemoryPropertiesKhr
    {
        /// <summary>
        /// A bitmask describing the features of handle type.
        /// </summary>
        public ExternalMemoryFeaturesKhr ExternalMemoryFeatures;
        /// <summary>
        /// A bitmask specifying handle types that can be used to import objects from which handle
        /// type can be exported.
        /// </summary>
        public ExternalMemoryHandleTypesKhr ExportFromImportedHandleTypes;
        /// <summary>
        /// A bitmask specifying handle types which can be specified at the same time as handle type
        /// when creating an image compatible with external memory.
        /// </summary>
        public ExternalMemoryHandleTypesKhr CompatibleHandleTypes;
    }
}
