using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Bitfield describing features of an external fence handle type.
    /// </summary>
    [Flags]
    public enum ExternalFenceFeatureFlagsKhr
    {
        /// <summary>
        /// Indicates handles of this type can be exported from Vulkan fence objects.
        /// </summary>
        Exportable = 1 << 0,
        /// <summary>
        /// Indicates handles of this type can be imported to Vulkan fence objects.
        /// </summary>
        Importable = 1 << 1
    }
}
