using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying sparse image format inputs.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceSparseImageFormatInfo2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The image format.
        /// </summary>
        public Format Format;
        /// <summary>
        /// The dimensionality of image.
        /// </summary>
        public ImageType ImageType;
        /// <summary>
        /// The number of samples per texel as defined in <see cref="SampleCounts"/>.
        /// </summary>
        public SampleCounts Samples;
        /// <summary>
        /// A bitmask describing the intended usage of the image.
        /// </summary>
        public ImageUsages Usage;
        /// <summary>
        /// The tiling arrangement of the data elements in memory.
        /// </summary>
        public ImageTiling Tiling;
    }
}
