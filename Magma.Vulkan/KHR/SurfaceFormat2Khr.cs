using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing a supported swapchain format tuple.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SurfaceFormat2Khr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Describes a format-color space pair that is compatible with the specified surface.
        /// </summary>
        public SurfaceFormatKhr SurfaceFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="SurfaceFormat2Khr"/> structure.
        /// </summary>
        /// <param name="surfaceFormat">
        /// Describes a format-color space pair that is compatible with the specified surface.
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public SurfaceFormat2Khr(SurfaceFormatKhr surfaceFormat, IntPtr next = default)
        {
            Type = StructureType.SurfaceFormat2Khr;
            Next = next;
            SurfaceFormat = surfaceFormat;
        }

        internal void Prepare()
        {
            Type = StructureType.SurfaceFormat2Khr;
        }
    }
}
