using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying, for a given subpass/input attachment pair, which aspect can be read.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RenderPassInputAttachmentAspectCreateInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The number of elements in the <see cref="AspectReferences"/> array.
        /// </summary>
        public int AspectReferenceCount;
        /// <summary>
        /// Points to an array of <see cref="AspectReferenceCount"/> number of <see
        /// cref="InputAttachmentAspectReferenceKhr"/> structures describing which aspect(s) can be
        /// accessed for a given input attachment within a given subpass.
        /// </summary>
        public IntPtr AspectReferences;
    }
}
