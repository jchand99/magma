using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying supported external handle capabilities.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExternalBufferPropertiesKhr
    {
        internal StructureType Type;

        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Specifies various capabilities of the external handle type when used with the specified
        /// buffer creation parameters.
        /// </summary>
        public ExternalMemoryPropertiesKhr ExternalMemoryProperties;
    }
}
