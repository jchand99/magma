using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure containing a rectangle, including layer, changed by <see
    /// cref="QueueExtensions.PresentKhr(VkQueue, PresentInfoKhr)"/> for a given <see cref="VkImage"/>.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct RectLayerKhr
    {
        /// <summary>
        /// The origin of the rectangle, in pixels.
        /// </summary>
        public Offset2D Offset;
        /// <summary>
        /// The size of the rectangle, in pixels.
        /// </summary>
        public Extent2D Extent;
        /// <summary>
        /// The layer of the image.
        /// <para>For images with only one layer, the value of <see cref="Layer"/> must be 0.</para>
        /// </summary>
        public int Layer;

        /// <summary>
        /// Initializes a new instance of the <see cref="RectLayerKhr"/> structure.
        /// </summary>
        /// <param name="offset">The origin of the rectangle, in pixels.</param>
        /// <param name="extent">The size of the rectangle, in pixels.</param>
        /// <param name="layer">
        /// The layer of the image.
        /// <para>For images with only one layer, the value of <see cref="Layer"/> must be 0.</para>
        /// </param>
        public RectLayerKhr(Offset2D offset, Extent2D extent, int layer)
        {
            Offset = offset;
            Extent = extent;
            Layer = layer;
        }
    }
}
