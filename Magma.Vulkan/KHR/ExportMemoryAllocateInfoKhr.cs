using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Specify exportable handle types for a device memory object.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExportMemoryAllocateInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Specifies one or more memory handle types the application can export from the resulting
        /// allocation. The application can request multiple handle types for the same allocation.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleTypes;
    }
}
