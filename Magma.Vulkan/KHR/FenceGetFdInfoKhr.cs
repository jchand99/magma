using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing a POSIX FD fence export operation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FenceGetFdInfoKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is the fence from which state will be exported.
        /// </summary>
        public long Fence;
        /// <summary>
        /// Is the type of handle requested.
        /// </summary>
        public ExternalFenceHandleTypeFlagsKhr HandleType;
    }
}
