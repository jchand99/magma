using System;

namespace Magma.Vulkan.KHR
{
    public static class DescriptorSetKhr
    {
        #region Methods
        public unsafe static void UpdateDescriptorSetWithTemplateKhr(this VkDescriptorSet descriptorSet, VkDevice device, DescriptorUpdateTemplateKhr descriptorUpdateTemplate, IntPtr data) => vkUpdateDescriptorSetWithTemplateKHR(device, descriptorSet, descriptorUpdateTemplate, data);
        #endregion

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, IntPtr, void> vkUpdateDescriptorSetWithTemplateKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, IntPtr, IntPtr, void>)Vulkan.GetStaticProcPointer("vkUpdateDescriptorSetWithTemplateKHR");
        #endregion
    }
}
