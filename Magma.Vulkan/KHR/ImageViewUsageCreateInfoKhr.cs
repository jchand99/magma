using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Specify the intended usage of an image view.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageViewUsageCreateInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask describing the allowed usages of the image view.
        /// </summary>
        public ImageUsages Usage;
    }
}
