using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying handle types that can be exported from a fence.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExportFenceCreateInfoKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is a bitmask of <see cref="ExternalFenceHandleTypeFlagsKhr"/> specifying one or more
        /// fence handle types the application can export from the resulting fence. The application
        /// can request multiple handle types for the same fence.
        /// </summary>
        public ExternalFenceHandleTypeFlagsKhr HandleTypes;
    }
}
