using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Specify a dedicated memory allocation resource.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryDedicatedAllocateInfoKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is 0 or a handle of an image which this memory will be bound to.
        /// </summary>
        public long Image;
        /// <summary>
        /// Is 0 or a handle of a buffer which this memory will be bound to.
        /// </summary>
        public long Buffer;
    }
}
