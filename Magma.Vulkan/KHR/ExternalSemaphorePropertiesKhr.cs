using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing supported external semaphore handle features.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExternalSemaphorePropertiesKhr
    {
        internal StructureType Type;

        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bitmask specifying handle types that can be used to import objects from which
        /// handleType can be exported.
        /// </summary>
        public ExternalSemaphoreHandleTypesKhr ExportFromImportedHandleTypes;
        /// <summary>
        /// A bitmask specifying handle types which can be specified at the same time as handleType
        /// when creating a semaphore.
        /// </summary>
        public ExternalSemaphoreHandleTypesKhr CompatibleHandleTypes;
        /// <summary>
        /// A bitmask describing the features of handle type.
        /// </summary>
        public ExternalSemaphoreFeaturesKhr ExternalSemaphoreFeatures;
    }
}
