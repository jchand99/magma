using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing dedicated allocation requirements of buffer and image resources.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryDedicatedRequirementsKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Indicates that the implementation would prefer a dedicated allocation for this resource.
        /// The application is still free to suballocate the resource but it may get better
        /// performance if a dedicated allocation is used.
        /// </summary>
        public bool PrefersDedicatedAllocation;
        /// <summary>
        /// Indicates that a dedicated allocation is required for this resource.
        /// </summary>
        public bool RequiresDedicatedAllocation;
    }
}
