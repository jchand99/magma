using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing features supported by VKKHR16bitStorage.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDevice16BitStorageFeaturesKhr
    {
        public StructureType Type;
        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// 16-bit integer/floating-point variables supported in BufferBlock.
        /// </summary>
        public bool StorageBuffer16BitAccess;
        /// <summary>
        /// 16-bit integer/floating-point variables supported in BufferBlock and Block.
        /// </summary>
        public bool UniformAndStorageBuffer16BitAccess;
        /// <summary>
        /// 16-bit integer/floating-point variables supported in PushConstant.
        /// </summary>
        public bool StoragePushConstant16;
        /// <summary>
        /// 16-bit integer/floating-point variables supported in shader inputs and outputs.
        /// </summary>
        public bool StorageInputOutput16;
    }
}
