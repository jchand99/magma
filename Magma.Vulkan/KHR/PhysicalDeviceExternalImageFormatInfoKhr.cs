using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying external image creation parameters.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceExternalImageFormatInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A bit indicating a memory handle type that will be used with the memory associated with
        /// the image.
        /// </summary>
        public ExternalMemoryHandleTypesKhr HandleType;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhysicalDeviceExternalImageFormatInfoKhr"/> structure.
        /// </summary>
        /// <param name="handleType">
        /// A bit indicating a memory handle type that will be used with the memory associated with
        /// the image.
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public PhysicalDeviceExternalImageFormatInfoKhr(ExternalMemoryHandleTypesKhr handleType,
            IntPtr next = default)
        {
            Type = StructureType.PhysicalDeviceExternalImageFormatInfo;
            Next = next;
            HandleType = handleType;
        }
    }
}
