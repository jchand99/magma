using System;

namespace Magma.Vulkan.KHR
{
    public static class CommandPoolKhr
    {
        #region Methods
        public unsafe static void TrimCommandPoolKhr(this VkCommandPool commandPool, VkDevice device, CommandPoolTrimFlags flags = CommandPoolTrimFlags.None) => vkTrimCommandPoolKHR(device, commandPool, flags);
        #endregion

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, CommandPoolTrimFlags, void> vkTrimCommandPoolKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, CommandPoolTrimFlags, void>)Vulkan.GetStaticProcPointer("vkTrimCommandPoolKHR");
        #endregion
    }

    // Structs

    // Is reserved for future use.
    [Flags]
    public enum CommandPoolTrimFlags
    {
        None = 0
    }
}
