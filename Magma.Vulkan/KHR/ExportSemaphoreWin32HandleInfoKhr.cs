using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying additional attributes of Windows handles exported from a semaphore.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExportSemaphoreWin32HandleInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A pointer to a Windows <c>SECURITY_ATTRIBUTES</c> structure specifying security
        /// attributes of the handle.
        /// </summary>
        public IntPtr Attributes;
        /// <summary>
        /// A <c>DWORD</c> specifying access rights of the handle.
        /// </summary>
        public int Access;
        /// <summary>
        /// A NULL-terminated UTF-16 string to associate with the underlying synchronization
        /// primitive referenced by NT handles exported from the created semaphore.
        /// </summary>
        public IntPtr Name;
    }
}
