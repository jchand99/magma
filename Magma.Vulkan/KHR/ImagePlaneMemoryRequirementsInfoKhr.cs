using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying image plane for memory requirements.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImagePlaneMemoryRequirementsInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The aspect corresponding to the image plane to query.
        /// </summary>
        public ImageAspects PlaneAspect;
    }
}
