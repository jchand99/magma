namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Enum specifying the point clipping behaviour.
    /// </summary>
    public enum PointClippingBehaviorKhr
    {
        AllClipPlanes = 0,
        UserClipPlanesOnly = 1
    }
}
