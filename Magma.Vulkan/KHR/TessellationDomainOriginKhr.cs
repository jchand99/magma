namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Enum describing tessellation domain origin.
    /// </summary>
    public enum TessellationDomainOriginKhr
    {
        /// <summary>
        /// Indicates that the origin of the domain space is in the upper left corner.
        /// </summary>
        UpperLeft = 0,
        /// <summary>
        /// Indicates that the origin of the domain space is in the lower left corner.
        /// </summary>
        LowerLeft = 1
    }
}
