using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    public class DisplayModeKhr
    {
        public VkPhysicalDevice PhysicalDevice { get; internal set; }
        internal IntPtr _Handle;
        private AllocationCallbacks? _AllocationCallbacks;

        public static implicit operator IntPtr(DisplayModeKhr displayMode) => displayMode == null ? IntPtr.Zero : displayMode._Handle;

        public DisplayModeKhr(VkPhysicalDevice parent, DisplayKhr display, DisplayModeCreateInfoKhr displayModeCreateInfoKhr, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            PhysicalDevice = PhysicalDevice;

            VkResult result = _CreateDisplayModeKhr(display, displayModeCreateInfoKhr, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Could not create DisplayModeKhr: ", result);
        }

        #region Methods
        private unsafe VkResult _CreateDisplayModeKhr(DisplayKhr display, DisplayModeCreateInfoKhr createInfo, AllocationCallbacks? allocationCallbacks, out IntPtr displayModeKhr)
        {
            fixed (IntPtr* ptr = &displayModeKhr)
            {
                createInfo.Prepare();
                if (allocationCallbacks.HasValue) return vkCreateDisplayModeKHR(PhysicalDevice, display, &createInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateDisplayModeKHR(PhysicalDevice, display, &createInfo, null, ptr);
            }
        }
        public unsafe VkResult GetDisplayPlaneCapabilitiesKhr(uint planeIndex, out DisplayPlaneCapabilitiesKhr capabilities)
        {
            var pointer = Interop.Alloc<DisplayPlaneCapabilitiesKhr>();
            VkResult result = vkGetDisplayPlaneCapabilitiesKHR(PhysicalDevice, _Handle, planeIndex, (DisplayPlaneCapabilitiesKhr*)pointer);
            capabilities = Interop.PtrToStructure<DisplayPlaneCapabilitiesKhr>(pointer);
            Interop.Free(pointer);
            return result;
        }
        #endregion

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DisplayModeCreateInfoKhr*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateDisplayModeKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, DisplayModeCreateInfoKhr*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateDisplayModeKHR");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, DisplayPlaneCapabilitiesKhr*, VkResult> vkGetDisplayPlaneCapabilitiesKHR = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, uint, DisplayPlaneCapabilitiesKhr*, VkResult>)Vulkan.GetStaticProcPointer("vkGetDisplayPlaneCapabilitiesKHR");
        #endregion
    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created display mode object.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DisplayModeCreateInfoKhr
    {
        internal StructureType Type;
        internal IntPtr Next;
        internal DisplayModeCreateFlagsKhr Flags;

        /// <summary>
        /// A structure describing the display parameters to use in creating the new mode. If the
        /// parameters are not compatible with the specified display, the implementation must throw
        /// with <see cref="VkResult.ErrorInitializationFailed"/>.
        /// </summary>
        public DisplayModeParametersKhr Parameters;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayModeParametersKhr"/> structure.
        /// </summary>
        /// <param name="parameters">
        /// A structure describing the display parameters to use in creating the new mode. If the
        /// parameters are not compatible with the specified display, the implementation must throw
        /// with <see cref="VkResult.ErrorInitializationFailed"/>.
        /// </param>
        public DisplayModeCreateInfoKhr(DisplayModeParametersKhr parameters)
        {
            Type = StructureType.DisplayModeCreateInfoKhr;
            Next = IntPtr.Zero;
            Flags = 0;
            Parameters = parameters;
        }

        internal void Prepare()
        {
            Type = StructureType.DisplayModeCreateInfoKhr;
        }
    }

    [Flags]
    internal enum DisplayModeCreateFlagsKhr
    {
        None = 0
    }
}
