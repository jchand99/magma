using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying a subpass/input attachment pair and an aspect mask that can be read.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct InputAttachmentAspectReferenceKhr
    {
        /// <summary>
        /// An index into the parent <see cref="RenderPassCreateInfo.Subpasses"/>.
        /// </summary>
        public int Subpass;
        public int InputAttachmentIndex;
        /// <summary>
        /// A mask of which aspect(s) can be accessed within the specified subpass.
        /// </summary>
        public int AspectMask;
    }
}
