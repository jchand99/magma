using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing Y'CbCr conversion features that can be supported by an implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDeviceSamplerYcbcrConversionFeaturesKhr
    {
        public StructureType Type;
        public IntPtr Next;
        public bool SamplerYcbcrConversion;
    }
}
