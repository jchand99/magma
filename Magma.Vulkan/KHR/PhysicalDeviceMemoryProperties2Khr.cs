using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying physical device memory properties.
    /// </summary>
    public struct PhysicalDeviceMemoryProperties2Khr
    {
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// A structure which is populated with the same values as in <see cref="VkPhysicalDevice.GetMemoryProperties"/>.
        /// </summary>
        public PhysicalDeviceMemoryProperties MemoryProperties;

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public StructureType Type;
            public IntPtr Next;
            public PhysicalDeviceMemoryProperties.Native MemoryProperties;
        }

        internal static void FromNative(ref Native native, out PhysicalDeviceMemoryProperties2Khr managed)
        {
            managed.Next = native.Next;
            PhysicalDeviceMemoryProperties.FromNative(ref native.MemoryProperties, out managed.MemoryProperties);
        }
    }
}
