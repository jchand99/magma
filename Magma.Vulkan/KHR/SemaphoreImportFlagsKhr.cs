using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Bitmask specifying additional parameters of semaphore payload import.
    /// </summary>
    [Flags]
    public enum SemaphoreImportFlagsKhr
    {
        /// <summary>
        /// Indicates that the semaphore payload will be imported only temporarily, regardless of the
        /// permanence of handleType.
        /// </summary>
        Temporary = 1 << 0
    }
}
