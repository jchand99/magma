
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing capabilities of a mode and plane combination.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct DisplayPlaneCapabilitiesKhr
    {
        /// <summary>
        /// A bitmask of <see cref="DisplayPlaneAlphasKhr"/> describing the supported alpha blending modes.
        /// </summary>
        public DisplayPlaneAlphasKhr SupportedAlpha;
        /// <summary>
        /// Is the minimum source rectangle offset supported by this plane using the specified mode.
        /// </summary>
        public Offset2D MinSrcPosition;
        /// <summary>
        /// The maximum source rectangle offset supported by this plane using the specified mode. The
        /// x and y components of maxSrcPosition must each be greater than or equal to the x and y
        /// components of <see cref="MinSrcPosition"/>, respectively.
        /// </summary>
        public Offset2D MaxSrcPosition;
        /// <summary>
        /// The minimum source rectangle size supported by this plane using the specified mode.
        /// </summary>
        public Extent2D MinSrcExtent;
        /// <summary>
        /// The maximum source rectangle size supported by this plane using the specified mode.
        /// </summary>
        public Extent2D MaxSrcExtent;
        /// <summary>
        /// Has similar semantics to <see cref="MinSrcPosition"/>, but apply to the output region
        /// within the mode rather than the input region within the source image.
        /// <para>Unlike <see cref="MinSrcPosition"/>, may contain negative values.</para>
        /// </summary>
        public Offset2D MinDstPosition;
        /// <summary>
        /// Has similar semantics to <see cref="MaxSrcPosition"/>, but apply to the output region
        /// within the mode rather than the input region within the source image.
        /// <para>Unlike <see cref="MaxSrcPosition"/>, may contain negative values.</para>
        /// </summary>
        public Offset2D MaxDstPosition;
        /// <summary>
        /// Has similar semantics to <see cref="MinSrcExtent"/>, but apply to the output region
        /// within the mode rather than the input region within the source image.
        /// </summary>
        public Extent2D MinDstExtent;
        /// <summary>
        /// Has similar semantics to <see cref="MaxSrcExtent"/>, but apply to the output region
        /// within the mode rather than the input region within the source image.
        /// </summary>
        public Extent2D MaxDstExtent;
    }
}
