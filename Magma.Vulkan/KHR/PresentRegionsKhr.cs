using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure hint of rectangular regions changed by <see cref="QueueExtensions.PresentKhr(Queue, PresentInfoKhr)"/>.
    /// <para>
    /// When the "VK_KHR_incremental_present" extension is enabled, additional fields can be
    /// specified that allow an application to specify that only certain rectangular regions of the
    /// presentable images of a swapchain are changed.
    /// </para>
    /// <para>
    /// This is an optimization hint that a presentation engine may use to only update the region of
    /// a surface that is actually changing. The application still must ensure that all pixels of a
    /// presented image contain the desired values, in case the presentation engine ignores this hint.
    /// </para>
    /// <para>
    /// An application can provide this hint by including the <see cref="PresentRegionsKhr"/>
    /// structure in the <see cref="PresentInfoKhr.Next"/> chain.
    /// </para>
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PresentRegionsKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The number of swapchains being presented to by this command.
        /// </summary>
        public int SwapchainCount;
        /// <summary>
        /// Is <c>null</c> or a pointer to an array of <see cref="PresentRegionKhr"/> elements with
        /// <see cref="SwapchainCount"/> entries. If not <c>null</c>, each element of <see
        /// cref="Regions"/> contains the region that has changed since the last present to the
        /// swapchain in the corresponding entry in the <see cref="PresentInfoKhr.Swapchains"/> array.
        /// </summary>
        public PresentRegionKhr* Regions;

        /// <summary>
        /// Initializes a new instance of the <see cref="PresentRegionsKhr"/> structure.
        /// </summary>
        /// <param name="swapchainCount">The number of swapchains being presented to by this command.</param>
        /// <param name="regions">
        /// Is <c>null</c> or a pointer to an array of <see cref="PresentRegionKhr"/> elements with
        /// <see cref="SwapchainCount"/> entries. If not <c>null</c>, each element of <see
        /// cref="Regions"/> contains the region that has changed since the last present to the
        /// swapchain in the corresponding entry in the <see cref="PresentInfoKhr.Swapchains"/> array.
        /// </param>
        /// <param name="next">
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </param>
        public PresentRegionsKhr(int swapchainCount, PresentRegionKhr* regions, IntPtr next = default)
        {
            Type = StructureType.PresentRegionsKhr;
            Next = next;
            SwapchainCount = swapchainCount;
            Regions = regions;
        }
    }
}
