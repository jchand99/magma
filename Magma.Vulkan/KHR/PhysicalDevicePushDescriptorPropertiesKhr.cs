using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure describing push descriptor limits that can be supported by an implementation.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PhysicalDevicePushDescriptorPropertiesKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Pointer to next structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The maximum number of descriptors that can be used in a descriptor set created with <see
        /// cref="DescriptorSetLayoutCreateFlags.PushDescriptorKhr"/> set.
        /// </summary>
        public int MaxPushDescriptors;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhysicalDevicePushDescriptorPropertiesKhr"/> structure.
        /// </summary>
        /// <param name="maxPushDescriptors">
        /// The maximum number of descriptors that can be used in a descriptor set created with <see
        /// cref="DescriptorSetLayoutCreateFlags.PushDescriptorKhr"/> set.
        /// </param>
        /// <param name="next">Pointer to next structure.</param>
        public PhysicalDevicePushDescriptorPropertiesKhr(int maxPushDescriptors, IntPtr next = default(IntPtr))
        {
            Type = StructureType.PhysicalDevicePushDescriptorPropertiesKhr;
            Next = next;
            MaxPushDescriptors = maxPushDescriptors;
        }
    }
}
