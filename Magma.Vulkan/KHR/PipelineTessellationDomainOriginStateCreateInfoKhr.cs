using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying the orientation of the tessellation domain.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct PipelineTessellationDomainOriginStateCreateInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Controls the origin of the tessellation domain space.
        /// </summary>
        public TessellationDomainOriginKhr DomainOrigin;
    }
}
