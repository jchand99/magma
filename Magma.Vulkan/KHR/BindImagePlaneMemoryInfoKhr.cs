using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying how to bind an image plane to memory.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BindImagePlaneMemoryInfoKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// The aspect of the disjoint image plane to bind.
        /// </summary>
        public ImageAspects PlaneAspect;
    }
}
