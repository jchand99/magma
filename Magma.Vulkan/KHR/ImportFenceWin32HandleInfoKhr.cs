using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// (None).
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ImportFenceWin32HandleInfoKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is the fence into which the state will be imported.
        /// </summary>
        public long Fence;
        /// <summary>
        /// Is a bitmask of <see cref="FenceImportFlagsKhr"/> specifying additional parameters for
        /// the fence payload import operation.
        /// </summary>
        public FenceImportFlagsKhr Flags;
        /// <summary>
        /// Specifies the type of handle.
        /// </summary>
        public ExternalFenceHandleTypeFlagsKhr HandleType;
        /// <summary>
        /// Is the external handle to import, or <c>null</c>.
        /// </summary>
        public IntPtr Handle;
        /// <summary>
        /// Is the NULL-terminated UTF-16 string naming the underlying synchronization primitive to
        /// import, or <c>null</c>.
        /// </summary>
        public IntPtr Name;
    }
}
