using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying additional attributes of Windows handles exported from a fence.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct ExportFenceWin32HandleInfoKhr
    {
        /// <summary>
        /// Is the type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Is a pointer to a Windows <c>SECURITY_ATTRIBUTES</c> structure specifying security
        /// attributes of the handle.
        /// </summary>
        public IntPtr Attributes;
        /// <summary>
        /// Is a <c>xDWORD</c> specifying access rights of the handle.
        /// </summary>
        public int Access;
        /// <summary>
        /// Is a NULL-terminated UTF-16 string to associate with the underlying synchronization
        /// primitive referenced by NT handles exported from the created fence.
        /// </summary>
        public IntPtr Name;
    }
}
