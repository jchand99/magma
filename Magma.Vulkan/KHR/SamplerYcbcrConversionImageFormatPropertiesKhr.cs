using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Structure specifying combined image sampler descriptor count for multi-planar images.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SamplerYcbcrConversionImageFormatPropertiesKhr
    {
        /// <summary>
        /// The type of this structure.
        /// </summary>
        public StructureType Type;
        /// <summary>
        /// Is <see cref="IntPtr.Zero"/> or a pointer to an extension-specific structure.
        /// </summary>
        public IntPtr Next;
        /// <summary>
        /// Tthe number of combined image sampler descriptors that the implementation uses to access
        /// the format.
        /// </summary>
        public int CombinedImageSamplerDescriptorCount;
    }
}
