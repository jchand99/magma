namespace Magma.Vulkan.KHR
{
    /// <summary>
    /// Indicates the valid usage of the descriptor update template.
    /// </summary>
    public enum DescriptorUpdateTemplateTypeKhr
    {
        /// <summary>
        /// Specifies that the descriptor update template will be used for descriptor set updates only.
        /// </summary>
        DescriptorSet = 0,
        /// <summary>
        /// Specifies that the descriptor update template will be used for push descriptor updates only.
        /// </summary>
        PushDescriptors = 1
    }
}
