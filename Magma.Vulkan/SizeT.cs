using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
  [StructLayout(LayoutKind.Sequential)]
  public struct SizeT : IComparable<SizeT>, IEquatable<SizeT>
  {
    private IntPtr _Value;

    public SizeT(int value)
    {
      _Value = new IntPtr(value);
    }

    public SizeT(long value)
    {
      _Value = new IntPtr(value);
    }

    public IntPtr ToPointer()
    {
      return _Value;
    }

    public int CompareTo(SizeT other)
    {
      return other._Value.ToInt64().CompareTo(_Value.ToInt64());
    }

    public override bool Equals(object obj)
    {
      return obj is SizeT t && Equals(t);
    }

    public bool Equals(SizeT other)
    {
      return _Value.Equals(other._Value);
    }

    public override int GetHashCode()
    {
      return HashCode.Combine(_Value);
    }

    public override string ToString()
    {
      return _Value.ToString();
    }

    public static bool operator ==(SizeT left, SizeT right)
    {
      return left.Equals(right);
    }

    public static bool operator !=(SizeT left, SizeT right)
    {
      return !left.Equals(right);
    }

    public static implicit operator SizeT(int value)
    {
      return new SizeT(value);
    }

    public static implicit operator SizeT(long value)
    {
      return new SizeT(value);
    }

    public static implicit operator SizeT(IntPtr value)
    {
      return new SizeT { _Value = value };
    }

    public static implicit operator int(SizeT size)
    {
      return size._Value.ToInt32();
    }

    public static implicit operator long(SizeT size)
    {
      return size._Value.ToInt64();
    }

    public static implicit operator IntPtr(SizeT size)
    {
      return size._Value;
    }
  }
}