using System;
using System.Runtime.InteropServices;

namespace Magma.Vulkan
{
    public class VkFence : DeviceBoundObject
    {
        public VkFence(VkDevice parent, FenceCreateInfo fenceCreateInfo, AllocationCallbacks? allocationCallbacks = null)
        {
            _AllocationCallbacks = allocationCallbacks;
            LogicalDevice = parent;

            VkResult result = _CreateFence(fenceCreateInfo, allocationCallbacks, out _Handle);
            if (result != VkResult.Success)
                throw new VulkanException("Failed to create Fence: ", result);
        }

        #region Core Methods
        private unsafe VkResult _CreateFence(FenceCreateInfo fenceCreateInfo, AllocationCallbacks? allocationCallbacks, out IntPtr fence)
        {
            fixed (IntPtr* ptr = &fence)
            {
                if (_AllocationCallbacks.HasValue) return vkCreateFence(LogicalDevice._Handle, &fenceCreateInfo, (AllocationCallbacks*)&allocationCallbacks, ptr);
                else return vkCreateFence(LogicalDevice._Handle, &fenceCreateInfo, null, ptr);
            }
        }

        public unsafe VkResult Reset()
        {
            fixed (IntPtr* ptr = &_Handle) return vkResetFences(LogicalDevice._Handle, 1, ptr);
        }

        public unsafe VkResult ResetFences(VkFence[] fences)
        {
            var handles = stackalloc IntPtr[fences?.Length ?? 0];
            for (int i = 0; i < fences.Length; i++)
                handles[i] = fences[i]._Handle;

            return vkResetFences(LogicalDevice._Handle, (uint)(fences?.Length ?? 0), handles);
        }

        public unsafe VkResult Wait(ulong timeout)
        {
            fixed (IntPtr* ptr = &_Handle) return vkWaitForFences(LogicalDevice._Handle, 1, ptr, false, timeout);
        }

        public static unsafe VkResult WaitForFences(VkDevice parent, VkFence[] fences, bool waitAll, ulong timeout)
        {
            IntPtr[] handles = new IntPtr[fences.Length];
            for (int i = 0; i < fences.Length; i++)
                handles[i] = fences[i]._Handle;

            fixed (IntPtr* ptr = handles) return vkWaitForFences(parent, (uint)(fences?.Length ?? 0), ptr, waitAll, timeout);
        }

        public unsafe VkResult GetStatus() => vkGetFenceStatus(LogicalDevice._Handle, _Handle);

        internal override unsafe void Destroy()
        {
            if (_AllocationCallbacks != null)
            {
                fixed (AllocationCallbacks?* ptr = &_AllocationCallbacks)
                    vkDestroyFence(LogicalDevice._Handle, _Handle, ptr);
            }
            else
            {
                vkDestroyFence(LogicalDevice._Handle, _Handle, null);
            }
        }
        #endregion

        #region Core C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, FenceCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult> vkCreateFence = (delegate* unmanaged[Cdecl]<IntPtr, FenceCreateInfo*, AllocationCallbacks*, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkCreateFence");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, VkResult> vkResetFences = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, VkResult>)Vulkan.GetStaticProcPointer("vkResetFences");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, bool, UInt64, VkResult> vkWaitForFences = (delegate* unmanaged[Cdecl]<IntPtr, uint, IntPtr*, bool, UInt64, VkResult>)Vulkan.GetStaticProcPointer("vkWaitForFences");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult> vkGetFenceStatus = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, VkResult>)Vulkan.GetStaticProcPointer("vkGetFenceStatus");
        internal unsafe static delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void> vkDestroyFence = (delegate* unmanaged[Cdecl]<IntPtr, IntPtr, AllocationCallbacks?*, void>)Vulkan.GetStaticProcPointer("vkDestroyFence");
        #endregion


    }

    // Structs

    /// <summary>
    /// Structure specifying parameters of a newly created fence.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FenceCreateInfo
    {
        internal StructureType Type;
        internal IntPtr Next;

        /// <summary>
        /// Specifies the initial state and behavior of the fence.
        /// </summary>
        public FenceCreateFlags Flags;

        /// <summary>
        /// Initializes a new instance of the <see cref="FenceCreateInfo"/> structure.
        /// </summary>
        /// <param name="flags">Specifies the initial state and behavior of the fence.</param>
        public FenceCreateInfo(FenceCreateFlags flags = 0)
        {
            Type = StructureType.FenceCreateInfo;
            Next = IntPtr.Zero;
            Flags = flags;
        }

        internal void Prepare()
        {
            Type = StructureType.FenceCreateInfo;
        }
    }

    /// <summary>
    /// Bitmask specifying initial state and behavior of a fence.
    /// </summary>
    [Flags]
    public enum FenceCreateFlags
    {
        /// <summary>
        /// Specifies that the fence object is created in the unsignaled state.
        /// </summary>
        None = 0,
        /// <summary>
        /// Specifies that the fence object is created in the signaled state. Otherwise, it is
        /// created in the unsignaled state.
        /// </summary>
        Signaled = 1 << 0
    }
}
