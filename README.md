# Magma
Magma is an all in one API wrapper that contains multiple assemblies for use with 2D/3D graphics and maths written in C#.

## WARNING
    This project is only compatible with net5.0 assemblies.
You can get the latest dotnet version [Here](https://dotnet.microsoft.com/download)


### Magma.Vulkan
---
Magma.Vulkan is a managed OOP wrapper of Khronos Group's [Vulkan](https://www.khronos.org/vulkan/) API written in C#.

Types:
- VkInstance
- VkPhysicalDevice
- VkDevice
- VkSwapchainKhr
- VkPipeline
- VkRenderPass
- VkShaderModule
- VkFramebuffer
- VkCommandBuffer
- etc.

Example Code:
```c#
InstanceCreateInfo info = new InstanceCreateInfo;
VkInstance instance = new VkInstance(info);
```

### Magma.GLFW
---
Magma.GLFW is a static functional wrapper of the open source [Glfw](https://www.glfw.org/) API Written in C#.

Static Classes:
- Glfw

Example Code:
```c#
IntPtr window = Glfw.CreateWindow(1280, 720, "Title", IntPtr.Zero, IntPtr.Zero);
Glfw.SetCursorPosCallback(window, (window, xPos, yPos) =>
{
   Console.WriteLine($"X: {xPos}, Y: {yPos}");
});
```

### Magma.OpenGL
---
Magma.OpenGL is a static functional wrapper of Khronos Group's [OpenGL](https://www.opengl.org/) API Written in C#.

Static Classes:
- Gl

Example Code:
```c#
Gl.ClearColor(1.0f, 1.0f, 1.0f, 1.0f);
string version = Gl.GetString(StringName.Version);
```

### Magma.OpenAL
---
Magma.OpenAL is a static functional wrapper of Creative Technology's [OpenAL](https://www.openal.org/) API written in C#.
Magma.OpenAL also contains a wrapper of ALC for handling the OpenAL contexts.

Static Classes:
- Al
- Alc

Example Code:
```c#
Al.DopplerFactor(1.0f);
Alc.MakeContextCurrent(context);
```

### Magma.Maths
---
Magma.Maths is a maths library for use with either Magma.OpenGL or Magma.Vulkan.

Structures:
- Matrix4f/d/i
- Matrix3f/d/i
- Matrix2f/d/i
- Vector4f/d/i
- Vector3f/d/i
- Vector2f/d/i

Numerical Extensions:
- Float and double have extension methods to convert angle to radians.

Example Code:
```c#
Matrix4f identity = Matrix4f.Identity;
Matrix4f mat = new Matrix4f(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f, 16.0f);

Matrix4f result = Matrix4f.Add(identity, mat);
```

### Magma.Stbi
---
Magma.Stbi is a wrapper around the single-header image loading library stbi.

Classes:
- StbiImage
- Stbi

Example Code:
```c#
byte[] contents = File.ReadAllBytes("YourImageFile.png");
StbiImage image = new StbiImage(contents);
```

### Magma.ObjectLoader
---
TODO: Be able to load in .obj files from blender into Obj and Mat classes for use within Vulkan/OpenGL.
