## Magma.Stbi

This wrapper reads image file information from a data stream or span of byte data by utilizing the single header image file reader, [stbi](https://github.com/nothings/stb).

### Example Code:

```c#
using System;
using System.IO;
using Magma.Stbi;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] arr = File.ReadAllBytes("YourImageFile.png");

            using StbiImage image = new StbiImage(arr);

            Console.WriteLine(image.Width);
            Console.WriteLine(image.Height);
            Console.WriteLine(image.Channels);

            ReadOnlySpan<byte> pixelData = image.Data;
        }
    }
}

```
