using System;
using System.Text;

namespace Magma.Stbi
{
    /// <summary>
    /// Static Stbi class for static functions.
    /// </summary>
    public static class Stbi
    {
        /// <summary>
        /// Fllips the image file vertically on load.
        /// </summary>
        /// <param name="shouldFlip">Whether the file data should be flipped vertially or not.</param>
        public static unsafe void SetFlipVerticallyOnLoad(bool shouldFlip) => stbiSetFlipVerticallyOnLoad(shouldFlip ? 1 : 0);

        /// <summary>
        /// Gets the failure version when stbi fails to read the data.
        /// </summary>
        /// <returns>Failure string.</returns>
        internal static unsafe string FailureReason()
        {
            byte* ptr = stbiFailureReason();
            byte* walkPtr = ptr;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(ptr, (int)(walkPtr - ptr));
        }

        /// <summary>
        /// Gets information from data without creating an <see cref="StbiImage"/>.
        /// </summary>
        /// <param name="data">Data to read from.</param>
        /// <param name="width">Returned width.</param>
        /// <param name="height">Returned height.</param>
        /// <param name="channels">Returned number of color channels.</param>
        public static unsafe void InfoFromMemory(ReadOnlySpan<byte> data, out int width, out int height, out int channels)
        {
            fixed (int* wptr = &width)
            fixed (int* hptr = &height)
            fixed (int* cptr = &channels)
            fixed (byte* ptr = data)
            {
                if (!stbiInfoFromMemory(ptr, data.Length, wptr, hptr, cptr))
                    throw new ArgumentException("Stbi failed to retrieve image meta-data from the provided data.");
            }
        }

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<int, void> stbiSetFlipVerticallyOnLoad = (delegate* unmanaged[Cdecl]<int, void>)StbiUtil.GetStaticProcPointer("stbiSetFlipVerticallyOnLoad");
        internal unsafe static delegate* unmanaged[Cdecl]<byte*> stbiFailureReason = (delegate* unmanaged[Cdecl]<byte*>)StbiUtil.GetStaticProcPointer("stbiFailureReason");
        internal unsafe static delegate* unmanaged[Cdecl]<byte*, int, int*, int*, int*, bool> stbiInfoFromMemory = (delegate* unmanaged[Cdecl]<byte*, int, int*, int*, int*, bool>)StbiUtil.GetStaticProcPointer("stbiInfoFromMemory");
        #endregion
    }
}
