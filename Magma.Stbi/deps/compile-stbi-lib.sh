#! /bin/bash

# Change directory to the location of the script.
cd "$(dirname "$(readlink -f "$0")")"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    cd libstbi/src/
    mkdir build
    cd build
    cmake ../../
    make install
elif [[ "$OSTYPE" == "darwin"* ]]; then
    cd libstbi/src/
    mkdir build
    cd build
    cmake -DCMAKE_OSX_DEPLOYMENT_TARGET=10.9 ../../
    make install
elif [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]; then
    cd libstbi/src/
    mkdir build
    cd build
    cmake  ../../
    msbuild /v:m /p:Configuration=Release INSTALL.vcxproj
else
    echo "Operating system not supported for this script!"
fi
