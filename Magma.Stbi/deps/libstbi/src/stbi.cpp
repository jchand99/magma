#include <inttypes.h>

#define STBI_ASSERT(x)
#define STBI_NO_STDIO
#define STBI_FAILURE_USERMSG
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#ifdef _WIN32
    #define DLLEXPORT __declspec(dllexport)
#else
    #define DLLEXPORT __attribute__((visibility("default")))
#endif

extern "C" {
    DLLEXPORT bool stbiLoadFromMemoryIntoBuffer(const unsigned char* data, int64_t len, int desiredChannels, unsigned char* dst) {
        int width, height, channels;
        unsigned char* mem = stbi_load_from_memory(data, (int)len, &width, &height, &channels, desiredChannels);

        if (!mem) {
            return false;
        }

        memcpy(dst, mem, ((size_t)width * height) * desiredChannels);

        stbi_image_free(mem);
        return true;
    }

    DLLEXPORT bool stbiInfoFromMemory(const unsigned char* data, int64_t len, int* w, int* h, int* channels) {
        return stbi_info_from_memory(data, (int)len, w, h, channels) == 1;
    }

    DLLEXPORT unsigned char* stbiLoadFromMemory(const unsigned char* data, int64_t len, int* w, int* h, int* channels, int desiredChannels) {
        return stbi_load_from_memory(data, (int)len, w, h, channels, desiredChannels);
    }

    DLLEXPORT void stbiSetFlipVerticallyOnLoad(int shouldFlip) {
        stbi_set_flip_vertically_on_load(shouldFlip);
    }

    DLLEXPORT void stbiFree(unsigned char* pixels) {
        stbi_image_free(pixels);
    }

    DLLEXPORT const char* stbiFailureReason() {
        return stbi_failure_reason();
    }
}
