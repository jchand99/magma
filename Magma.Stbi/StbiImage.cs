using System;
using System.IO;

namespace Magma.Stbi
{
    /// <summary>
    /// C# representation of Stbi-read image file.
    /// </summary>
    public class StbiImage : IDisposable
    {
        /// <summary>
        /// The width of the image in pixels.
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// The height of the image in pixels.
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// The number of color channels in the image.
        /// </summary>
        public int Channels { get; private set; }

        private unsafe byte* _Pixels;

        /// <summary>
        /// Raw image pixels stored in row-major order, pixels by pixel. Each pixels represents <see cref="Channels"/> of bytes ordered RGBA.
        /// </summary>
        public unsafe ReadOnlySpan<byte> Pixels
        {
            get
            {
                return new ReadOnlySpan<byte>(_Pixels, Width * Height * Channels);
            }
        }

        /// <summary>
        /// Creates an image from memory.
        /// </summary>
        /// <param name="data">The data to read from.</param>
        /// <param name="desiredChannels">The desired number of channels to read from.</param>
        public unsafe StbiImage(ReadOnlySpan<byte> data, int desiredChannels = 0)
        {
            _Pixels = null;
            fixed (byte* ptr = data)
            {
                int width, height, channels;
                _Pixels = stbiLoadFromMemory(ptr, data.Length, &width, &height, &channels, desiredChannels);

                if (_Pixels == null) throw new ArgumentException($"Stbi failed to laod an image from the data provided: {Stbi.FailureReason()}");

                Width = width;
                Height = height;
                Channels = channels;
            }
        }

        /// <summary>
        /// Creates an image from a <seealso cref="MemoryStream"/>.
        /// </summary>
        /// <param name="data">Data stream to read from.</param>
        /// <param name="desiredChannels">The desired number of channels to read from.</param>
        public unsafe StbiImage(MemoryStream data, int desiredChannels = 0)
        {
            _Pixels = null;
            Span<byte> span = data.GetBuffer();
            fixed (byte* ptr = span)
            {
                if (!stbiLoadFromMemoryIntoBuffer(ptr, span.Length, desiredChannels, _Pixels))
                    throw new ArgumentException($"Stbi failed to load image from the provided memory stream: {Stbi.FailureReason()}");
            }
        }

        #region C Functions
        internal unsafe static delegate* unmanaged[Cdecl]<byte*, void> stbiFree = (delegate* unmanaged[Cdecl]<byte*, void>)StbiUtil.GetStaticProcPointer("stbiFree");
        internal unsafe static delegate* unmanaged[Cdecl]<byte*, int, int, byte*, bool> stbiLoadFromMemoryIntoBuffer = (delegate* unmanaged[Cdecl]<byte*, int, int, byte*, bool>)StbiUtil.GetStaticProcPointer("stbiLoadFromMemoryIntoBuffer");
        internal unsafe static delegate* unmanaged[Cdecl]<byte*, int, int*, int*, int*, int, byte*> stbiLoadFromMemory = (delegate* unmanaged[Cdecl]<byte*, int, int*, int*, int*, int, byte*>)StbiUtil.GetStaticProcPointer("stbiLoadFromMemory");
        #endregion

        private bool disposedValue;

        /// <summary>
        /// Cleans up the loaded image memory.
        /// </summary>
        /// <param name="disposing">Value for disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                unsafe
                {
                    if (_Pixels != null)
                    {
                        stbiFree(_Pixels);
                    }
                }
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources

        /// <summary>
        /// Desctructor for StbiImage.
        /// </summary>
        ~StbiImage()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        /// <summary>
        /// Cleans up the loaded image memory.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
