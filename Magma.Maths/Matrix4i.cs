using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 4-Dimensional Matrix of ints (Row major).
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 64)]
    public struct Matrix4i : IEquatable<Matrix4i>
    {

        /// <summary>
        /// Row one.
        /// </summary>
        public Vector4i R1;
        /// <summary>
        /// Row two.
        /// </summary>
        public Vector4i R2;
        /// <summary>
        /// Row three.
        /// </summary>
        public Vector4i R3;
        /// <summary>
        /// Row four.
        /// </summary>
        public Vector4i R4;

        /// <summary>
        /// Total count of elements.
        /// </summary>
        public const int Count = 16;

        /// <summary>
        /// Total size in bytes of matrix.
        /// </summary>
        public const int SizeInBytes = sizeof(int) * Count;

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ e11, e12, e13, e14 ]</para>
        /// <para>[ e21, e22, e23, e24 ]</para>
        /// <para>[ e31, e32, e33, e34 ]</para>
        /// <para>[ e41, e42, e43, e44 ]</para>
        /// </summary>
        /// <param name="e11">First element of row one.</param>
        /// <param name="e12">Second element of row one.</param>
        /// <param name="e13">Third element of row one.</param>
        /// <param name="e14">Fourth element of row one.</param>
        /// <param name="e21">First element of row two.</param>
        /// <param name="e22">Second element of row two.</param>
        /// <param name="e23">Third element of row two.</param>
        /// <param name="e24">Fourth element of row two.</param>
        /// <param name="e31">First element of row three.</param>
        /// <param name="e32">Second element of row three.</param>
        /// <param name="e33">Third element of row three.</param>
        /// <param name="e34">Fourth element of row three.</param>
        /// <param name="e41">First element of row four.</param>
        /// <param name="e42">Second element of row four.</param>
        /// <param name="e43">Third element of row four.</param>
        /// <param name="e44">Fourth element of row four.</param>
        public Matrix4i(int e11, int e12, int e13, int e14, int e21, int e22, int e23, int e24, int e31, int e32, int e33, int e34, int e41, int e42, int e43, int e44)
        {
            R1 = new Vector4i(e11, e12, e13, e14);
            R2 = new Vector4i(e21, e22, e23, e24);
            R3 = new Vector4i(e31, e32, e33, e34);
            R4 = new Vector4i(e41, e42, e43, e44);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, m.r1.z, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, m.r2.z, 0 ]</para>
        /// <para>[ m.r3.x, m.r3.y, m.r3.z, 0 ]</para>
        /// <para>[ 0,      0,      0,      1 ]</para>
        ///
        /// <para>Example Non-Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, m.r1.z, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, m.r2.z, 0 ]</para>
        /// <para>[ m.r3.x, m.r3.y, m.r3.z, 0 ]</para>
        /// <para>[ 0,      0,      0,      0 ]</para>
        /// </summary>
        /// <param name="matrix">Matrix to put in top left corner of new Matrix3d.</param>
        /// <param name="fromIdentity">If true sets e33 to 1 as part of identity matrix.</param>
        public Matrix4i(Matrix3i matrix, bool fromIdentity)
        {
            R1 = new Vector4i(matrix.R1.X, matrix.R1.Y, matrix.R1.Z, 0);
            R2 = new Vector4i(matrix.R2.X, matrix.R2.Y, matrix.R2.Z, 0);
            R3 = new Vector4i(matrix.R3.X, matrix.R3.Y, matrix.R3.Z, 0);
            if (fromIdentity)
            {
                R4 = new Vector4i(0, 0, 0, 1);
            }
            else
            {
                R4 = new Vector4i(0, 0, 0, 0);
            }
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ topleft.r1.x,    topleft.r1.y,    topright.r1.x,    topright.r1.y    ]</para>
        /// <para>[ topleft.r2.x,    topleft.r2.y,    topright.r2.x,    topright.r2.y    ]</para>
        /// <para>[ bottomleft.r1.x, bottomleft.r1.y, bottomright.r1.x, bottomright.r1.y ]</para>
        /// <para>[ bottomleft.r2.x, bottomleft.r2.y, bottomright.r2.x, bottomright.r2.y ]</para>
        /// </summary>
        /// <param name="topLeft"></param>
        /// <param name="topRight"></param>
        /// <param name="bottomLeft"></param>
        /// <param name="bottomRight"></param>
        public Matrix4i(Matrix2i topLeft, Matrix2i topRight, Matrix2i bottomLeft, Matrix2i bottomRight)
        {
            R1 = new Vector4i(topLeft.R1.X, topLeft.R1.Y, topRight.R1.X, topRight.R1.Y);
            R2 = new Vector4i(topLeft.R2.X, topLeft.R2.Y, topRight.R2.X, topRight.R2.Y);
            R3 = new Vector4i(bottomLeft.R1.X, bottomLeft.R1.Y, bottomRight.R1.X, bottomRight.R1.Y);
            R4 = new Vector4i(bottomLeft.R2.X, bottomLeft.R2.Y, bottomRight.R2.X, bottomRight.R2.Y);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ v1.x, v1.y, v1.z, v1.w ]</para>
        /// <para>[ v2.x, v2.y, v2.z, v2.w ]</para>
        /// <para>[ v3.x, v3.y, v3.z, v3.w ]</para>
        /// <para>[ v4.x, v4.y, v4.z, v4.w ]</para>
        /// </summary>
        /// <param name="v1">Row one.</param>
        /// <param name="v2">Row two.</param>
        /// <param name="v3">Row three.</param>
        /// <param name="v4">Row four.</param>
        public Matrix4i(Vector4i v1, Vector4i v2, Vector4i v3, Vector4i v4)
        {
            R1 = v1;
            R2 = v2;
            R3 = v3;
            R4 = v4;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ diagonal, 0, 0, 0 ]</para>
        /// <para>[ 0, diagonal, 0, 0 ]</para>
        /// <para>[ 0, 0, diagonal, 0 ]</para>
        /// <para>[ 0, 0, 0, diagonal ]</para>
        /// </summary>
        /// <param name="diagonal">Value that will be the diagonal</param>
        public Matrix4i(int diagonal)
        {
            R1 = new Vector4i(diagonal, 0, 0, 0);
            R2 = new Vector4i(0, diagonal, 0, 0);
            R3 = new Vector4i(0, 0, diagonal, 0);
            R4 = new Vector4i(0, 0, 0, diagonal);
        }

        /// <summary>
        /// Identity matrix.
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 0, 0, 0 ]</para>
        /// <para>[ 0, 1, 0, 0 ]</para>
        /// <para>[ 0, 0, 1, 0 ]</para>
        /// <para>[ 0, 0, 0, 1 ]</para>
        ///
        /// </summary>
        /// <returns>Identity matrix.</returns>
        public static Matrix4i Identity => new Matrix4i(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

        /// <summary>
        /// Adds two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A + A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix4i Add(Matrix4i m1, Matrix4i m2)
        {
            return new Matrix4i(m1.R1.X + m2.R1.X,
                                m1.R1.Y + m2.R1.Y,
                                m1.R1.Z + m2.R1.Z,
                                m1.R1.W + m2.R1.W,
                                m1.R2.X + m2.R2.X,
                                m1.R2.Y + m2.R2.Y,
                                m1.R2.Z + m2.R2.Z,
                                m1.R2.W + m2.R2.W,
                                m1.R3.X + m2.R3.X,
                                m1.R3.Y + m2.R3.Y,
                                m1.R3.Z + m2.R3.Z,
                                m1.R3.W + m2.R3.W,
                                m1.R4.X + m2.R4.X,
                                m1.R4.Y + m2.R4.Y,
                                m1.R4.Z + m2.R4.Z,
                                m1.R4.W + m2.R4.W);
        }

        /// <summary>
        /// Subtracts the two matrices.
        ///
        /// <para>Example:</para>
        /// <para>A - A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix4i Subtract(Matrix4i m1, Matrix4i m2)
        {
            return new Matrix4i(m1.R1.X - m2.R1.X,
                                m1.R1.Y - m2.R1.Y,
                                m1.R1.Z - m2.R1.Z,
                                m1.R1.W - m2.R1.W,
                                m1.R2.X - m2.R2.X,
                                m1.R2.Y - m2.R2.Y,
                                m1.R2.Z - m2.R2.Z,
                                m1.R2.W - m2.R2.W,
                                m1.R3.X - m2.R3.X,
                                m1.R3.Y - m2.R3.Y,
                                m1.R3.Z - m2.R3.Z,
                                m1.R3.W - m2.R3.W,
                                m1.R4.X - m2.R4.X,
                                m1.R4.Y - m2.R4.Y,
                                m1.R4.Z - m2.R4.Z,
                                m1.R4.W - m2.R4.W);
        }

        /// <summary>
        /// Multiplies the matrix by a scalar value.
        ///
        /// <para>Example:</para>
        /// <para>A * s</para>
        /// </summary>
        /// <param name="m1">Matrix to be multiplied.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix4i Multiply(Matrix4i m1, int scalar)
        {
            return new Matrix4i(m1.R1.X * scalar,
                                m1.R1.Y * scalar,
                                m1.R1.Z * scalar,
                                m1.R1.W * scalar,
                                m1.R2.X * scalar,
                                m1.R2.Y * scalar,
                                m1.R2.Z * scalar,
                                m1.R2.W * scalar,
                                m1.R3.X * scalar,
                                m1.R3.Y * scalar,
                                m1.R3.Z * scalar,
                                m1.R3.W * scalar,
                                m1.R4.X * scalar,
                                m1.R4.Y * scalar,
                                m1.R4.Z * scalar,
                                m1.R4.W * scalar);
        }

        /// <summary>
        /// Multiplies the two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A * A</para>
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix4i Multiply(Matrix4i m1, Matrix4i m2)
        {
            return new Matrix4i(m2.R1.X * m1.R1.X + m2.R1.Y * m1.R2.X + m2.R1.Z * m1.R3.X + m2.R1.W * m1.R4.X,
                                m2.R1.X * m1.R1.Y + m2.R1.Y * m1.R2.Y + m2.R1.Z * m1.R3.Y + m2.R1.W * m1.R4.Y,
                                m2.R1.X * m1.R1.Z + m2.R1.Y * m1.R2.Z + m2.R1.Z * m1.R3.Z + m2.R1.W * m1.R4.Z,
                                m2.R1.X * m1.R1.W + m2.R1.Y * m1.R2.W + m2.R1.Z * m1.R3.W + m2.R1.W * m1.R4.W,
                                m2.R2.X * m1.R1.X + m2.R2.Y * m1.R2.X + m2.R2.Z * m1.R3.X + m2.R2.W * m1.R4.X,
                                m2.R2.X * m1.R1.Y + m2.R2.Y * m1.R2.Y + m2.R2.Z * m1.R3.Y + m2.R2.W * m1.R4.Y,
                                m2.R2.X * m1.R1.Z + m2.R2.Y * m1.R2.Z + m2.R2.Z * m1.R3.Z + m2.R2.W * m1.R4.Z,
                                m2.R2.X * m1.R1.W + m2.R2.Y * m1.R2.W + m2.R2.Z * m1.R3.W + m2.R2.W * m1.R4.W,
                                m2.R3.X * m1.R1.X + m2.R3.Y * m1.R2.X + m2.R3.Z * m1.R3.X + m2.R3.W * m1.R4.X,
                                m2.R3.X * m1.R1.Y + m2.R3.Y * m1.R2.Y + m2.R3.Z * m1.R3.Y + m2.R3.W * m1.R4.Y,
                                m2.R3.X * m1.R1.Z + m2.R3.Y * m1.R2.Z + m2.R3.Z * m1.R3.Z + m2.R3.W * m1.R4.Z,
                                m2.R3.X * m1.R1.W + m2.R3.Y * m1.R2.W + m2.R3.Z * m1.R3.W + m2.R3.W * m1.R4.W,
                                m2.R4.X * m1.R1.X + m2.R4.Y * m1.R2.X + m2.R4.Z * m1.R3.X + m2.R4.W * m1.R4.X,
                                m2.R4.X * m1.R1.Y + m2.R4.Y * m1.R2.Y + m2.R4.Z * m1.R3.Y + m2.R4.W * m1.R4.Y,
                                m2.R4.X * m1.R1.Z + m2.R4.Y * m1.R2.Z + m2.R4.Z * m1.R3.Z + m2.R4.W * m1.R4.Z,
                                m2.R4.X * m1.R1.W + m2.R4.Y * m1.R2.W + m2.R4.Z * m1.R3.W + m2.R4.W * m1.R4.W);
        }

        /// <summary>
        /// Multiplies matrix and (column) vector.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and vector as column vector.</returns>
        public static Vector4i Multiply(Matrix4i m, Vector4i v)
        {
            return new Vector4i(
                m.R1.X * v.X + m.R1.Y * v.Y + m.R1.Z * v.Z + m.R1.W * v.W,
                m.R2.X * v.X + m.R2.Y * v.Y + m.R2.Z * v.Z + m.R2.W * v.W,
                m.R3.X * v.X + m.R3.Y * v.Y + m.R3.Z * v.Z + m.R3.W * v.W,
                m.R4.X * v.X + m.R4.Y * v.Y + m.R4.Z * v.Z + m.R4.W * v.W
            );
        }

        /// <summary>
        /// Multiplies matrix and (row) vector.
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as row vector.</returns>
        public static Vector4i Multiply(Vector4i v, Matrix4i m)
        {
            return new Vector4i(
                v.X * m.R1.X + v.Y * m.R2.X + v.Z * m.R3.X + v.W * m.R4.X,
                v.X * m.R1.Y + v.Y * m.R2.Y + v.Z * m.R3.Y + v.W * m.R4.Y,
                v.X * m.R1.Z + v.Y * m.R2.Z + v.Z * m.R3.Z + v.W * m.R4.Z,
                v.X * m.R1.W + v.Y * m.R2.W + v.Z * m.R3.W + v.W * m.R4.W
            );
        }

        /// <summary>
        /// Creates translation matrix.
        /// </summary>
        /// <param name="vector">Translation vector.</param>
        /// <returns>Translation matrix.</returns>
        public static Matrix4i Translation(Vector3i vector)
        {
            Matrix4i result = Identity;
            result.R4.X = vector.X;
            result.R4.Y = vector.Y;
            result.R4.Z = vector.Z;
            return result;
        }

        /// <summary>
        /// Creates translation matrix.
        /// </summary>
        /// <param name="vector">Translation vector.</param>
        /// <returns>Translation matrix.</returns>
        public Matrix4i Translate(Vector3i vector) => this = Translate(this, vector);

        /// <summary>
        /// Creates translation matrix.
        /// </summary>
        /// <param name="matrix">Matrix to translate.</param>
        /// <param name="vector">Translation vector.</param>
        /// <returns>Translation matrix.</returns>
        public static Matrix4i Translate(Matrix4i matrix, Vector3i vector)
        {
            return matrix * Translation(vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4i Scaled(Vector3i vector)
        {
            Matrix4i result = Identity;
            result.R1.X = vector.X;
            result.R2.Y = vector.Y;
            result.R3.Z = vector.Z;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix4i Scale(Vector3i vector) => this = Scale(this, vector);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4i Scale(Matrix4i matrix, Vector3i vector)
        {
            return matrix * Scaled(vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4i Scaled(int scalar)
        {
            Matrix4i result = Identity;
            result.R1.X = scalar;
            result.R2.Y = scalar;
            result.R3.Z = scalar;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix4i Scale(int scalar) => this = Scale(this, scalar);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4i Scale(Matrix4i matrix, int scalar)
        {
            return matrix * Scaled(scalar);
        }

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <returns>Transposed matrix.</returns>
        public Matrix4i Transpose() => this = Transpose(this);

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to be transposed.</param>
        /// <returns>Transposed matrix.</returns>
        public static Matrix4i Transpose(Matrix4i matrix)
        {
            return new Matrix4i(matrix.R1.X, matrix.R2.X, matrix.R3.X, matrix.R4.X,
                                matrix.R1.Y, matrix.R2.Y, matrix.R3.Y, matrix.R4.Y,
                                matrix.R1.Z, matrix.R2.Z, matrix.R3.Z, matrix.R4.Z,
                                matrix.R1.W, matrix.R2.W, matrix.R4.Z, matrix.R4.W);
        }

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <returns>Determinant of matrix.</returns>
        public float Determinant() => Determinant(this);

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Determinant of matrix.</returns>
        public static float Determinant(Matrix4i matrix)
        {
            return ((matrix.R1.X * (matrix.R2.Y * (matrix.R3.Z * matrix.R4.W - matrix.R4.Z * matrix.R3.W) + matrix.R3.Y * (matrix.R4.Z * matrix.R2.W - matrix.R2.Z * matrix.R4.W) + matrix.R4.Y * (matrix.R2.Z * matrix.R3.W - matrix.R3.Z * matrix.R2.W)))
                 - (matrix.R1.Y * (matrix.R2.X * (matrix.R3.Z * matrix.R4.W - matrix.R4.Z * matrix.R3.W) + matrix.R3.X * (matrix.R4.Z * matrix.R2.W - matrix.R2.Z * matrix.R4.W) + matrix.R4.X * (matrix.R2.Z * matrix.R3.W - matrix.R3.Z * matrix.R2.W)))
                 + (matrix.R1.Z * (matrix.R2.X * (matrix.R3.Y * matrix.R4.W - matrix.R4.Y * matrix.R3.W) + matrix.R3.X * (matrix.R4.Y * matrix.R2.W - matrix.R2.Y * matrix.R4.W) + matrix.R4.X * (matrix.R2.Y * matrix.R3.W - matrix.R3.Y * matrix.R2.W)))
                 - (matrix.R1.W * (matrix.R2.X * (matrix.R3.Y * matrix.R4.Z - matrix.R4.Y * matrix.R3.Z) + matrix.R3.X * (matrix.R4.Y * matrix.R2.Z - matrix.R2.Y * matrix.R4.Z) + matrix.R4.X * (matrix.R2.Y * matrix.R3.Z - matrix.R3.Y * matrix.R2.Z))));
        }

        /// <summary>
        /// Converts matrix to array by appending rows together in order.
        /// </summary>
        /// <returns>Array of ints.</returns>
        public int[] ToArray()
        {
            return new int[] { R1.X, R1.Y, R1.Z, R1.W, R2.X, R2.Y, R2.Z, R2.W, R3.X, R3.Y, R3.Z, R3.W, R4.X, R4.Y, R4.Z, R4.W };
        }

        /// <summary>
        /// Compares two matrices.
        /// </summary>
        /// <param name="other">Matrix to compare to.</param>
        /// <returns>True if all values in the two matrices are the same.</returns>
        public bool Equals(Matrix4i other)
        {
            return R1.X == other.R1.X &&
                    R1.Y == other.R1.Y &&
                    R1.Z == other.R1.Z &&
                    R1.W == other.R1.W &&
                    R2.X == other.R2.X &&
                    R2.Y == other.R2.Y &&
                    R2.Z == other.R2.Z &&
                    R2.W == other.R2.W &&
                    R3.X == other.R3.X &&
                    R3.Y == other.R3.Y &&
                    R3.Z == other.R3.Z &&
                    R3.W == other.R3.W &&
                    R4.X == other.R4.X &&
                    R4.Y == other.R4.Y &&
                    R4.Z == other.R4.Z &&
                    R4.W == other.R4.W;
        }

        /// <summary>
        /// Compares matrix to object.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if object is of type Matrix4i, is not null, and all values in matrices are the same.</returns>
        public override bool Equals(object obj)
        {
            Matrix4i? matrix = (Matrix4i)obj;

            if (matrix == null) return false;

            return Equals(matrix);
        }

        /// <summary>
        /// Calculates hash code of matrix.
        /// </summary>
        /// <returns>Hashcode of matrix.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(
                HashCode.Combine(R1.X, R1.Y, R1.Z, R1.W, R2.X, R2.Y, R2.Z, R2.W),
                HashCode.Combine(R3.X, R3.Y, R3.Z, R3.W, R4.X, R4.Y, R4.Z, R4.W)
            );
        }

        /// <summary>
        /// Converts matrix to string.
        /// </summary>
        /// <returns>Matrix in form of a string.</returns>
        public override string ToString()
        {
            return $"[{R1.X:0.000}, {R1.Y:0.000}, {R1.Z:0.000}, {R1.W:0.000}]\n[{R2.X:0.000}, {R2.Y:0.000}, {R2.Z:0.000}, {R2.W:0.000}]\n[{R3.X:0.000}, {R3.Y:0.000}, {R3.Z:0.000}, {R3.W:0.000}]\n[{R4.X:0.000}, {R4.Y:0.000}, {R4.Z:0.000}, {R4.W:0.000}]\n";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for matrix addition.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix4i operator +(Matrix4i m1, Matrix4i m2) => Add(m1, m2);

        /// <summary>
        /// Operator overload for matrix subtraction.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix4i operator -(Matrix4i m1, Matrix4i m2) => Subtract(m1, m2);

        /// <summary>
        /// Operator overload for matrix multiplication.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix4i operator *(Matrix4i m1, Matrix4i m2) => Multiply(m1, m2);

        /// <summary>
        /// Operator overload for matrix-scalar multiplication
        /// </summary>
        /// <param name="m1">Matrix to be multiplied by.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix4i operator *(Matrix4i m1, int scalar) => Multiply(m1, scalar);

        /// <summary>
        /// Operator overload for matrix and column vector multiplication.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and column vector as column vector.</returns>
        public static Vector4i operator *(Matrix4i m, Vector4i v) => Multiply(m, v);

        /// <summary>
        /// Operator overload for row vector and matrix multiplication
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as a row vector.</returns>
        public static Vector4i operator *(Vector4i v, Matrix4i m) => Multiply(v, m);

        /// <summary>
        /// Operator overload for matrix equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are equal.</returns>
        public static bool operator ==(Matrix4i m1, Matrix4i m2)
        {
            return m1.Equals(m2);
        }

        /// <summary>
        /// Operator overload for matrix not equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are not equal.</returns>
        public static bool operator !=(Matrix4i m1, Matrix4i m2)
        {
            return !m1.Equals(m2);
        }

        #endregion
    }
}
