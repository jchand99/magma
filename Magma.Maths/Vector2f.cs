using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 2-Dimentional vector of floats.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 8)]
    public struct Vector2f : IEquatable<Vector2f>
    {

        /// <summary>
        /// X component of vector.
        /// </summary>

        public float X;
        /// <summary>
        /// Y component of vector.
        /// </summary>

        public float Y;
        /// <summary>
        /// Total count of components in vector.
        /// </summary>

        public const int Count = 2;
        /// <summary>
        /// Total size in bytes of vector.
        /// </summary>

        public const int SizeInBytes = sizeof(float) * Count;

        /// <summary>
        /// Zero vector
        ///
        /// <para>Example:</para>
        /// <para>[ 0, 0 ]</para>
        /// </summary>
        /// <returns>Zero vector.</returns>
        public static readonly Vector2f Zero = new Vector2f(0.0f);

        /// <summary>
        /// One vector
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 1 ]</para>
        /// </summary>
        /// <returns>One vector.</returns>
        public static readonly Vector2f One = new Vector2f(1.0f);

        /// <summary>
        /// Positive infinity vector.
        ///
        /// <para>Example:</para>
        /// <para>[ inf, inf ]</para>
        /// </summary>
        /// <returns>Positive infinity vector.</returns>
        public static readonly Vector2f PositiveInfinity = new Vector2f(float.PositiveInfinity);

        /// <summary>
        /// Negative infinity vector.
        /// <para>Example:</para>
        /// <para>[ -inf, -inf ]</para>
        /// </summary>
        /// <returns>Negative infinity vector.</returns>
        public static readonly Vector2f NegativeInfinity = new Vector2f(float.NegativeInfinity);

        /// <summary>
        /// Up vector.
        /// <para>Example:</para>
        /// <para>[ 0, 1 ]</para>
        /// </summary>
        /// <returns>Up vector.</returns>
        public static readonly Vector2f Up = new Vector2f(0.0f, 1.0f);

        /// <summary>
        /// Down vector.
        /// <para>Example:</para>
        /// <para>[ 0, -1 ]</para>
        /// </summary>
        /// <returns>Down vector.</returns>
        public static readonly Vector2f Down = new Vector2f(0.0f, -1.0f);

        /// <summary>
        /// Left vector.
        /// <para>Example:</para>
        /// <para>[ -1, 0 ]</para>
        /// </summary>
        /// <returns>Left vector.</returns>
        public static readonly Vector2f Left = new Vector2f(-1.0f, 0.0f);

        /// <summary>
        /// Right vector.
        /// <para>Example:</para>
        /// <para>[ 1, 0 ]</para>
        /// </summary>
        /// <returns>Right vector.</returns>
        public static readonly Vector2f Right = new Vector2f(1.0f, 0.0f);


        /// <summary>
        /// <para>Example:</para>
        /// <para>[ x, y ]</para>
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        public Vector2f(float x, float y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ scalar, scalar ]</para>
        /// </summary>
        /// <param name="scalar">Scalar for all components of vector.</param>
        public Vector2f(float scalar) : this(scalar, scalar) { }

        /// <summary>
        /// Grabs the nth component of the vector.
        /// </summary>
        /// <value>Nth component of the vector.</value>
        public float this[int index]
        {
            get
            {
                if (index == 0) return X;
                else if (index == 1) return Y;
                else throw new IndexOutOfRangeException($"Index {index} out of range.");
            }
            set
            {
                if (index == 0) X = value;
                else if (index == 1) Y = value;
                else throw new IndexOutOfRangeException($"Index {index} out of range.");
            }
        }

        /// <summary>
        /// Calculates magnitude of vector.
        /// </summary>
        /// <returns>Magnitude of vector.</returns>
        public float Magnitude() => Magnitude(this);

        /// <summary>
        /// Calculates magnitude of vector.
        /// </summary>
        /// <param name="v">Vector to calculate.</param>
        /// <returns>Magnitude of vector.</returns>
        public static float Magnitude(Vector2f v)
        {
            return MathF.Sqrt((v.X * v.X) + (v.Y * v.Y));
        }

        /// <summary>
        /// Normalizes vector.
        /// </summary>
        /// <returns>Normalized vector.</returns>
        public Vector2f Normalize() => this = Normalize(this);

        /// <summary>
        /// Normalizes vector.
        /// </summary>
        /// <param name="v">Vector to normalize.</param>
        /// <returns>Normalized vector.</returns>
        public static Vector2f Normalize(Vector2f v)
        {
            return v / Magnitude(v);
        }

        /// <summary>
        /// Calculates dot product of vectors.
        /// </summary>
        /// <param name="vector">Vector to dot product with this.</param>
        /// <returns>Dot product of this and vector.</returns>
        public float Dot(Vector2f vector) => Dot(this, vector);

        /// <summary>
        /// Calculates dot product of vectors.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Dot product of two vectors.</returns>
        public static float Dot(Vector2f v1, Vector2f v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        /// <summary>
        /// Calculates cross product of two vectors.
        /// </summary>
        /// <param name="vector">Vector to cross with this.</param>
        /// <returns>Cross product of this and vector.</returns>
        public Vector2f Cross(Vector2f vector) => this = Cross(this, vector);

        /// <summary>
        /// Calculates cross product of two vectors.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Cross product of two vectors.</returns>
        public static Vector2f Cross(Vector2f v1, Vector2f v2)
        {
            return new Vector2f(v1.X * v2.Y - v1.Y * v2.X);
        }

        /// <summary>
        /// Projects one vector onto another.
        /// </summary>
        /// <param name="vector">Vector to project onto.</param>
        /// <returns>Projected vector.</returns>
        public Vector2f Project(Vector2f vector) => this = Project(this, vector);

        /// <summary>
        /// Projects one vector onto another.
        /// </summary>
        /// <param name="v1">Vector to project.</param>
        /// <param name="v2">Vector to project onto.</param>
        /// <returns>Projection vector.</returns>
        public static Vector2f Project(Vector2f v1, Vector2f v2)
        {
            return v2 * (Dot(v1, v2) / Dot(v2, v2));
        }

        /// <summary>
        /// Rejects one vector from another.
        /// </summary>
        /// <param name="vector">Vector to reject from.</param>
        /// <returns>Rejected vector.</returns>
        public Vector2f Reject(Vector2f vector) => this = Reject(this, vector);

        /// <summary>
        /// Rejects one vector from another.
        /// </summary>
        /// <param name="v1">Vector to reject.</param>
        /// <param name="v2">Vector to reject from.</param>
        /// <returns>Rejection vector.</returns>
        public static Vector2f Reject(Vector2f v1, Vector2f v2)
        {
            return v1 - v2 * (Dot(v1, v2) / Dot(v2, v2));
        }

        /// <summary>
        /// Compares two vectors together.
        /// </summary>
        /// <param name="other">Other vector to compare to.</param>
        /// <returns>True if all components of vector are the same.</returns>
        public bool Equals(Vector2f other)
        {
            return X == other.X && Y == other.Y;
        }

        /// <summary>
        /// Compares this vector with object together.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if the object is of type Vector2d, is not null, and all components are the same.</returns>
        public override bool Equals(object obj)
        {
            Vector2f? vector = (Vector2f)obj;

            if (vector == null) return false;

            return Equals(vector);
        }

        /// <summary>
        /// Calculates hash code of vector.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        /// <summary>
        /// Converts vector to string.
        /// </summary>
        /// <returns>Vector as string.</returns>
        public override string ToString()
        {
            return $"[{X:0.000}, {Y:0.000}]";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for vector-scalar addition.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="scalar">Scalar to add to vector.</param>
        /// <returns>Sum of vector and scalar.</returns>
        public static Vector2f operator +(Vector2f v, float scalar)
        {
            return new Vector2f(v.X + scalar, v.Y + scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar addition.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="scalar">Scalar to add to vector.</param>
        /// <returns>Sum of vector and scalar.</returns>
        public static Vector2f operator +(float scalar, Vector2f v)
        {
            return new Vector2f(v.X + scalar, v.Y + scalar);
        }

        /// <summary>
        /// Operator overload for vector-vector addition.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Sum of two vectors.</returns>
        public static Vector2f operator +(Vector2f v1, Vector2f v2)
        {
            return new Vector2f(v1.X + v2.X, v1.Y + v2.Y);
        }

        /// <summary>
        /// Operator overload for vector-scalar subtraction.
        /// </summary>
        /// <param name="v">Vector to subtract from.</param>
        /// <param name="scalar">Scalar to subtract from vector.</param>
        /// <returns>Difference between vector and scalar.</returns>
        public static Vector2f operator -(Vector2f v, float scalar)
        {
            return new Vector2f(v.X - scalar, v.Y - scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar subtraction.
        /// </summary>
        /// <param name="v">Vector to subtract from.</param>
        /// <param name="scalar">Scalar to subtract from vector.</param>
        /// <returns>Difference between vector and scalar.</returns>
        public static Vector2f operator -(float scalar, Vector2f v)
        {
            return new Vector2f(v.X - scalar, v.Y - scalar);
        }

        /// <summary>
        /// OPerator overload for vector-vector subtraction.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Difference of two vectors.</returns>
        public static Vector2f operator -(Vector2f v1, Vector2f v2)
        {
            return new Vector2f(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Operator overload for vector-scalar multiplication.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <param name="scalar">Scalar to multiply vectory by.</param>
        /// <returns>Product of vector and scalar.</returns>
        public static Vector2f operator *(Vector2f v, float scalar)
        {
            return new Vector2f(v.X * scalar, v.Y * scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar multiplication.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <param name="scalar">Scalar to multiply vectory by.</param>
        /// <returns>Product of vector and scalar.</returns>
        public static Vector2f operator *(float scalar, Vector2f v)
        {
            return new Vector2f(v.X * scalar, v.Y * scalar);
        }

        /// <summary>
        /// Operator overload for vector-vector multiplication.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Product of two vectors.</returns>
        public static Vector2f operator *(Vector2f v1, Vector2f v2)
        {
            return new Vector2f(v1.X * v2.X, v1.Y * v2.Y);
        }

        /// <summary>
        /// Operator overload for vector-scalar division.
        /// </summary>
        /// <param name="v">Vector to divide by.</param>
        /// <param name="scalar">Scalar to divide vector by.</param>
        /// <returns>Quotient of vector and scalar.</returns>
        public static Vector2f operator /(Vector2f v, float scalar)
        {
            float s = 1.0f / scalar;

            return new Vector2f(v.X * s, v.Y * s);
        }

        /// <summary>
        /// Operator overload for vector-scalar division.
        /// </summary>
        /// <param name="v">Vector to divide by.</param>
        /// <param name="scalar">Scalar to divide vector by.</param>
        /// <returns>Quotient of vector and scalar.</returns>
        public static Vector2f operator /(float scalar, Vector2f v)
        {
            float s = 1.0f / scalar;

            return new Vector2f(v.X * s, v.Y * s);
        }

        /// <summary>
        /// Operator overload for vector-vector division.
        /// </summary>
        /// <param name="v1">Numerator vector.</param>
        /// <param name="v2">Denominator vector.</param>
        /// <returns>Quotient of two vectors.</returns>
        public static Vector2f operator /(Vector2f v1, Vector2f v2)
        {
            float x = 1.0f / v2.X;
            float y = 1.0f / v2.Y;

            return new Vector2f(v1.X * x, v1.Y * y);
        }

        /// <summary>
        /// Operator overload for vector increment.
        /// </summary>
        /// <param name="v">Vector to increment.</param>
        /// <returns>Vector with all components incremented by 1.</returns>
        public static Vector2f operator ++(Vector2f v) => new Vector2f(v.X + 1.0f, v.Y + 1.0f);

        /// <summary>
        /// Operator overload for vector decrement.
        /// </summary>
        /// <param name="v">Vector to decrement.</param>
        /// <returns>Vector with all components decremented by 1.</returns>
        public static Vector2f operator --(Vector2f v) => new Vector2f(v.X - 1.0f, v.Y - 1.0f);

        /// <summary>
        /// Implicit operator overload to convert Vector3f to vector2f.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector2f(Vector3f v) => new Vector2f(v.X, v.Y);

        /// <summary>
        /// Implicit operator overload to convert Vector4f to vector2f.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector2f(Vector4f v) => new Vector2f(v.X, v.Y);

        /// <summary>
        /// Implicit operator overload to convert Vector2i to vector2f.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector2f(Vector2i v) => new Vector2f(v.X, v.Y);

        /// <summary>
        /// Implicit operator overload to convert Vector2d to vector2f.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static explicit operator Vector2f(Vector2d v) => new Vector2f((float)v.X, (float)v.Y);

        /// <summary>
        /// Operator overload for vector equality.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>True if all components of vectors are the same.</returns>
        public static bool operator ==(Vector2f v1, Vector2f v2)
        {
            return v1.Equals(v2);
        }

        /// <summary>
        /// Operator overload for vector inequality.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>True if any components of the two vectors are not the same.</returns>
        public static bool operator !=(Vector2f v1, Vector2f v2)
        {
            return !v1.Equals(v2);
        }

        #endregion
    }
}
