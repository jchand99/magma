using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 3-Dimensional Matrix of ints (Row major).
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 36)]
    public struct Matrix3i : IEquatable<Matrix3i>
    {
        /// <summary>
        /// Row one.
        /// </summary>
        public Vector3i R1;

        /// <summary>
        /// Row two.
        /// </summary>
        public Vector3i R2;

        /// <summary>
        /// Row three.
        /// </summary>
        public Vector3i R3;

        /// <summary>
        /// Total count of elements.
        /// </summary>
        public const int Count = 9;

        /// <summary>
        /// Total size in bytes of matrix.
        /// </summary>
        public const int SizeInBytes = sizeof(int) * Count;

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ e11, e12, e13 ]</para>
        /// <para>[ e21, e22, e23 ]</para>
        /// <para>[ e31, e32, e33 ]</para>
        /// </summary>
        /// <param name="e11">First element of row one.</param>
        /// <param name="e12">Second element of row one.</param>
        /// <param name="e13">Third element of row one.</param>
        /// <param name="e21">First element of row two.</param>
        /// <param name="e22">Second element of row two.</param>
        /// <param name="e23">Third element of row two.</param>
        /// <param name="e31">First element of row three.</param>
        /// <param name="e32">Second element of row three.</param>
        /// <param name="e33">Third element of row three.</param>
        public Matrix3i(int e11, int e12, int e13, int e21, int e22, int e23, int e31, int e32, int e33)
        {
            R1 = new Vector3i(e11, e12, e13);
            R2 = new Vector3i(e21, e22, e23);
            R3 = new Vector3i(e31, e32, e33);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, 0 ]</para>
        /// <para>[ 0,      0,      1 ]</para>
        ///
        /// <para>Example Non-Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, 0 ]</para>
        /// <para>[ 0,      0,      0 ]</para>
        /// </summary>
        /// <param name="matrix">Matrix to put in top left corner of new Matrix3d.</param>
        /// <param name="fromIdentity">If true sets e33 to 1 as part of identity matrix.</param>
        public Matrix3i(Matrix2i matrix, bool fromIdentity)
        {
            R1 = new Vector3i(matrix.R1.X, matrix.R1.Y, 0);
            R2 = new Vector3i(matrix.R2.X, matrix.R2.Y, 0);
            if (fromIdentity)
                R3 = new Vector3i(0, 0, 1);
            else
                R3 = new Vector3i(0, 0, 0);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ v1.x, v1.y, v1.z ]</para>
        /// <para>[ v2.x, v2.y, v2.z ]</para>
        /// <para>[ v3.x, v3.y, v3.z ]</para>
        /// </summary>
        /// <param name="v1">Row one.</param>
        /// <param name="v2">Row two.</param>
        /// <param name="v3">Row three.</param>
        public Matrix3i(Vector3i v1, Vector3i v2, Vector3i v3)
        {
            R1 = v1;
            R2 = v2;
            R3 = v3;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ diagonal, 0, 0 ]</para>
        /// <para>[ 0, diagonal, 0 ]</para>
        /// <para>[ 0, 0, diagonal ]</para>
        /// </summary>
        /// <param name="diagonal">Value that will be the diagonal</param>
        public Matrix3i(int diagonal)
        {
            R1 = new Vector3i(diagonal, 0, 0);
            R2 = new Vector3i(0, diagonal, 0);
            R3 = new Vector3i(0, 0, diagonal);
        }

        /// <summary>
        /// Identity matrix.
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 0, 0 ]</para>
        /// <para>[ 0, 1, 0 ]</para>
        /// <para>[ 0, 0, 1 ]</para>
        ///
        /// </summary>
        /// <returns>Identity matrix.</returns>
        public static Matrix3i Identity => new Matrix3i(1, 0, 0, 0, 1, 0, 0, 0, 1);

        /// <summary>
        /// Adds two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A + A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix3i Add(Matrix3i m1, Matrix3i m2)
        {
            return new Matrix3i(m1.R1.X + m2.R1.X,
                                m1.R1.Y + m2.R1.Y,
                                m1.R1.Z + m2.R1.Z,
                                m1.R2.X + m2.R2.X,
                                m1.R2.Y + m2.R2.Y,
                                m1.R2.Z + m2.R2.Z,
                                m1.R3.X + m2.R3.X,
                                m1.R3.Y + m2.R3.Y,
                                m1.R3.Z + m2.R3.Z);
        }

        /// <summary>
        /// Subtracts the two matrices.
        ///
        /// <para>Example:</para>
        /// <para>A - A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix3i Subtract(Matrix3i m1, Matrix3i m2)
        {
            return new Matrix3i(m1.R1.X - m2.R1.X,
                                m1.R1.Y - m2.R1.Y,
                                m1.R1.Z - m2.R1.Z,
                                m1.R2.X - m2.R2.X,
                                m1.R2.Y - m2.R2.Y,
                                m1.R2.Z - m2.R2.Z,
                                m1.R3.X - m2.R3.X,
                                m1.R3.Y - m2.R3.Y,
                                m1.R3.Z - m2.R3.Z);
        }

        /// <summary>
        /// Multiplies the matrix by a scalar value.
        ///
        /// <para>Example:</para>
        /// <para>A * s</para>
        /// </summary>
        /// <param name="m1">Matrix to be multiplied.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix3i Multiply(Matrix3i m1, int scalar)
        {
            return new Matrix3i(m1.R1.X * scalar,
                                m1.R1.Y * scalar,
                                m1.R1.Z * scalar,
                                m1.R2.X * scalar,
                                m1.R2.Y * scalar,
                                m1.R2.Z * scalar,
                                m1.R3.X * scalar,
                                m1.R3.Y * scalar,
                                m1.R3.Z * scalar);
        }

        /// <summary>
        /// Multiplies the two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A * A</para>
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix3i Multiply(Matrix3i m1, Matrix3i m2)
        {
            return new Matrix3i(m2.R1.X * m1.R1.X + m2.R1.Y * m1.R2.X + m2.R1.Z * m1.R3.X,
                                m2.R1.X * m1.R1.Y + m2.R1.Y * m1.R2.Y + m2.R1.Z * m1.R3.Y,
                                m2.R1.X * m1.R1.Z + m2.R1.Y * m1.R2.Z + m2.R1.Z * m1.R3.Z,
                                m2.R2.X * m1.R1.X + m2.R2.Y * m1.R2.X + m2.R2.Z * m1.R3.X,
                                m2.R2.X * m1.R1.Y + m2.R2.Y * m1.R2.Y + m2.R2.Z * m1.R3.Y,
                                m2.R2.X * m1.R1.Z + m2.R2.Y * m1.R2.Z + m2.R2.Z * m1.R3.Z,
                                m2.R3.X * m1.R1.X + m2.R3.Y * m1.R2.X + m2.R3.Z * m1.R3.X,
                                m2.R3.X * m1.R1.Y + m2.R3.Y * m1.R2.Y + m2.R3.Z * m1.R3.Y,
                                m2.R3.X * m1.R1.Z + m2.R3.Y * m1.R2.Z + m2.R3.Z * m1.R3.Z);
        }

        /// <summary>
        /// Multiplies matrix and (column) vector.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and vector as column vector.</returns>
        public static Vector3i Multiply(Matrix3i m, Vector3i v)
        {
            return new Vector3i(
                m.R1.X * v.X + m.R1.Y * v.Y + m.R1.Z * v.Z,
                m.R2.X * v.X + m.R2.Y * v.Y + m.R2.Z * v.Z,
                m.R3.X * v.X + m.R3.Y * v.Y + m.R3.Z * v.Z
            );
        }

        /// <summary>
        /// Multiplies matrix and (row) vector.
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as row vector.</returns>
        public static Vector3i Multiply(Vector3i v, Matrix3i m)
        {
            return new Vector3i(
                v.X * m.R1.X + v.Y * m.R2.X + v.Z * m.R3.X,
                v.X * m.R1.Y + v.Y * m.R2.Y + v.Z * m.R3.Y,
                v.X * m.R1.Z + v.Y * m.R2.Z + v.Z * m.R3.Z
            );
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3i Scaled(Vector3i vector)
        {
            Matrix3i result = Identity;
            result.R1.X = vector.X;
            result.R2.Y = vector.Y;
            result.R3.Z = vector.Z;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix3i Scale(Vector3i vector) => this = Scale(this, vector);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3i Scale(Matrix3i matrix, Vector3i vector)
        {
            return matrix * Scaled(vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3i Scaled(int scalar)
        {
            Matrix3i result = Identity;
            result.R1.X = scalar;
            result.R2.Y = scalar;
            result.R3.Z = scalar;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix3i Scale(int scalar) => this = Scale(this, scalar);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3i Scale(Matrix3i matrix, int scalar)
        {
            return matrix * Scaled(scalar);
        }

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <returns>Transposed matrix.</returns>
        public Matrix3i Transpose() => this = Transpose(this);

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to be transposed.</param>
        /// <returns>Transposed matrix.</returns>
        public static Matrix3i Transpose(Matrix3i matrix)
        {
            return new Matrix3i(matrix.R1.X, matrix.R2.X, matrix.R3.X,
                                matrix.R1.Y, matrix.R2.Y, matrix.R3.Y,
                                matrix.R1.Z, matrix.R2.Z, matrix.R3.Z);
        }

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <returns>Determinant of matrix.</returns>
        public float Determinant() => Determinant(this);

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Determinant of matrix.</returns>
        public static float Determinant(Matrix3i matrix)
        {
            return (matrix.R1.X * (matrix.R2.Y * matrix.R3.Z - matrix.R2.Z * matrix.R3.Y) + matrix.R1.Y * (matrix.R2.Z * matrix.R3.X - matrix.R2.X * matrix.R3.Z) + matrix.R1.Z * (matrix.R2.X * matrix.R3.Y - matrix.R2.Y * matrix.R3.X));
        }

        /// <summary>
        /// Converts matrix to array by appending rows together in order.
        /// </summary>
        /// <returns>Array of ints.</returns>
        public int[] ToArray()
        {
            return new int[] { R1.X, R1.Y, R1.Z, R2.X, R2.Y, R2.Z, R3.X, R3.Y, R3.Z };
        }

        /// <summary>
        /// Compares two matrices.
        /// </summary>
        /// <param name="other">Matrix to compare to.</param>
        /// <returns>True if all values in the two matrices are the same.</returns>
        public bool Equals(Matrix3i other)
        {
            return R1.X == other.R1.X &&
                    R1.Y == other.R1.Y &&
                    R1.Z == other.R1.Z &&
                    R2.X == other.R2.X &&
                    R2.Y == other.R2.Y &&
                    R2.Z == other.R2.Z &&
                    R3.X == other.R3.X &&
                    R3.Y == other.R3.Y &&
                    R3.Z == other.R3.Z;
        }

        /// <summary>
        /// Compares matrix to object.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if object is of type Matrix3i, is not null, and all values in matrices are the same.</returns>
        public override bool Equals(object obj)
        {
            Matrix3i? matrix = (Matrix3i)obj;

            if (matrix == null) return false;

            return Equals(matrix);
        }

        /// <summary>
        /// Calculates hash code of matrix.
        /// </summary>
        /// <returns>Hashcode of matrix.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(
                HashCode.Combine(R1.X, R1.Y, R1.Z, R2.X, R2.Y, R2.Z, R3.X, R3.Y),
                R3.Z.GetHashCode()
            );
        }

        /// <summary>
        /// Converts matrix to string.
        /// </summary>
        /// <returns>Matrix in form of a string.</returns>
        public override string ToString()
        {
            return $"[{R1.X:0.000}, {R1.Y:0.000}, {R1.Z:0.000}]\n[{R2.X:0.000}, {R2.Y:0.000}, {R2.Z:0.000}]\n[{R3.X:0.000}, {R3.Y:0.000}, {R3.Z:0.000}]\n";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for matrix addition.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix3i operator +(Matrix3i m1, Matrix3i m2) => Add(m1, m2);

        /// <summary>
        /// Operator overload for matrix subtraction.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix3i operator -(Matrix3i m1, Matrix3i m2) => Subtract(m1, m2);

        /// <summary>
        /// Operator overload for matrix multiplication.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix3i operator *(Matrix3i m1, Matrix3i m2) => Multiply(m1, m2);

        /// <summary>
        /// Operator overload for matrix-scalar multiplication
        /// </summary>
        /// <param name="m1">Matrix to be multiplied by.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix3i operator *(Matrix3i m1, int scalar) => Multiply(m1, scalar);

        /// <summary>
        /// Operator overload for matrix and column vector multiplication.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and column vector as column vector.</returns>
        public static Vector3i operator *(Matrix3i m, Vector3i v) => Multiply(m, v);

        /// <summary>
        /// Operator overload for row vector and matrix multiplication
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as a row vector.</returns>
        public static Vector3i operator *(Vector3i v, Matrix3i m) => Multiply(v, m);

        /// <summary>
        /// Implicit operator overload converting Matrix4i to Matrix3i.
        ///
        /// <para>Takes top left corner of Matrix4i values.</para>
        /// </summary>
        /// <param name="matrix">Matrix to convert down.</param>
        public static implicit operator Matrix3i(Matrix4i matrix)
        => new Matrix3i(matrix.R1.X, matrix.R1.Y, matrix.R1.Z,
                        matrix.R2.X, matrix.R2.Y, matrix.R2.Z,
                        matrix.R3.X, matrix.R3.Y, matrix.R3.Z);

        /// <summary>
        /// Operator overload for matrix equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are equal.</returns>
        public static bool operator ==(Matrix3i m1, Matrix3i m2)
        {
            return m1.Equals(m2);
        }

        /// <summary>
        /// Operator overload for matrix not equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are not equal.</returns>
        public static bool operator !=(Matrix3i m1, Matrix3i m2)
        {
            return !m1.Equals(m2);
        }

        #endregion
    }
}
