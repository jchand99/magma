using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 2-Dimensional Matrix of doubles (Row major).
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 32)]
    public struct Matrix2d : IEquatable<Matrix2d>
    {
        /// <summary>
        /// Row one.
        /// </summary>
        public Vector2d R1;

        /// <summary>
        /// Row two.
        /// </summary>
        public Vector2d R2;

        /// <summary>
        /// Total count of elements.
        /// </summary>
        public const int Count = 4;

        /// <summary>
        /// Total size in bytes of matrix.
        /// </summary>
        public const int SizeInBytes = sizeof(double) * Count;

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ e11, e12 ]</para>
        /// <para>[ e21, e22 ]</para>
        /// </summary>
        /// <param name="e11">First element of row one.</param>
        /// <param name="e12">Second element of row one.</param>
        /// <param name="e21">First element of row two.</param>
        /// <param name="e22">Second element of row two.</param>
        public Matrix2d(double e11, double e12, double e21, double e22)
        {
            R1 = new Vector2d(e11, e12);
            R2 = new Vector2d(e21, e22);

        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ v1.x, v1.y ]</para>
        /// <para>[ v2.x, v2.y ]</para>
        /// </summary>
        /// <param name="v1">Row one.</param>
        /// <param name="v2">Row two.</param>
        public Matrix2d(Vector2d v1, Vector2d v2)
        {
            R1 = v1;
            R2 = v2;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ diagonal, 0 ]</para>
        /// <para>[ 0, diagonal ]</para>
        /// </summary>
        /// <param name="diagonal">Value that will be the diagonal.</param>
        public Matrix2d(double diagonal)
        {
            R1 = new Vector2d(diagonal, 0.0d);
            R2 = new Vector2d(0.0d, diagonal);
        }

        /// <summary>
        /// Identity matrix.
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 0 ]</para>
        /// <para>[ 0, 1 ]</para>
        ///
        /// </summary>
        /// <returns>Matrix2d identity.</returns>
        public static readonly Matrix2d Identity = new Matrix2d(1.0d, 0.0d, 0.0d, 1.0d);

        /// <summary>
        /// Adds two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A + A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix2d Add(Matrix2d m1, Matrix2d m2)
        {
            return new Matrix2d(m1.R1.X + m2.R1.X,
                                m1.R1.Y + m2.R1.Y,
                                m1.R2.X + m2.R2.X,
                                m1.R2.Y + m2.R2.Y);
        }

        /// <summary>
        /// Subtracts the two matrices.
        ///
        /// <para>Example:</para>
        /// <para>A - A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix2d Subtract(Matrix2d m1, Matrix2d m2)
        {
            return new Matrix2d(m1.R1.X - m2.R1.X,
                                m1.R1.Y - m2.R1.Y,
                                m1.R2.X - m2.R2.X,
                                m1.R2.Y - m2.R2.Y);
        }

        /// <summary>
        /// Multiplies the matrix by a scalar value.
        ///
        /// <para>Example:</para>
        /// <para>A * s</para>
        /// </summary>
        /// <param name="m1">Matrix to be multiplied.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix2d Multiply(Matrix2d m1, double scalar)
        {
            return new Matrix2d(m1.R1.X * scalar,
                                m1.R1.Y * scalar,
                                m1.R2.X * scalar,
                                m1.R2.Y * scalar);
        }

        /// <summary>
        /// Multiplies the two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A * A</para>
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix2d Multiply(Matrix2d m1, Matrix2d m2)
        {
            return new Matrix2d(m2.R1.X * m1.R1.X + m2.R1.Y * m1.R1.Y,
                                m2.R1.X * m1.R2.X + m2.R1.Y * m1.R2.Y,
                                m2.R2.X * m1.R1.X + m2.R2.Y * m1.R1.Y,
                                m2.R2.X * m1.R2.X + m2.R2.Y * m1.R2.Y);
        }

        /// <summary>
        /// Multiplies matrix and (column) vector.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and vector as column vector.</returns>
        public static Vector2d Multiply(Matrix2d m, Vector2d v)
        {
            return new Vector2d(
                m.R1.X * v.X + m.R1.Y * v.Y,
                m.R2.X * v.X + m.R2.Y * v.Y
            );
        }

        /// <summary>
        /// Multiplies matrix and (row) vector.
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as row vector.</returns>
        public static Vector2d Multiply(Vector2d v, Matrix2d m)
        {
            return new Vector2d(
                v.X * m.R1.X + v.Y * m.R2.X,
                v.X * m.R1.Y + v.Y * m.R2.Y
            );
        }

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <returns>Transposed matrix.</returns>
        public Matrix2d Transpose() => this = Transpose(this);

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to be transposed.</param>
        /// <returns>Transposed matrix.</returns>
        public static Matrix2d Transpose(Matrix2d matrix)
        {
            return new Matrix2d(matrix.R1.X, matrix.R2.X,
                                matrix.R1.Y, matrix.R2.Y);
        }

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <returns>Determinant of matrix.</returns>
        public double Determinant() => Determinant(this);

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Determinant of matrix.</returns>
        public static double Determinant(Matrix2d matrix)
        {
            return (matrix.R1.X * matrix.R2.Y) - (matrix.R2.X * matrix.R1.Y);
        }

        /// <summary>
        /// Converts matrix to array by appending rows together in order.
        /// </summary>
        /// <returns>Array of doubles.</returns>
        public double[] ToArray()
        {
            return new double[] { R1.X, R1.Y, R2.X, R2.Y };
        }

        /// <summary>
        /// Compares two matrices.
        /// </summary>
        /// <param name="other">Matrix to compare to.</param>
        /// <returns>True if all values in the two matrices are the same.</returns>
        public bool Equals(Matrix2d other)
        {
            return R1.X == other.R1.X &&
                    R1.Y == other.R1.Y &&
                    R2.X == other.R2.X &&
                    R2.Y == other.R2.Y;
        }

        /// <summary>
        /// Compares matrix to object.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if object is of type Matrix2d, is not null, and all values in matrices are the same.</returns>
        public override bool Equals(object obj)
        {
            Matrix2d? matrix = (Matrix2d)obj;

            if (matrix == null) return false;

            return Equals(matrix);
        }

        /// <summary>
        /// Calculates hash code of matrix.
        /// </summary>
        /// <returns>Hashcode of matrix.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(R1.X, R1.Y, R2.X, R2.Y);
        }

        /// <summary>
        /// Converts matrix to string.
        /// </summary>
        /// <returns>Matrix in form of a string.</returns>
        public override string ToString()
        {
            return $"[{R1.X:0.000}, {R1.Y:0.000}]\n[{R2.X:0.000}, {R2.Y:0.000}]\n";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for matrix addition.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix2d operator +(Matrix2d m1, Matrix2d m2) => Add(m1, m2);

        /// <summary>
        /// Operator overload for matrix subtraction.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix2d operator -(Matrix2d m1, Matrix2d m2) => Subtract(m1, m2);

        /// <summary>
        /// Operator overload for matrix multiplication.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix2d operator *(Matrix2d m1, Matrix2d m2) => Multiply(m1, m2);

        /// <summary>
        /// Operator overload for matrix-scalar multiplication
        /// </summary>
        /// <param name="m1">Matrix to be multiplied by.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix2d operator *(Matrix2d m1, double scalar) => Multiply(m1, scalar);

        /// <summary>
        /// Operator overload for matrix and column vector multiplication.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and column vector as column vector.</returns>
        public static Vector2d operator *(Matrix2d m, Vector2d v) => Multiply(m, v);

        /// <summary>
        /// Operator overload for row vector and matrix multiplication
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as a row vector.</returns>
        public static Vector2d operator *(Vector2d v, Matrix2d m) => Multiply(v, m);

        /// <summary>
        /// Implicit operator overload converting Matrix3d to Matrix2d.
        ///
        /// <para>Takes top left corner of Matrix3d values.</para>
        /// </summary>
        /// <param name="matrix">Matrix to convert down.</param>
        public static implicit operator Matrix2d(Matrix3d matrix)
        => new Matrix2d(matrix.R1.X, matrix.R1.Y, matrix.R2.X, matrix.R2.Y);

        /// <summary>
        /// Implicit operator overload converting Matrix4d to Matrix2d.
        ///
        /// <para>Takes top left corner of Matrix4d values.</para>
        /// </summary>
        /// <param name="matrix">Matrix to convert down.</param>
        public static implicit operator Matrix2d(Matrix4d matrix)
        => new Matrix2d(matrix.R1.X, matrix.R1.Y, matrix.R2.X, matrix.R2.Y);

        /// <summary>
        /// Operator overload for matrix equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are equal.</returns>
        public static bool operator ==(Matrix2d m1, Matrix2d m2)
        {
            return m1.Equals(m2);
        }

        /// <summary>
        /// Operator overload for matrix not equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are not equal.</returns>
        public static bool operator !=(Matrix2d m1, Matrix2d m2)
        {
            return !m1.Equals(m2);
        }

        #endregion
    }
}
