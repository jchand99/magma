## Magma.Maths

This is a maths library that is meant to be used in conjunction with the Magma.OpenGL and Magma.Vulkan assemblies.

### Example Code:

```c#
using System;
using Magma.Maths;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix4f transform = Matrix4f.Translation(new Vector3f(1.0f, 1.0f, 1.0f))
                                        .Rotate(45.0f.ToRadians(), new Vector3f(1.0f, 0.0f, 0.0f))
                                        .Scale(new Vector3f(1.0f, 1.5f, 1.0f));

            float det = transform.Determinant();

            Vector3f vec = new Vector3f(100.0f, 2.0f, -93.4f);
            vec.Normalize();

            Console.WriteLine(transform.ToString());
            Console.WriteLine(det);
            Console.WriteLine(vec.ToString());
        }
    }
}
```

### Output:
```
[1.000, 0.000, 0.000, 0.000]
[0.000, 1.061, 1.061, 0.000]
[0.000, -0.707, 0.707, 0.000]
[1.000, 1.000, 1.000, 1.000]

1.4999999
[0.731, 0.015, -0.683]
```
