using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 3-Dimensional Matrix of floats (Row major).
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 36)]
    public struct Matrix3f : IEquatable<Matrix3f>
    {

        /// <summary>
        /// Row one.
        /// </summary>
        public Vector3f R1;
        /// <summary>
        /// Row two.
        /// </summary>
        public Vector3f R2;
        /// <summary>
        /// Row three.
        /// </summary>
        public Vector3f R3;

        /// <summary>
        /// Total count of elements.
        /// </summary>
        public const int Count = 9;

        /// <summary>
        /// Total size in bytes of matrix.
        /// </summary>
        public const int SizeInBytes = sizeof(float) * Count;

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ e11, e12, e13 ]</para>
        /// <para>[ e21, e22, e23 ]</para>
        /// <para>[ e31, e32, e33 ]</para>
        /// </summary>
        /// <param name="e11">First element of row one.</param>
        /// <param name="e12">Second element of row one.</param>
        /// <param name="e13">Third element of row one.</param>
        /// <param name="e21">First element of row two.</param>
        /// <param name="e22">Second element of row two.</param>
        /// <param name="e23">Third element of row two.</param>
        /// <param name="e31">First element of row three.</param>
        /// <param name="e32">Second element of row three.</param>
        /// <param name="e33">Third element of row three.</param>
        public Matrix3f(float e11, float e12, float e13, float e21, float e22, float e23, float e31, float e32, float e33)
        {
            R1 = new Vector3f(e11, e12, e13);
            R2 = new Vector3f(e21, e22, e23);
            R3 = new Vector3f(e31, e32, e33);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, 0 ]</para>
        /// <para>[ 0,      0,      1 ]</para>
        ///
        /// <para>Example Non-Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, 0 ]</para>
        /// <para>[ 0,      0,      0 ]</para>
        /// </summary>
        /// <param name="matrix">Matrix to put in top left corner of new Matrix3d.</param>
        /// <param name="fromIdentity">If true sets e33 to 1 as part of identity matrix.</param>
        public Matrix3f(Matrix2f matrix, bool fromIdentity)
        {
            R1 = new Vector3f(matrix.R1.X, matrix.R1.Y, 0.0f);
            R2 = new Vector3f(matrix.R2.X, matrix.R2.Y, 0.0f);
            if (fromIdentity)
                R3 = new Vector3f(0.0f, 0.0f, 1.0f);
            else
                R3 = new Vector3f(0.0f, 0.0f, 0.0f);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ v1.x, v1.y, v1.z ]</para>
        /// <para>[ v2.x, v2.y, v2.z ]</para>
        /// <para>[ v3.x, v3.y, v3.z ]</para>
        /// </summary>
        /// <param name="v1">Row one.</param>
        /// <param name="v2">Row two.</param>
        /// <param name="v3">Row three.</param>
        public Matrix3f(Vector3f v1, Vector3f v2, Vector3f v3)
        {
            R1 = v1;
            R2 = v2;
            R3 = v3;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ diagonal, 0, 0 ]</para>
        /// <para>[ 0, diagonal, 0 ]</para>
        /// <para>[ 0, 0, diagonal ]</para>
        /// </summary>
        /// <param name="diagonal">Value that will be the diagonal</param>
        public Matrix3f(float diagonal)
        {
            R1 = new Vector3f(diagonal, 0.0f, 0.0f);
            R2 = new Vector3f(0.0f, diagonal, 0.0f);
            R3 = new Vector3f(0.0f, 0.0f, diagonal);
        }

        /// <summary>
        /// Identity matrix.
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 0, 0 ]</para>
        /// <para>[ 0, 1, 0 ]</para>
        /// <para>[ 0, 0, 1 ]</para>
        ///
        /// </summary>
        /// <returns>Identity matrix.</returns>
        public static Matrix3f Identity => new Matrix3f(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f);

        /// <summary>
        /// Adds two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A + A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix3f Add(Matrix3f m1, Matrix3f m2)
        {
            return new Matrix3f(m1.R1.X + m2.R1.X,
                                m1.R1.Y + m2.R1.Y,
                                m1.R1.Z + m2.R1.Z,
                                m1.R2.X + m2.R2.X,
                                m1.R2.Y + m2.R2.Y,
                                m1.R2.Z + m2.R2.Z,
                                m1.R3.X + m2.R3.X,
                                m1.R3.Y + m2.R3.Y,
                                m1.R3.Z + m2.R3.Z);
        }

        /// <summary>
        /// Subtracts the two matrices.
        ///
        /// <para>Example:</para>
        /// <para>A - A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix3f Subtract(Matrix3f m1, Matrix3f m2)
        {
            return new Matrix3f(m1.R1.X - m2.R1.X,
                                m1.R1.Y - m2.R1.Y,
                                m1.R1.Z - m2.R1.Z,
                                m1.R2.X - m2.R2.X,
                                m1.R2.Y - m2.R2.Y,
                                m1.R2.Z - m2.R2.Z,
                                m1.R3.X - m2.R3.X,
                                m1.R3.Y - m2.R3.Y,
                                m1.R3.Z - m2.R3.Z);
        }

        /// <summary>
        /// Multiplies the matrix by a scalar value.
        ///
        /// <para>Example:</para>
        /// <para>A * s</para>
        /// </summary>
        /// <param name="m1">Matrix to be multiplied.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix3f Multiply(Matrix3f m1, float scalar)
        {
            return new Matrix3f(m1.R1.X * scalar,
                                m1.R1.Y * scalar,
                                m1.R1.Z * scalar,
                                m1.R2.X * scalar,
                                m1.R2.Y * scalar,
                                m1.R2.Z * scalar,
                                m1.R3.X * scalar,
                                m1.R3.Y * scalar,
                                m1.R3.Z * scalar);
        }

        /// <summary>
        /// Multiplies the two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A * A</para>
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix3f Multiply(Matrix3f m1, Matrix3f m2)
        {
            return new Matrix3f(m2.R1.X * m1.R1.X + m2.R1.Y * m1.R2.X + m2.R1.Z * m1.R3.X,
                                m2.R1.X * m1.R1.Y + m2.R1.Y * m1.R2.Y + m2.R1.Z * m1.R3.Y,
                                m2.R1.X * m1.R1.Z + m2.R1.Y * m1.R2.Z + m2.R1.Z * m1.R3.Z,
                                m2.R2.X * m1.R1.X + m2.R2.Y * m1.R2.X + m2.R2.Z * m1.R3.X,
                                m2.R2.X * m1.R1.Y + m2.R2.Y * m1.R2.Y + m2.R2.Z * m1.R3.Y,
                                m2.R2.X * m1.R1.Z + m2.R2.Y * m1.R2.Z + m2.R2.Z * m1.R3.Z,
                                m2.R3.X * m1.R1.X + m2.R3.Y * m1.R2.X + m2.R3.Z * m1.R3.X,
                                m2.R3.X * m1.R1.Y + m2.R3.Y * m1.R2.Y + m2.R3.Z * m1.R3.Y,
                                m2.R3.X * m1.R1.Z + m2.R3.Y * m1.R2.Z + m2.R3.Z * m1.R3.Z);
        }

        /// <summary>
        /// Multiplies matrix and (column) vector.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and vector as column vector.</returns>
        public static Vector3f Multiply(Matrix3f m, Vector3f v)
        {
            return new Vector3f(
                m.R1.X * v.X + m.R1.Y * v.Y + m.R1.Z * v.Z,
                m.R2.X * v.X + m.R2.Y * v.Y + m.R2.Z * v.Z,
                m.R3.X * v.X + m.R3.Y * v.Y + m.R3.Z * v.Z
            );
        }

        /// <summary>
        /// Multiplies matrix and (row) vector.
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as row vector.</returns>
        public static Vector3f Multiply(Vector3f v, Matrix3f m)
        {
            return new Vector3f(
                v.X * m.R1.X + v.Y * m.R2.X + v.Z * m.R3.X,
                v.X * m.R1.Y + v.Y * m.R2.Y + v.Z * m.R3.Y,
                v.X * m.R1.Z + v.Y * m.R2.Z + v.Z * m.R3.Z
            );
        }

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public static Matrix3f Rotation(float angle, Vector3f vector)
        {
            float cos = MathF.Cos(angle);
            float sin = MathF.Sin(angle);

            Vector3f axis = Vector3f.Normalize(vector);
            Vector3f temp = (1.0f - cos) * axis;

            Matrix3f result = Identity;
            result.R1.X = cos + temp.X * axis.X;
            result.R1.Y = temp.X * axis.Y + sin * axis.Z;
            result.R1.Z = temp.X * axis.Z - sin * axis.Y;
            result.R2.X = temp.Y * axis.X - sin * axis.Z;
            result.R2.Y = cos + temp.Y * axis.Y;
            result.R2.Z = temp.Y * axis.Z + sin * axis.X;
            result.R3.X = temp.Z * axis.X + sin * axis.Y;
            result.R3.Y = temp.Z * axis.Y - sin * axis.X;
            result.R3.Z = cos + temp.Z * axis.Z;
            return result;
        }

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public Matrix3f Rotate(float angle, Vector3f vector) => this = Rotate(this, angle, vector);

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="matrix">Matrix to rotate.</param>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public static Matrix3f Rotate(Matrix3f matrix, float angle, Vector3f vector)
        {
            return matrix * Rotation(angle, vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3f Scaled(Vector3f vector)
        {
            Matrix3f result = Identity;
            result.R1.X = vector.X;
            result.R2.Y = vector.Y;
            result.R3.Z = vector.Z;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix3f Scale(Vector3f vector) => this = Scale(this, vector);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3f Scale(Matrix3f matrix, Vector3f vector)
        {
            return matrix * Scaled(vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3f Scaled(float scalar)
        {
            Matrix3f result = Identity;
            result.R1.X = scalar;
            result.R2.Y = scalar;
            result.R3.Z = scalar;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix3f Scale(float scalar) => this = Scale(this, scalar);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3f Scale(Matrix3f matrix, float scalar)
        {
            return matrix * Scaled(scalar);
        }

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <returns>Transposed matrix.</returns>
        public Matrix3f Transpose() => this = Transpose(this);

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to be transposed.</param>
        /// <returns>Transposed matrix.</returns>
        public static Matrix3f Transpose(Matrix3f matrix)
        {
            return new Matrix3f(matrix.R1.X, matrix.R2.X, matrix.R3.X,
                                matrix.R1.Y, matrix.R2.Y, matrix.R3.Y,
                                matrix.R1.Z, matrix.R2.Z, matrix.R3.Z);
        }

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <returns>Determinant of matrix.</returns>
        public float Determinant() => Determinant(this);

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Determinant of matrix.</returns>
        public static float Determinant(Matrix3f matrix)
        {
            return (matrix.R1.X * (matrix.R2.Y * matrix.R3.Z - matrix.R2.Z * matrix.R3.Y) + matrix.R1.Y * (matrix.R2.Z * matrix.R3.X - matrix.R2.X * matrix.R3.Z) + matrix.R1.Z * (matrix.R2.X * matrix.R3.Y - matrix.R2.Y * matrix.R3.X));
        }

        /// <summary>
        /// Inverts the matrix.
        /// </summary>
        /// <returns>Inverted matrix.</returns>
        public Matrix3f Inverse() => this = Invert(this);

        /// <summary>
        /// Inverts the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to invert.</param>
        /// <returns>Inverted matrix.</returns>
        public static Matrix3f Invert(Matrix3f matrix)
        {
            Vector3f a = new Vector3f(matrix.R1.X, matrix.R2.X, matrix.R3.X);
            Vector3f b = new Vector3f(matrix.R1.Y, matrix.R2.Y, matrix.R3.Y);
            Vector3f c = new Vector3f(matrix.R1.Z, matrix.R2.Z, matrix.R3.Z);

            Vector3f r0 = Vector3f.Cross(b, c);
            Vector3f r1 = Vector3f.Cross(c, a);
            Vector3f r2 = Vector3f.Cross(a, b);

            float inverseDeterminant = 1.0f / Vector3f.Dot(r2, c);

            return new Matrix3f(r0.X * inverseDeterminant, r0.Y * inverseDeterminant, r0.Z * inverseDeterminant,
                                r1.X * inverseDeterminant, r1.Y * inverseDeterminant, r1.Z * inverseDeterminant,
                                r2.X * inverseDeterminant, r2.Y * inverseDeterminant, r2.Z * inverseDeterminant);
        }

        /// <summary>
        /// Converts matrix to array by appending rows together in order.
        /// </summary>
        /// <returns>Array of floats.</returns>
        public float[] ToArray()
        {
            return new float[] { R1.X, R1.Y, R1.Z, R2.X, R2.Y, R2.Z, R3.X, R3.Y, R3.Z };
        }

        /// <summary>
        /// Compares two matrices.
        /// </summary>
        /// <param name="other">Matrix to compare to.</param>
        /// <returns>True if all values in the two matrices are the same.</returns>
        public bool Equals(Matrix3f other)
        {
            return R1.X == other.R1.X &&
                    R1.Y == other.R1.Y &&
                    R1.Z == other.R1.Z &&
                    R2.X == other.R2.X &&
                    R2.Y == other.R2.Y &&
                    R2.Z == other.R2.Z &&
                    R3.X == other.R3.X &&
                    R3.Y == other.R3.Y &&
                    R3.Z == other.R3.Z;
        }

        /// <summary>
        /// Compares matrix to object.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if object is of type Matrix3f, is not null, and all values in matrices are the same.</returns>
        public override bool Equals(object obj)
        {
            Matrix3f? matrix = (Matrix3f)obj;

            if (matrix == null) return false;

            return Equals(matrix);
        }

        /// <summary>
        /// Calculates hash code of matrix.
        /// </summary>
        /// <returns>Hashcode of matrix.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(
                HashCode.Combine(R1.X, R1.Y, R1.Z, R2.X, R2.Y, R2.Z, R3.X, R3.Y),
                R3.Z.GetHashCode()
            );
        }

        /// <summary>
        /// Converts matrix to string.
        /// </summary>
        /// <returns>Matrix in form of a string.</returns>
        public override string ToString()
        {
            return $"[{R1.X:0.000}, {R1.Y:0.000}, {R1.Z:0.000}]\n[{R2.X:0.000}, {R2.Y:0.000}, {R2.Z:0.000}]\n[{R3.X:0.000}, {R3.Y:0.000}, {R3.Z:0.000}]\n";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for matrix addition.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix3f operator +(Matrix3f m1, Matrix3f m2) => Add(m1, m2);

        /// <summary>
        /// Operator overload for matrix subtraction.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix3f operator -(Matrix3f m1, Matrix3f m2) => Subtract(m1, m2);

        /// <summary>
        /// Operator overload for matrix multiplication.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix3f operator *(Matrix3f m1, Matrix3f m2) => Multiply(m1, m2);

        /// <summary>
        /// Operator overload for matrix-scalar multiplication
        /// </summary>
        /// <param name="m1">Matrix to be multiplied by.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix3f operator *(Matrix3f m1, float scalar) => Multiply(m1, scalar);

        /// <summary>
        /// Operator overload for matrix and column vector multiplication.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and column vector as column vector.</returns>
        public static Vector3f operator *(Matrix3f m, Vector3f v) => Multiply(m, v);

        /// <summary>
        /// Operator overload for row vector and matrix multiplication
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as a row vector.</returns>
        public static Vector3f operator *(Vector3f v, Matrix3f m) => Multiply(v, m);

        /// <summary>
        /// Implicit operator overload converting Matrix4f to Matrix3f.
        ///
        /// <para>Takes top left corner of Matrix4f values.</para>
        /// </summary>
        /// <param name="matrix">Matrix to convert down.</param>
        public static implicit operator Matrix3f(Matrix4f matrix)
        => new Matrix3f(matrix.R1.X, matrix.R1.Y, matrix.R1.Z,
                        matrix.R2.X, matrix.R2.Y, matrix.R2.Z,
                        matrix.R3.X, matrix.R3.Y, matrix.R3.Z);

        /// <summary>
        /// Operator overload for matrix equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are equal.</returns>
        public static bool operator ==(Matrix3f m1, Matrix3f m2)
        {
            return m1.Equals(m2);
        }

        /// <summary>
        /// Operator overload for matrix not equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are not equal.</returns>
        public static bool operator !=(Matrix3f m1, Matrix3f m2)
        {
            return !m1.Equals(m2);
        }

        #endregion
    }
}
