using System;

namespace Magma.Maths
{
    /// <summary>
    /// Numerical extensions for radians.
    /// </summary>
    public static class NumericalExtensions
    {
        /// <summary>
        /// Converts angle in degrees to radians.
        /// </summary>
        /// <param name="angle">Angle in degrees.</param>
        /// <returns>Radians.</returns>
        public static float ToRadians(this float angle)
        {
            return (MathF.PI / 180) * angle;
        }

        /// <summary>
        /// Converts angle in degrees to radians.
        /// </summary>
        /// <param name="angle">Angle in degrees.</param>
        /// <returns>Radians.</returns>
        public static double ToRadians(this double angle)
        {
            return (Math.PI / 180) * angle;
        }
    }

    /// <summary>
    /// Radians class for converting to and from radians.
    /// </summary>
    public static class Radians
    {
        /// <summary>
        /// Converts degrees to radians.
        /// </summary>
        /// <param name="degrees">Degrees to convert.</param>
        /// <returns>Radians.</returns>
        public static float FromDegrees(float degrees)
        {
            return (MathF.PI / 180) * degrees;
        }

        /// <summary>
        /// Converts radians to degrees.
        /// </summary>
        /// <param name="radians">Radians to convert.</param>
        /// <returns>Degrees.</returns>
        public static float ToDegrees(float radians)
        {
            return (180 / MathF.PI) * radians;
        }

        /// <summary>
        /// Converts degrees to radians.
        /// </summary>
        /// <param name="degrees">Degrees to convert.</param>
        /// <returns>Radians.</returns>
        public static double FromDegrees(double degrees)
        {
            return (Math.PI / 180) * degrees;
        }

        /// <summary>
        /// Converts radians to degrees.
        /// </summary>
        /// <param name="radians">Radians to convert.</param>
        /// <returns>Degrees.</returns>
        public static double ToDegrees(double radians)
        {
            return (180 / Math.PI) * radians;
        }
    }
}
