using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 3-Dimentional vector of doubles.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 24)]
    public struct Vector3d : IEquatable<Vector3d>
    {
        /// <summary>
        /// X component of vector.
        /// </summary>
        public double X;

        /// <summary>
        /// Y component of vector.
        /// </summary>
        public double Y;

        /// <summary>
        /// Z component of vector.
        /// </summary>
        public double Z;

        /// <summary>
        /// Total count of components in vector.
        /// </summary>
        public const int Count = 3;

        /// <summary>
        /// Total size in bytes of vector.
        /// </summary>
        public const int SizeInBytes = sizeof(double) * Count;

        /// <summary>
        /// Zero vector
        ///
        /// <para>Example:</para>
        /// <para>[ 0, 0, 0 ]</para>
        /// </summary>
        /// <returns>Zero vector.</returns>
        public static readonly Vector3d Zero = new Vector3d(0.0d);

        /// <summary>
        /// One vector
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 1, 1 ]</para>
        /// </summary>
        /// <returns>One vector.</returns>
        public static readonly Vector3d One = new Vector3d(1.0d);

        /// <summary>
        /// Positive infinity vector.
        ///
        /// <para>Example:</para>
        /// <para>[ inf, inf, inf ]</para>
        /// </summary>
        /// <returns>Positive infinity vector.</returns>
        public static readonly Vector3d PositiveInfinity = new Vector3d(double.PositiveInfinity);

        /// <summary>
        /// Negative infinity vector.
        /// <para>Example:</para>
        /// <para>[ -inf, -inf, -inf ]</para>
        /// </summary>
        /// <returns>Negative infinity vector.</returns>
        public static readonly Vector3d NegativeInfinity = new Vector3d(double.NegativeInfinity);

        /// <summary>
        /// Up vector.
        /// <para>Example:</para>
        /// <para>[ 0, 1, 0 ]</para>
        /// </summary>
        /// <returns>Up vector.</returns>
        public static readonly Vector3d Up = new Vector3d(0.0d, 1.0d, 0.0d);

        /// <summary>
        /// Down vector.
        /// <para>Example:</para>
        /// <para>[ 0, -1, 0 ]</para>
        /// </summary>
        /// <returns>Down vector.</returns>
        public static readonly Vector3d Down = new Vector3d(0.0d, -1.0d, 0.0d);

        /// <summary>
        /// Left vector.
        /// <para>Example:</para>
        /// <para>[ -1, 0, 0 ]</para>
        /// </summary>
        /// <returns>Left vector.</returns>
        public static readonly Vector3d Left = new Vector3d(-1.0d, 0.0d, 0.0d);

        /// <summary>
        /// Right vector.
        /// <para>Example:</para>
        /// <para>[ 1, 0, 0 ]</para>
        /// </summary>
        /// <returns>Right vector.</returns>
        public static readonly Vector3d Right = new Vector3d(1.0d, 0.0d, 0.0d);

        /// <summary>
        /// Back vector.
        /// <para>Example:</para>
        /// <para>[ 0, 0, -1 ]</para>
        /// </summary>
        /// <returns>Back vector.</returns>
        public static readonly Vector3d Back = new Vector3d(0.0d, 0.0d, -1.0d);

        /// <summary>
        /// Forward vector.
        /// <para>Example:</para>
        /// <para>[ 0, 0, 1 ]</para>
        /// </summary>
        /// <returns>Forward vector.</returns>
        public static readonly Vector3d Forward = new Vector3d(0.0d, 0.0d, 1.0d);

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ x, y, z ]</para>
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        /// <param name="z">Z component.</param>
        public Vector3d(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ scalar, scalar, scalar ]</para>
        /// </summary>
        /// <param name="scalar">Scalar for all components of vector.</param>
        public Vector3d(double scalar) : this(scalar, scalar, scalar) { }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ v.X, v.Y, z ]</para>
        /// </summary>
        /// <param name="vector">Vector to be first two components.</param>
        /// <param name="z">Z component.</param>
        public Vector3d(Vector2f vector, double z) : this(vector.X, vector.Y, z) { }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ x, v.X, v.Y ]</para>
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="vector">Vector to be last two components.</param>
        public Vector3d(double x, Vector2f vector) : this(x, vector.X, vector.Y) { }

        /// <summary>
        /// Grabs the nth component of the vector.
        /// </summary>
        /// <value>Nth component of the vector.</value>
        public double this[int index]
        {
            get
            {
                if (index == 0) return X;
                else if (index == 1) return Y;
                else if (index == 2) return Z;
                else throw new IndexOutOfRangeException($"Index {index} out of range.");
            }
            set
            {
                if (index == 0) X = value;
                else if (index == 1) Y = value;
                else if (index == 2) Z = value;
                else throw new IndexOutOfRangeException($"Index {index} out of range.");
            }
        }

        /// <summary>
        /// Calculates magnitude of vector.
        /// </summary>
        /// <returns>Magnitude of vector.</returns>
        public double Magnitude() => Magnitude(this);

        /// <summary>
        /// Calculates magnitude of vector.
        /// </summary>
        /// <param name="v">Vector to calculate.</param>
        /// <returns>Magnitude of vector.</returns>
        public static double Magnitude(Vector3d v)
        {
            return Math.Sqrt((v.X * v.X) + (v.Y * v.Y) + (v.Z * v.Z));
        }

        /// <summary>
        /// Normalizes vector.
        /// </summary>
        /// <returns>Normalized vector.</returns>
        public Vector3d Normalize() => this = Normalize(this);

        /// <summary>
        /// Normalizes vector.
        /// </summary>
        /// <param name="v">Vector to normalize.</param>
        /// <returns>Normalized vector.</returns>
        public static Vector3d Normalize(Vector3d v)
        {
            return v / Magnitude(v);
        }

        /// <summary>
        /// Calculates dot product of vectors.
        /// </summary>
        /// <param name="vector">Vector to dot product with this.</param>
        /// <returns>Dot product of this and vector.</returns>
        public double Dot(Vector3d vector) => Dot(this, vector);

        /// <summary>
        /// Calculates dot product of vectors.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Dot product of two vectors.</returns>
        public static double Dot(Vector3d v1, Vector3d v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }

        /// <summary>
        /// Calculates cross product of two vectors.
        /// </summary>
        /// <param name="vector">Vector to cross with this.</param>
        /// <returns>Cross product of this and vector.</returns>
        public Vector3d Cross(Vector3d vector) => this = Cross(this, vector);

        /// <summary>
        /// Calculates cross product of two vectors.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Cross product of two vectors.</returns>
        public static Vector3d Cross(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.Y * v2.Z - v1.Z * v2.Y, v1.Z * v2.X - v1.X * v2.Z, v1.X * v2.Y - v1.Y * v2.X);
        }

        /// <summary>
        /// Projects one vector onto another.
        /// </summary>
        /// <param name="vector">Vector to project onto.</param>
        /// <returns>Projected vector.</returns>
        public Vector3d Project(Vector3d vector) => this = Project(this, vector);

        /// <summary>
        /// Projects one vector onto another.
        /// </summary>
        /// <param name="v1">Vector to project.</param>
        /// <param name="v2">Vector to project onto.</param>
        /// <returns>Projection vector.</returns>
        public static Vector3d Project(Vector3d v1, Vector3d v2)
        {
            return v2 * (Dot(v1, v2) / Dot(v2, v2));
        }

        /// <summary>
        /// Rejects one vector from another.
        /// </summary>
        /// <param name="vector">Vector to reject from.</param>
        /// <returns>Rejected vector.</returns>
        public Vector3d Reject(Vector3d vector) => this = Reject(this, vector);

        /// <summary>
        /// Rejects one vector from another.
        /// </summary>
        /// <param name="v1">Vector to reject.</param>
        /// <param name="v2">Vector to reject from.</param>
        /// <returns>Rejection vector.</returns>
        public static Vector3d Reject(Vector3d v1, Vector3d v2)
        {
            return v1 - v2 * (Dot(v1, v2) / Dot(v2, v2));
        }

        /// <summary>
        /// Compares two vectors together.
        /// </summary>
        /// <param name="other">Other vector to compare to.</param>
        /// <returns>True if all components of vector are the same.</returns>
        public bool Equals(Vector3d other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }

        /// <summary>
        /// Compares this vector with object together.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if the object is of type Vector2d, is not null, and all components are the same.</returns>
        public override bool Equals(object obj)
        {
            Vector3d? vector = (Vector3d)obj;

            if (vector == null) return false;

            return Equals(vector);
        }

        /// <summary>
        /// Calculates hash code of vector.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y, Z);
        }

        /// <summary>
        /// Converts vector to string.
        /// </summary>
        /// <returns>Vector as string.</returns>
        public override string ToString()
        {
            return $"[{X:0.000}, {Y:0.000}, {Z:0.000}]";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for vector-scalar addition.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="scalar">Scalar to add to vector.</param>
        /// <returns>Sum of vector and scalar.</returns>
        public static Vector3d operator +(Vector3d v, double scalar)
        {
            return new Vector3d(v.X + scalar, v.Y + scalar, v.Z + scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar addition.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="scalar">Scalar to add to vector.</param>
        /// <returns>Sum of vector and scalar.</returns>
        public static Vector3d operator +(double scalar, Vector3d v)
        {
            return new Vector3d(v.X + scalar, v.Y + scalar, v.Z + scalar);
        }

        /// <summary>
        /// Operator overload for vector-vector addition.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Sum of two vectors.</returns>
        public static Vector3d operator +(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        /// <summary>
        /// Operator overload for vector-scalar subtraction.
        /// </summary>
        /// <param name="v">Vector to subtract from.</param>
        /// <param name="scalar">Scalar to subtract from vector.</param>
        /// <returns>Difference between vector and scalar.</returns>
        public static Vector3d operator -(Vector3d v, double scalar)
        {
            return new Vector3d(v.X - scalar, v.Y - scalar, v.Z - scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar subtraction.
        /// </summary>
        /// <param name="v">Vector to subtract from.</param>
        /// <param name="scalar">Scalar to subtract from vector.</param>
        /// <returns>Difference between vector and scalar.</returns>
        public static Vector3d operator -(double scalar, Vector3d v)
        {
            return new Vector3d(v.X - scalar, v.Y - scalar, v.Z - scalar);
        }

        /// <summary>
        /// OPerator overload for vector-vector subtraction.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Difference of two vectors.</returns>
        public static Vector3d operator -(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        /// <summary>
        /// Operator overload for vector-scalar multiplication.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <param name="scalar">Scalar to multiply vectory by.</param>
        /// <returns>Product of vector and scalar.</returns>
        public static Vector3d operator *(Vector3d v, double scalar)
        {
            return new Vector3d(v.X * scalar, v.Y * scalar, v.Z * scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar multiplication.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <param name="scalar">Scalar to multiply vectory by.</param>
        /// <returns>Product of vector and scalar.</returns>
        public static Vector3d operator *(double scalar, Vector3d v)
        {
            return new Vector3d(v.X * scalar, v.Y * scalar, v.Z * scalar);
        }

        /// <summary>
        /// Operator overload for vector-vector multiplication.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Product of two vectors.</returns>
        public static Vector3d operator *(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
        }

        /// <summary>
        /// Operator overload for vector-scalar division.
        /// </summary>
        /// <param name="v">Vector to divide by.</param>
        /// <param name="scalar">Scalar to divide vector by.</param>
        /// <returns>Quotient of vector and scalar.</returns>
        public static Vector3d operator /(Vector3d v, double scalar)
        {
            double s = 1.0f / scalar;

            return new Vector3d(v.X * s, v.Y * s, v.Z * s);
        }

        /// <summary>
        /// Operator overload for vector-scalar division.
        /// </summary>
        /// <param name="v">Vector to divide by.</param>
        /// <param name="scalar">Scalar to divide vector by.</param>
        /// <returns>Quotient of vector and scalar.</returns>
        public static Vector3d operator /(double scalar, Vector3d v)
        {
            double s = 1.0f / scalar;

            return new Vector3d(v.X * s, v.Y * s, v.Z * s);
        }

        /// <summary>
        /// Operator overload for vector-vector division.
        /// </summary>
        /// <param name="v1">Numerator vector.</param>
        /// <param name="v2">Denominator vector.</param>
        /// <returns>Quotient of two vectors.</returns>
        public static Vector3d operator /(Vector3d v1, Vector3d v2)
        {
            double x = 1.0f / v2.X;
            double y = 1.0f / v2.Y;
            double z = 1.0f / v2.Z;

            return new Vector3d(v1.X * x, v1.Y * y, v1.Z * z);
        }

        /// <summary>
        /// Operator overload for vector increment.
        /// </summary>
        /// <param name="v">Vector to increment.</param>
        /// <returns>Vector with all components incremented by 1.</returns>
        public static Vector3d operator ++(Vector3d v) => new Vector3d(v.X + 1.0d, v.Y + 1.0d, v.Z + 1.0d);

        /// <summary>
        /// Operator overload for vector decrement.
        /// </summary>
        /// <param name="v">Vector to decrement.</param>
        /// <returns>Vector with all components decremented by 1.</returns>
        public static Vector3d operator --(Vector3d v) => new Vector3d(v.X - 1.0d, v.Y - 1.0d, v.Z - 1.0d);

        /// <summary>
        /// Implicit operator overload to convert Vector3f to vector3d.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector3d(Vector3f v) => new Vector3d(v.X, v.Y, v.Z);

        /// <summary>
        /// Implicit operator overload to convert Vector3i to vector3d.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector3d(Vector3i v) => new Vector3d(v.X, v.Y, v.Z);

        /// <summary>
        /// Implicit operator overload to convert Vector4d to vector3d.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector3d(Vector4d v) => new Vector3d(v.X, v.Y, v.Z);

        /// <summary>
        /// Operator overload for vector equality.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>True if all components of vectors are the same.</returns>
        public static bool operator ==(Vector3d v1, Vector3d v2)
        {
            return v1.Equals(v2);
        }

        /// <summary>
        /// Operator overload for vector inequality.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>True if any components of the two vectors are not the same.</returns>
        public static bool operator !=(Vector3d v1, Vector3d v2)
        {
            return !v1.Equals(v2);
        }

        #endregion
    }
}
