using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 4-Dimensional Matrix of floats (Row major).
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 64)]
    public struct Matrix4f : IEquatable<Matrix4f>
    {

        /// <summary>
        /// Row one.
        /// </summary>
        public Vector4f R1;
        /// <summary>
        /// Row two.
        /// </summary>
        public Vector4f R2;
        /// <summary>
        /// Row three.
        /// </summary>
        public Vector4f R3;
        /// <summary>
        /// Row four.
        /// </summary>
        public Vector4f R4;

        /// <summary>
        /// Total count of elements.
        /// </summary>
        public const int Count = 16;

        /// <summary>
        /// Total size in bytes of matrix.
        /// </summary>
        public const int SizeInBytes = sizeof(float) * Count;

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ e11, e12, e13, e14 ]</para>
        /// <para>[ e21, e22, e23, e24 ]</para>
        /// <para>[ e31, e32, e33, e34 ]</para>
        /// <para>[ e41, e42, e43, e44 ]</para>
        /// </summary>
        /// <param name="e11">First element of row one.</param>
        /// <param name="e12">Second element of row one.</param>
        /// <param name="e13">Third element of row one.</param>
        /// <param name="e14">Fourth element of row one.</param>
        /// <param name="e21">First element of row two.</param>
        /// <param name="e22">Second element of row two.</param>
        /// <param name="e23">Third element of row two.</param>
        /// <param name="e24">Fourth element of row two.</param>
        /// <param name="e31">First element of row three.</param>
        /// <param name="e32">Second element of row three.</param>
        /// <param name="e33">Third element of row three.</param>
        /// <param name="e34">Fourth element of row three.</param>
        /// <param name="e41">First element of row four.</param>
        /// <param name="e42">Second element of row four.</param>
        /// <param name="e43">Third element of row four.</param>
        /// <param name="e44">Fourth element of row four.</param>
        public Matrix4f(float e11, float e12, float e13, float e14, float e21, float e22, float e23, float e24, float e31, float e32, float e33, float e34, float e41, float e42, float e43, float e44)
        {
            R1 = new Vector4f(e11, e12, e13, e14);
            R2 = new Vector4f(e21, e22, e23, e24);
            R3 = new Vector4f(e31, e32, e33, e34);
            R4 = new Vector4f(e41, e42, e43, e44);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, m.r1.z, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, m.r2.z, 0 ]</para>
        /// <para>[ m.r3.x, m.r3.y, m.r3.z, 0 ]</para>
        /// <para>[ 0,      0,      0,      1 ]</para>
        ///
        /// <para>Example Non-Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, m.r1.z, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, m.r2.z, 0 ]</para>
        /// <para>[ m.r3.x, m.r3.y, m.r3.z, 0 ]</para>
        /// <para>[ 0,      0,      0,      0 ]</para>
        /// </summary>
        /// <param name="matrix">Matrix to put in top left corner of new Matrix3d.</param>
        /// <param name="fromIdentity">If true sets e33 to 1 as part of identity matrix.</param>
        public Matrix4f(Matrix3f matrix, bool fromIdentity)
        {
            R1 = new Vector4f(matrix.R1.X, matrix.R1.Y, matrix.R1.Z, 0.0f);
            R2 = new Vector4f(matrix.R2.X, matrix.R2.Y, matrix.R2.Z, 0.0f);
            R3 = new Vector4f(matrix.R3.X, matrix.R3.Y, matrix.R3.Z, 0.0f);
            if (fromIdentity)
            {
                R4 = new Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
            }
            else
            {
                R4 = new Vector4f(0.0f, 0.0f, 0.0f, 0.0f);
            }
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ topleft.r1.x,    topleft.r1.y,    topright.r1.x,    topright.r1.y    ]</para>
        /// <para>[ topleft.r2.x,    topleft.r2.y,    topright.r2.x,    topright.r2.y    ]</para>
        /// <para>[ bottomleft.r1.x, bottomleft.r1.y, bottomright.r1.x, bottomright.r1.y ]</para>
        /// <para>[ bottomleft.r2.x, bottomleft.r2.y, bottomright.r2.x, bottomright.r2.y ]</para>
        /// </summary>
        /// <param name="topLeft"></param>
        /// <param name="topRight"></param>
        /// <param name="bottomLeft"></param>
        /// <param name="bottomRight"></param>
        public Matrix4f(Matrix2f topLeft, Matrix2f topRight, Matrix2f bottomLeft, Matrix2f bottomRight)
        {
            R1 = new Vector4f(topLeft.R1.X, topLeft.R1.Y, topRight.R1.X, topRight.R1.Y);
            R2 = new Vector4f(topLeft.R2.X, topLeft.R2.Y, topRight.R2.X, topRight.R2.Y);
            R3 = new Vector4f(bottomLeft.R1.X, bottomLeft.R1.Y, bottomRight.R1.X, bottomRight.R1.Y);
            R4 = new Vector4f(bottomLeft.R2.X, bottomLeft.R2.Y, bottomRight.R2.X, bottomRight.R2.Y);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ v1.x, v1.y, v1.z, v1.w ]</para>
        /// <para>[ v2.x, v2.y, v2.z, v2.w ]</para>
        /// <para>[ v3.x, v3.y, v3.z, v3.w ]</para>
        /// <para>[ v4.x, v4.y, v4.z, v4.w ]</para>
        /// </summary>
        /// <param name="v1">Row one.</param>
        /// <param name="v2">Row two.</param>
        /// <param name="v3">Row three.</param>
        /// <param name="v4">Row four.</param>
        public Matrix4f(Vector4f v1, Vector4f v2, Vector4f v3, Vector4f v4)
        {
            R1 = v1;
            R2 = v2;
            R3 = v3;
            R4 = v4;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ diagonal, 0, 0, 0 ]</para>
        /// <para>[ 0, diagonal, 0, 0 ]</para>
        /// <para>[ 0, 0, diagonal, 0 ]</para>
        /// <para>[ 0, 0, 0, diagonal ]</para>
        /// </summary>
        /// <param name="diagonal">Value that will be the diagonal</param>
        public Matrix4f(float diagonal)
        {
            R1 = new Vector4f(diagonal, 0.0f, 0.0f, 0.0f);
            R2 = new Vector4f(0.0f, diagonal, 0.0f, 0.0f);
            R3 = new Vector4f(0.0f, 0.0f, diagonal, 0.0f);
            R4 = new Vector4f(0.0f, 0.0f, 0.0f, diagonal);
        }

        /// <summary>
        /// Identity matrix.
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 0, 0, 0 ]</para>
        /// <para>[ 0, 1, 0, 0 ]</para>
        /// <para>[ 0, 0, 1, 0 ]</para>
        /// <para>[ 0, 0, 0, 1 ]</para>
        ///
        /// </summary>
        /// <returns>Identity matrix.</returns>
        public static Matrix4f Identity => new Matrix4f(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);

        /// <summary>
        /// Adds two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A + A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix4f Add(Matrix4f m1, Matrix4f m2)
        {
            return new Matrix4f(m1.R1.X + m2.R1.X,
                                m1.R1.Y + m2.R1.Y,
                                m1.R1.Z + m2.R1.Z,
                                m1.R1.W + m2.R1.W,
                                m1.R2.X + m2.R2.X,
                                m1.R2.Y + m2.R2.Y,
                                m1.R2.Z + m2.R2.Z,
                                m1.R2.W + m2.R2.W,
                                m1.R3.X + m2.R3.X,
                                m1.R3.Y + m2.R3.Y,
                                m1.R3.Z + m2.R3.Z,
                                m1.R3.W + m2.R3.W,
                                m1.R4.X + m2.R4.X,
                                m1.R4.Y + m2.R4.Y,
                                m1.R4.Z + m2.R4.Z,
                                m1.R4.W + m2.R4.W);
        }

        /// <summary>
        /// Subtracts the two matrices.
        ///
        /// <para>Example:</para>
        /// <para>A - A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix4f Subtract(Matrix4f m1, Matrix4f m2)
        {
            return new Matrix4f(m1.R1.X - m2.R1.X,
                                m1.R1.Y - m2.R1.Y,
                                m1.R1.Z - m2.R1.Z,
                                m1.R1.W - m2.R1.W,
                                m1.R2.X - m2.R2.X,
                                m1.R2.Y - m2.R2.Y,
                                m1.R2.Z - m2.R2.Z,
                                m1.R2.W - m2.R2.W,
                                m1.R3.X - m2.R3.X,
                                m1.R3.Y - m2.R3.Y,
                                m1.R3.Z - m2.R3.Z,
                                m1.R3.W - m2.R3.W,
                                m1.R4.X - m2.R4.X,
                                m1.R4.Y - m2.R4.Y,
                                m1.R4.Z - m2.R4.Z,
                                m1.R4.W - m2.R4.W);
        }

        /// <summary>
        /// Multiplies the matrix by a scalar value.
        ///
        /// <para>Example:</para>
        /// <para>A * s</para>
        /// </summary>
        /// <param name="m1">Matrix to be multiplied.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix4f Multiply(Matrix4f m1, float scalar)
        {
            return new Matrix4f(m1.R1.X * scalar,
                                m1.R1.Y * scalar,
                                m1.R1.Z * scalar,
                                m1.R1.W * scalar,
                                m1.R2.X * scalar,
                                m1.R2.Y * scalar,
                                m1.R2.Z * scalar,
                                m1.R2.W * scalar,
                                m1.R3.X * scalar,
                                m1.R3.Y * scalar,
                                m1.R3.Z * scalar,
                                m1.R3.W * scalar,
                                m1.R4.X * scalar,
                                m1.R4.Y * scalar,
                                m1.R4.Z * scalar,
                                m1.R4.W * scalar);
        }

        /// <summary>
        /// Multiplies the two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A * A</para>
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix4f Multiply(Matrix4f m1, Matrix4f m2)
        {
            return new Matrix4f(m2.R1.X * m1.R1.X + m2.R1.Y * m1.R2.X + m2.R1.Z * m1.R3.X + m2.R1.W * m1.R4.X,
                                m2.R1.X * m1.R1.Y + m2.R1.Y * m1.R2.Y + m2.R1.Z * m1.R3.Y + m2.R1.W * m1.R4.Y,
                                m2.R1.X * m1.R1.Z + m2.R1.Y * m1.R2.Z + m2.R1.Z * m1.R3.Z + m2.R1.W * m1.R4.Z,
                                m2.R1.X * m1.R1.W + m2.R1.Y * m1.R2.W + m2.R1.Z * m1.R3.W + m2.R1.W * m1.R4.W,
                                m2.R2.X * m1.R1.X + m2.R2.Y * m1.R2.X + m2.R2.Z * m1.R3.X + m2.R2.W * m1.R4.X,
                                m2.R2.X * m1.R1.Y + m2.R2.Y * m1.R2.Y + m2.R2.Z * m1.R3.Y + m2.R2.W * m1.R4.Y,
                                m2.R2.X * m1.R1.Z + m2.R2.Y * m1.R2.Z + m2.R2.Z * m1.R3.Z + m2.R2.W * m1.R4.Z,
                                m2.R2.X * m1.R1.W + m2.R2.Y * m1.R2.W + m2.R2.Z * m1.R3.W + m2.R2.W * m1.R4.W,
                                m2.R3.X * m1.R1.X + m2.R3.Y * m1.R2.X + m2.R3.Z * m1.R3.X + m2.R3.W * m1.R4.X,
                                m2.R3.X * m1.R1.Y + m2.R3.Y * m1.R2.Y + m2.R3.Z * m1.R3.Y + m2.R3.W * m1.R4.Y,
                                m2.R3.X * m1.R1.Z + m2.R3.Y * m1.R2.Z + m2.R3.Z * m1.R3.Z + m2.R3.W * m1.R4.Z,
                                m2.R3.X * m1.R1.W + m2.R3.Y * m1.R2.W + m2.R3.Z * m1.R3.W + m2.R3.W * m1.R4.W,
                                m2.R4.X * m1.R1.X + m2.R4.Y * m1.R2.X + m2.R4.Z * m1.R3.X + m2.R4.W * m1.R4.X,
                                m2.R4.X * m1.R1.Y + m2.R4.Y * m1.R2.Y + m2.R4.Z * m1.R3.Y + m2.R4.W * m1.R4.Y,
                                m2.R4.X * m1.R1.Z + m2.R4.Y * m1.R2.Z + m2.R4.Z * m1.R3.Z + m2.R4.W * m1.R4.Z,
                                m2.R4.X * m1.R1.W + m2.R4.Y * m1.R2.W + m2.R4.Z * m1.R3.W + m2.R4.W * m1.R4.W);
        }

        /// <summary>
        /// Multiplies matrix and (column) vector.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and vector as column vector.</returns>
        public static Vector4f Multiply(Matrix4f m, Vector4f v)
        {
            return new Vector4f(
                m.R1.X * v.X + m.R1.Y * v.Y + m.R1.Z * v.Z + m.R1.W * v.W,
                m.R2.X * v.X + m.R2.Y * v.Y + m.R2.Z * v.Z + m.R2.W * v.W,
                m.R3.X * v.X + m.R3.Y * v.Y + m.R3.Z * v.Z + m.R3.W * v.W,
                m.R4.X * v.X + m.R4.Y * v.Y + m.R4.Z * v.Z + m.R4.W * v.W
            );
        }

        /// <summary>
        /// Multiplies matrix and (row) vector.
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as row vector.</returns>
        public static Vector4f Multiply(Vector4f v, Matrix4f m)
        {
            return new Vector4f(
                v.X * m.R1.X + v.Y * m.R2.X + v.Z * m.R3.X + v.W * m.R4.X,
                v.X * m.R1.Y + v.Y * m.R2.Y + v.Z * m.R3.Y + v.W * m.R4.Y,
                v.X * m.R1.Z + v.Y * m.R2.Z + v.Z * m.R3.Z + v.W * m.R4.Z,
                v.X * m.R1.W + v.Y * m.R2.W + v.Z * m.R3.W + v.W * m.R4.W
            );
        }

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public static Matrix4f Rotation(float angle, Vector3f vector)
        {
            float cos = MathF.Cos(angle);
            float sin = MathF.Sin(angle);

            Vector3f axis = Vector3f.Normalize(vector);
            Vector3f temp = (1.0f - cos) * axis;

            Matrix4f result = Identity;
            result.R1.X = cos + temp.X * axis.X;
            result.R1.Y = temp.X * axis.Y + sin * axis.Z;
            result.R1.Z = temp.X * axis.Z - sin * axis.Y;
            result.R2.X = temp.Y * axis.X - sin * axis.Z;
            result.R2.Y = cos + temp.Y * axis.Y;
            result.R2.Z = temp.Y * axis.Z + sin * axis.X;
            result.R3.X = temp.Z * axis.X + sin * axis.Y;
            result.R3.Y = temp.Z * axis.Y - sin * axis.X;
            result.R3.Z = cos + temp.Z * axis.Z;
            return result;
        }

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        public Matrix4f Rotate(float angle, Vector3f vector) => this = Rotate(this, angle, vector);

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="matrix">Matrix to rotate.</param>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public static Matrix4f Rotate(Matrix4f matrix, float angle, Vector3f vector)
        {
            return matrix * Rotation(angle, vector);
        }

        /// <summary>
        /// Creates translation matrix.
        /// </summary>
        /// <param name="vector">Translation vector.</param>
        /// <returns>Translation matrix.</returns>
        public static Matrix4f Translation(Vector3f vector)
        {
            Matrix4f result = Identity;
            result.R4.X = vector.X;
            result.R4.Y = vector.Y;
            result.R4.Z = vector.Z;
            return result;
        }

        /// <summary>
        /// Creates translation matrix.
        /// </summary>
        /// <param name="vector">Translation vector.</param>
        /// <returns>Translation matrix.</returns>
        public Matrix4f Translate(Vector3f vector) => this = Translate(this, vector);

        /// <summary>
        /// Creates translation matrix.
        /// </summary>
        /// <param name="matrix">Matrix to translate.</param>
        /// <param name="vector">Translation vector.</param>
        /// <returns>Translation matrix.</returns>
        public static Matrix4f Translate(Matrix4f matrix, Vector3f vector)
        {
            return matrix * Translation(vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4f Scaled(Vector3f vector)
        {
            Matrix4f result = Identity;
            result.R1.X = vector.X;
            result.R2.Y = vector.Y;
            result.R3.Z = vector.Z;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix4f Scale(Vector3f vector) => this = Scale(this, vector);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4f Scale(Matrix4f matrix, Vector3f vector)
        {
            return matrix * Scaled(vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4f Scaled(float scalar)
        {
            Matrix4f result = Identity;
            result.R1.X = scalar;
            result.R2.Y = scalar;
            result.R3.Z = scalar;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix4f Scale(float scalar) => this = Scale(this, scalar);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix4f Scale(Matrix4f matrix, float scalar)
        {
            return matrix * Scaled(scalar);
        }

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <returns>Transposed matrix.</returns>
        public Matrix4f Transpose() => this = Transpose(this);

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to be transposed.</param>
        /// <returns>Transposed matrix.</returns>
        public static Matrix4f Transpose(Matrix4f matrix)
        {
            return new Matrix4f(matrix.R1.X, matrix.R2.X, matrix.R3.X, matrix.R4.X,
                                matrix.R1.Y, matrix.R2.Y, matrix.R3.Y, matrix.R4.Y,
                                matrix.R1.Z, matrix.R2.Z, matrix.R3.Z, matrix.R4.Z,
                                matrix.R1.W, matrix.R2.W, matrix.R4.Z, matrix.R4.W);
        }

        /// <summary>
        /// Creates look at matrix.
        /// </summary>
        /// <param name="eye">Position of the camera or "eyes"</param>
        /// <param name="center">Center point.</param>
        /// <param name="up">Up vector</param>
        /// <returns>Look at matrix.</returns>
        public static Matrix4f LookAt(Vector3f eye, Vector3f center, Vector3f up)
        {
            Matrix4f result = Identity;

            Vector3f position = Vector3f.Normalize(center - eye);
            Vector3f target = Vector3f.Normalize(Vector3f.Cross(position, up));
            Vector3f upNew = Vector3f.Cross(target, position);

            result.R1.X = target.X;
            result.R2.X = target.Y;
            result.R3.X = target.Z;
            result.R1.Y = upNew.X;
            result.R2.Y = upNew.Y;
            result.R3.Y = upNew.Z;
            result.R1.Z = -position.X;
            result.R2.Z = -position.Y;
            result.R3.Z = -position.Z;
            result.R4.X = -Vector3f.Dot(target, eye);
            result.R4.Y = -Vector3f.Dot(upNew, eye);
            result.R4.Z = Vector3f.Dot(position, eye);
            return result;
        }

        /// <summary>
        /// Creates perspective view matrix.
        /// </summary>
        /// <param name="fov">Field of view.</param>
        /// <param name="aspectRatio">Aspect ratio for view.</param>
        /// <param name="near">Near plane.</param>
        /// <param name="far">Far plane.</param>
        /// <returns>Perspective view matrix.</returns>
        public static Matrix4f Perspective(float fov, float aspectRatio, float near, float far)
        {
            Matrix4f result = Identity;
            float tangent = MathF.Tan(fov / 2);

            result.R1.X = 1.0f / (aspectRatio * tangent);
            result.R2.Y = 1.0f / tangent;
            result.R3.Z = -(far + near) / (far - near);
            result.R3.W = -1.0f;
            result.R4.Z = -(2.0f * far * near) / (far - near);
            result.R4.W = 0.0f;
            return result;
        }

        /// <summary>
        /// Creates an orthographic view matrix.
        /// </summary>
        /// <param name="left">Left side of view.</param>
        /// <param name="right">Right side of view.</param>
        /// <param name="top">Top side of view.</param>
        /// <param name="bottom">Bottom side of view.</param>
        /// <param name="near">Near plane.</param>
        /// <param name="far">Far plane.</param>
        /// <returns>Orthographic view matrix.</returns>
        public static Matrix4f Orthographic(float left, float right, float bottom, float top, float near, float far)
        {
            Matrix4f result = Identity;

            result.R1.X = 2.0f / (right - left);
            result.R2.Y = 2.0f / (top - bottom);
            result.R3.Z = -2.0f / (far - near);
            result.R4.X = -(right + left) / (right - left);
            result.R4.Y = -(top + bottom) / (top - bottom);
            result.R4.Z = -(far + near) / (far - near);
            return result;
        }

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <returns>Determinant of matrix.</returns>
        public float Determinant() => Determinant(this);

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Determinant of matrix.</returns>
        public static float Determinant(Matrix4f matrix)
        {
            return ((matrix.R1.X * (matrix.R2.Y * (matrix.R3.Z * matrix.R4.W - matrix.R4.Z * matrix.R3.W) + matrix.R3.Y * (matrix.R4.Z * matrix.R2.W - matrix.R2.Z * matrix.R4.W) + matrix.R4.Y * (matrix.R2.Z * matrix.R3.W - matrix.R3.Z * matrix.R2.W)))
                 - (matrix.R1.Y * (matrix.R2.X * (matrix.R3.Z * matrix.R4.W - matrix.R4.Z * matrix.R3.W) + matrix.R3.X * (matrix.R4.Z * matrix.R2.W - matrix.R2.Z * matrix.R4.W) + matrix.R4.X * (matrix.R2.Z * matrix.R3.W - matrix.R3.Z * matrix.R2.W)))
                 + (matrix.R1.Z * (matrix.R2.X * (matrix.R3.Y * matrix.R4.W - matrix.R4.Y * matrix.R3.W) + matrix.R3.X * (matrix.R4.Y * matrix.R2.W - matrix.R2.Y * matrix.R4.W) + matrix.R4.X * (matrix.R2.Y * matrix.R3.W - matrix.R3.Y * matrix.R2.W)))
                 - (matrix.R1.W * (matrix.R2.X * (matrix.R3.Y * matrix.R4.Z - matrix.R4.Y * matrix.R3.Z) + matrix.R3.X * (matrix.R4.Y * matrix.R2.Z - matrix.R2.Y * matrix.R4.Z) + matrix.R4.X * (matrix.R2.Y * matrix.R3.Z - matrix.R3.Y * matrix.R2.Z))));
        }

        /// <summary>
        /// Inverts the matrix.
        /// </summary>
        /// <returns>Inverted matrix.</returns>
        public Matrix4f Inverse() => this = Invert(this);

        /// <summary>
        /// Inverts the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to invert.</param>
        /// <returns>Inverted matrix.</returns>
        public static Matrix4f Invert(Matrix4f matrix)
        {
            Vector3f a = new Vector3f(matrix.R1.X, matrix.R2.X, matrix.R3.X);
            Vector3f b = new Vector3f(matrix.R1.Y, matrix.R2.Y, matrix.R3.Y);
            Vector3f c = new Vector3f(matrix.R1.Z, matrix.R2.Z, matrix.R3.Z);
            Vector3f d = new Vector3f(matrix.R1.W, matrix.R2.W, matrix.R3.W);

            float x = matrix.R4.X;
            float y = matrix.R4.Y;
            float z = matrix.R4.Z;
            float w = matrix.R4.W;

            Vector3f s = Vector3f.Cross(a, b);
            Vector3f t = Vector3f.Cross(c, d);
            Vector3f u = a * y - b * x;
            Vector3f v = c * w - d * z;

            float inverseDeterminant = 1.0f / (Vector3f.Dot(s, v) + Vector3f.Dot(t, u));
            s *= inverseDeterminant;
            t *= inverseDeterminant;
            u *= inverseDeterminant;
            v *= inverseDeterminant;

            Vector3f r0 = Vector3f.Cross(b, v) + t * y;
            Vector3f r1 = Vector3f.Cross(v, a) - t * x;
            Vector3f r2 = Vector3f.Cross(d, u) + s * w;
            Vector3f r3 = Vector3f.Cross(u, c) - s * z;

            return new Matrix4f(r0.X, -r0.Y, r0.Z, -Vector3f.Dot(b, t),
                                -r1.X, r1.Y, -r1.Z, Vector3f.Dot(a, t),
                                r2.X, -r2.Y, r2.Z, -Vector3f.Dot(d, s),
                                r3.X, r3.Y, -r3.Z, Vector3f.Dot(c, s));
        }

        /// <summary>
        /// Converts matrix to array by appending rows together in order.
        /// </summary>
        /// <returns>Array of floats.</returns>
        public float[] ToArray()
        {
            return new float[] { R1.X, R1.Y, R1.Z, R1.W, R2.X, R2.Y, R2.Z, R2.W, R3.X, R3.Y, R3.Z, R3.W, R4.X, R4.Y, R4.Z, R4.W };
        }

        /// <summary>
        /// Compares two matrices.
        /// </summary>
        /// <param name="other">Matrix to compare to.</param>
        /// <returns>True if all values in the two matrices are the same.</returns>
        public bool Equals(Matrix4f other)
        {
            return R1.X == other.R1.X &&
                    R1.Y == other.R1.Y &&
                    R1.Z == other.R1.Z &&
                    R1.W == other.R1.W &&
                    R2.X == other.R2.X &&
                    R2.Y == other.R2.Y &&
                    R2.Z == other.R2.Z &&
                    R2.W == other.R2.W &&
                    R3.X == other.R3.X &&
                    R3.Y == other.R3.Y &&
                    R3.Z == other.R3.Z &&
                    R3.W == other.R3.W &&
                    R4.X == other.R4.X &&
                    R4.Y == other.R4.Y &&
                    R4.Z == other.R4.Z &&
                    R4.W == other.R4.W;
        }

        /// <summary>
        /// Compares matrix to object.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if object is of type Matrix4f, is not null, and all values in matrices are the same.</returns>
        public override bool Equals(object obj)
        {
            Matrix4f? matrix = (Matrix4f)obj;

            if (matrix == null) return false;

            return Equals(matrix);
        }

        /// <summary>
        /// Calculates hash code of matrix.
        /// </summary>
        /// <returns>Hashcode of matrix.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(
                HashCode.Combine(R1.X, R1.Y, R1.Z, R1.W, R2.X, R2.Y, R2.Z, R2.W),
                HashCode.Combine(R3.X, R3.Y, R3.Z, R3.W, R4.X, R4.Y, R4.Z, R4.W)
            );
        }

        /// <summary>
        /// Converts matrix to string.
        /// </summary>
        /// <returns>Matrix in form of a string.</returns>
        public override string ToString()
        {
            return $"[{R1.X:0.000}, {R1.Y:0.000}, {R1.Z:0.000}, {R1.W:0.000}]\n[{R2.X:0.000}, {R2.Y:0.000}, {R2.Z:0.000}, {R2.W:0.000}]\n[{R3.X:0.000}, {R3.Y:0.000}, {R3.Z:0.000}, {R3.W:0.000}]\n[{R4.X:0.000}, {R4.Y:0.000}, {R4.Z:0.000}, {R4.W:0.000}]\n";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for matrix addition.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix4f operator +(Matrix4f m1, Matrix4f m2) => Add(m1, m2);

        /// <summary>
        /// Operator overload for matrix subtraction.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix4f operator -(Matrix4f m1, Matrix4f m2) => Subtract(m1, m2);

        /// <summary>
        /// Operator overload for matrix multiplication.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix4f operator *(Matrix4f m1, Matrix4f m2) => Multiply(m1, m2);

        /// <summary>
        /// Operator overload for matrix-scalar multiplication
        /// </summary>
        /// <param name="m1">Matrix to be multiplied by.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix4f operator *(Matrix4f m1, float scalar) => Multiply(m1, scalar);

        /// <summary>
        /// Operator overload for matrix and column vector multiplication.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and column vector as column vector.</returns>
        public static Vector4f operator *(Matrix4f m1, Vector4f v) => Multiply(m1, v);

        /// <summary>
        /// Operator overload for row vector and matrix multiplication
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as a row vector.</returns>
        public static Vector4f operator *(Vector4f v, Matrix4f m) => Multiply(v, m);

        /// <summary>
        /// Operator overload for matrix equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are equal.</returns>
        public static bool operator ==(Matrix4f m1, Matrix4f m2)
        {
            return m1.Equals(m2);
        }

        /// <summary>
        /// Operator overload for matrix not equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are not equal.</returns>
        public static bool operator !=(Matrix4f m1, Matrix4f m2)
        {
            return !m1.Equals(m2);
        }

        #endregion
    }
}
