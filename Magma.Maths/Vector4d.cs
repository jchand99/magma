using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 4-Dimentional vector of doubles.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 32)]
    public struct Vector4d : IEquatable<Vector4d>
    {
        /// <summary>
        /// X component of vector.
        /// </summary>
        public double X;

        /// <summary>
        /// Y component of vector.
        /// </summary>
        public double Y;

        /// <summary>
        /// Z component of vector.
        /// </summary>
        public double Z;

        /// <summary>
        /// W component of vector.
        /// </summary>
        public double W;

        /// <summary>
        /// Total count of components in vector.
        /// </summary>
        public const int Count = 4;

        /// <summary>
        /// Total size in bytes of vector.
        /// </summary>
        public const int SizeInBytes = sizeof(double) * Count;

        /// <summary>
        /// Zero vector
        ///
        /// <para>Example:</para>
        /// <para>[ 0, 0, 0, 0 ]</para>
        /// </summary>
        /// <returns>Zero vector.</returns>
        public static readonly Vector4d Zero = new Vector4d(0.0d);

        /// <summary>
        /// One vector
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 1, 1, 1 ]</para>
        /// </summary>
        /// <returns>One vector.</returns>
        public static readonly Vector4d One = new Vector4d(1.0d);

        /// <summary>
        /// Positive infinity vector.
        ///
        /// <para>Example:</para>
        /// <para>[ inf, inf, inf, inf ]</para>
        /// </summary>
        /// <returns>Positive infinity vector.</returns>
        public static readonly Vector4d PositiveInfinity = new Vector4d(double.PositiveInfinity);

        /// <summary>
        /// Negative infinity vector.
        /// <para>Example:</para>
        /// <para>[ -inf, -inf, -inf, -inf ]</para>
        /// </summary>
        /// <returns>Negative infinity vector.</returns>
        public static readonly Vector4d NegativeInfinity = new Vector4d(double.NegativeInfinity);

        /// <summary>
        /// Up vector.
        /// <para>Example:</para>
        /// <para>[ 0, 1, 0, 0 ]</para>
        /// </summary>
        /// <returns>Up vector.</returns>
        public static readonly Vector4d Up = new Vector4d(0.0d, 1.0d, 0.0d, 0.0d);

        /// <summary>
        /// Down vector.
        /// <para>Example:</para>
        /// <para>[ 0, -1, 0, 0 ]</para>
        /// </summary>
        /// <returns>Down vector.</returns>
        public static readonly Vector4d Down = new Vector4d(0.0d, -1.0d, 0.0d, 0.0d);

        /// <summary>
        /// Left vector.
        /// <para>Example:</para>
        /// <para>[ -1, 0, 0, 0 ]</para>
        /// </summary>
        /// <returns>Left vector.</returns>
        public static readonly Vector4d Left = new Vector4d(-1.0d, 0.0d, 0.0d, 0.0d);

        /// <summary>
        /// Right vector.
        /// <para>Example:</para>
        /// <para>[ 1, 0, 0, 0 ]</para>
        /// </summary>
        /// <returns>Right vector.</returns>
        public static readonly Vector4d Right = new Vector4d(1.0d, 0.0d, 0.0d, 0.0d);

        /// <summary>
        /// Back vector.
        /// <para>Example:</para>
        /// <para>[ 0, 0, -1, 0 ]</para>
        /// </summary>
        /// <returns>Back vector.</returns>
        public static readonly Vector4d Back = new Vector4d(0.0d, 0.0d, -1.0d, 0.0d);

        /// <summary>
        /// Forward vector.
        /// <para>Example:</para>
        /// <para>[ 0, 0, 1, 0 ]</para>
        /// </summary>
        /// <returns>Forward vector.</returns>
        public static readonly Vector4d Forward = new Vector4d(0.0d, 0.0d, 1.0d, 0.0d);

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ x, y, z, w ]</para>
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        /// <param name="z">Z component.</param>
        /// <param name="w">W component.</param>
        public Vector4d(double x, double y, double z, double w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ scalar, scalar, scalar, scalar ]</para>
        /// </summary>
        /// <param name="scalar">Scalar for all the components of the vector.</param>
        public Vector4d(double scalar) : this(scalar, scalar, scalar, scalar) { }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ v.x, v.y, z, w ]</para>
        /// </summary>
        /// <param name="vector">Vector for the x and y components.</param>
        /// <param name="z">Z component.</param>
        /// <param name="w">W component.</param>
        public Vector4d(Vector2f vector, double z, double w) : this(vector.X, vector.Y, z, w) { }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ x, y, v.x, v.y ]</para>
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        /// <param name="vector">Vector for the z and w components.</param>
        public Vector4d(double x, double y, Vector2f vector) : this(x, y, vector.X, vector.Y) { }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ v1.x, v1.y, v2.x, v2.y ]</para>
        /// </summary>
        /// <param name="v1">Vector for x and y components.</param>
        /// <param name="v2">Vector for z and w components.</param>
        public Vector4d(Vector2f v1, Vector2f v2) : this(v1.X, v1.Y, v2.X, v2.Y) { }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ v.x, v.y, v.z, w ]</para>
        /// </summary>
        /// <param name="vector">Vector for x, y and z components.</param>
        /// <param name="w">W component.</param>
        public Vector4d(Vector3f vector, double w) : this(vector.X, vector.Y, vector.Z, w) { }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ x, v.x, v.y, v.z ]</para>
        /// </summary>
        /// <param name="x">X component</param>
        /// <param name="vector">Vector for y, z and w components.</param>
        public Vector4d(double x, Vector3f vector) : this(x, vector.X, vector.Y, vector.Z) { }

        /// <summary>
        /// Grabs the nth component of the vector.
        /// </summary>
        /// <value>Nth component of the vector.</value>
        public double this[int index]
        {
            get
            {
                if (index == 0) return X;
                else if (index == 1) return Y;
                else if (index == 2) return Z;
                else if (index == 3) return W;
                else throw new IndexOutOfRangeException($"Index {index} out of range.");
            }
            set
            {
                if (index == 0) X = value;
                else if (index == 1) Y = value;
                else if (index == 2) Z = value;
                else if (index == 3) W = value;
                else throw new IndexOutOfRangeException($"Index {index} out of range.");
            }
        }

        /// <summary>
        /// Calculates magnitude of vector.
        /// </summary>
        /// <returns>Magnitude of vector.</returns>
        public double Magnitude() => Magnitude(this);

        /// <summary>
        /// Calculates magnitude of vector.
        /// </summary>
        /// <param name="v">Vector to calculate.</param>
        /// <returns>Magnitude of vector.</returns>
        public static double Magnitude(Vector4d v)
        {
            return Math.Sqrt((v.X * v.X) + (v.Y * v.Y) + (v.Z * v.Z) + (v.W * v.W));
        }

        /// <summary>
        /// Normalizes vector.
        /// </summary>
        /// <returns>Normalized vector.</returns>
        public Vector4d Normalize() => this = Normalize(this);

        /// <summary>
        /// Normalizes vector.
        /// </summary>
        /// <param name="v">Vector to normalize.</param>
        /// <returns>Normalized vector.</returns>
        public static Vector4d Normalize(Vector4d v)
        {
            return v / Magnitude(v);
        }

        /// <summary>
        /// Calculates dot product of vectors.
        /// </summary>
        /// <param name="vector">Vector to dot product with this.</param>
        /// <returns>Dot product of this and vector.</returns>
        public double Dot(Vector4d vector) => Dot(this, vector);

        /// <summary>
        /// Calculates dot product of vectors.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Dot product of two vectors.</returns>
        public static double Dot(Vector4d v1, Vector4d v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z + v1.W * v2.W;
        }

        /// <summary>
        /// Projects one vector onto another.
        /// </summary>
        /// <param name="vector">Vector to project onto.</param>
        /// <returns>Projected vector.</returns>
        public Vector4d Project(Vector4d vector) => this = Project(this, vector);

        /// <summary>
        /// Projects one vector onto another.
        /// </summary>
        /// <param name="v1">Vector to project.</param>
        /// <param name="v2">Vector to project onto.</param>
        /// <returns>Projection vector.</returns>
        public static Vector4d Project(Vector4d v1, Vector4d v2)
        {
            return v2 * (Dot(v1, v2) / Dot(v2, v2));
        }

        /// <summary>
        /// Rejects one vector from another.
        /// </summary>
        /// <param name="vector">Vector to reject from.</param>
        /// <returns>Rejected vector.</returns>
        public Vector4d Reject(Vector4d vector) => this = Reject(this, vector);

        /// <summary>
        /// Rejects one vector from another.
        /// </summary>
        /// <param name="v1">Vector to reject.</param>
        /// <param name="v2">Vector to reject from.</param>
        /// <returns>Rejection vector.</returns>
        public static Vector4d Reject(Vector4d v1, Vector4d v2)
        {
            return v1 - v2 * (Dot(v1, v2) / Dot(v2, v2));
        }

        /// <summary>
        /// Compares two vectors together.
        /// </summary>
        /// <param name="other">Other vector to compare to.</param>
        /// <returns>True if all components of vector are the same.</returns>
        public bool Equals(Vector4d other)
        {
            return X == other.X && Y == other.Y && Z == other.Z && W == other.W;
        }

        /// <summary>
        /// Compares this vector with object together.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if the object is of type Vector2d, is not null, and all components are the same.</returns>
        public override bool Equals(object obj)
        {
            Vector4d? vector = (Vector4d)obj;

            if (vector == null) return false;

            return Equals(vector);
        }

        /// <summary>
        /// Calculates hash code of vector.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y, Z, W);
        }

        /// <summary>
        /// Converts vector to string.
        /// </summary>
        /// <returns>Vector as string.</returns>
        public override string ToString()
        {
            return $"[{X:0.000}, {Y:0.000}, {Z:0.000}, {W:0.000}]";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for vector-scalar addition.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="scalar">Scalar to add to vector.</param>
        /// <returns>Sum of vector and scalar.</returns>
        public static Vector4d operator +(Vector4d v, double scalar)
        {
            return new Vector4d(v.X + scalar, v.Y + scalar, v.Z + scalar, v.W + scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar addition.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="scalar">Scalar to add to vector.</param>
        /// <returns>Sum of vector and scalar.</returns>
        public static Vector4d operator +(double scalar, Vector4d v)
        {
            return new Vector4d(v.X + scalar, v.Y + scalar, v.Z + scalar, v.W + scalar);
        }

        /// <summary>
        /// Operator overload for vector-vector addition.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Sum of two vectors.</returns>
        public static Vector4d operator +(Vector4d v1, Vector4d v2)
        {
            return new Vector4d(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z, v1.W + v2.W);
        }

        /// <summary>
        /// Operator overload for vector-scalar subtraction.
        /// </summary>
        /// <param name="v">Vector to subtract from.</param>
        /// <param name="scalar">Scalar to subtract from vector.</param>
        /// <returns>Difference between vector and scalar.</returns>
        public static Vector4d operator -(Vector4d v, double scalar)
        {
            return new Vector4d(v.X - scalar, v.Y - scalar, v.Z - scalar, v.W - scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar subtraction.
        /// </summary>
        /// <param name="v">Vector to subtract from.</param>
        /// <param name="scalar">Scalar to subtract from vector.</param>
        /// <returns>Difference between vector and scalar.</returns>
        public static Vector4d operator -(double scalar, Vector4d v)
        {
            return new Vector4d(v.X - scalar, v.Y - scalar, v.Z - scalar, v.W - scalar);
        }

        /// <summary>
        /// OPerator overload for vector-vector subtraction.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Difference of two vectors.</returns>
        public static Vector4d operator -(Vector4d v1, Vector4d v2)
        {
            return new Vector4d(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z, v1.W - v2.W);
        }

        /// <summary>
        /// Operator overload for vector-scalar multiplication.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <param name="scalar">Scalar to multiply vectory by.</param>
        /// <returns>Product of vector and scalar.</returns>
        public static Vector4d operator *(Vector4d v, double scalar)
        {
            return new Vector4d(v.X * scalar, v.Y * scalar, v.Z * scalar, v.W * scalar);
        }

        /// <summary>
        /// Operator overload for vector-scalar multiplication.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <param name="scalar">Scalar to multiply vectory by.</param>
        /// <returns>Product of vector and scalar.</returns>
        public static Vector4d operator *(double scalar, Vector4d v)
        {
            return new Vector4d(v.X * scalar, v.Y * scalar, v.Z * scalar, v.W * scalar);
        }

        /// <summary>
        /// Operator overload for vector-vector multiplication.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>Product of two vectors.</returns>
        public static Vector4d operator *(Vector4d v1, Vector4d v2)
        {
            return new Vector4d(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z, v1.W * v2.W);
        }

        /// <summary>
        /// Operator overload for vector-scalar division.
        /// </summary>
        /// <param name="v">Vector to divide by.</param>
        /// <param name="scalar">Scalar to divide vector by.</param>
        /// <returns>Quotient of vector and scalar.</returns>
        public static Vector4d operator /(Vector4d v, double scalar)
        {
            double s = 1.0f / scalar;

            return new Vector4d(v.X * s, v.Y * s, v.Z * s, v.W * s);
        }

        /// <summary>
        /// Operator overload for vector-scalar division.
        /// </summary>
        /// <param name="v">Vector to divide by.</param>
        /// <param name="scalar">Scalar to divide vector by.</param>
        /// <returns>Quotient of vector and scalar.</returns>
        public static Vector4d operator /(double scalar, Vector4d v)
        {
            double s = 1.0f / scalar;

            return new Vector4d(v.X * s, v.Y * s, v.Z * s, v.W * s);
        }

        /// <summary>
        /// Operator overload for vector-vector division.
        /// </summary>
        /// <param name="v1">Numerator vector.</param>
        /// <param name="v2">Denominator vector.</param>
        /// <returns>Quotient of two vectors.</returns>
        public static Vector4d operator /(Vector4d v1, Vector4d v2)
        {
            double x = 1.0f / v2.X;
            double y = 1.0f / v2.Y;
            double z = 1.0f / v2.Z;
            double w = 1.0f / v2.W;

            return new Vector4d(v1.X * x, v1.Y * y, v1.Z * z, v1.W * w);
        }

        /// <summary>
        /// Operator overload for vector increment.
        /// </summary>
        /// <param name="v">Vector to increment.</param>
        /// <returns>Vector with all components incremented by 1.</returns>
        public static Vector4d operator ++(Vector4d v) => new Vector4d(v.X + 1.0d, v.Y + 1.0d, v.Z + 1.0d, v.W + 1.0d);

        /// <summary>
        /// Operator overload for vector decrement.
        /// </summary>
        /// <param name="v">Vector to decrement.</param>
        /// <returns>Vector with all components decremented by 1.</returns>
        public static Vector4d operator --(Vector4d v) => new Vector4d(v.X - 1.0d, v.Y - 1.0d, v.Z - 1.0d, v.W - 1.0d);

        /// <summary>
        /// Implicit operator overload to convert Vector4f to vector4d.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector4d(Vector4f v) => new Vector4d(v.X, v.Y, v.Z, v.W);

        /// <summary>
        /// Implicit operator overload to convert Vector4i to vector4d.
        /// </summary>
        /// <param name="v">Vector to convert.</param>
        public static implicit operator Vector4d(Vector4i v) => new Vector4d(v.X, v.Y, v.Z, v.W);

        /// <summary>
        /// Operator overload for vector equality.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>True if all components of vectors are the same.</returns>
        public static bool operator ==(Vector4d v1, Vector4d v2)
        {
            return v1.Equals(v2);
        }

        /// <summary>
        /// Operator overload for vector inequality.
        /// </summary>
        /// <param name="v1">Left vector.</param>
        /// <param name="v2">Right vector.</param>
        /// <returns>True if any components of the two vectors are not the same.</returns>
        public static bool operator !=(Vector4d v1, Vector4d v2)
        {
            return !v1.Equals(v2);
        }

        #endregion
    }
}
