using System;
using System.Runtime.InteropServices;

namespace Magma.Maths
{
    /// <summary>
    /// 3-Dimensional Matrix of doubles (Row major).
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 72)]
    public struct Matrix3d : IEquatable<Matrix3d>
    {
        /// <summary>
        /// Row one.
        /// </summary>
        public Vector3d R1;

        /// <summary>
        /// Row two.
        /// </summary>
        public Vector3d R2;

        /// <summary>
        /// Row three.
        /// </summary>
        public Vector3d R3;

        /// <summary>
        /// Total count of elements.
        /// </summary>
        public const int Count = 9;

        /// <summary>
        /// Total size in bytes of matrix.
        /// </summary>
        public const int SizeInBytes = sizeof(double) * Count;

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ e11, e12, e13 ]</para>
        /// <para>[ e21, e22, e23 ]</para>
        /// <para>[ e31, e32, e33 ]</para>
        /// </summary>
        /// <param name="e11">First element of row one.</param>
        /// <param name="e12">Second element of row one.</param>
        /// <param name="e13">Third element of row one.</param>
        /// <param name="e21">First element of row two.</param>
        /// <param name="e22">Second element of row two.</param>
        /// <param name="e23">Third element of row two.</param>
        /// <param name="e31">First element of row three.</param>
        /// <param name="e32">Second element of row three.</param>
        /// <param name="e33">Third element of row three.</param>
        public Matrix3d(double e11, double e12, double e13, double e21, double e22, double e23, double e31, double e32, double e33)
        {
            R1 = new Vector3d(e11, e12, e13);
            R2 = new Vector3d(e21, e22, e23);
            R3 = new Vector3d(e31, e32, e33);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, 0 ]</para>
        /// <para>[ 0,      0,      1 ]</para>
        ///
        /// <para>Example Non-Identity:</para>
        /// <para>[ m.r1.x, m.r1.y, 0 ]</para>
        /// <para>[ m.r2.x, m.r2.y, 0 ]</para>
        /// <para>[ 0,      0,      0 ]</para>
        /// </summary>
        /// <param name="matrix">Matrix to put in top left corner of new Matrix3d.</param>
        /// <param name="fromIdentity">If true sets e33 to 1 as part of identity matrix.</param>
        public Matrix3d(Matrix2d matrix, bool fromIdentity)
        {
            R1 = new Vector3d(matrix.R1.X, matrix.R1.Y, 0.0d);
            R2 = new Vector3d(matrix.R2.X, matrix.R2.Y, 0.0d);
            if (fromIdentity)
                R3 = new Vector3d(0.0d, 0.0d, 1.0d);
            else
                R3 = new Vector3d(0.0d, 0.0d, 0.0d);
        }

        /// <summary>
        /// <para>Example From Identity:</para>
        /// <para>[ v1.x, v1.y, v1.z ]</para>
        /// <para>[ v2.x, v2.y, v2.z ]</para>
        /// <para>[ v3.x, v3.y, v3.z ]</para>
        /// </summary>
        /// <param name="v1">Row one.</param>
        /// <param name="v2">Row two.</param>
        /// <param name="v3">Row three.</param>
        public Matrix3d(Vector3d v1, Vector3d v2, Vector3d v3)
        {
            R1 = v1;
            R2 = v2;
            R3 = v3;
        }

        /// <summary>
        /// <para>Example:</para>
        /// <para>[ diagonal, 0, 0 ]</para>
        /// <para>[ 0, diagonal, 0 ]</para>
        /// <para>[ 0, 0, diagonal ]</para>
        /// </summary>
        /// <param name="diagonal">Value that will be the diagonal</param>
        public Matrix3d(double diagonal)
        {
            R1 = new Vector3d(diagonal, 0.0d, 0.0d);
            R2 = new Vector3d(0.0d, diagonal, 0.0d);
            R3 = new Vector3d(0.0d, 0.0d, diagonal);
        }

        /// <summary>
        /// Identity matrix.
        ///
        /// <para>Example:</para>
        /// <para>[ 1, 0, 0 ]</para>
        /// <para>[ 0, 1, 0 ]</para>
        /// <para>[ 0, 0, 1 ]</para>
        ///
        /// </summary>
        /// <returns>Identity matrix.</returns>
        public static Matrix3d Identity => new Matrix3d(1.0d, 0.0d, 0.0d, 0.0d, 1.0d, 0.0d, 0.0d, 0.0d, 1.0d);

        /// <summary>
        /// Adds two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A + A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix3d Add(Matrix3d m1, Matrix3d m2)
        {
            return new Matrix3d(m1.R1.X + m2.R1.X,
                                m1.R1.Y + m2.R1.Y,
                                m1.R1.Z + m2.R1.Z,
                                m1.R2.X + m2.R2.X,
                                m1.R2.Y + m2.R2.Y,
                                m1.R2.Z + m2.R2.Z,
                                m1.R3.X + m2.R3.X,
                                m1.R3.Y + m2.R3.Y,
                                m1.R3.Z + m2.R3.Z);
        }

        /// <summary>
        /// Subtracts the two matrices.
        ///
        /// <para>Example:</para>
        /// <para>A - A</para>
        /// </summary>
        /// <param name="m1">Matrix one.</param>
        /// <param name="m2">Matrix two.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix3d Subtract(Matrix3d m1, Matrix3d m2)
        {
            return new Matrix3d(m1.R1.X - m2.R1.X,
                                m1.R1.Y - m2.R1.Y,
                                m1.R1.Z - m2.R1.Z,
                                m1.R2.X - m2.R2.X,
                                m1.R2.Y - m2.R2.Y,
                                m1.R2.Z - m2.R2.Z,
                                m1.R3.X - m2.R3.X,
                                m1.R3.Y - m2.R3.Y,
                                m1.R3.Z - m2.R3.Z);
        }

        /// <summary>
        /// Multiplies the matrix by a scalar value.
        ///
        /// <para>Example:</para>
        /// <para>A * s</para>
        /// </summary>
        /// <param name="m1">Matrix to be multiplied.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix3d Multiply(Matrix3d m1, double scalar)
        {
            return new Matrix3d(m1.R1.X * scalar,
                                m1.R1.Y * scalar,
                                m1.R1.Z * scalar,
                                m1.R2.X * scalar,
                                m1.R2.Y * scalar,
                                m1.R2.Z * scalar,
                                m1.R3.X * scalar,
                                m1.R3.Y * scalar,
                                m1.R3.Z * scalar);
        }

        /// <summary>
        /// Multiplies the two matrices together.
        ///
        /// <para>Example:</para>
        /// <para>A * A</para>
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix3d Multiply(Matrix3d m1, Matrix3d m2)
        {
            return new Matrix3d(m2.R1.X * m1.R1.X + m2.R1.Y * m1.R2.X + m2.R1.Z * m1.R3.X,
                                m2.R1.X * m1.R1.Y + m2.R1.Y * m1.R2.Y + m2.R1.Z * m1.R3.Y,
                                m2.R1.X * m1.R1.Z + m2.R1.Y * m1.R2.Z + m2.R1.Z * m1.R3.Z,
                                m2.R2.X * m1.R1.X + m2.R2.Y * m1.R2.X + m2.R2.Z * m1.R3.X,
                                m2.R2.X * m1.R1.Y + m2.R2.Y * m1.R2.Y + m2.R2.Z * m1.R3.Y,
                                m2.R2.X * m1.R1.Z + m2.R2.Y * m1.R2.Z + m2.R2.Z * m1.R3.Z,
                                m2.R3.X * m1.R1.X + m2.R3.Y * m1.R2.X + m2.R3.Z * m1.R3.X,
                                m2.R3.X * m1.R1.Y + m2.R3.Y * m1.R2.Y + m2.R3.Z * m1.R3.Y,
                                m2.R3.X * m1.R1.Z + m2.R3.Y * m1.R2.Z + m2.R3.Z * m1.R3.Z);
        }

        /// <summary>
        /// Multiplies matrix and (column) vector.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and vector as column vector.</returns>
        public static Vector3d Multiply(Matrix3d m, Vector3d v)
        {
            return new Vector3d(
                m.R1.X * v.X + m.R1.Y * v.Y + m.R1.Z * v.Z,
                m.R2.X * v.X + m.R2.Y * v.Y + m.R2.Z * v.Z,
                m.R3.X * v.X + m.R3.Y * v.Y + m.R3.Z * v.Z
            );
        }

        /// <summary>
        /// Multiplies matrix and (row) vector.
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as row vector.</returns>
        public static Vector3d Multiply(Vector3d v, Matrix3d m)
        {
            return new Vector3d(
                v.X * m.R1.X + v.Y * m.R2.X + v.Z * m.R3.X,
                v.X * m.R1.Y + v.Y * m.R2.Y + v.Z * m.R3.Y,
                v.X * m.R1.Z + v.Y * m.R2.Z + v.Z * m.R3.Z
            );
        }

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public static Matrix3d Rotation(double angle, Vector3d vector)
        {
            double cos = Math.Cos(angle);
            double sin = Math.Sin(angle);

            Vector3d axis = Vector3d.Normalize(vector);
            Vector3d temp = (1.0f - cos) * axis;

            Matrix3d result = Identity;
            result.R1.X = cos + temp.X * axis.X;
            result.R1.Y = temp.X * axis.Y + sin * axis.Z;
            result.R1.Z = temp.X * axis.Z - sin * axis.Y;
            result.R2.X = temp.Y * axis.X - sin * axis.Z;
            result.R2.Y = cos + temp.Y * axis.Y;
            result.R2.Z = temp.Y * axis.Z + sin * axis.X;
            result.R3.X = temp.Z * axis.X + sin * axis.Y;
            result.R3.Y = temp.Z * axis.Y - sin * axis.X;
            result.R3.Z = cos + temp.Z * axis.Z;
            return result;
        }

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public Matrix3d Rotate(double angle, Vector3d vector) => this = Rotate(this, angle, vector);

        /// <summary>
        /// Creates a rotation matrix.
        /// </summary>
        /// <param name="matrix">Matrix to rotate.</param>
        /// <param name="angle">Angle to rotate in radians.</param>
        /// <param name="vector">Rotation vector.</param>
        /// <returns>Rotation matrix.</returns>
        public static Matrix3d Rotate(Matrix3d matrix, double angle, Vector3d vector)
        {
            return matrix * Rotation(angle, vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3d Scaled(Vector3d vector)
        {
            Matrix3d result = Identity;
            result.R1.X = vector.X;
            result.R2.Y = vector.Y;
            result.R3.Z = vector.Z;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix3d Scale(Vector3d vector) => this = Scale(this, vector);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="vector">Scale vector.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3d Scale(Matrix3d matrix, Vector3d vector)
        {
            return matrix * Scaled(vector);
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3d Scaled(double scalar)
        {
            Matrix3d result = Identity;
            result.R1.X = scalar;
            result.R2.Y = scalar;
            result.R3.Z = scalar;
            return result;
        }

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public Matrix3d Scale(double scalar) => this = Scale(this, scalar);

        /// <summary>
        /// Creates scaled matrix.
        /// </summary>
        /// <param name="matrix">Matrix to scale.</param>
        /// <param name="scalar">Scalar to scale matrix by.</param>
        /// <returns>Scaled matrix.</returns>
        public static Matrix3d Scale(Matrix3d matrix, double scalar)
        {
            return matrix * Scaled(scalar);
        }

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <returns>Transposed matrix.</returns>
        public Matrix3d Transpose() => this = Transpose(this);

        /// <summary>
        /// Transposes the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to be transposed.</param>
        /// <returns>Transposed matrix.</returns>
        public static Matrix3d Transpose(Matrix3d matrix)
        {
            return new Matrix3d(matrix.R1.X, matrix.R2.X, matrix.R3.X,
                                matrix.R1.Y, matrix.R2.Y, matrix.R3.Y,
                                matrix.R1.Z, matrix.R2.Z, matrix.R3.Z);
        }

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <returns>Determinant of matrix.</returns>
        public double Determinant() => Determinant(this);

        /// <summary>
        /// Calculates determinant of matrix.
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Determinant of matrix.</returns>
        public static double Determinant(Matrix3d matrix)
        {
            return (matrix.R1.X * (matrix.R2.Y * matrix.R3.Z - matrix.R2.Z * matrix.R3.Y) + matrix.R1.Y * (matrix.R2.Z * matrix.R3.X - matrix.R2.X * matrix.R3.Z) + matrix.R1.Z * (matrix.R2.X * matrix.R3.Y - matrix.R2.Y * matrix.R3.X));
        }

        /// <summary>
        /// Inverts the matrix.
        /// </summary>
        /// <returns>Inverted matrix.</returns>
        public Matrix3d Inverse() => this = Invert(this);

        /// <summary>
        /// Inverts the matrix.
        /// </summary>
        /// <param name="matrix">Matrix to invert.</param>
        /// <returns>Inverted matrix.</returns>
        public static Matrix3d Invert(Matrix3d matrix)
        {
            Vector3d a = new Vector3d(matrix.R1.X, matrix.R2.X, matrix.R3.X);
            Vector3d b = new Vector3d(matrix.R1.Y, matrix.R2.Y, matrix.R3.Y);
            Vector3d c = new Vector3d(matrix.R1.Z, matrix.R2.Z, matrix.R3.Z);

            Vector3d r0 = Vector3d.Cross(b, c);
            Vector3d r1 = Vector3d.Cross(c, a);
            Vector3d r2 = Vector3d.Cross(a, b);

            double inverseDeterminant = 1.0 / Vector3d.Dot(r2, c);

            return new Matrix3d(r0.X * inverseDeterminant, r0.Y * inverseDeterminant, r0.Z * inverseDeterminant,
                                r1.X * inverseDeterminant, r1.Y * inverseDeterminant, r1.Z * inverseDeterminant,
                                r2.X * inverseDeterminant, r2.Y * inverseDeterminant, r2.Z * inverseDeterminant);
        }

        /// <summary>
        /// Converts matrix to array by appending rows together in order.
        /// </summary>
        /// <returns>Array of doubles.</returns>

        public double[] ToArray()
        {
            return new double[] { R1.X, R1.Y, R1.Z, R2.X, R2.Y, R2.Z, R3.X, R3.Y, R3.Z };
        }

        /// <summary>
        /// Compares two matrices.
        /// </summary>
        /// <param name="other">Matrix to compare to.</param>
        /// <returns>True if all values in the two matrices are the same.</returns>
        public bool Equals(Matrix3d other)
        {
            return R1.X == other.R1.X &&
                    R1.Y == other.R1.Y &&
                    R1.Z == other.R1.Z &&
                    R2.X == other.R2.X &&
                    R2.Y == other.R2.Y &&
                    R2.Z == other.R2.Z &&
                    R3.X == other.R3.X &&
                    R3.Y == other.R3.Y &&
                    R3.Z == other.R3.Z;
        }

        /// <summary>
        /// Compares matrix to object.
        /// </summary>
        /// <param name="obj">object to compare to.</param>
        /// <returns>True if object is of type Matrix3d, is not null, and all values in matrices are the same.</returns>
        public override bool Equals(object obj)
        {
            Matrix3d? matrix = (Matrix3d)obj;

            if (matrix == null) return false;

            return Equals(matrix);
        }

        /// <summary>
        /// Calculates hash code of matrix.
        /// </summary>
        /// <returns>Hashcode of matrix.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(
                HashCode.Combine(R1.X, R1.Y, R1.Z, R2.X, R2.Y, R2.Z, R3.X, R3.Y),
                R3.Z.GetHashCode()
            );
        }

        /// <summary>
        /// Converts matrix to string.
        /// </summary>
        /// <returns>Matrix in form of a string.</returns>
        public override string ToString()
        {
            return $"[{R1.X:0.000}, {R1.Y:0.000}, {R1.Z:0.000}]\n[{R2.X:0.000}, {R2.Y:0.000}, {R2.Z:0.000}]\n[{R3.X:0.000}, {R3.Y:0.000}, {R3.Z:0.000}]\n";
        }

        #region Operator Overloads

        /// <summary>
        /// Operator overload for matrix addition.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Sum of two matrices.</returns>
        public static Matrix3d operator +(Matrix3d m1, Matrix3d m2) => Add(m1, m2);

        /// <summary>
        /// Operator overload for matrix subtraction.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Difference of two matrices.</returns>
        public static Matrix3d operator -(Matrix3d m1, Matrix3d m2) => Subtract(m1, m2);

        /// <summary>
        /// Operator overload for matrix multiplication.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>Product of two matrices.</returns>
        public static Matrix3d operator *(Matrix3d m1, Matrix3d m2) => Multiply(m1, m2);

        /// <summary>
        /// Operator overload for matrix-scalar multiplication
        /// </summary>
        /// <param name="m1">Matrix to be multiplied by.</param>
        /// <param name="scalar">Scalar to multiply matrix by.</param>
        /// <returns>Product of matrix and scalar.</returns>
        public static Matrix3d operator *(Matrix3d m1, double scalar) => Multiply(m1, scalar);

        /// <summary>
        /// Operator overload for matrix and column vector multiplication.
        /// </summary>
        /// <param name="m">Left matrix.</param>
        /// <param name="v">Right vector.</param>
        /// <returns>Product of matrix and column vector as column vector.</returns>
        public static Vector3d operator *(Matrix3d m, Vector3d v) => Multiply(m, v);

        /// <summary>
        /// Operator overload for row vector and matrix multiplication
        /// </summary>
        /// <param name="v">Left vector.</param>
        /// <param name="m">Right matrix.</param>
        /// <returns>Product of row vector and matrix as a row vector.</returns>
        public static Vector3d operator *(Vector3d v, Matrix3d m) => Multiply(v, m);

        /// <summary>
        /// Implicit operator overload converting Matrix4d to Matrix3d.
        ///
        /// <para>Takes top left corner of Matrix4d values.</para>
        /// </summary>
        /// <param name="matrix">Matrix to convert down.</param>
        public static implicit operator Matrix3d(Matrix4d matrix)
        => new Matrix3d(matrix.R1.X, matrix.R1.Y, matrix.R1.Z,
                        matrix.R2.X, matrix.R2.Y, matrix.R2.Z,
                        matrix.R3.X, matrix.R3.Y, matrix.R3.Z);

        /// <summary>
        /// Operator overload for matrix equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are equal.</returns>
        public static bool operator ==(Matrix3d m1, Matrix3d m2)
        {
            return m1.Equals(m2);
        }

        /// <summary>
        /// Operator overload for matrix not equals.
        /// </summary>
        /// <param name="m1">Left matrix.</param>
        /// <param name="m2">Right matrix.</param>
        /// <returns>True if matrices are not equal.</returns>
        public static bool operator !=(Matrix3d m1, Matrix3d m2)
        {
            return !m1.Equals(m2);
        }

        #endregion
    }
}
