## Magma.GLFW

This library tries to stay as close to the unmanaged function declarations as possible with a few variations and overrides to help make the code managed.
You should be able to follow any Glfw tutorial without issue.

### Example Code:

```c#
using System;
using Magma.GLFW;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!Glfw.Init()) Environment.Exit(-1);

            var window = Glfw.CreateWindow(1280, 720, "Title", IntPtr.Zero, IntPtr.Zero);

            Glfw.SetCursorPosCallback(window, (window, x, y) =>
            {
                Console.WriteLine($"X: {x}, Y: {y}");
            });

            while (!Glfw.WindowShouldClose(window))
            {
                Glfw.PollEvents();

                Glfw.SwapBuffers(window);
            }

            Glfw.DestroyWindow(window);
            Glfw.Terminate();
        }
    }
}

```
