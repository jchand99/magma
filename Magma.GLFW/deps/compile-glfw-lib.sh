#! /bin/bash

build_make() {
    cd glfw/
    mkdir build
    cd build
    cmake -DBUILD_SHARED_LIBS=ON .. && make
}

cleanup() {
    cd ..
    rm -r build/
}

# Change directory to the location of the script.
cd "$(dirname "$(readlink -f "$0")")"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    mkdir -p ../magma-libs/glfw/linux-x64
    build_make
    cp src/libglfw.so src/libglfw.so.* ../../../magma-libs/glfw/linux-x64
    cleanup
elif [[ "$OSTYPE" == "darwin"* ]]; then
    mkdir -p ../magma-libs/glfw/mac-x64
    build_make
    cp src/libglfw.*.dylib ../../../magma-libs/glfw/mac-x64
    cleanup
elif [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]; then
    mkdir -p ../magma-libs/glfw/win-x64
    build_make
    cp src/glfw*.dll ../../../magma-libs/glfw/win-x64
    cleanup
else
    echo "Operating system not supported for this script!"
fi

echo "Successfully built! GLFW binaries can be found in 'magma-libs/glfw/(your os)' :)"
