namespace Magma.GLFW
{
    public static partial class Glfw
    {
        public const int NativeContextApi = 0x00036001;
        public const int EglContextApi = 0x00036002;
        public const int OsmesaContextApi = 0x00036003;
        public const int DontCare = -1;
        public const int True = 1;
        public const int False = 0;
        public const int NoApi = 0;
        public const int OpenGLApi = 0x00030001;
        public const int OpenGLESApi = 0x00030002;
        public const int OpenGLCoreProfile = 0x00032001;
        public const int OpenGLAnyProfile = 0;
        public const int OpenGLCompatProfile = 0x00032002;
        public const int NoResetNotification = 0x00031001;
        public const int NoRobustness = 0;
        public const int LoseContextOnReset = 0x00031002;
        public const int AnyReleaseBehavior = 0;
        public const int ReleaseBehaviorNone = 0x00035002;
        public const int ReleaseBehaviorFlush = 0x00035001;
    }
}
