using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using static System.Runtime.InteropServices.OSPlatform;
using static System.Runtime.InteropServices.RuntimeInformation;

namespace Magma.GLFW
{
    public unsafe static class GlfwUtil
    {
        private static IntPtr _Handle;

        static GlfwUtil()
        {
            _Handle = GetGlfwLibraryNameCandidates()
                .Select(LoadLibrary)
                .FirstOrDefault(handle => handle != IntPtr.Zero);
            // _Handle = LoadLibrary("glfw");
            if (_Handle == IntPtr.Zero)
                throw new NotImplementedException("Glfw native library was not found.");
        }

        public static void* GetStaticProcPointer(string procName)
        {
            IntPtr handle;
            if (IsOSPlatform(Windows))
            {
                handle = Kernel32GetProcAddress(_Handle, procName);
            }
            else if (IsOSPlatform(Linux) || IsOSPlatform(OSX))
            {
                handle = LibDLGetProcAddress(_Handle, procName);
            }
            else
            {
                throw new NotImplementedException();
            }
            return handle == IntPtr.Zero ? null : (void*)handle;
        }

        private static IntPtr LoadLibrary(string fileName)
        {
            IntPtr handle;
            if (IsOSPlatform(Windows))
            {
                handle = Kernel32LoadLibrary(fileName);
            }
            else if (IsOSPlatform(Linux) || IsOSPlatform(OSX))
            {
                handle = LibDLLoadLibrary(fileName, LibDLRtldNow);
            }
            else
            {
                throw new NotImplementedException();
            }
            return handle;
        }

        private static IEnumerable<string> GetGlfwLibraryNameCandidates()
        {
            if (IsOSPlatform(Windows))
            {
                yield return $"{System.AppContext.BaseDirectory}magma-libs/glfw/win-x64/glfw3.dll";
            }
            else if (IsOSPlatform(Linux))
            {
                Console.WriteLine(Directory.GetCurrentDirectory());
                yield return $"{System.AppContext.BaseDirectory}magma-libs/glfw/linux-x64/libglfw.so";
                yield return $"{System.AppContext.BaseDirectory}magma-libs/glfw/linux-x64/libglfw.so.3";
                yield return $"{System.AppContext.BaseDirectory}magma-libs/glfw/linux-x64/libglfw.so.3.3";
            }
            else if (IsOSPlatform(OSX))
            {
                yield return $"{System.AppContext.BaseDirectory}magma-libs/glfw/mac-x64/libglfw.3.dylib";
            }
            throw new NotImplementedException("Ran out of places to look for the Glfw native library");
        }

        [DllImport("kernel32", EntryPoint = "LoadLibrary")]
        private static extern IntPtr Kernel32LoadLibrary(string fileName);

        [DllImport("kernel32", EntryPoint = "GetProcAddress")]
        private static extern IntPtr Kernel32GetProcAddress(IntPtr module, string procName);

        [DllImport("libdl", EntryPoint = "dlopen")]
        private static extern IntPtr LibDLLoadLibrary(string fileName, int flags);

        [DllImport("libdl", EntryPoint = "dlsym")]
        private static extern IntPtr LibDLGetProcAddress(IntPtr handle, string name);

        private const int LibDLRtldNow = 2;
    }
}
