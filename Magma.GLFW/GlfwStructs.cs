using System.Runtime.InteropServices;

namespace Magma.GLFW
{

    /// <summary>
    ///     Glfw Image
    /// </summary>
    public unsafe struct Image
    {
        /// <summary>
        ///     Width of image.
        /// </summary>
        public int Width;

        /// <summary>
        ///     Height of image.
        /// </summary>
        public int Height;

        /// <summary>
        ///     Pixels of image.
        /// </summary>
        public byte[] Pixels;

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public int Width;
            public int Height;
            public byte* PixelsPtr;
        }

        internal void ToNative(out Native i)
        {
            fixed (byte* pixelsPtr = Pixels)
            {
                i.Height = Height;
                i.PixelsPtr = pixelsPtr;
                i.Width = Width;
            }
        }
    }

    /// <summary>
    ///     Glfw GammaRamp
    /// </summary>
    public unsafe struct GammaRamp
    {
        /// <summary>
        ///     Red channel.
        /// </summary>
        public ushort Red;

        /// <summary>
        ///     Green channel.
        /// </summary>
        public ushort Green;

        /// <summary>
        ///     Blue channel.
        /// </summary>
        public ushort Blue;

        /// <summary>
        ///     Size of gammaramp.
        /// </summary>
        public uint Size;

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public ushort* Red;
            public ushort* Green;
            public ushort* Blue;
            public uint Size;
        }

        internal void ToNative(out Native g)
        {
            fixed (ushort* bp = &Blue)
            fixed (ushort* rp = &Red)
            fixed (ushort* gp = &Green)
            {
                g.Blue = bp;
                g.Red = rp;
                g.Green = gp;
                g.Size = Size;
            }
        }

        internal static void FromNative(ref Native native, out GammaRamp ramp)
        {
            ramp.Blue = *native.Blue;
            ramp.Green = *native.Green;
            ramp.Red = *native.Red;
            ramp.Size = native.Size;
        }
    }

    /// <summary>
    ///     State of Glfw Gamepad.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct GamepadState
    {
        /// <summary>
        ///     Array of button state values.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
        public byte[] Buttons;

        /// <summary>
        ///     Array of axis states.
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
        public float[] Axes;

    }
}
