using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Magma.GLFW
{
    public static unsafe partial class Glfw
    {

        /// <summary>
        ///     This function initializes the GLFW library. Before most GLFW functions can be used, GLFW must be initialized, and
        ///     before an application terminates GLFW should be terminated in order to free any resources allocated during or after
        ///     initialization.
        ///     <para>
        ///         If this function fails, it calls <see cref="Terminate" /> before returning. If it succeeds, you should call
        ///         <see cref="Terminate" /> before the application exits
        ///     </para>
        ///     <para>
        ///         Additional calls to this function after successful initialization but before termination will return
        ///         <c>true</c> immediately.
        ///     </para>
        /// </summary>
        /// <returns><c>true</c> if successful, or <c>false</c> if an error occurred.</returns>
        public static bool Init() => Delegates.glfwInit() == 1;

        /// <summary>
        ///     This function destroys all remaining windows and cursors, restores any modified gamma ramps and frees any other
        ///     allocated resources. Once this function is called, you must again call <see cref="Init" /> successfully before you
        ///     will be able to use most GLFW functions.
        ///     If GLFW has been successfully initialized, this function should be called before the application exits. If
        ///     initialization fails, there is no need to call this function, as it is called by <see cref="Init" /> before it
        ///     returns failure.
        /// </summary>
        /// <note type="warning">
        ///     The contexts of any remaining windows must not be current on any other thread when this function
        ///     is called.
        /// </note>
        public static void Terminate() => Delegates.glfwTerminate();

        /// <summary>
        ///     Sets hints for the next call to <see cref="CreateWindow" />. The hints, once set, retain their values
        ///     until changed by a call to <see cref="WindowHint(WindowHint, int)" /> or <see cref="DefaultWindowHints" />, or until the
        ///     library is
        ///     terminated.
        ///     <para>
        ///         This function does not check whether the specified hint values are valid. If you set hints to invalid values
        ///         this will instead be reported by the next call to <see cref="CreateWindow" />.
        ///     </para>
        /// </summary>
        /// <param name="hint">The hint.</param>
        /// <param name="value">The value.</param>
        public static void WindowHint(WindowHint hint, int value) => Delegates.glfwWindowHint(hint, value);

        /// <summary>
        ///     Sets hints for the next call to <see cref="CreateWindow" />. The hints, once set, retain their values until
        ///     changed by a call to this function or <see cref="DefaultWindowHints" />,  or until the library is terminated.
        ///     <para>
        ///         Some hints are platform specific. These may be set on any platform but they will only affect their
        ///         specific platform. Other platforms will ignore them. Setting these hints requires no platform specific
        ///         headers or functions.
        ///     </para>
        /// </summary>
        /// <param name="hint">The window hit to set.</param>
        /// <param name="value">The new value of the window hint.</param>
        public static void WindowHintString(WindowHintString hint, string value)
        {
            fixed (byte* bytePtr = Encoding.UTF8.GetBytes(value))
                Delegates.glfwWindowHintStringDelegate(hint, bytePtr);
        }

        /// <summary>
        ///     This function creates a window and its associated OpenGL or OpenGL ES context. Most of the options controlling how
        ///     the window and its context should be created are specified with <seealso cref="GLFW.WindowHint"/>.
        ///     <para>
        ///         Successful creation does not change which context is current. Before you can use the newly created context,
        ///         you need to make it current. For information about the share parameter, see Context object sharing.
        ///     </para>
        ///     <para>
        ///         The created window, framebuffer and context may differ from what you requested, as not all parameters and hints are hard constraints.
        ///         This includes the size of the window, especially for full screen windows. To query the actual attributes of the created window,
        ///         framebuffer and context, see <seealso cref="GetWindowAttrib"/>, <seealso cref="GetWindowSize"/> and <seealso cref="GetFramebufferSize"/>.
        ///     </para>
        ///     <para>
        ///         To create a full screen window, you need to specify the monitor the window will cover. If no monitor is specified, the window will
        ///         be windowed mode. Unless you have a way for the user to choose a specific monitor, it is recommended that you pick the primary monitor.
        ///         For more information on how to query connected monitors, see <seealso href="https://www.glfw.org/docs/latest/monitor_guide.html#monitor_monitors">Retrieving monitors</seealso>.
        ///     </para>
        ///     <para>
        ///         For full screen windows, the specified size becomes the resolution of the window's desired video mode. As long as a full screen window is not iconified,
        ///         the supported video mode most closely matching the desired video mode is set for the specified monitor. For more information about full screen windows,
        ///         including the creation of so called windowed full screen or borderless full screen windows, see "Windowed full screen" windows.
        ///     </para>
        ///     <para>
        ///         Once you have created the window, you can switch it between windowed and full screen mode with glfwSetWindowMonitor. This will not affect its OpenGL or OpenGL ES context.
        ///     </para>
        ///     <para>
        ///         By default, newly created windows use the placement recommended by the window system. To create the window at a specific position,
        ///         make it initially invisible using the GLFW_VISIBLE window hint, set its position and then show it.
        ///     </para>
        ///     <para>
        ///         As long as at least one full screen window is not iconified, the screensaver is prohibited from starting.
        ///     </para>
        ///     <para>
        ///         Window systems put limits on window sizes. Very large or very small window dimensions may be overridden by the window system on creation. Check the actual size after creation.
        ///     </para>
        ///     <para>
        ///         The swap interval is not set during window creation and the initial value may vary depending on driver settings and defaults.
        ///     </para>
        /// </summary>
        /// <param name="width">Width of window.</param>
        /// <param name="height">Height of window.</param>
        /// <param name="title">Title of window.</param>
        /// <param name="monitor">Monitor pointer.</param>
        /// <param name="share">Share pointer.</param>
        /// <returns>The handle of the created window, or IntPtr.Zero if an error occurred.</returns>
        public static IntPtr CreateWindow(int width, int height, string title, IntPtr monitor, IntPtr share)
        {
            fixed (byte* bytePtr = Encoding.UTF8.GetBytes(title))
                return Delegates.glfwCreateWindow(width, height, bytePtr, monitor, share);
        }

        /// <summary>
        ///     This function makes the OpenGL or OpenGL ES context of the specified window current on the calling thread.
        ///     <para>
        ///         A context can only be made current on a single thread at a time and each thread can have only a single
        ///         current context at a time.
        ///     </para>
        ///     <para>By default, making a context non-current implicitly forces a pipeline flush.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void MakeContextCurrent(IntPtr window) => Delegates.glfwMakeContextCurrent(window);

        /// <summary>
        ///     Gets the value of the close flag of the specified window.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <returns><c>true</c> if close flag is present; otherwise <c>false</c>.</returns>
        public static bool WindowShouldClose(IntPtr window) => Delegates.glfwWindowShouldClose(window);

        /// <summary>
        ///     This function processes only those events that are already in the event queue and then returns immediately.
        ///     Processing events will cause the window and input callbacks associated with those events to be called.
        ///     <para>
        ///         On some platforms, a window move, resize or menu operation will cause event processing to block. This is due
        ///         to how event processing is designed on those platforms. You can use the window refresh callback to redraw the
        ///         contents of your window when necessary during such operations.
        ///     </para>
        ///     <para>
        ///         On some platforms, certain events are sent directly to the application without going through the event queue,
        ///         causing callbacks to be called outside of a call to one of the event processing functions.
        ///     </para>
        /// </summary>
        public static void PollEvents() => Delegates.glfwPollEvents();

        /// <summary>
        ///     This function retrieves the major, minor and revision numbers of the GLFW library.
        ///     <para>
        ///         It is intended for when you are using GLFW as a shared library and want to ensure that you are using the
        ///         minimum required version.
        ///     </para>
        /// </summary>
        /// <param name="major">The major.</param>
        /// <param name="minor">The minor.</param>
        /// <param name="revision">The revision.</param>
        /// <seealso cref="Version" />
        public static void GetVersion(out int major, out int minor, out int revision)
        {
            fixed (int* majorPtr = &major)
            fixed (int* minorPtr = &minor)
            fixed (int* revPtr = &revision)
                Delegates.glfwGetVersion(majorPtr, minorPtr, revPtr);
        }

        /// <summary>
        ///     Gets the compile-time generated version string of the GLFW library binary.
        ///     <para>It describes the version, platform, compiler and any platform-specific compile-time options.</para>
        /// </summary>
        /// <returns>A pointer to the null-terminated UTF-8 encoded version string.</returns>
        /// <seealso cref="Glfw.GetVersion" />
        public static IntPtr GetVersionString() => Delegates.glfwGetVersionString();

        /// <summary>
        ///     This function returns and clears the error code of the last error that occurred on the calling thread,
        ///     and optionally a UTF-8 encoded human-readable description of it. If no error has occurred since
        ///     the last call, it returns GLFW_NO_ERROR (zero) and the description pointer is set to NULL.
        /// </summary>
        /// <param name="description">Description of error returned.</param>
        /// <returns>The last error code for the calling thread, or GLFW_NO_ERROR (zero).</returns>
#nullable enable
        public static int GetError(out string? description)
        {
            byte* descPtr;
            int val = Delegates.glfwGetError(&descPtr);
            byte* walkPtr = descPtr;
            while (*walkPtr != 0) walkPtr++;
            description = Encoding.UTF8.GetString(descPtr, (int)(walkPtr - descPtr));
            return val;
        }
#nullable disable
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ErrorCallback(int error_code, byte* description);
        internal static ErrorCallback _SetErrorCallback;

        /// <summary>
        ///     Sets the error callback, which is called with an error code and a human-readable description each
        ///     time a GLFW error occurs.
        /// </summary>
        /// <param name="errorCallback">The callback function, or <c>null</c> to unbind this callback.</param>
        /// <returns>The previously set callback function, or <c>null</c> if no callback was already set.</returns>
        public static ErrorCallback SetErrorCallback(Action<int, string> errorCallback)
        {
            _SetErrorCallback = (error_code, description) =>
            {
                byte* walk = description;
                while (*walk != 0) walk++;
                errorCallback(error_code, Encoding.UTF8.GetString(description, (int)(walk - description)));
            };
            return Delegates.glfwSetErrorCallback(_SetErrorCallback);
        }

        /// <summary>
        ///     This function sets hints for the next initialization of GLFW.
        ///     <para>
        ///         The values you set hints to are never reset by GLFW, but they only take effect during initialization.
        ///         Once GLFW has been initialized, any values you set will be ignored until the library is terminated and
        ///         initialized again.>.
        ///     </para>
        /// </summary>
        /// <param name="hint">
        ///     The hint, valid values are <see cref="InitHint.JoystickHatButtons" />,
        ///     <see cref="InitHint.CocoaMenubar" />, and <see cref="InitHint.CocoaChdirResources" />.
        /// </param>
        /// <param name="value">The value of the hint.</param>
        public static void InitHint(InitHint hint, bool value) => Delegates.glfwInitHintDelegate(hint, value);

        /// <summary>
        ///     This function resets all window hints to their default values.
        /// </summary>
        public static void DefaultWindowHints() => Delegates.glfwDefaultWindowHints();

        /// <summary>
        ///     This function destroys the specified window and its context. On calling this function, no further callbacks will be
        ///     called for that window.
        ///     <para>If the context of the specified window is current on the main thread, it is detached before being destroyed.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void DestroyWindow(IntPtr window) => Delegates.glfwDestroyWindow(window);

        /// <summary>
        ///     Sets the value of the close flag of the specified window.
        ///     <para>This can be used to override the user's attempt to close the window, or to signal that it should be closed.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="close"><c>true</c> to set close flag, or <c>false</c> to cancel flag.</param>
        public static void SetWindowShouldClose(IntPtr window, bool close) => Delegates.glfwSetWindowShouldClose(window, close);

        /// <summary>
        ///     Sets the window title, encoded as UTF-8, of the specified window.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="title">The title to set.</param>
        public static void SetWindowTitle(IntPtr window, string title)
        {
            fixed (byte* bytePtr = Encoding.UTF8.GetBytes(title))
                Delegates.glfwSetWindowTitle(window, bytePtr);
        }

        /// <summary>
        ///     Sets the icon of the specified window. If passed an array of candidate images, those of or closest to
        ///     the sizes desired by the system are selected. If no images are specified, the window reverts to its default icon.
        ///     <para>
        ///         The desired image sizes varies depending on platform and system settings. The selected images will be
        ///         rescaled as needed. Good sizes include 16x16, 32x32 and 48x48.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="count">The number of images in <paramref name="images" />.</param>
        /// <param name="images">An array of icon images.</param>
        public static void SetWindowIcon(IntPtr window, int count, Image[] images)
        {
            var arr = stackalloc Image.Native[count];
            for (int i = 0; i < count; i++)
            {
                images[i].ToNative(out var native);
                arr[i] = native;
            }
            Delegates.glfwSetWindowIcon(window, count, arr);
        }

        /// <summary>
        ///     This function retrieves the position, in screen coordinates, of the upper-left corner of the client area of the
        ///     specified window.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="x">The x-coordinate of the upper-left corner of the client area.</param>
        /// <param name="y">The y-coordinate of the upper-left corner of the client area.</param>
        public static void GetWindowPos(IntPtr window, out int x, out int y)
        {
            fixed (int* xposPtr = &x)
            fixed (int* yposPtr = &y)
                Delegates.glfwGetWindowPos(window, xposPtr, yposPtr);
        }

        /// <summary>
        ///     Sets the position, in screen coordinates, of the upper-left corner of the client area of the
        ///     specified windowed mode window.
        ///     <para>If the window is a full screen window, this function does nothing.</para>
        /// </summary>
        /// <note type="important">
        ///     Do not use this function to move an already visible window unless you have very good reasons for
        ///     doing so, as it will confuse and annoy the user.
        /// </note>
        /// <param name="window">A window instance.</param>
        /// <param name="x">The x-coordinate of the upper-left corner of the client area.</param>
        /// <param name="y">The y-coordinate of the upper-left corner of the client area.</param>
        /// <remarks>
        ///     The window manager may put limits on what positions are allowed. GLFW cannot and should not override these
        ///     limits.
        /// </remarks>
        public static void SetWindowPos(IntPtr window, int x, int y) => Delegates.glfwSetWindowPos(window, x, y);

        /// <summary>
        ///     This function retrieves the size, in screen coordinates, of the client area of the specified window.
        ///     <para>
        ///         If you wish to retrieve the size of the framebuffer of the window in pixels, use
        ///         <see cref="GetFramebufferSize" />.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="width">The width, in screen coordinates.</param>
        /// <param name="height">The height, in screen coordinates.</param>
        public static void GetWindowSize(IntPtr window, out int width, out int height)
        {
            fixed (int* widthPtr = &width)
            fixed (int* heightPtr = &height)
                Delegates.glfwGetWindowSize(window, widthPtr, heightPtr);
        }

        /// <summary>
        ///     Sets the size limits of the client area of the specified window. If the window is full screen, the
        ///     size limits only take effect once it is made windowed. If the window is not resizable, this function does nothing.
        ///     <para>The size limits are applied immediately to a windowed mode window and may cause it to be resized.</para>
        ///     <para>
        ///         The maximum dimensions must be greater than or equal to the minimum dimensions and all must be greater than
        ///         or equal to zero.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="minWidth">The minimum width of the client area.</param>
        /// <param name="minHeight">The minimum height of the client area.</param>
        /// <param name="maxWidth">The maximum width of the client area.</param>
        /// <param name="maxHeight">The maximum height of the client area.</param>
        public static void SetWindowSizeLimits(IntPtr window, int minWidth, int minHeight, int maxWidth, int maxHeight) => Delegates.glfwSetWindowSizeLimits(window, minWidth, minHeight, maxWidth, maxHeight);

        /// <summary>
        ///     Sets the required aspect ratio of the client area of the specified window. If the window is full
        ///     screen, the aspect ratio only takes effect once it is made windowed. If the window is not resizable, this function
        ///     does nothing.
        ///     <para>
        ///         The aspect ratio is specified as a numerator and a denominator and both values must be greater than zero. For
        ///         example, the common 16:9 aspect ratio is specified as 16 and 9, respectively.
        ///     </para>
        ///     <para>
        ///         If the numerator and denominator is set to <see cref="Glfw.DontCare"/> then the aspect ratio limit is
        ///         disabled.
        ///     </para>
        ///     <para>The aspect ratio is applied immediately to a windowed mode window and may cause it to be resized.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="numer">The numerator of the desired aspect ratio.</param>
        /// <param name="denom">The denominator of the desired aspect ratio.</param>
        public static void SetWindowAspectRatio(IntPtr window, int numer, int denom) => Delegates.glfwSetWindowAspectRatio(window, numer, denom);

        /// <summary>
        ///     Sets the size, in screen coordinates, of the client area of the specified window.
        ///     <para>
        ///         For full screen windows, this function updates the resolution of its desired video mode and switches to the
        ///         video mode closest to it, without affecting the window's context. As the context is unaffected, the bit depths
        ///         of the framebuffer remain unchanged.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="width">The desired width, in screen coordinates, of the window client area.</param>
        /// <param name="height">The desired height, in screen coordinates, of the window client area.</param>
        /// <remarks>The window manager may put limits on what sizes are allowed. GLFW cannot and should not override these limits.</remarks>
        public static void SetWindowSize(IntPtr window, int width, int height) => Delegates.glfwSetWindowSize(window, width, height);

        /// <summary>
        ///     This function retrieves the size, in pixels, of the framebuffer of the specified window.
        ///     <para>If you wish to retrieve the size of the window in screen coordinates, use <see cref="GetWindowSize" />.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="width">The width, in pixels, of the framebuffer.</param>
        /// <param name="height">The height, in pixels, of the framebuffer.</param>
        public static void GetFramebufferSize(IntPtr window, out int width, out int height)
        {
            fixed (int* widthPtr = &width)
            fixed (int* heightPtr = &height)
                Delegates.glfwGetFramebufferSize(window, widthPtr, heightPtr);
        }

        /// <summary>
        ///     This function retrieves the size, in screen coordinates, of each edge of the frame of the specified window.
        ///     <para>
        ///         This size includes the title bar, if the window has one. The size of the frame may vary depending on the
        ///         window-related hints used to create it.
        ///     </para>
        ///     <para>
        ///         Because this function retrieves the size of each window frame edge and not the offset along a particular
        ///         coordinate axis, the retrieved values will always be zero or positive.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="left">The size, in screen coordinates, of the left edge of the window frame</param>
        /// <param name="top">The size, in screen coordinates, of the top edge of the window frame</param>
        /// <param name="right">The size, in screen coordinates, of the right edge of the window frame.</param>
        /// <param name="bottom">The size, in screen coordinates, of the bottom edge of the window frame</param>
        public static void GetWindowFrameSize(IntPtr window, out int left, out int top, out int right, out int bottom)
        {
            fixed (int* leftPtr = &left)
            fixed (int* topPtr = &top)
            fixed (int* rightPtr = &right)
            fixed (int* bottomPtr = &bottom)
                Delegates.glfwGetWindowFrameSize(window, leftPtr, topPtr, rightPtr, bottomPtr);
        }

        /// <summary>
        ///     Retrieves the content scale for the specified window. The content scale is the ratio between the current DPI and
        ///     the platform's default DPI. This is especially important for text and any UI elements. If the pixel dimensions of
        ///     your UI scaled by this look appropriate on your machine then it should appear at a reasonable size on other
        ///     machines regardless of their DPI and scaling settings. This relies on the system DPI and scaling settings being
        ///     somewhat correct.
        ///     <para>
        ///         On systems where each monitors can have its own content scale, the window content scale will depend on which
        ///         monitor the system considers the window to be on.
        ///     </para>
        /// </summary>
        /// <param name="window">The window to query.</param>
        /// <param name="xScale">The content scale on the x-axis.</param>
        /// <param name="yScale">The content scale on the y-axis.</param>
        public static void GetWindowContentScale(IntPtr window, out float xScale, out float yScale)
        {
            fixed (float* xscalePtr = &xScale)
            fixed (float* yscalePtr = &yScale)
                Delegates.glfwGetWindowContentScale(window, xscalePtr, yscalePtr);
        }

        /// <summary>
        ///     Returns the opacity of the window, including any decorations.
        /// </summary>
        /// <param name="window">The window to query.</param>
        /// <returns>The opacity value of the specified window, a value between <c>0.0</c> and <c>1.0</c> inclusive.</returns>
        public static float GetWindowOpacity(IntPtr window) => Delegates.glfwGetWindowOpacity(window);

        /// <summary>
        ///     Sets the opacity of the window, including any decorations.
        ///     <para>
        ///         The opacity (or alpha) value is a positive finite number between zero and one, where zero is fully
        ///         transparent and one is fully opaque.
        ///     </para>
        /// </summary>
        /// <param name="window">The window to set the opacity for.</param>
        /// <param name="opacity">The desired opacity of the specified window.</param>
        public static void SetWindowOpacity(IntPtr window, float opacity) => Delegates.glfwSetWindowOpacity(window, opacity);

        /// <summary>
        ///     This function iconifies (minimizes) the specified window if it was previously restored.
        ///     <para>If the window is already iconified, this function does nothing.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void IconifyWindow(IntPtr window) => Delegates.glfwIconifyWindow(window);

        /// <summary>
        ///     This function restores the specified window if it was previously iconified (minimized) or maximized.
        ///     <para>If the window is already restored, this function does nothing.</para>
        ///     <para>
        ///         If the specified window is a full screen window, the resolution chosen for the window is restored on the
        ///         selected monitor.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void RestoreWindow(IntPtr window) => Delegates.glfwRestoreWindow(window);

        /// <summary>
        ///     This function maximizes the specified window if it was previously not maximized. If the window is already
        ///     maximized, this function does nothing.
        ///     <para>If the specified window is a full screen window, this function does nothing.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void MaximizeWindow(IntPtr window) => Delegates.glfwMaximizeWindow(window);

        /// <summary>
        ///     This function makes the specified window visible if it was previously hidden. If the window is already visible or
        ///     is in full screen mode, this function does nothing.
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void ShowWindow(IntPtr window) => Delegates.glfwShowWindow(window);

        /// <summary>
        ///     This function hides the specified window if it was previously visible. If the window is already hidden or is in
        ///     full screen mode, this function does nothing.
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void HideWindow(IntPtr window) => Delegates.glfwHideWindow(window);

        /// <summary>
        ///     This function brings the specified window to front and sets input focus. The window should already be visible and
        ///     not iconified.
        /// </summary>
        /// <param name="window">The window.</param>
        public static void FocusWindow(IntPtr window) => Delegates.glfwFocusWindow(window);

        /// <summary>
        ///     Requests user attention to the specified <paramref name="window" />. On platforms where this is not supported,
        ///     attention is
        ///     requested to the application as a whole.
        ///     <para>
        ///         Once the user has given attention, usually by focusing the window or application, the system will end the
        ///         request automatically.
        ///     </para>
        /// </summary>
        /// <param name="window">The window to request user attention to.</param>
        public static void RequestWindowAttention(IntPtr window) => Delegates.glfwRequestWindowAttention(window);

        /// <summary>
        ///     Gets the handle of the monitor that the specified window is in full screen on.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <returns>The monitor, or <c>IntPtr.Zero</c> if the window is in windowed mode or an error occurred.</returns>
        public static IntPtr GetWindowMonitor(IntPtr window) => Delegates.glfwGetWindowMonitor(window);

        /// <summary>
        ///     Sets the monitor that the window uses for full screen mode or, if the monitor is
        ///     <c>IntPtr.Zero</c>, makes it windowed mode.
        ///     <para>
        ///         When setting a monitor, this function updates the width, height and refresh rate of the desired video mode
        ///         and switches to the video mode closest to it. The window position is ignored when setting a monitor.
        ///     </para>
        ///     <para>
        ///         When the monitor is <c>IntPtr.Zero</c>, the position, width and height are used to place the window
        ///         client area. The refresh rate is ignored when no monitor is specified.
        ///     </para>
        ///     <para>
        ///         If you only wish to update the resolution of a full screen window or the size of a windowed mode window, use
        ///         <see cref="SetWindowSize" />.
        ///     </para>
        ///     <para>
        ///         When a window transitions from full screen to windowed mode, this function restores any previous window
        ///         settings such as whether it is decorated, floating, resizable, has size or aspect ratio limits, etc..
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="monitor">The desired monitor, or <c>IntPtr.Zero</c> to set windowed mode.</param>
        /// <param name="x">The desired x-coordinate of the upper-left corner of the client area.</param>
        /// <param name="y">The desired y-coordinate of the upper-left corner of the client area.</param>
        /// <param name="width">The desired width, in screen coordinates, of the client area or video mode.</param>
        /// <param name="height">The desired height, in screen coordinates, of the client area or video mode.</param>
        /// <param name="refreshRate">The desired refresh rate, in Hz, of the video mode.</param>
        public static void SetWindowMonitor(IntPtr window, IntPtr monitor, int x, int y, int width, int height, int refreshRate) => Delegates.glfwSetWindowMonitor(window, monitor, x, y, width, height, refreshRate);

        /// <summary>
        ///     Gets the value of the specified window attribute.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="attribute">The attribute to retrieve.</param>
        /// <returns>The value of the <paramref name="attribute" />.</returns>
        public static int GetWindowAttrib(IntPtr window, WindowAttribute attribute) => Delegates.glfwGetWindowAttrib(window, attribute);

        /// <summary>
        ///     Sets the value of an attribute of the specified window.
        /// </summary>
        /// <param name="window">
        ///     The window to set the attribute for
        ///     <para>Valid attributes include:</para>
        ///     <para>
        ///         <see cref="WindowAttribute.Decorated" />
        ///     </para>
        ///     <para>
        ///         <see cref="WindowAttribute.Resizable" />
        ///     </para>
        ///     <para>
        ///         <see cref="WindowAttribute.Floating" />
        ///     </para>
        ///     <para>
        ///         <see cref="WindowAttribute.AutoIconify" />
        ///     </para>
        ///     <para>
        ///         <see cref="WindowAttribute.Focused" />
        ///     </para>
        /// </param>
        /// <param name="attrib">A supported window attribute.</param>
        /// <param name="value">The value to set.</param>
        public static void SetWindowAttrib(IntPtr window, WindowAttribute attrib, int value) => Delegates.glfwSetWindowAttrib(window, attrib, value);

        /// <summary>
        ///     Sets the user-defined pointer of the specified window. The current value is retained until the window
        ///     is destroyed. The initial value is <see cref="IntPtr.Zero" />.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="pointer">The user pointer value.</param>
        public static void SetWindowUserPointer(IntPtr window, long pointer) => Delegates.glfwSetWindowUserPointer(window, pointer);

        /// <summary>
        ///     Gets the current value of the user-defined pointer of the specified window. The initial value is
        ///     <see cref="IntPtr.Zero" />.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <returns>The user-defined pointer.</returns>
        public static long GetWindowUserPointer(IntPtr window) => Delegates.glfwGetWindowUserPointer(window);


        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void PosCallback(IntPtr window, int xpos, int ypos);
        internal static PosCallback _SetWindowPosCallback;
        /// <summary>
        ///     Sets the position callback of the specified window, which is called when the window is moved.
        ///     <para>The callback is provided with the screen position of the upper-left corner of the client area of the window.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The position callback to be invoked on position changes.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static PosCallback SetWindowPosCallback(IntPtr window, Action<IntPtr, int, int> callback)
        {
            _SetWindowPosCallback = (window, xpos, ypos) => callback(window, xpos, ypos);
            return Delegates.glfwSetWindowPosCallback(window, _SetWindowPosCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void SizeCallback(IntPtr window, int width, int height);
        internal static SizeCallback _SetWindowSizeCallback;
        /// <summary>
        ///     Sets the size callback of the specified window, which is called when the window is resized.
        ///     <para>The callback is provided with the size, in screen coordinates, of the client area of the window.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The size callback to be invoked on size changes.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static SizeCallback SetWindowSizeCallback(IntPtr window, Action<IntPtr, int, int> callback)
        {
            _SetWindowSizeCallback = (window, width, height) => callback(window, width, height);
            return Delegates.glfwSetWindowSizeCallback(window, _SetWindowSizeCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void CloseCallback(IntPtr window);
        internal static CloseCallback _SetWindowCloseCallback;
        /// <summary>
        ///     Sets the close callback of the specified window, which is called when the user attempts to close the
        ///     window, for example by clicking the close widget in the title bar.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static CloseCallback SetWindowCloseCallback(IntPtr window, Action<IntPtr> callback)
        {
            _SetWindowCloseCallback = (window) => callback(window);
            return Delegates.glfwSetWindowCloseCallback(window, _SetWindowCloseCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void RefreshCallback(IntPtr window);
        internal static RefreshCallback _SetWindowRefreshCallback;
        /// <summary>
        ///     Sets the refresh callback of the specified window, which is called when the client area of the window
        ///     needs to be redrawn, for example if the window has been exposed after having been covered by another window.
        ///     <para>
        ///         On compositing window systems such as Aero, Compiz or Aqua, where the window contents are saved off-screen,
        ///         this callback may be called only very infrequently or never at all.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static RefreshCallback SetWindowRefreshCallback(IntPtr window, Action<IntPtr> callback)
        {
            _SetWindowRefreshCallback = (window) => callback(window);
            return Delegates.glfwSetWindowRefreshCallback(window, _SetWindowRefreshCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void FocusCallback(IntPtr window, bool focused);
        internal static FocusCallback _SetWindowFocusCallback;
        /// <summary>
        ///     Sets the focus callback of the specified window, which is called when the window gains or loses input
        ///     focus.
        ///     <para>
        ///         After the focus callback is called for a window that lost input focus, synthetic key and mouse button release
        ///         events will be generated for all such that had been pressed.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        public static FocusCallback SetWindowFocusCallback(IntPtr window, Action<IntPtr, bool> callback)
        {
            _SetWindowFocusCallback = (window, focused) => callback(window, focused);
            return Delegates.glfwSetWindowFocusCallback(window, _SetWindowFocusCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void IconifyCallback(IntPtr window, bool iconified);
        internal static IconifyCallback _SetWindowIconifyCallback;
        /// <summary>
        ///     Sets the iconification callback of the specified window, which is called when the window is iconified
        ///     or restored.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static IconifyCallback SetWindowIconifyCallback(IntPtr window, Action<IntPtr, bool> callback)
        {
            _SetWindowIconifyCallback = (window, iconified) => callback(window, iconified);
            return Delegates.glfwSetWindowIconifyCallback(window, _SetWindowIconifyCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void MaximizeCallback(IntPtr window, bool maximized);
        internal static MaximizeCallback _SetWindowMaximizeCallback;
        /// <summary>
        ///     Sets the maximization callback of the specified <paramref name="window," /> which is called when the window is
        ///     maximized or restored.
        /// </summary>
        /// <param name="window">The window whose callback to set.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static MaximizeCallback SetWindowMaximizeCallback(IntPtr window, Action<IntPtr, bool> callback)
        {
            _SetWindowMaximizeCallback = (window, maximized) => callback(window, maximized);
            return Delegates.glfwSetWindowMaximizeCallback(window, _SetWindowMaximizeCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void FramebufferSizeCallback(IntPtr window, int width, int height);
        internal static FramebufferSizeCallback _SetFramebufferSizeCallback;
        /// <summary>
        ///     Sets the framebuffer resize callback of the specified window, which is called when the framebuffer of
        ///     the specified window is resized.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static FramebufferSizeCallback SetFramebufferSizeCallback(IntPtr window, Action<IntPtr, int, int> callback)
        {
            _SetFramebufferSizeCallback = (window, width, height) => callback(window, width, height);
            return Delegates.glfwSetFramebufferSizeCallback(window, _SetFramebufferSizeCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ContentScaleCallback(IntPtr window, float xscale, float yscale);
        internal static ContentScaleCallback _SetWindowContentScaleCallback;
        /// <summary>
        ///     Sets the window content scale callback of the specified window, which is called when the content scale of the
        ///     specified window changes.
        /// </summary>
        /// <param name="window">The window whose callback to set.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static ContentScaleCallback SetWindowContentScaleCallback(IntPtr window, Action<IntPtr, float, float> callback)
        {
            _SetWindowContentScaleCallback = (window, xscale, yscale) => callback(window, xscale, yscale);
            return Delegates.glfwSetWindowContentScaleCallback(window, _SetWindowContentScaleCallback);
        }

        /// <summary>
        ///     This function puts the calling thread to sleep until at least one event is available in the event queue. Once one
        ///     or more events are available, it behaves exactly like glfwPollEvents, i.e. the events in the queue are processed
        ///     and the function then returns immediately. Processing events will cause the window and input callbacks associated
        ///     with those events to be called.
        ///     <para>
        ///         Since not all events are associated with callbacks, this function may return without a callback having been
        ///         called even if you are monitoring all callbacks.
        ///     </para>
        ///     <para>
        ///         On some platforms, a window move, resize or menu operation will cause event processing to block. This is due
        ///         to how event processing is designed on those platforms. You can use the window refresh callback to redraw the
        ///         contents of your window when necessary during such operations.
        ///     </para>
        /// </summary>
        public static void WaitEvents() => Delegates.glfwWaitEvents();

        /// <summary>
        ///     This function puts the calling thread to sleep until at least one event is available in the event queue, or until
        ///     the specified timeout is reached. If one or more events are available, it behaves exactly like
        ///     <see cref="PollEvents" />, i.e. the events in the queue are processed and the function then returns immediately.
        ///     Processing events will cause the window and input callbacks associated with those events to be called.
        ///     <para>The timeout value must be a positive finite number.</para>
        ///     <para>
        ///         Since not all events are associated with callbacks, this function may return without a callback having been
        ///         called even if you are monitoring all callbacks.
        ///     </para>
        ///     <para>
        ///         On some platforms, a window move, resize or menu operation will cause event processing to block. This is due
        ///         to how event processing is designed on those platforms. You can use the window refresh callback to redraw the
        ///         contents of your window when necessary during such operations.
        ///     </para>
        /// </summary>
        /// <param name="timeout">The maximum amount of time, in seconds, to wait.</param>
        public static void WaitEventsTimeout(double timeout) => Delegates.glfwWaitEventsTimeout(timeout);

        /// <summary>
        ///     This function posts an empty event from the current thread to the event queue, causing <see cref="WaitEvents" /> or
        ///     <see cref="WaitEventsTimeout " /> to return.
        /// </summary>
        public static void PostEmptyEvent() => Delegates.glfwPostEmptyEvent();

        /// <summary>
        ///     This function swaps the front and back buffers of the specified window when rendering with OpenGL or OpenGL ES.
        ///     <para>
        ///         If the swap interval is greater than zero, the GPU driver waits the specified number of screen updates before
        ///         swapping the buffers.
        ///     </para>
        ///     <para>This function does not apply to Vulkan.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        public static void SwapBuffers(IntPtr window) => Delegates.glfwSwapBuffers(window);

        /// <summary>
        ///     This function returns the window whose OpenGL or OpenGL ES context is current on the calling thread.
        /// </summary>
        /// <returns>The window whose context is current, or <c>IntPtr.Zero</c> if no window's context is current.</returns>
        public static IntPtr GetCurrentContext() => Delegates.glfwGetCurrentContext();

        /// <summary>
        ///     Sets the swap interval for the current OpenGL or OpenGL ES context, i.e. the number of screen updates
        ///     to wait from the time <see cref="SwapBuffers" /> was called before swapping the buffers and returning.
        ///     <para>This is sometimes called vertical synchronization, vertical retrace synchronization or just vsync.</para>
        ///     <para>
        ///         A context must be current on the calling thread. Calling this function without a current context will cause
        ///         an exception.
        ///     </para>
        ///     <para>
        ///         This function does not apply to Vulkan. If you are rendering with Vulkan, see the present mode of your
        ///         swapchain instead.
        ///     </para>
        /// </summary>
        /// <param name="interval">
        ///     The minimum number of screen updates to wait for until the buffers are swapped by
        ///     <see cref="SwapBuffers" />.
        /// </param>
        public static void SwapInterval(int interval) => Delegates.glfwSwapInterval(interval);

        /// <summary>
        ///     Gets whether the specified API extension is supported by the current OpenGL or OpenGL ES context.
        ///     <para>It searches both for client API extension and context creation API extensions.</para>
        /// </summary>
        /// <param name="extension">The extension name as an array of ASCII encoded bytes.</param>
        /// <returns><c>true</c> if the extension is supported; otherwise <c>false</c>.</returns>
        public static bool ExtensionSupported(string extension)
        {
            fixed (byte* bytePtr = Encoding.UTF8.GetBytes(extension))
                return Delegates.glfwExtensionSupported(bytePtr) == 1;
        }

        /// <summary>
        ///     This function returns the address of the specified OpenGL or OpenGL ES core or extension function, if it is supported by the current context.
        ///     <para>
        ///         A context must be current on the calling thread. Calling this function without a current context will cause a GLFW_NO_CURRENT_CONTEXT error.
        ///     </para>
        ///     <para>
        ///         This function does not apply to Vulkan. If you are rendering with Vulkan, see glfwGetInstanceProcAddress, vkGetInstanceProcAddr and vkGetDeviceProcAddr instead.
        ///     </para>
        /// </summary>
        /// <param name="procname">The ASCII encoded name of the function.</param>
        /// <returns>The address of the function, or <c>IntPtr.Zero</c> if an error occurred.</returns>
        public static IntPtr GetProcAddress(string procname)
        {
            fixed (byte* bytePtr = Encoding.ASCII.GetBytes(procname))
                return Delegates.glfwGetProcAddress(bytePtr);
        }

        /// <summary>
        ///     This function returns whether the Vulkan loader and any minimally functional ICD have been found.
        ///     <para>
        ///         The availability of a Vulkan loader and even an ICD does not by itself guarantee
        ///         that surface creation or even instance creation is possible. For example, on Fermi
        ///         systems Nvidia will install an ICD that provides no actual Vulkan support.
        ///         Call glfwGetRequiredInstanceExtensions to check whether the extensions necessary
        ///         for Vulkan surface creation are available and glfwGetPhysicalDevicePresentationSupport to check
        ///     </para>
        /// </summary>
        /// <returns><c>true</c> if Vulkan is minimally available, or <c>false</c> otherwise.</returns>
        public static bool VulkanSupported() => Delegates.glfwVulkanSupported() == 1;

        /// <summary>
        ///     This function returns an array of names of Vulkan instance extensions required by GLFW
        ///     for creating Vulkan surfaces for GLFW windows. If successful, the list will always contain
        ///     VK_KHR_surface, so if you don't require any additional extensions you can pass this list directly
        ///     to the VkInstanceCreateInfo struct.
        ///     <para>
        ///         If Vulkan is not available on the machine, this function returns NULL and generates a
        ///         GLFW_API_UNAVAILABLE error. Call <seealso cref="VulkanSupported"/> to check whether Vulkan is at
        ///         least minimally available.
        ///     </para>
        ///     <para>
        ///         If Vulkan is available but no set of extensions allowing window surface creation was found,
        ///         this function returns NULL. You may still use Vulkan for off-screen rendering and compute work.
        ///     </para>
        /// </summary>
        /// <param name="count">Where to store the number of extensions in the returned array. This is set to zero if an error occurred.</param>
        /// <returns>An array of ASCII encoded extension names, or an empty array if an error occurred.</returns>
        public static string[] GetRequiredInstanceExtensions(out int count)
        {
            fixed (int* countPtr = &count)
            {
                byte** arrayPtr = Delegates.glfwGetRequiredInstanceExtensions(countPtr);
                if (count == 0) return new string[0];

                var array = new string[count];
                for (int i = 0; i < count; i++)
                {
                    byte* walkPtr = arrayPtr[i];
                    while (*walkPtr != 0) walkPtr++;
                    array[i] = Encoding.ASCII.GetString(arrayPtr[i], (int)(walkPtr - arrayPtr[i]));
                }
                return array;
            }
        }

        /// <summary>
        ///     Gets the address of the specified OpenGL or OpenGL ES core or extension function, if it is
        ///     supported by the current context.
        ///     This function does not apply to Vulkan. If you are rendering with Vulkan, use
        ///     vkGetInstanceProcAddress instead.
        /// </summary>
        /// <param name="instance">Vulkan instance.</param>
        /// <param name="procName">Name of the function.</param>
        /// <returns>The address of the function, or <see cref="IntPtr.Zero" /> if an error occurred.</returns>
        public static IntPtr GetInstanceProcAddress(IntPtr instance, string procName)
        {
            fixed (byte* bytePtr = Encoding.UTF8.GetBytes(procName))
                return Delegates.glfwGetInstanceProcAddress(instance, bytePtr);
        }

        /// <summary>
        ///     This function returns whether the specified queue family of the specified physical device supports presentation to the platform GLFW was built for.
        ///     <para>
        ///         If Vulkan or the required window surface creation instance extensions are not
        ///         available on the machine, or if the specified instance was not created with the
        ///         required extensions, this function returns GLFW_FALSE and generates a GLFW_API_UNAVAILABLE
        ///         error. Call <seealso cref="VulkanSupported"/> to check whether Vulkan is at least minimally available and
        ///         <seealso cref="GetRequiredInstanceExtensions"/> to check what instance extensions are required.
        ///     </para>
        /// </summary>
        /// <param name="instance">The instance that the physical device belongs to.</param>
        /// <param name="device">The physical device that the queue family belongs to.</param>
        /// <param name="queuefamily">The index of the queue family to query.</param>
        /// <returns><c>true</c> if the queue family supports presentation, or <c>false</c> otherwise.</returns>
        public static int GetPhysicalDevicePresentationSupport(IntPtr instance, IntPtr device, int queuefamily) => Delegates.glfwGetPhysicalDevicePresentationSupport(instance, device, queuefamily);

        /// <summary>
        ///     This function creates a Vulkan surface for the specified window.
        ///     <para>
        ///         If the Vulkan loader or at least one minimally functional ICD were not found,
        ///         this function returns VK_ERROR_INITIALIZATION_FAILED and generates a
        ///         GLFW_API_UNAVAILABLE error. Call <seealso cref="VulkanSupported"/> to check whether Vulkan
        ///         is at least minimally available.
        ///     </para>
        ///     <para>
        ///         If the required window surface creation instance extensions are not available or if the
        ///         specified instance was not created with these extensions enabled, this function returns
        ///         VK_ERROR_EXTENSION_NOT_PRESENT and generates a GLFW_API_UNAVAILABLE error.
        ///         Call <seealso cref="GetRequiredInstanceExtensions"/> to check what instance extensions are required.
        ///     </para>
        ///     <para>
        ///         The window surface cannot be shared with another API so the window must have been
        ///         created with the client api hint set to GLFW_NO_API otherwise it generates a
        ///         GLFW_INVALID_VALUE error and returns VK_ERROR_NATIVE_WINDOW_IN_USE_KHR.
        ///     </para>
        ///     <para>
        ///         The window surface must be destroyed before the specified Vulkan instance.
        ///         It is the responsibility of the caller to destroy the window surface. GLFW does
        ///         not destroy it for you. Call vkDestroySurfaceKHR to destroy the surface.
        ///     </para>
        /// </summary>
        /// <param name="instance">The Vulkan instance to create the surface in.</param>
        /// <param name="window">The window to create the surface for.</param>
        /// <param name="allocator">The allocator to use, or NULL to use the default allocator.</param>
        /// <param name="surface">Where to store the handle of the surface. This is set to VK_NULL_HANDLE if an error occurred.</param>
        /// <returns></returns>
        public static int CreateWindowSurface(IntPtr instance, IntPtr window, IntPtr allocator, out IntPtr surface)
        {
            fixed (IntPtr* surfacePtr = &surface)
                return Delegates.glfwCreateWindowSurface(instance, window, allocator, surfacePtr);
        }

        /// <summary>
        ///     Gets an array of handles for all currently connected monitors.
        ///     <para>The primary monitor is always first in the array.</para>
        /// </summary>
        /// <value>
        ///     The monitors.
        /// </value>
#nullable enable
        public static IntPtr[]? GetMonitors(out int count)
        {
            fixed (int* countPtr = &count)
            {
                IntPtr* ptr = Delegates.glfwGetMonitors(countPtr);
                if (count == 0) return null;

                var array = new IntPtr[count];
                for (int i = 0; i < count; i++)
                {
                    array[i] = ptr[i];
                }
                return array;
            }
        }
#nullable disable

        /// <summary>
        ///     Gets the primary monitor. This is usually the monitor where elements like the task bar or global menu bar are
        ///     located.
        /// </summary>
        /// <value>
        ///     The primary monitor, or <c>IntPtr.Zero</c> if no monitors were found or if an error occurred.
        /// </value>
        public static IntPtr GetPrimaryMonitor() => Delegates.glfwGetPrimaryMonitor();

        /// <summary>
        ///     Gets the position, in screen coordinates, of the upper-left corner of the specified monitor.
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <param name="x">The monitor x-coordinate.</param>
        /// <param name="y">The monitor y-coordinate.</param>
        public static void GetMonitorPos(IntPtr monitor, out int x, out int y)
        {
            fixed (int* xposPtr = &x)
            fixed (int* yposPtr = &y)
                Delegates.glfwGetMonitorPos(monitor, xposPtr, yposPtr);
        }

        /// <summary>
        ///     Returns the position, in screen coordinates, of the upper-left corner of the work area of the specified
        ///     monitor along with the work area size in screen coordinates.
        ///     <para>
        ///         The work area is defined as the area of the monitor not occluded by the operating system task bar
        ///         where present. If no task bar exists then the work area is the monitor resolution in screen
        ///         coordinates.
        ///     </para>
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <param name="x">The x-coordinate.</param>
        /// <param name="y">The y-coordinate.</param>
        /// <param name="width">The monitor width.</param>
        /// <param name="height">The monitor height.</param>
        public static void GetMonitorWorkarea(IntPtr monitor, out int x, out int y, out int width, out int height)
        {
            fixed (int* xposPtr = &x)
            fixed (int* yposPtr = &y)
            fixed (int* widthPtr = &width)
            fixed (int* heightPtr = &height)
                Delegates.glfwGetMonitorWorkarea(monitor, xposPtr, yposPtr, widthPtr, heightPtr);
        }

        /// <summary>
        ///     Gets the size, in millimeters, of the display area of the specified monitor.
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <param name="width">The width, in millimeters, of the monitor's display area.</param>
        /// <param name="height">The height, in millimeters, of the monitor's display area.</param>
        public static void GetMonitorPhysicalSize(IntPtr monitor, out int width, out int height)
        {
            fixed (int* widthMMPtr = &width)
            fixed (int* heightMMPtr = &height)
                Delegates.glfwGetMonitorPhysicalSize(monitor, widthMMPtr, heightMMPtr);
        }

        /// <summary>
        ///     Retrieves the content scale for the specified monitor. The content scale is the ratio between the
        ///     current DPI and the platform's default DPI.
        ///     <para>
        ///         This is especially important for text and any UI elements. If the pixel dimensions of your UI scaled by
        ///         this look appropriate on your machine then it should appear at a reasonable size on other machines
        ///         regardless of their DPI and scaling settings. This relies on the system DPI and scaling settings being
        ///         somewhat correct.
        ///     </para>
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <param name="xScale">The scale on the x-axis.</param>
        /// <param name="yScale">The scale on the y-axis.</param>
        public static void GetMonitorContentScale(IntPtr monitor, out float xScale, out float yScale)
        {
            fixed (float* xscalePtr = &xScale)
            fixed (float* yscalePtr = &yScale)
                Delegates.glfwGetMonitorContentScale(monitor, xscalePtr, yscalePtr);
        }

        /// <summary>
        ///     This function returns a human-readable name, encoded as UTF-8, of the specified monitor.
        ///     The name typically reflects the make and model of the monitor and is not guaranteed to be
        ///     unique among the connected monitors.
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <returns>The UTF-8 encoded name of the monitor, or NULL if an error occurred.</returns>
        public static string GetMonitorName(IntPtr monitor)
        {
            byte* pointer = Delegates.glfwGetMonitorName(monitor);
            byte* walkPtr = pointer;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(pointer, (int)(walkPtr - pointer));

        }

        /// <summary>
        ///     This function sets the user-defined pointer of the specified <paramref name="monitor" />.
        ///     <para>The current value is retained until the monitor is disconnected.</para>
        /// </summary>
        /// <param name="monitor">The monitor whose pointer to set.</param>
        /// <param name="pointer">The user-defined pointer value.</param>
        public static void SetMonitorUserPointer(IntPtr monitor, long pointer) => Delegates.glfwSetMonitorUserPointer(monitor, pointer);

        /// <summary>
        ///     Returns the current value of the user-defined pointer of the specified <paramref name="monitor" />.
        /// </summary>
        /// <param name="monitor">The monitor whose pointer to return.</param>
        /// <returns>The user-pointer, or <see cref="IntPtr.Zero" /> if none is defined.</returns>
        public static long GetMonitorUserPointer(IntPtr monitor) => Delegates.glfwGetMonitorUserPointer(monitor);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void MonitorCallback(IntPtr window, Connection @event);
        internal static MonitorCallback _SetMonitorCallback;
        /// <summary>
        ///     Sets the monitor configuration callback, or removes the currently set callback. This is called when a
        ///     monitor is connected to or disconnected from the system.
        /// </summary>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static MonitorCallback SetMonitorCallback(Action<IntPtr, Connection> callback)
        {
            _SetMonitorCallback = (window, @event) => callback(window, @event);
            return Delegates.glfwSetMonitorCallback(_SetMonitorCallback);
        }

        /// <summary>
        ///     Gets an array of all video modes supported by the specified monitor.
        ///     <para>
        ///         The returned array is sorted in ascending order, first by color bit depth (the sum of all channel depths) and
        ///         then by resolution area (the product of width and height).
        ///     </para>
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <param name="count">Number of video modes.</param>
        /// <returns>The array of video modes.</returns>
        public static GammaRamp GetVideoModes(IntPtr monitor, out int count)
        {
            fixed (int* countPtr = &count)
            {
                GammaRamp.Native r = Delegates.glfwGetVideoModes(monitor, countPtr);
                GammaRamp.FromNative(ref r, out GammaRamp result);
                return result;
            }
        }

        /// <summary>
        ///     Gets the current video mode of the specified monitor.
        ///     <para>
        ///         If you have created a full screen window for that monitor, the return value will depend on whether that
        ///         window is iconified.
        ///     </para>
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <returns>The current mode of the monitor, or <c>null</c> if an error occurred.</returns>
        public static GammaRamp GetVideoMode(IntPtr monitor)
        {
            GammaRamp.Native r = Delegates.glfwGetVideoMode(monitor);
            GammaRamp.FromNative(ref r, out GammaRamp result);
            return result;
        }

        /// <summary>
        ///     This function generates a 256-element gamma ramp from the specified exponent and then calls
        ///     <see cref="SetGammaRamp" /> with it.
        ///     <para>The value must be a finite number greater than zero.</para>
        /// </summary>
        /// <param name="monitor">The monitor whose gamma ramp to set.</param>
        /// <param name="gamma">The desired exponent.</param>
        public static void SetGamma(IntPtr monitor, float gamma) => Delegates.glfwSetGamma(monitor, gamma);

        /// <summary>
        ///     Gets the current gamma ramp of the specified monitor.
        /// </summary>
        /// <param name="monitor">The monitor to query.</param>
        /// <returns>The current gamma ramp, or empty structure if an error occurred.</returns>
        public static GammaRamp GetGammaRamp(IntPtr monitor)
        {
            GammaRamp.Native r = Delegates.glfwGetGammaRamp(monitor);
            GammaRamp.FromNative(ref r, out GammaRamp result);
            return result;
        }

        /// <summary>
        ///     Sets the current gamma ramp for the specified monitor.
        ///     <para>
        ///         The original gamma ramp for that monitor is saved by GLFW the first time this function is called and is
        ///         restored by <see cref="Terminate" />.
        ///     </para>
        ///     <para>WARNING: Gamma ramps with sizes other than 256 are not supported on some platforms (Windows).</para>
        /// </summary>
        /// <param name="monitor">The monitor whose gamma ramp to set.</param>
        /// <param name="ramp">The gamma ramp to use.</param>
        public static void SetGammaRamp(IntPtr monitor, GammaRamp ramp)
        {
            ramp.ToNative(out GammaRamp.Native native);
            Delegates.glfwSetGammaRamp(monitor, native);
        }

        /// <summary>
        ///     Gets the value of an input option for the specified window.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="mode">The mode to query.</param>
        /// <returns>Dependent on mode being queried.</returns>
        public static InputType GetInputMode(IntPtr window, InputType mode) => Delegates.glfwGetInputMode(window, mode);

        /// <summary>
        ///     Sets an input mode option for the specified window.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="mode">The mode to set a new value for.</param>
        /// <param name="value">The new value of the specified input mode.</param>
        public static void SetInputMode(IntPtr window, InputType mode, CursorMode value) => Delegates.glfwSetInputModeCURSOR(window, mode, value);

        /// <summary>
        ///     Sets an input mode option for the specified window.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="mode">The mode to set a new value for.</param>
        /// <param name="value">The new value of the specified input mode.</param>
        public static void SetInputMode(IntPtr window, InputType mode, bool value) => Delegates.glfwSetInputModeBOOL(window, mode, value);

        /// <summary>
        ///     This function returns whether raw mouse motion is supported on the current system.
        ///     <para>
        ///         This status does not change after GLFW has been initialized so you only need to check this once. If you
        ///         attempt to enable raw motion on a system that does not support it, an error will be emitted.
        ///     </para>
        /// </summary>
        /// <returns><c>true</c> if raw mouse motion is supported on the current machine, or <c>false</c> otherwise.</returns>
        public static bool RawMouseMotionSupported() => Delegates.glfwRawMouseMotionSupported() == 1;

        /// <summary>
        ///     This function returns the name of the specified printable key,
        ///     encoded as UTF-8. This is typically the character that key would
        ///     produce without any modifier keys, intended for displaying key bindings
        ///     to the user. For dead keys, it is typically the diacritic it would add to a character.
        ///     <para>
        ///         If the key is <seealso cref="KeyCode.Unknown"/>, the scancode is used to identify the key, otherwise
        ///         the scancode is ignored. If you specify a non-printable key, or <seealso cref="KeyCode.Unknown"/> and a
        ///         scancode that maps to a non-printable key, this function returns NULL but does not emit
        ///         an error.
        ///     </para>
        ///     <para>
        ///         This behavior allows you to always pass in the arguments in the key callback without modification.
        ///     </para>
        ///     <para>
        ///
        ///     </para>
        /// </summary>
        /// <note>Do not use this function for text input. You will break text input for many languages even if it happens to work for yours.</note>
        /// <param name="key">The key to query, or <seealso cref="KeyCode.Unknown"/>.</param>
        /// <param name="scancode"></param>
        /// <returns>The UTF-8 encoded, layout-specific name of the key, or NULL.</returns>
        public static string GetKeyName(KeyCode key, int scancode)
        {
            byte* pointer = Delegates.glfwGetKeyName(key, scancode);
            byte* walkPtr = pointer;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(pointer, (int)(walkPtr - pointer));
        }

        /// <summary>
        ///     Returns the platform-specific scan-code of the specified key.
        ///     <para>If the key is <see cref="KeyCode.Unknown" /> or does not exist on the keyboard this method will return -1.</para>
        /// </summary>
        /// <param name="key">The named key to query.</param>
        /// <returns>The platform-specific scan-code for the key, or -1 if an error occurred.</returns>
        public static int GetKeyScancode(KeyCode key) => Delegates.glfwGetKeyScancode(key);

        /// <summary>
        ///     Gets the last state reported for the specified key to the specified window.
        ///     <para>The higher-level action <see cref="InputState.Repeat" /> is only reported to the key callback.</para>
        ///     <para>
        ///         If the sticky keys input mode is enabled, this function returns <see cref="InputState.Press" /> the first
        ///         time you call it for a key that was pressed, even if that key has already been released.
        ///     </para>
        ///     <para>
        ///         The key functions deal with physical keys, with key tokens named after their use on the standard US keyboard
        ///         layout. If you want to input text, use the Unicode character callback instead.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="key">The key to query.</param>
        /// <returns>Either <see cref="InputState.Press" /> or <see cref="InputState.Release" />.</returns>
        public static InputState GetKey(IntPtr window, KeyCode key) => Delegates.glfwGetKey(window, key);

        /// <summary>
        ///     Gets the last state reported for the specified mouse button to the specified window.
        ///     <para>
        ///         If the <see cref="InputType.StickyMouseButtons" /> input mode is enabled, this function returns
        ///         <see cref="InputState.Press" /> the first time you call it for a mouse button that was pressed, even if that
        ///         mouse button has already been released.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="button">The desired mouse button.</param>
        /// <returns>The input state of the <paramref name="button" />.</returns>
        public static InputState GetMouseButton(IntPtr window, MouseButton button) => Delegates.glfwGetMouseButton(window, button);

        /// <summary>
        ///     Gets the position of the cursor, in screen coordinates, relative to the upper-left corner of the
        ///     client area of the specified window
        ///     <para>
        ///         If the cursor is disabled then the cursor position is unbounded and limited only by the minimum and maximum
        ///         values of a double.
        ///     </para>
        ///     <para>
        ///         The coordinate can be converted to their integer equivalents with the floor function. Casting directly to an
        ///         integer type works for positive coordinates, but fails for negative ones.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="x">The cursor x-coordinate, relative to the left edge of the client area.</param>
        /// <param name="y">The cursor y-coordinate, relative to the left edge of the client area.</param>
        public static void GetCursorPos(IntPtr window, out double x, out double y)
        {
            fixed (double* xposPtr = &x)
            fixed (double* yposPtr = &y)
                Delegates.glfwGetCursorPos(window, xposPtr, yposPtr);
        }

        /// <summary>
        ///     Sets the position, in screen coordinates, of the cursor relative to the upper-left corner of the
        ///     client area of the specified window. The window must have input focus. If the window does not have input focus when
        ///     this function is called, it fails silently.
        ///     <para>
        ///         If the cursor mode is disabled then the cursor position is unconstrained and limited only by the minimum and
        ///         maximum values of a <see cref="double" />.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="x">The desired x-coordinate, relative to the left edge of the client area.</param>
        /// <param name="y">The desired y-coordinate, relative to the left edge of the client area.</param>
        public static void SetCursorPos(IntPtr window, double x, double y) => Delegates.glfwSetCursorPos(window, x, y);

        /// <summary>
        ///     Creates a new custom cursor image that can be set for a window with glfwSetCursor.
        ///     <para>
        ///         The cursor can be destroyed with <see cref="DestroyCursor" />. Any remaining cursors are destroyed by
        ///         <see cref="Terminate" />.
        ///     </para>
        ///     <para>
        ///         The pixels are 32-bit, little-endian, non-premultiplied RGBA, i.e. eight bits per channel. They are arranged
        ///         canonically as packed sequential rows, starting from the top-left corner.
        ///     </para>
        ///     <para>
        ///         The cursor hotspot is specified in pixels, relative to the upper-left corner of the cursor image. Like all
        ///         other coordinate systems in GLFW, the X-axis points to the right and the Y-axis points down.
        ///     </para>
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="xHotspot">The x hotspot.</param>
        /// <param name="yHotspot">The y hotspot.</param>
        /// <returns>The created cursor.</returns>
        public static IntPtr CreateCursor(Image image, int xHotspot, int yHotspot)
        {
            image.ToNative(out var native);
            return Delegates.glfwCreateCursor(&native, xHotspot, yHotspot);
        }

        /// <summary>
        ///     Returns a cursor with a standard shape, that can be set for a window with <see cref="SetCursor" />.
        /// </summary>
        /// <param name="shape">The type of cursor to create.</param>
        /// <returns>A new cursor ready to use or <c>IntPtr.Zero</c> if an error occurred.</returns>
        public static IntPtr CreateStandardCursor(CursorShape shape) => Delegates.glfwCreateStandardCursor(shape);

        /// <summary>
        ///     This function destroys a cursor previously created with <see cref="CreateCursor" />. Any remaining cursors will be
        ///     destroyed by <see cref="Terminate" />.
        /// </summary>
        /// <param name="cursor">The cursor object to destroy.</param>
        public static void DestroyCursor(IntPtr cursor) => Delegates.glfwDestroyCursor(cursor);

        /// <summary>
        ///     Sets the cursor image to be used when the cursor is over the client area of the specified window.
        ///     <para>The set cursor will only be visible when the cursor mode of the window is <see cref="CursorMode.Normal" />.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="cursor">The cursor to set, or <c>IntPtr.Zero</c> to switch back to the default arrow cursor.</param>
        public static void SetCursor(IntPtr window, IntPtr cursor) => Delegates.glfwSetCursor(window, cursor);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void KeyCallback(IntPtr window, KeyCode key, int scancode, InputState buttonState, ModifierKey modifierKey);
        internal static KeyCallback _SetKeyCallback;
        /// <summary>
        ///     Sets the key callback of the specified window, which is called when a key is pressed, repeated or
        ///     released.
        ///     <para>
        ///         The key functions deal with physical keys, with layout independent key tokens named after their values in the
        ///         standard US keyboard layout. If you want to input text, use the character callback instead.
        ///     </para>
        ///     <para>
        ///         When a window loses input focus, it will generate synthetic key release events for all pressed keys. You can
        ///         tell these events from user-generated events by the fact that the synthetic ones are generated after the focus
        ///         loss event has been processed, i.e. after the window focus callback has been called.
        ///     </para>
        ///     <para>
        ///         The scancode of a key is specific to that platform or sometimes even to that machine. Scancodes are intended
        ///         to allow users to bind keys that don't have a GLFW key token. Such keys have key set to
        ///         <see cref="KeyCode.Unknown" />, their state is not saved and so it cannot be queried with <see cref="GetKey" />.
        ///     </para>
        ///     <para>Sometimes GLFW needs to generate synthetic key events, in which case the scancode may be zero.</para>
        /// </summary>
        /// <param name="window">The new key callback, or <c>null</c> to remove the currently set callback.</param>
        /// <param name="callback">The key callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static KeyCallback SetKeyCallback(IntPtr window, Action<IntPtr, KeyCode, int, InputState, ModifierKey> callback)
        {
            _SetKeyCallback = (window, key, scancode, buttonState, modifierKey) => callback(window, key, scancode, buttonState, modifierKey);
            return Delegates.glfwSetKeyCallback(window, _SetKeyCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void CharCallback(IntPtr window, uint codepoint);
        internal static CharCallback _SetCharCallback;
        /// <summary>
        ///     Sets the character callback of the specified window, which is called when a Unicode character is
        ///     input.
        ///     <para>
        ///         The character callback is intended for Unicode text input. As it deals with characters, it is keyboard layout
        ///         dependent, whereas the key callback is not. Characters do not map 1:1 to physical keys, as a key may produce
        ///         zero, one or more characters. If you want to know whether a specific physical key was pressed or released, see
        ///         the key callback instead.
        ///     </para>
        ///     <para>
        ///         The character callback behaves as system text input normally does and will not be called if modifier keys are
        ///         held down that would prevent normal text input on that platform, for example a Super (Command) key on OS X or
        ///         Alt key on Windows. There is a character with modifiers callback that receives these events.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static CharCallback SetCharCallback(IntPtr window, Action<IntPtr, uint> callback)
        {
            _SetCharCallback = (window, codepoint) => callback(window, codepoint);
            return Delegates.glfwSetCharCallback(window, _SetCharCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void CharModsCallback(IntPtr window, uint codepoint, ModifierKey modifierKey);
        internal static CharModsCallback _SetCharModsCallback;
        /// <summary>
        ///     Sets the character with modifiers callback of the specified window, which is called when a Unicode
        ///     character is input regardless of what modifier keys are used.
        ///     <para>
        ///         The character with modifiers callback is intended for implementing custom Unicode character input. For
        ///         regular Unicode text input, see the character callback. Like the character callback, the character with
        ///         modifiers callback deals with characters and is keyboard layout dependent. Characters do not map 1:1 to
        ///         physical keys, as a key may produce zero, one or more characters. If you want to know whether a specific
        ///         physical key was pressed or released, see the key callback instead.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or an error occurred.</returns>
        [Obsolete("Scheduled for removal in version 4.0")]
        /// <summary>
        ///     Sets the character with modifiers callback of the specified window, which is called when a Unicode
        ///     character is input regardless of what modifier keys are used.
        ///     <para>
        ///         The character with modifiers callback is intended for implementing custom Unicode character input. For
        ///         regular Unicode text input, see the character callback. Like the character callback, the character with
        ///         modifiers callback deals with characters and is keyboard layout dependent. Characters do not map 1:1 to
        ///         physical keys, as a key may produce zero, one or more characters. If you want to know whether a specific
        ///         physical key was pressed or released, see the key callback instead.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or an error occurred.</returns>
        public static CharModsCallback SetCharModsCallback(IntPtr window, Action<IntPtr, uint, ModifierKey> callback)
        {
            _SetCharModsCallback = (window, codepoint, modifierKey) => callback(window, codepoint, modifierKey);
            return Delegates.glfwSetCharModsCallback(window, _SetCharModsCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void MouseButtonCallback(IntPtr window, MouseButton button, InputState buttonState, ModifierKey modifierKey);
        internal static MouseButtonCallback _SetMouseButtonCallback;
        /// <summary>
        ///     Sets the mouse button callback of the specified window, which is called when a mouse button is
        ///     pressed or released.
        ///     <para>
        ///         When a window loses input focus, it will generate synthetic mouse button release events for all pressed mouse
        ///         buttons. You can tell these events from user-generated events by the fact that the synthetic ones are generated
        ///         after the focus loss event has been processed, i.e. after the window focus callback has been called.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static MouseButtonCallback SetMouseButtonCallback(IntPtr window, Action<IntPtr, MouseButton, InputState, ModifierKey> callback)
        {
            _SetMouseButtonCallback = (window, button, buttonState, modifierKey) => callback(window, button, buttonState, modifierKey);
            return Delegates.glfwSetMouseButtonCallback(window, _SetMouseButtonCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void CursorPosCallback(IntPtr window, double xpos, double ypos);
        internal static CursorPosCallback _SetCursorPosCallback;
        /// <summary>
        ///     Sets the cursor position callback of the specified window, which is called when the cursor is moved.
        ///     <para>
        ///         The callback is provided with the position, in screen coordinates, relative to the upper-left corner of the
        ///         client area of the window.
        ///     </para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or<c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static CursorPosCallback SetCursorPosCallback(IntPtr window, Action<IntPtr, double, double> callback)
        {
            _SetCursorPosCallback = (window, xpos, ypos) => callback(window, xpos, ypos);
            return Delegates.glfwSetCursorPosCallback(window, _SetCursorPosCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void CursorEnterCallback(IntPtr window, bool entered);
        internal static CursorEnterCallback _SetCursorEnterCallback;
        /// <summary>
        ///     Sets the cursor boundary crossing callback of the specified window, which is called when the cursor
        ///     enters or leaves the client area of the window.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static CursorEnterCallback SetCursorEnterCallback(IntPtr window, Action<IntPtr, bool> callback)
        {
            _SetCursorEnterCallback = (window, entered) => callback(window, entered);
            return Delegates.glfwSetCursorEnterCallback(window, _SetCursorEnterCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ScrollCallback(IntPtr window, double xoffset, double yoffset);
        internal static ScrollCallback _SetScrollCallback;
        /// <summary>
        ///     Sets the scroll callback of the specified window, which is called when a scrolling device is used,
        ///     such as a mouse wheel or scrolling area of a touchpad.
        ///     <para>The scroll callback receives all scrolling input, like that from a mouse wheel or a touchpad scrolling area.</para>
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="callback">	The new scroll callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static ScrollCallback SetScrollCallback(IntPtr window, Action<IntPtr, double, double> callback)
        {
            _SetScrollCallback = (window, xoffset, yoffset) => callback(window, xoffset, yoffset);
            return Delegates.glfwSetScrollCallback(window, _SetScrollCallback);
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void DropCallback(IntPtr window, int path_count, byte** paths);
        internal static DropCallback _SetDropCallback;
        /// <summary>
        ///     Sets the file drop callback of the specified window, which is called when one or more dragged files
        ///     are dropped on the window.
        ///     <para>
        ///         Because the path array and its strings may have been generated specifically for that event, they are not
        ///         guaranteed to be valid after the callback has returned. If you wish to use them after the callback returns, you
        ///         need to make a deep copy.
        ///     </para>
        /// </summary>
        /// <param name="window">The window whose callback to set.</param>
        /// <param name="callback">The new file drop callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static DropCallback SetDropCallback(IntPtr window, Action<IntPtr, string[]> callback)
        {
            _SetDropCallback = (window, path_count, paths) =>
            {
                string[] array = new string[path_count];
                for (int i = 0; i < path_count; i++)
                {
                    byte* walk = paths[i];
                    while (*walk != 0) walk++;

                    array[i] = Encoding.UTF8.GetString(paths[i], (int)(walk - paths[i]));
                }

                callback(window, array);
            };
            return Delegates.glfwSetDropCallback(window, _SetDropCallback);
        }

        /// <summary>
        ///     Gets whether the specified joystick is present.
        /// </summary>
        /// <param name="joystick">The joystick to query.</param>
        /// <returns><c>true</c> if the joystick is present, or <c>false</c> otherwise.</returns>
        public static bool JoystickPresent(int joystick) => Delegates.glfwJoystickPresent(joystick) == 1;
#nullable enable
        /// <summary>
        ///     Gets the float values of all the joystick axis present.
        /// </summary>
        /// <param name="joystick">Joystick to query.</param>
        /// <param name="count">Count of axis.</param>
        /// <returns>Array of axis.</returns>
        public static float[]? GetJoystickAxes(int joystick, out int count)
        {
            fixed (int* countPtr = &count)
            {
                float* p = Delegates.glfwGetJoystickAxes(joystick, countPtr);
                if (count == 0) return null;

                var array = new float[count];
                for (int i = 0; i < count; i++)
                {
                    array[i] = p[i];
                }
                return array;
            }
        }
#nullable disable

        /// <summary>
        ///     This function returns the state of all buttons of the specified joystick.
        ///     Each element in the array is either <seealso cref="InputState.Press"/> or
        ///     <seealso cref="InputState.Release"/>.
        ///     <para>
        ///         For backward compatibility with earlier versions that did not have <seealso cref="GetJoystickHats"/>,
        ///         the button array also includes all hats, each represented as four buttons.
        ///         The hats are in the same order as returned by glfwGetJoystickHats and are in
        ///         the order up, right, down and left. To disable these extra buttons, set
        ///         the <seealso cref="InitHint.JoystickHatButtons"/> init hint before initialization.
        ///     </para>
        ///     <para>
        ///         If the specified joystick is not present this function will return NULL but
        ///         will not generate an error. This can be used instead of first calling <seealso cref="JoystickPresent"/>.
        ///     </para>
        /// </summary>
        /// <param name="jid">The joystick to query.</param>
        /// <param name="count">Where to store the number of button states in the returned array. This is set to zero if the joystick is not present or an error occurred.</param>
        /// <returns>An array of button states, or NULL if the joystick is not present or an error occurred.</returns>
        public static InputState[] GetJoystickButtons(int jid, out int count)
        {
            InputState[] inputStates;
            fixed (int* countPtr = &count)
            {
                var states = Delegates.glfwGetJoystickButtons(jid, countPtr);

                inputStates = new InputState[count];
                for (int i = 0; i < count; i++)
                    inputStates[i] = *(states + i);

                return inputStates;
            }
        }

        /// <summary>
        ///     This function returns the state of all hats of the specified joystick.
        ///     <para>
        ///         The diagonal directions are bitwise combinations of the primary
        ///         (up, right, down and left) directions and you can test for these
        ///         individually by ANDing it with the corresponding direction.
        ///     </para>
        ///     <para>
        ///         If the specified joystick is not present this function will return NULL
        ///         but will not generate an error. This can be used instead of first
        ///         calling <seealso cref="JoystickPresent"/>.
        ///     </para>
        /// </summary>
        /// <param name="jid">The joystick to query.</param>
        /// <param name="count">Where to store the number of hat states in the returned array. This is set to zero if the joystick is not present or an error occurred.</param>
        /// <returns></returns>
        public static JoystickHatState[] GetJoystickHats(int jid, out int count)
        {
            JoystickHatState[] hatStates;
            fixed (int* countPtr = &count)
            {
                var states = Delegates.glfwGetJoystickHats(jid, countPtr);

                hatStates = new JoystickHatState[count];
                for (int i = 0; i < count; i++)
                    hatStates[i] = *(states + i);

                return hatStates;
            }
        }

        /// <summary>
        ///     Gets the name of the specified joystick.
        ///     <para>
        ///         Querying a joystick slot with no device present is not an error. <see cref="JoystickPresent" /> to check
        ///         device presence.
        ///     </para>
        /// </summary>
        /// <param name="joystick">The joystick to query.</param>
        /// <returns>The name of the joystick, or <c>null</c> if the joystick is not present or an error occurred.</returns>
        public static string GetJoystickName(int joystick)
        {
            byte* pointer = Delegates.glfwGetJoystickName(joystick);
            byte* walkPtr = pointer;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(pointer, (int)(walkPtr - pointer));
        }

        /// <summary>
        ///     Returns the SDL compatible GUID, as a hexadecimal string, of the specified joystick.
        ///     <para>
        ///         The GUID is what connects a joystick to a gamepad mapping. A connected joystick will always have a GUID even
        ///         if there is no gamepad mapping assigned to it.
        ///     </para>
        /// </summary>
        /// <param name="joystickId">The joystick to query.</param>
        /// <returns>The GUID of the joystick, or <c>null</c> if the joystick is not present or an error occurred.</returns>
        public static Guid GetJoystickGUID(int joystickId) => Delegates.glfwGetJoystickGUID(joystickId);

        /// <summary>
        ///     This function sets the user-defined pointer of the specified joystick.
        ///     <para>The current value is retained until the joystick is disconnected.</para>
        /// </summary>
        /// <param name="joystickId">The joystick whose pointer to set.</param>
        /// <param name="pointer">The new value.</param>
        public static void SetJoystickUserPointer(int joystickId, long pointer) => Delegates.glfwSetJoystickUserPointer(joystickId, pointer);

        /// <summary>
        ///     This function returns the current value of the user-defined pointer of the specified joystick.
        /// </summary>
        /// <param name="joystickId">The joystick whose pointer to return.</param>
        /// <returns>The user-defined pointer, or <see cref="IntPtr.Zero" /> if never defined.</returns>
        public static long GetJoystickUserPointer(int joystickId) => Delegates.glfwGetJoystickUserPointer(joystickId);

        /// <summary>
        ///     Returns whether the specified joystick is both present and has a gamepad mapping.
        /// </summary>
        /// <param name="jid">The joystick to query.</param>
        /// <returns><c>true</c> if a joystick is both present and has a gamepad mapping, or <c>false</c> otherwise.</returns>
        public static int JoystickIsGamepad(int jid) => Delegates.glfwJoystickIsGamepad(jid);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void JoystickCallback(int jid, Connection @event);
        internal static JoystickCallback _SetJoystickCallback;
        /// <summary>
        ///     Sets the joystick configuration callback, or removes the currently set callback.
        ///     <para>This is called when a joystick is connected to or disconnected from the system.</para>
        /// </summary>
        /// <param name="callback">The new callback, or <c>null</c> to remove the currently set callback.</param>
        /// <returns>The previously set callback, or <c>null</c> if no callback was set or the library had not been initialized.</returns>
        public static JoystickCallback SetJoystickCallback(Action<int, Connection> callback)
        {
            _SetJoystickCallback = (jid, @event) => callback(jid, @event);
            return Delegates.glfwSetJoystickCallback(_SetJoystickCallback);
        }

        /// <summary>
        ///     Parses the specified string and updates the internal list with any gamepad mappings it finds.
        ///     <para>
        ///         This string may contain either a single gamepad mapping or many mappings separated by newlines. The parser
        ///         supports the full format of the SDL <c>gamecontrollerdb.txt</c> source file including empty lines and comments.
        ///     </para>
        /// </summary>
        /// <param name="mappings">The string containing the gamepad mappings.</param>
        /// <returns><c>true</c> if successful, or <c>false</c> if an error occurred.</returns>
        public static int UpdateGamepadMappings(string mappings)
        {
            fixed (byte* bytePtr = Encoding.UTF8.GetBytes(mappings))
                return Delegates.glfwUpdateGamepadMappings(bytePtr);
        }

        /// <summary>
        ///     Returns the human-readable name of the gamepad from the gamepad mapping assigned to the specified joystick.
        /// </summary>
        /// <param name="gamepadId">The joystick to query.</param>
        /// <returns>
        ///     The name of the gamepad, or <c>null</c> if the joystick is not present, does not have a mapping or an error
        ///     occurred.
        /// </returns>
        public static string GetGamepadName(int gamepadId)
        {
            byte* pointer = Delegates.glfwGetGamepadName(gamepadId);
            byte* walkPtr = pointer;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(pointer, (int)(walkPtr - pointer));
        }

        /// <summary>
        ///     Retrieves the state of the specified joystick remapped to an Xbox-like gamepad.
        /// </summary>
        /// <param name="joystickId">The joystick to query.</param>
        /// <param name="state">The gamepad input state of the joystick.</param>
        /// <returns>
        ///     <c>true</c> if successful, or <c>false</c> if no joystick is connected, it has no gamepad mapping or an error
        ///     occurred.
        /// </returns>
        public static int GetGamepadState(int joystickId, GamepadState state) => Delegates.glfwGetGamepadState(joystickId, state);

        /// <summary>
        ///     Sets the system clipboard to the specified string.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <param name="str">The string to set to the clipboard.</param>
        public static void SetClipboardString(IntPtr window, string str)
        {
            fixed (byte* bytePtr = Encoding.UTF8.GetBytes(str))
                Delegates.glfwSetClipboardString(window, bytePtr);
        }

        /// <summary>
        ///     Gets the contents of the system clipboard, if it contains or is convertible to a UTF-8 encoded
        ///     string.
        /// </summary>
        /// <param name="window">A window instance.</param>
        /// <returns>The contents of the clipboard as a UTF-8 encoded string, or <c>null</c> if an error occurred.</returns>
        public static string GetClipboardString(IntPtr window)
        {
            byte* pointer = Delegates.glfwGetClipboardString(window);
            byte* walkPtr = pointer;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(pointer, (int)(walkPtr - pointer));
        }

        /// <summary>
        ///     Gets the value of the GLFW timer.
        ///     <para>
        ///         The resolution of the timer is system dependent, but is usually on the order of a few micro- or nanoseconds.
        ///         It uses the highest-resolution monotonic time source on each supported platform.
        ///     </para>
        /// </summary>
        /// <value>
        ///     The time.
        /// </value>
        public static double GetTime() => Delegates.glfwGetTime();

        /// <summary>
        ///     Sets the value of the GLFW timer.
        ///     <para>
        ///         The resolution of the timer is system dependent, but is usually on the order of a few micro- or nanoseconds.
        ///         It uses the highest-resolution monotonic time source on each supported platform.
        ///     </para>
        /// </summary>
        /// <value>
        ///     The time.
        /// </value>
        public static void SetTime(double time) => Delegates.glfwSetTime(time);

        /// <summary>
        ///     Gets the current value of the raw timer, measured in 1 / frequency seconds.
        /// </summary>
        /// <value>
        ///     The timer value.
        /// </value>
        public static ulong GetTimerValue() => Delegates.glfwGetTimerValue();

        /// <summary>
        ///     Gets the frequency, in Hz, of the raw timer.
        /// </summary>
        /// <value>
        ///     The frequency of the timer, in Hz, or zero if an error occurred.
        /// </value>
        public static ulong GetTimerFrequency() => Delegates.glfwGetTimerFrequency();
    }
}
