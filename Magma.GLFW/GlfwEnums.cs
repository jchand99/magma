using System;

namespace Magma.GLFW
{
    /// <summary>
    ///     There are a number of hints that can be set before the creation of a window and context.
    ///     Some affect the window itself, others affect the framebuffer or context.
    ///     These hints are set to their default values each time the library is initialized
    ///     with <seealso cref="Glfw.Init"/>. Integer value hints can be set individually with <seealso cref="Glfw.WindowHint(WindowHint, int)"/>
    ///     and string value hints with <seealso cref="Glfw.WindowHintString"/>. You can reset all at once to
    ///     their defaults with <seealso cref="Glfw.DefaultWindowHints"/>.
    ///     <para>
    ///         Some hints are platform specific. These are always valid to set on any platform but they will only
    ///         affect their specific platform. Other platforms will ignore them. Setting these hints requires no
    ///         platform specific headers or calls.
    ///     </para>
    /// </summary>
    public enum WindowHint : int
    {
        /// <summary>
        ///     specifies whether the windowed mode window will be resizable by the user. The window will still be
        ///     resizable using the glfwSetWindowSize function. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        ///     This hint is ignored for full screen and undecorated windows.
        /// </summary>
        Resizable = 0x00020003,

        /// <summary>
        ///     specifies whether the windowed mode window will be initially visible.
        ///     Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This hint is ignored for full screen windows.
        /// </summary>
        Visible = 0x00020004,

        /// <summary>
        ///     specifies whether the windowed mode window will have window decorations such as a border, a close widget, etc.
        ///     An undecorated window will not be resizable by the user but will still allow the user to generate close events
        ///     on some platforms. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This hint is ignored for full screen windows.
        /// </summary>
        Decorated = 0x00020005,

        /// <summary>
        ///     specifies whether the windowed mode window will be given input focus when created.
        ///     Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This hint is ignored for full screen and initially hidden windows.
        /// </summary>
        Focused = 0x00020001,

        /// <summary>
        ///     specifies whether the full screen window will automatically iconify and restore the previous
        ///     video mode on input focus loss. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This hint is ignored for windowed mode windows.
        /// </summary>
        AutoIconify = 0x00020006,

        /// <summary>
        ///     specifies whether the windowed mode window will be floating above other regular windows, also called topmost or always-on-top.
        ///     This is intended primarily for debugging purposes and cannot be used to implement proper full screen windows.
        ///     Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This hint is ignored for full screen windows.
        /// </summary>
        Floating = 0x00020007,

        /// <summary>
        ///     specifies whether the windowed mode window will be maximized when created.
        ///     Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This hint is ignored for full screen windows.
        /// </summary>
        Maximized = 0x00020008,

        /// <summary>
        ///     specifies whether the cursor should be centered over newly created full screen windows.
        ///     Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This hint is ignored for windowed mode windows.
        /// </summary>
        CenterCursor = 0x00020009,

        /// <summary>
        ///     specifies whether the window framebuffer will be transparent.
        ///     If enabled and supported by the system, the window framebuffer alpha channel will be used
        ///     to combine the framebuffer with the background. This does not affect window decorations.
        ///     Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        /// </summary>
        TransparentFramebuffer = 0x0002000A,

        /// <summary>
        ///     specifies whether the window will be given input focus when <see cref="Glfw.ShowWindow"/> is called. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        /// </summary>
        FocusOnShow = 0x0002000C,

        /// <summary>
        ///     specified whether the window content area should be resized based on the monitor content scale of
        ///     any monitor it is placed on. This includes the initial placement when the window is created. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        /// </summary>
        ScaleToMonitor = 0x0002200C,

        /// <summary>
        ///     specify the desired bit depths of the various components of the default framebuffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// </summary>
        RedBits = 0x00021001,

        /// <summary>
        ///     specify the desired bit depths of the various components of the default framebuffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// </summary>
        GreenBits = 0x00021002,

        /// <summary>
        ///     specify the desired bit depths of the various components of the default framebuffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// </summary>
        BlueBits = 0x00021003,

        /// <summary>
        ///     specify the desired bit depths of the various components of the default framebuffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// </summary>
        AlphaBits = 0x00021004,

        /// <summary>
        ///     specify the desired bit depths of the various components of the default framebuffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// </summary>
        DepthBits = 0x00021005,

        /// <summary>
        ///     specify the desired bit depths of the various components of the default framebuffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// </summary>
        StencilBits = 0x00021006,

        /// <summary>
        ///     specify the desired bit depths of the various components of the accumulation buffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// <para><note>Accumulation buffers are a legacy OpenGL feature and should not be used in new code.</note></para>
        /// </summary>
        AccumRedBits = 0x00021007,

        /// <summary>
        ///     specify the desired bit depths of the various components of the accumulation buffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// <para><note>Accumulation buffers are a legacy OpenGL feature and should not be used in new code.</note></para>
        /// </summary>
        AccumGreenBits = 0x00021008,

        /// <summary>
        ///     specify the desired bit depths of the various components of the accumulation buffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// <para><note>Accumulation buffers are a legacy OpenGL feature and should not be used in new code.</note></para>
        /// </summary>
        AccumBlueBits = 0x00021009,

        /// <summary>
        ///     specify the desired bit depths of the various components of the accumulation buffer. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// <para><note>Accumulation buffers are a legacy OpenGL feature and should not be used in new code.</note></para>
        /// </summary>
        AccumAlphaBits = 0x0002100A,

        /// <summary>
        ///     specifies the desired number of auxiliary buffers. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// <para><note>Auxiliary buffers are a legacy OpenGL feature and should not be used in new code.</note></para>
        /// </summary>
        AuxBuffers = 0x0002100B,

        /// <summary>
        ///     specifies whether to use OpenGL stereoscopic rendering. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This is a hard constraint.
        /// </summary>
        Stereo = 0x0002100C,

        /// <summary>
        ///     specifies the desired number of samples to use for multisampling. Zero disables multisampling. A value of <see cref="Glfw.DontCare"/> means the application has no preference.
        /// </summary>
        Samples = 0x0002100D,

        /// <summary>
        ///     specifies whether the framebuffer should be sRGB capable. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        /// <para><note>OpenGL: If enabled and supported by the system, the GL_FRAMEBUFFER_SRGB enable will control sRGB rendering. By default, sRGB rendering will be disabled.</note></para>
        /// <para><note>OpenGL ES: If enabled and supported by the system, the context will always have sRGB rendering enabled.</note></para>
        /// </summary>
        SrgbCapable = 0x0002100E,

        /// <summary>
        ///     specifies whether the framebuffer should be double buffered. You nearly always want to use double buffering. This is a hard constraint. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        /// </summary>
        DoubleBuffer = 0x00021010,

        /// <summary>
        ///     specifies the desired refresh rate for full screen windows. A value of <see cref="Glfw.DontCare"/> means the highest available refresh rate will be used. This hint is ignored for windowed mode windows.
        /// </summary>
        RefreshRate = 0x0002100F,

        /// <summary>
        ///     specifies which client API to create the context for. Possible values are <see cref="Glfw.OpenGLApi"/>, <see cref="Glfw.OpenGLESApi"/> and <see cref="Glfw.NoApi"/>. This is a hard constraint.
        /// </summary>
        ClientApi = 0x00022001,

        /// <summary>
        ///     specifies which context creation API to use to create the context. Possible values are <see cref="Glfw.NativeContextApi"/>, <see cref="Glfw.EglContextApi"/>
        ///     and <see cref="Glfw.OsmesaContextApi"/>. This is a hard constraint. If no client API is requested, this hint is ignored.
        /// <para><note>macOS: The EGL API is not available on this platform and requests to use it will fail.</note></para>
        /// <para><note>Wayland: The EGL API is the native context creation API, so this hint will have no effect.</note></para>
        /// <para><note>OSMesa: As its name implies, an OpenGL context created with OSMesa does not
        ///             update the window contents when its buffers are swapped. Use OpenGL functions or the OSMesa native access functions
        ///             GetOSMesaColorBuffer and glfwGetOSMesaDepthBuffer to retrieve the framebuffer contents.</note></para>
        /// </summary>
        ContextCreationApi = 0x0002200B,

        /// <summary>
        ///     specify the client API version that the created context must be compatible with. The exact behavior of these hints depend on the requested client API.
        /// </summary>
        ContextVersionMajor = 0x00022002,

        /// <summary>
        ///     specify the client API version that the created context must be compatible with. The exact behavior of these hints depend on the requested client API.
        /// </summary>
        ContextVersionMinor = 0x00022003,

        /// <summary>
        ///     specifies whether the OpenGL context should be forward-compatible, i.e. one where all functionality deprecated in the requested version of OpenGL is removed.
        ///     This must only be used if the requested OpenGL version is 3.0 or above. If OpenGL ES is requested, this hint is ignored.
        /// <para><note>Forward-compatibility is described in detail in the <seealso href="https://www.opengl.org/registry">OpenGL Reference Manual.</seealso></note></para>
        /// </summary>
        OpenGLForwardCompat = 0x00022006,

        /// <summary>
        ///     specifies whether the context should be created in debug mode, which may provide additional error and diagnostic reporting functionality.
        ///     Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        /// <para><note>Debug contexts for OpenGL and OpenGL ES are described in detail by the GL_KHR_debug extension.</note></para>
        /// </summary>
        OpenGLDebugContext = 0x00022007,

        /// <summary>
        ///     specifies which OpenGL profile to create the context for. Possible values are one of <see cref="Glfw.OpenGLCoreProfile"/> or
        ///     <see cref="Glfw.OpenGLCompatProfile"/>, or <see cref="Glfw.OpenGLAnyProfile"/> to not request a specific profile. If requesting an OpenGL version below 3.2,
        ///     <see cref="Glfw.OpenGLAnyProfile"/> must be used. If OpenGL ES is requested, this hint is ignored.
        /// <para><note>OpenGL profiles are described in detail in the <seealso href="https://www.opengl.org/registry">OpenGL Reference Manual.</seealso></note></para>
        /// </summary>
        OpenGLProfile = 0x00022008,

        /// <summary>
        ///     specifies the robustness strategy to be used by the context. This can be one of <see cref="Glfw.NoResetNotification"/> or <see cref="Glfw.LoseContextOnReset"/>,
        ///     or <see cref="Glfw.NoRobustness"/> to not request a robustness strategy.
        /// </summary>
        ContextRobustness = 0x00022005,

        /// <summary>
        ///     specifies the release behavior to be used by the context. Possible values are one of <see cref="Glfw.AnyReleaseBehavior"/>,
        ///     <see cref="Glfw.ReleaseBehaviorFlush"/> or <see cref="Glfw.ReleaseBehaviorNone"/>. If the behavior is <see cref="Glfw.AnyReleaseBehavior"/>,
        ///     the default behavior of the context creation API will be used.
        ///     If the behavior is <see cref="Glfw.ReleaseBehaviorFlush"/>, the pipeline will be flushed whenever the context is
        ///     released from being the current one. If the behavior is <see cref="Glfw.ReleaseBehaviorNone"/>, the pipeline will not be flushed on release.
        /// <para><note>Context release behaviors are described in detail by the GL_KHR_context_flush_control extension.</note></para>
        /// </summary>
        ContextReleaseBehavior = 0x00022009,

        /// <summary>
        ///     specifies whether errors should be generated by the context. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>.
        ///     If enabled, situations that would have generated errors instead cause undefined behavior.
        /// <para><note>The no error mode for OpenGL and OpenGL ES is described in detail by the GL_KHR_no_error extension.</note></para>
        /// </summary>
        ContextNoError = 0x0002200A,

        /// <summary>
        ///     specifies whether to use full resolution framebuffers on Retina displays. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This is ignored on other platforms.
        /// </summary>
        CocoaRetinaFramebuffer = 0x00023001,

        /// <summary>
        ///     specifies whether to in Automatic Graphics Switching, i.e. to allow the system to choose the integrated GPU for
        ///     the OpenGL context and move it between GPUs if necessary or whether to force it to always run on the discrete GPU.
        ///     This only affects systems with both integrated and discrete GPUs. Possible values are <see cref="Glfw.True"/> and <see cref="Glfw.False"/>. This is ignored on other platforms.
        ///     <para>
        ///         Simpler programs and tools may want to enable this to save power, while games and other applications performing advanced rendering will want to leave it disabled.
        ///     </para>
        ///     <para>
        ///         A bundled application that wishes to participate in Automatic Graphics Switching should also declare this in its Info.plist by setting the NSSupportsAutomaticGraphicsSwitching key to true.
        ///     </para>
        /// </summary>
        CocoaGraphicsSwitching = 0x00023003,
    }

    /// <summary>
    ///     Window Hints for string values.
    /// </summary>
    public enum WindowHintString : int
    {
        /// <summary>
        ///     specifies the UTF-8 encoded name to use for autosaving the window frame, or if empty disables frame autosaving for the window.
        ///     This is ignored on other platforms. This is set with <see cref="Glfw.WindowHintString"/>.
        /// </summary>
        CocoaFrameName = 0x00023002,

        /// <summary>
        ///     specifies the desired ASCII encoded class and instance parts of the ICCCM WM_CLASS window property. These are set with <see cref="Glfw.WindowHintString"/>.
        /// </summary>
        X11ClassName = 0x00024001,

        /// <summary>
        ///     specifies the desired ASCII encoded class and instance parts of the ICCCM WM_CLASS window property. These are set with <see cref="Glfw.WindowHintString"/>.
        /// </summary>
        X11InstanceName = 0x00024002
    }

    public enum WindowAttribute : int
    {
        /// <summary>
        ///     specifies whether the window is focused.
        /// </summary>
        Focused = 0x00020001,

        /// <summary>
        ///     specifies whether the window is iconified.
        /// </summary>
        Iconified = 0x00020002,

        /// <summary>
        ///     specifies whether the window is maximized.
        /// </summary>
        Maximized = 0x00020008,

        /// <summary>
        ///     specifies whether the window is in hovered mode.
        /// </summary>
        Hovered = 0x0002000B,

        /// <summary>
        ///     specifies whether the window is visible.
        /// </summary>
        Visible = 0x00020004,

        /// <summary>
        ///     specifies whether the window is resizable.
        /// </summary>
        Resizable = 0x00020003,

        /// <summary>
        ///     specifies whether the window is decorated.
        /// </summary>
        Decorated = 0x00020005,

        /// <summary>
        ///     specifies whether the full screen window is set to auto iconify.
        /// </summary>
        AutoIconify = 0x00020006,

        /// <summary>
        ///     specifies whether the window is floating.
        /// </summary>
        Floating = 0x00020007,

        /// <summary>
        ///     specifies whether the window framebuffer is transparent.
        /// </summary>
        TransparentFrameBuffer = 0x0002000A,

        /// <summary>
        ///     specifies whether the window will be given input focus when <see cref="Glfw.ShowWindow"/> is called.
        /// </summary>
        FocusOnShow = 0x0002000C,
    }

    /// <summary>
    /// Initialization hints
    /// </summary>
    public enum InitHint : int
    {
        /// <summary>
        ///     Enable joystick hat buttons.
        /// </summary>
        JoystickHatButtons = 0x00050001,

        /// <summary>
        ///     Enable Cocoa chdir resources.
        /// </summary>
        CocoaChdirResources = 0x00051001,

        /// <summary>
        /// Enable Cocoa menubar.
        /// </summary>
        CocoaMenubar = 0x00051002,
    }

    /// <summary>
    ///     Button input states.
    /// </summary>
    public enum InputState : int
    {
        /// <summary>
        ///     Button is released.
        /// </summary>
        Release = 0,

        /// <summary>
        ///     Button is pressed.
        /// </summary>
        Press = 1,

        /// <summary>
        ///     Button is repeated.
        /// </summary>
        Repeat = 2
    }

    /// <summary>
    ///     Axis of gamepad.
    /// </summary>
    public enum GamepadAxis : int
    {
        /// <summary>
        ///     Left joystick X axis.
        /// </summary>
        LeftX = 0,

        /// <summary>
        ///     Left joystick Y axis.
        /// </summary>
        LeftY = 1,

        /// <summary>
        ///     Right joystick X axis.
        /// </summary>
        RightX = 2,

        /// <summary>
        ///     Right joystick Y axis.
        /// </summary>
        RightY = 3,

        /// <summary>
        ///     Left trigger axis.
        /// </summary>
        LeftTrigger = 4,

        /// <summary>
        ///  Right trigger axis.
        /// </summary>
        RightTrigger = 5,

        /// <summary>
        ///  Last axis.
        /// </summary>
        AxisLast = RightTrigger
    }

    /// <summary>
    ///     Buttons for gamepad.
    /// </summary>
    public enum GamePadButton : int
    {
        /// <summary>
        ///     A button.
        /// </summary>
        A = 0,

        /// <summary>
        ///     B button.
        /// </summary>
        B = 1,

        /// <summary>
        ///     X button.
        /// </summary>
        X = 2,

        /// <summary>
        ///     Y button.
        /// </summary>
        Y = 3,

        /// <summary>
        ///     LeftBumper button.
        /// </summary>
        LeftBumper = 4,

        /// <summary>
        ///     RightBumper button.
        /// </summary>
        RightBumper = 5,

        /// <summary>
        ///     Back button.
        /// </summary>
        Back = 6,

        /// <summary>
        ///     Start button.
        /// </summary>
        Start = 7,

        /// <summary>
        ///     Guide button.
        /// </summary>
        Guide = 8,

        /// <summary>
        ///     LeftThumb button.
        /// </summary>
        LeftThumb = 9,

        /// <summary>
        ///     RightThumb button.
        /// </summary>
        RightThumb = 10,

        /// <summary>
        ///     DPadUp button.
        /// </summary>
        DPadUp = 11,

        /// <summary>
        ///     DPadRight button.
        /// </summary>
        DPadRight = 12,

        /// <summary>
        ///     DPadDown button.
        /// </summary>
        DPadDown = 13,

        /// <summary>
        ///     DPadLeft button.
        /// </summary>
        DPadLeft = 14,

        /// <summary>
        ///     Last button.
        /// </summary>
        Last = DPadLeft,

        /// <summary>
        ///     Cross button.
        /// </summary>
        Cross = A,

        /// <summary>
        ///     Circle button.
        /// </summary>
        Circle = B,

        /// <summary>
        ///     Square button.
        /// </summary>
        Square = X,

        /// <summary>
        ///     Triangle button.
        /// </summary>
        Triangle = Y
    }

    /// <summary>
    ///     States of joystick hats.
    /// </summary>
    [Flags]
    public enum JoystickHatState : int
    {
        /// <summary>
        ///     Centered state.
        /// </summary>
        Centered = 0,

        /// <summary>
        ///     Up state.
        /// </summary>
        Up = 1,

        /// <summary>
        ///     Right state.
        /// </summary>
        Right = 2,

        /// <summary>
        ///     Down state.
        /// </summary>
        Down = 4,

        /// <summary>
        ///     Left state.
        /// </summary>
        Left = 8,

        /// <summary>
        ///     RightUp state.
        /// </summary>
        RightUp = Right | Up,

        /// <summary>
        ///     RightDown state.
        /// </summary>
        RightDown = Right | Down,

        /// <summary>
        ///     LeftUp state.
        /// </summary>
        LeftUp = Left | Up,

        /// <summary>
        ///     LeftDown state.
        /// </summary>
        LeftDown = Left | Down
    }

    /// <summary>
    ///     Joystick values.
    /// </summary>
    public enum Joystick : int
    {
        /// <summary>
        ///     Joystick 0.
        /// </summary>
        Joystick1 = 0,

        /// <summary>
        ///     Joystick 1.
        /// </summary>
        Joystick2 = 1,

        /// <summary>
        ///     Joystick 2.
        /// </summary>
        Joystick3 = 2,

        /// <summary>
        ///     Joystick 3.
        /// </summary>
        Joystick4 = 3,

        /// <summary>
        ///     Joystick 4.
        /// </summary>
        Joystick5 = 4,

        /// <summary>
        ///     Joystick 5.
        /// </summary>
        Joystick6 = 5,

        /// <summary>
        ///     Joystick 6.
        /// </summary>
        Joystick7 = 6,

        /// <summary>
        ///     Joystick 7.
        /// </summary>
        Joystick8 = 7,

        /// <summary>
        ///     Joystick 8.
        /// </summary>
        Joystick9 = 8,

        /// <summary>
        ///     Joystick 9.
        /// </summary>
        Joystick10 = 9,

        /// <summary>
        ///     Joystick 10.
        /// </summary>
        Joystick11 = 10,

        /// <summary>
        ///     Joystick 11.
        /// </summary>
        Joystick12 = 11,

        /// <summary>
        ///     Joystick 12.
        /// </summary>
        Joystick13 = 12,

        /// <summary>
        ///     Joystick 13.
        /// </summary>
        Joystick14 = 13,

        /// <summary>
        ///     Joystick 14.
        /// </summary>
        Joystick15 = 14,

        /// <summary>
        ///     Joystick 15.
        /// </summary>
        Joystick16 = 15,

        /// <summary>
        ///     Last joystick.
        /// </summary>
        JoystickLast = Joystick16
    }

    /// <summary>
    ///     Keycodes for keyboards.
    /// </summary>
    public enum KeyCode
    {
        /// <summary>
        /// Uknown key.
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// Space key.
        /// </summary>
        Space = 32,

        /// <summary>
        /// Apostrophe key.
        /// </summary>
        Apostrophe = 39,

        /// <summary>
        /// Comma key.
        /// </summary>
        Comma = 44,

        /// <summary>
        /// Minus key.
        /// </summary>
        Minus = 45,

        /// <summary>
        /// Period key.
        /// </summary>
        Period = 46,

        /// <summary>
        /// Slash key.
        /// </summary>
        Slash = 47,

        /// <summary>
        /// Alpha0 key.
        /// </summary>
        Alpha0 = 48,

        /// <summary>
        /// Alpha1 key.
        /// </summary>
        Alpha1 = 49,

        /// <summary>
        /// Alpha2 key.
        /// </summary>
        Alpha2 = 50,

        /// <summary>
        /// Alpha3 key.
        /// </summary>
        Alpha3 = 51,

        /// <summary>
        /// Alpha4 key.
        /// </summary>
        Alpha4 = 52,

        /// <summary>
        /// Alpha5 key.
        /// </summary>
        Alpha5 = 53,

        /// <summary>
        /// Alpha6 key.
        /// </summary>
        Alpha6 = 54,

        /// <summary>
        /// Alpha7 key.
        /// </summary>
        Alpha7 = 55,

        /// <summary>
        /// Alpha8 key.
        /// </summary>
        Alpha8 = 56,

        /// <summary>
        /// Alpha9 key.
        /// </summary>
        Alpha9 = 57,

        /// <summary>
        /// Semicolon key.
        /// </summary>
        Semicolon = 59,

        /// <summary>
        /// Equal key.
        /// </summary>
        Equal = 61,

        /// <summary>
        /// A key.
        /// </summary>
        A = 65,

        /// <summary>
        /// B key.
        /// </summary>
        B = 66,

        /// <summary>
        /// C key.
        /// </summary>
        C = 67,

        /// <summary>
        /// D key.
        /// </summary>
        D = 68,

        /// <summary>
        /// E key.
        /// </summary>
        E = 69,

        /// <summary>
        /// F key.
        /// </summary>
        F = 70,

        /// <summary>
        /// G key.
        /// </summary>
        G = 71,

        /// <summary>
        /// H key.
        /// </summary>
        H = 72,

        /// <summary>
        /// I key.
        /// </summary>
        I = 73,

        /// <summary>
        /// J key.
        /// </summary>
        J = 74,

        /// <summary>
        /// K key.
        /// </summary>
        K = 75,

        /// <summary>
        /// L key.
        /// </summary>
        L = 76,

        /// <summary>
        /// M key.
        /// </summary>
        M = 77,

        /// <summary>
        /// N key.
        /// </summary>
        N = 78,

        /// <summary>
        /// O key.
        /// </summary>
        O = 79,

        /// <summary>
        /// P key.
        /// </summary>
        P = 80,

        /// <summary>
        /// Q key.
        /// </summary>
        Q = 81,

        /// <summary>
        /// R key.
        /// </summary>
        R = 82,

        /// <summary>
        /// S key.
        /// </summary>
        S = 83,

        /// <summary>
        /// T key.
        /// </summary>
        T = 84,

        /// <summary>
        /// U key.
        /// </summary>
        U = 85,

        /// <summary>
        /// V key.
        /// </summary>
        V = 86,

        /// <summary>
        /// W key.
        /// </summary>
        W = 87,

        /// <summary>
        /// X key.
        /// </summary>
        X = 88,

        /// <summary>
        /// Y key.
        /// </summary>
        Y = 89,

        /// <summary>
        /// Z key.
        /// </summary>
        Z = 90,

        /// <summary>
        /// LeftBracket key.
        /// </summary>
        LeftBracket = 91,

        /// <summary>
        /// Backslash key.
        /// </summary>
        Backslash = 92,

        /// <summary>
        /// RightBracket key.
        /// </summary>
        RightBracket = 93,

        /// <summary>
        /// GraveAccent key.
        /// </summary>
        GraveAccent = 96,

        /// <summary>
        /// World1 key.
        /// </summary>
        World1 = 161,

        /// <summary>
        /// World2 key.
        /// </summary>
        World2 = 162,

        /// <summary>
        /// Escape key.
        /// </summary>
        Escape = 256,

        /// <summary>
        /// Enter key.
        /// </summary>
        Enter = 257,

        /// <summary>
        /// Tab key.
        /// </summary>
        Tab = 258,

        /// <summary>
        /// Backspace key.
        /// </summary>
        Backspace = 259,

        /// <summary>
        /// Insert key.
        /// </summary>
        Insert = 260,

        /// <summary>
        /// Delete key.
        /// </summary>
        Delete = 261,

        /// <summary>
        /// Right key.
        /// </summary>
        Right = 262,

        /// <summary>
        /// Left key.
        /// </summary>
        Left = 263,

        /// <summary>
        /// Down key.
        /// </summary>
        Down = 264,

        /// <summary>
        /// Up key.
        /// </summary>
        Up = 265,

        /// <summary>
        /// PageUp key.
        /// </summary>
        PageUp = 266,

        /// <summary>
        /// PageDown key.
        /// </summary>
        PageDown = 267,

        /// <summary>
        /// Home key.
        /// </summary>
        Home = 268,

        /// <summary>
        /// End key.
        /// </summary>
        End = 269,

        /// <summary>
        /// CapsLock key.
        /// </summary>
        CapsLock = 280,

        /// <summary>
        /// ScrollLock key.
        /// </summary>
        ScrollLock = 281,

        /// <summary>
        /// NumLock key.
        /// </summary>
        NumLock = 282,

        /// <summary>
        /// PrintScreen key.
        /// </summary>
        PrintScreen = 283,

        /// <summary>
        /// Pause key.
        /// </summary>
        Pause = 284,

        /// <summary>
        /// F1 key.
        /// </summary>
        F1 = 290,

        /// <summary>
        /// F2 key.
        /// </summary>
        F2 = 291,

        /// <summary>
        /// F3 key.
        /// </summary>
        F3 = 292,

        /// <summary>
        /// F4 key.
        /// </summary>
        F4 = 293,

        /// <summary>
        /// F5 key.
        /// </summary>
        F5 = 294,

        /// <summary>
        /// F6 key.
        /// </summary>
        F6 = 295,

        /// <summary>
        /// F7 key.
        /// </summary>
        F7 = 296,

        /// <summary>
        /// F8 key.
        /// </summary>
        F8 = 297,

        /// <summary>
        /// F9 key.
        /// </summary>
        F9 = 298,

        /// <summary>
        /// F10 key.
        /// </summary>
        F10 = 299,

        /// <summary>
        /// F11 key.
        /// </summary>
        F11 = 300,

        /// <summary>
        /// F12 key.
        /// </summary>
        F12 = 301,

        /// <summary>
        /// F13 key.
        /// </summary>
        F13 = 302,

        /// <summary>
        /// F14 key.
        /// </summary>
        F14 = 303,

        /// <summary>
        /// F15 key.
        /// </summary>
        F15 = 304,

        /// <summary>
        /// F16 key.
        /// </summary>
        F16 = 305,

        /// <summary>
        /// F17 key.
        /// </summary>
        F17 = 306,

        /// <summary>
        /// F18 key.
        /// </summary>
        F18 = 307,

        /// <summary>
        /// F19 key.
        /// </summary>
        F19 = 308,

        /// <summary>
        /// F20 key.
        /// </summary>
        F20 = 309,

        /// <summary>
        /// F21 key.
        /// </summary>
        F21 = 310,

        /// <summary>
        /// F22 key.
        /// </summary>
        F22 = 311,

        /// <summary>
        /// F23 key.
        /// </summary>
        F23 = 312,

        /// <summary>
        /// F24 key.
        /// </summary>
        F24 = 313,

        /// <summary>
        /// F25 key.
        /// </summary>
        F25 = 314,

        /// <summary>
        /// Keypad0 key.
        /// </summary>
        Keypad0 = 320,

        /// <summary>
        /// Keypad1 key.
        /// </summary>
        Keypad1 = 321,

        /// <summary>
        /// Keypad2 key.
        /// </summary>
        Keypad2 = 322,

        /// <summary>
        /// Keypad3 key.
        /// </summary>
        Keypad3 = 323,

        /// <summary>
        /// Keypad4 key.
        /// </summary>
        Keypad4 = 324,

        /// <summary>
        /// Keypad5 key.
        /// </summary>
        Keypad5 = 325,

        /// <summary>
        /// Keypad6 key.
        /// </summary>
        Keypad6 = 326,

        /// <summary>
        /// Keypad7 key.
        /// </summary>
        Keypad7 = 327,

        /// <summary>
        /// Keypad8 key.
        /// </summary>
        Keypad8 = 328,

        /// <summary>
        /// Keypad9 key.
        /// </summary>
        Keypad9 = 329,

        /// <summary>
        /// KeypadDecimal key.
        /// </summary>
        KeypadDecimal = 330,

        /// <summary>
        /// KeypadDivide key.
        /// </summary>
        KeypadDivide = 331,

        /// <summary>
        /// KeypadMultiply key.
        /// </summary>
        KeypadMultiply = 332,

        /// <summary>
        /// KeypadSubtract key.
        /// </summary>
        KeypadSubtract = 333,

        /// <summary>
        /// KeypadAdd key.
        /// </summary>
        KeypadAdd = 334,

        /// <summary>
        /// KeypadEnter key.
        /// </summary>
        KeypadEnter = 335,

        /// <summary>
        /// KeypadEqual key.
        /// </summary>
        KeypadEqual = 336,

        /// <summary>
        /// LeftShift key.
        /// </summary>
        LeftShift = 340,

        /// <summary>
        /// LeftControl key.
        /// </summary>
        LeftControl = 341,

        /// <summary>
        /// LeftAlt key.
        /// </summary>
        LeftAlt = 342,

        /// <summary>
        /// LeftSuper key.
        /// </summary>
        LeftSuper = 343,

        /// <summary>
        /// RightShift key.
        /// </summary>
        RightShift = 344,

        /// <summary>
        /// RightControl key.
        /// </summary>
        RightControl = 345,

        /// <summary>
        /// RightAlt key.
        /// </summary>
        RightAlt = 346,

        /// <summary>
        /// RightSuper key.
        /// </summary>
        RightSuper = 347,

        /// <summary>
        /// Menu key.
        /// </summary>
        Menu = 348
    }

    /// <summary>
    ///     Modifier key for keyboard.
    /// </summary>
    public enum ModifierKey : int
    {
        /// <summary>
        /// Shift key.
        /// </summary>
        Shift = 0x0001,

        /// <summary>
        /// Control key.
        /// </summary>
        Control = 0x0002,

        /// <summary>
        /// Alt key.
        /// </summary>
        Alt = 0x0004,

        /// <summary>
        /// Super key.
        /// </summary>
        Super = 0x0008,

        /// <summary>
        /// CapsLock key.
        /// </summary>
        CapsLock = 0x0010,

        /// <summary>
        /// NumLock key.
        /// </summary>
        NumLock = 0x0020
    }

    /// <summary>
    ///     Mouse buttons.
    /// </summary>
    public enum MouseButton : int
    {
        /// <summary>
        /// Button 1.
        /// </summary>
        Button1 = 0,

        /// <summary>
        /// Button 1.
        /// </summary>
        Button2 = 1,

        /// <summary>
        /// Button 1.
        /// </summary>
        Button3 = 2,

        /// <summary>
        /// Button 1.
        /// </summary>
        Button4 = 3,

        /// <summary>
        /// Button 1.
        /// </summary>
        Button5 = 4,

        /// <summary>
        /// Button 1.
        /// </summary>
        Button6 = 5,

        /// <summary>
        /// Button 1.
        /// </summary>
        Button7 = 6,

        /// <summary>
        /// Button 1.
        /// </summary>
        Button8 = 7,

        /// <summary>
        /// Last button.
        /// </summary>
        Last = Button8,

        /// <summary>
        /// Last button.
        /// </summary>
        Left = Button1,

        /// <summary>
        /// Last button.
        /// </summary>
        Right = Button2,

        /// <summary>
        /// Last button.
        /// </summary>
        Middle = Button3
    }

    /// <summary>
    ///     Cursor shapes.
    /// </summary>
    public enum CursorShape : int
    {
        /// <summary>
        ///     Arrow cursor shape.
        /// </summary>
        Arrow = 0x00036001,

        /// <summary>
        ///     IBeam cursor shape.
        /// </summary>
        IBeam = 0x00036002,

        /// <summary>
        ///     Crosshair cursor shape.
        /// </summary>
        Crosshair = 0x00036003,

        /// <summary>
        ///     Hand cursor shape.
        /// </summary>
        Hand = 0x00036004,

        /// <summary>
        ///     Hresize cursor shape.
        /// </summary>
        Hresize = 0x00036005,

        /// <summary>
        ///     VResize cursor shape.
        /// </summary>
        VResize = 0x00036006,
    }

    /// <summary>
    ///     Connection state.
    /// </summary>
    public enum Connection : int
    {
        /// <summary>
        ///     Connected state.
        /// </summary>
        Connected = 0x00040001,

        /// <summary>
        ///     Disconnected state.
        /// </summary>
        Disconnected = 0x00040002
    }

    /// <summary>
    ///     Input types.
    /// </summary>
    public enum InputType : int
    {
        /// <summary>
        ///     Cursor input mode.
        /// </summary>
        Cursor = 0x00033001,

        /// <summary>
        ///     StickyKeys input mode.
        /// </summary>
        StickyKeys = 0x00033002,

        /// <summary>
        ///     StickyMouseButtons input mode.
        /// </summary>
        StickyMouseButtons = 0x00033003,

        /// <summary>
        ///     LockKeyMods input mode.
        /// </summary>
        LockKeyMods = 0x00033004,

        /// <summary>
        ///     RawMouseMotion input mode.
        /// </summary>
        RawMouseMotion = 0x00033005
    }

    /// <summary>
    ///     Cursor modes
    /// </summary>
    public enum CursorMode : int
    {
        /// <summary>
        ///     Normal cursor type.
        /// </summary>
        Normal = 0x00034001,

        /// <summary>
        ///     Hidden cursor type.
        /// </summary>
        Hidden = 0x00034002,

        /// <summary>
        ///     Disabled cursor type.
        /// </summary>
        Disabled = 0x00034003,
    }
}
