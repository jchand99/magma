# ChangeLog

All notable changes will be added to this changelog.
The format is based on [Keep a Changelog](http://keepachangelog.com/)

## [Version-1.0.0] - 2021-05-08
### Added
- OpenGL:
    - Gl static class for OpenGL 4.0+ functions
    - Bug fixes
    - Method overloads
    - Improved Enum types and methods
- OpenAL
    - OpenAL 1.1 static functions.
    - Al static class
    - Alc static class
- Vulkan:
    - Full standard, EXT and KHR support
    - Bug fixes
    - Improved structs
    - Lambdas
- Glfw:
    - Bug fixes
    - Lambdas
    - Improved Structs and methods
- Logger
    - Bug Fixes
    - Assert logic fix
    - Debugger attributes
    - Improved readability in log message
- Maths
    - Matrix4/3/2 float/double/integer
    - Vector4/3/2 float/double/integer
    - Complete documentation

- Moved to dotnet 5
    - using new net5.0 assemblies
- Delegate pointers!
    - using all new `delegate*<>` syntax so function calls are super fast!

## [Unreleased]

### Not Implemented
- ObjectLoader
