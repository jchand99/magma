using System;

namespace Magma.OpenAL.ALC
{
    public static partial class Alc
    {
        internal unsafe class Delegates
        {
            internal static readonly delegate*<IntPtr, int*, IntPtr> alcCreateContext = (delegate*<IntPtr, int*, IntPtr>) OpenAL.GetStaticProcPointer("alcCreateContext");
            internal static readonly delegate*<IntPtr, bool> alcMakeContextCurrent = (delegate*<IntPtr, bool>) OpenAL.GetStaticProcPointer("alcMakeContextCurrent");
            internal static readonly delegate*<IntPtr, void> alcProcessContext = (delegate*<IntPtr, void>) OpenAL.GetStaticProcPointer("alcProcessContext");
            internal static readonly delegate*<IntPtr, void> alcSuspendContext = (delegate*<IntPtr, void>) OpenAL.GetStaticProcPointer("alcSuspendContext");
            internal static readonly delegate*<IntPtr, void> alcDestroyContext = (delegate*<IntPtr, void>) OpenAL.GetStaticProcPointer("alcDestroyContext");
            internal static readonly delegate*<IntPtr> alcGetCurrentContext = (delegate*<IntPtr>) OpenAL.GetStaticProcPointer("alcGetCurrentContext");
            internal static readonly delegate*<IntPtr, IntPtr> alcGetContextsDevice = (delegate*<IntPtr, IntPtr>) OpenAL.GetStaticProcPointer("alcGetContextsDevice");
            internal static readonly delegate*<byte*, IntPtr> alcOpenDevice = (delegate*<byte*, IntPtr>) OpenAL.GetStaticProcPointer("alcOpenDevice");
            internal static readonly delegate*<IntPtr, bool> alcCloseDevice = (delegate*<IntPtr, bool>) OpenAL.GetStaticProcPointer("alcCloseDevice");
            internal static readonly delegate*<IntPtr, AlcResult> alcGetError = (delegate*<IntPtr, AlcResult>) OpenAL.GetStaticProcPointer("alcGetError");
            internal static readonly delegate*<IntPtr, byte*, bool> alcIsExtensionPresent = (delegate*<IntPtr, byte*, bool>) OpenAL.GetStaticProcPointer("alcIsExtensionPresent");
            internal static readonly delegate*<IntPtr, byte*, IntPtr> alcGetProcAddress = (delegate*<IntPtr, byte*, IntPtr>) OpenAL.GetStaticProcPointer("alcGetProcAddress");
            internal static readonly delegate*<IntPtr, byte*, int> alcGetEnumValue = (delegate*<IntPtr, byte*, int>) OpenAL.GetStaticProcPointer("alcGetEnumValue");
            internal static readonly delegate*<IntPtr, AlcContextString, byte*> alcGetString = (delegate*<IntPtr, AlcContextString, byte*>) OpenAL.GetStaticProcPointer("alcGetString");
            internal static readonly delegate*<IntPtr, IntegerAttrib, int, int*, void> alcGetIntegerv = (delegate*<IntPtr, IntegerAttrib, int, int*, void>) OpenAL.GetStaticProcPointer("alcGetIntegerv");
            internal static readonly delegate*<byte*, int, int, int, IntPtr> alcCaptureOpenDevice = (delegate*<byte*, int, int, int, IntPtr>) OpenAL.GetStaticProcPointer("alcCaptureOpenDevice");
            internal static readonly delegate*<IntPtr, bool> alcCaptureCloseDevice = (delegate*<IntPtr, bool>) OpenAL.GetStaticProcPointer("alcCaptureCloseDevice");
            internal static readonly delegate*<IntPtr, void> alcCaptureStart = (delegate*<IntPtr, void>) OpenAL.GetStaticProcPointer("alcCaptureStart");
            internal static readonly delegate*<IntPtr, void> alcCaptureStop = (delegate*<IntPtr, void>) OpenAL.GetStaticProcPointer("alcCaptureStop");
            internal static readonly delegate*<IntPtr, IntPtr, int, void> alcCaptureSamples= (delegate*<IntPtr, IntPtr, int, void>) OpenAL.GetStaticProcPointer("alcCaptureSamples");
        }
    }
}
