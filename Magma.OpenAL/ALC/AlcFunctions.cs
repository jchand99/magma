using System;
using System.Text;

namespace Magma.OpenAL.ALC
{
    public static unsafe partial class Alc
    {
        public static IntPtr CreateContext(IntPtr device, int[] attrlist)
        {
            fixed (int* attrListPtr = attrlist)
                return Delegates.alcCreateContext(device, attrListPtr);
        }
        public static bool MakeContextCurrent(IntPtr context) => Delegates.alcMakeContextCurrent(context);
        public static void ProcessContext(IntPtr context) => Delegates.alcProcessContext(context);
        public static void SuspendContext(IntPtr context) => Delegates.alcSuspendContext(context);
        public static void DestroyContext(IntPtr context) => Delegates.alcDestroyContext(context);
        public static IntPtr GetCurrentContext() => Delegates.alcGetCurrentContext();
        public static IntPtr GetContextsDevice(IntPtr context) => Delegates.alcGetContextsDevice(context);
        public static IntPtr OpenDevice(string deviceName)
        {
            fixed (byte* deviceNamePtr = Encoding.UTF8.GetBytes(deviceName))
                return Delegates.alcOpenDevice(deviceNamePtr);
        }
        public static bool CloseDevice(IntPtr device) => Delegates.alcCloseDevice(device);
        public static AlcResult GetError(IntPtr device) => Delegates.alcGetError(device);
        public static bool IsExtensionPresent(IntPtr device, string extname)
        {
            fixed (byte* extnamePtr = Encoding.UTF8.GetBytes(extname))
                return Delegates.alcIsExtensionPresent(device, extnamePtr);
        }
        public static IntPtr GetProcAddress(IntPtr device, string funcname)
        {
            fixed (byte* funcnamePtr = Encoding.UTF8.GetBytes(funcname))
                return Delegates.alcGetProcAddress(device, funcnamePtr);
        }
        public static int GetEnumValue(IntPtr device, string enumname)
        {
            fixed (byte* enumnamePtr = Encoding.UTF8.GetBytes(enumname))
                return Delegates.alcGetEnumValue(device, enumnamePtr);
        }
        public static string GetString(IntPtr device, AlcContextString contextString)
        {
            byte* ptr = Delegates.alcGetString(device, contextString);
            byte* walkPtr = ptr;
            while (*walkPtr != 0) walkPtr++;
            return Encoding.UTF8.GetString(ptr, (int)(walkPtr - ptr));
        }
#nullable enable
        public static void GetIntegerv(IntPtr device, IntegerAttrib integerAttrib, int size, out int[]? values)
        {
            if (size == 0)
            {
                values = null;
                return;
            }

            var arr = stackalloc int[size];
            values = new int[size];
            Delegates.alcGetIntegerv(device, integerAttrib, size, arr);
            for (int i = 0; i < size; i++)
                values[i] = arr[i];
        }
#nullable disable
        public static IntPtr CaptureOpenDevice(string devicename, int frequency, int format, int buffersize)
        {
            fixed (byte* devicenamePtr = Encoding.UTF8.GetBytes(devicename))
                return Delegates.alcCaptureOpenDevice(devicenamePtr, frequency, format, buffersize);
        }
        public static bool CaptureCloseDevice(IntPtr device) => Delegates.alcCaptureCloseDevice(device);
        public static void CaptureStart(IntPtr device) => Delegates.alcCaptureStart(device);
        public static void CaptureStop(IntPtr device) => Delegates.alcCaptureStop(device);
        public static void CaptureSamples(IntPtr device, IntPtr buffer, int samples) => Delegates.alcCaptureSamples(device, buffer, samples);
    }
}
