namespace Magma.OpenAL.ALC
{
    public enum ContextProperty
    {
        None = 0,
        Frequency = 0x1007,
        MonoSources = 0x1010,
        StereoSources = 0x1011,
        Refresh = 0x1008,
        Sync = 0x1009
    }

    public enum AlcResult
    {
        NoError = 0,
        InvalidDevice = 0xA001,
        InvalidContext = 0xA002,
        InvalidEnum = 0xA003,
        InvalidValue = 0xA004,
        OutOfMemory = 0xA005
    }

    public enum AlcContextString
    {
        None = 0,
        DefaultDeviceSpecifier = 0x1004,
        CaptureDefaultDeviceSpecifier = 0x311,
        DeviceSpecifier = 0x1005,
        CaptureDeviceSpecifier = 0x310,
        Extensions = 0x1006
    }

    public enum IntegerAttrib
    {
        None = 0,
        MajorVersion = 0x1000,
        MinorVersion = 0x1001,
        AtributeSize = 0x1002,
        AllAttributes = 0x1003
    }
}
