## Magma.OpenAL

This library tries to stay as close to the unmanaged function declarations as possible with a few variations and overrides.
You should be able to follow any OpenAL tutorial without issue.
