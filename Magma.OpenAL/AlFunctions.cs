using System;
using System.Text;

namespace Magma.OpenAL
{
    public unsafe partial class Al
    {
        public static void DopplerFactor(float value) => Delegates.alDopplerFactor(value);
        public static void DopplerVelocity(float value) => Delegates.alDopplerVelocity(value);
        public static void SpeedOfSound(float value) => Delegates.alSpeedOfSound(value);
        public static void DistanceModel(DistanceModel distanceModel) => Delegates.alDistanceModel(distanceModel);
        public static void Enable(int capability) => Delegates.alEnable(capability);
        public static void Disable(int capability) => Delegates.alDisable(capability);
        public static bool IsEnabled(int capability) => Delegates.alIsEnabled(capability);
        public static string GetString(AlContextString contextString) => Delegates.alGetString(contextString);
        public static void GetBooleanv(StateProperty stateProperty, out bool data)
        {
            fixed(bool *dataPtr = &data)
            Delegates.alGetBooleanv(stateProperty, dataPtr);
        }
        public static void GetIntegerv(StateProperty stateProperty, out int data)
        {
            fixed(int *dataPtr = &data)
            Delegates.alGetIntegerv(stateProperty, dataPtr);
        }
        public static void GetFloatv(StateProperty stateProperty, out float data)
        {
            fixed(float *dataPtr = &data)
            Delegates.alGetFloatv(stateProperty, dataPtr);
        }
        public static void GetDoublev(StateProperty stateProperty, out double data)
        {
            fixed(double *dataPtr = &data)
            Delegates.alGetDoublev(stateProperty, dataPtr);
        }
        public static bool GetBoolean(StateProperty stateProperty) => Delegates.alGetBoolean(stateProperty);
        public static int GetInteger(StateProperty stateProperty) => Delegates.alGetInteger(stateProperty);
        public static float GetFloat(StateProperty stateProperty) => Delegates.alGetFloat(stateProperty);
        public static double GetDouble(StateProperty stateProperty) => Delegates.alGetDouble(stateProperty);
        public static AlResult GetError() => Delegates.alGetError();
        public static bool IsExtensionPresent(string extname)
        {
            fixed(byte *extnamePtr = Encoding.UTF8.GetBytes(extname))
            return Delegates.alIsExtensionPresent(extnamePtr);
        }
        public static IntPtr GetProcAddress(string fname)
        {
            fixed(byte *fnamePtr = Encoding.UTF8.GetBytes(fname))
            return Delegates.alGetProcAddress(fnamePtr);
        }
        public static int GetEnumValue(string ename)
        {
            fixed(byte *enamePtr = Encoding.UTF8.GetBytes(ename))
            return Delegates.alGetEnumValue(enamePtr);
        }

        public static void Listenerf(ListenerProperty listenerProperty, float value) => Delegates.alListenerf(listenerProperty, value);
        public static void Listener3f(ListenerProperty listenerProperty, float value1, float value2, float value3) => Delegates.alListener3f(listenerProperty, value1, value2, value3);
        public static void Listenerfv(ListenerProperty listenerProperty, float[] values)
        {
            fixed(float *valuesPtr = values)
            Delegates.alListenerfv(listenerProperty, valuesPtr);
        }
        public static void Listeneri(ListenerProperty listenerProperty, int value) => Delegates.alListeneri(listenerProperty, value);
        public static void Listener3i(ListenerProperty listenerProperty, int value1, int value2, int value3) => Delegates.alListener3i(listenerProperty, value1, value2, value3);
        public static void Listeneriv(ListenerProperty listenerProperty, int[] values)
        {
            fixed(int *valuesPtr = values)
            Delegates.alListeneriv(listenerProperty, valuesPtr);
        }
        public static void GetListenerf(ListenerProperty listenerProperty, out float value)
        {
            fixed(float *valuePtr = &value)
            Delegates.alGetListenerf(listenerProperty, valuePtr);
        }
        public static void GetListener3f(ListenerProperty listenerProperty, out float value1, out float value2, out float value3)
        {
            fixed(float *value1Ptr = &value1)
            fixed(float *value2Ptr = &value2)
            fixed(float *value3Ptr = &value3)
            Delegates.alGetListener3f(listenerProperty, value1Ptr, value2Ptr, value3Ptr);
        }
        public static void GetListenerfv(ListenerProperty listenerProperty, out float[] values, int count)
        {
            var arr = stackalloc float[count];
            values = new float[count];
            Delegates.alGetListenerfv(listenerProperty, arr);
            for(int i = 0; i < count; i++)
                values[i] = arr[i];
        }
        public static void GetListeneri(ListenerProperty listenerProperty, out int value)
        {
            fixed(int *valuePtr = &value)
            Delegates.alGetListeneri(listenerProperty, valuePtr);
        }
        public static void GetListener3i(ListenerProperty listenerProperty, out int value1, out int value2, out int value3)
        {
            fixed(int *value1Ptr = &value1)
            fixed(int *value2Ptr = &value2)
            fixed(int *value3Ptr = &value3)
            Delegates.alGetListener3i(listenerProperty, value1Ptr, value2Ptr, value3Ptr);
        }
        public static void GetListeneriv(ListenerProperty listenerProperty, out int[] values, int count)
        {
            var arr = stackalloc int[count];
            values = new int[count];
            Delegates.alGetListeneriv(listenerProperty, arr);
            for(int i = 0; i < count; i++)
                values[i] = arr[i];
        }
        public static void GenSources(int n, out uint[] sources)
        {
            var arr = stackalloc uint[n];
            sources = new uint[n];
            Delegates.alGenSources(n, arr);
            for(int i = 0; i < n; i++)
                sources[i] = arr[i];
        }
        public static void DeleteSources(int n, uint[] sources)
        {
            fixed(uint *sourcesPtr = sources)
            Delegates.alDeleteSources(n, sourcesPtr);
        }
        public static bool IsSource(uint source) => Delegates.alIsSource(source);
        public static void Sourcef(uint source, SourceProperty sourceProperty, float value) => Delegates.alSourcef(source, sourceProperty, value);
        public static void Source3f(uint source, SourceProperty sourceProperty, float value1, float value2, float value3) => Delegates.alSource3f(source, sourceProperty, value1, value2, value3);
        public static void Sourcefv(uint source, SourceProperty sourceProperty, float[] values)
        {
            fixed(float *valuesPtr = values)
            Delegates.alSourcefv(source, sourceProperty, valuesPtr);
        }
        public static void Sourcei(uint source, SourceProperty sourceProperty, int value) => Delegates.alSourcei(source, sourceProperty, value);
        public static void Source3i(uint source, SourceProperty sourceProperty, int value1, int value2, int value3) => Delegates.alSource3i(source, sourceProperty, value1, value2, value3);
        public static void Sourceiv(uint source, SourceProperty sourceProperty, int[] values)
        {
            fixed(int *valuesPtr = values)
            Delegates.alSourceiv(source, sourceProperty, valuesPtr);
        }
        public static void GetSourcef(uint source, SourceProperty sourceProperty, out float value)
        {
            fixed(float *valuePtr = &value)
            Delegates.alGetSourcef(source, sourceProperty, valuePtr);
        }
        public static void GetSource3f(uint source, SourceProperty sourceProperty, out float value1, out float value2, out float value3)
        {
            fixed(float *value1Ptr = &value1)
            fixed(float *value2Ptr = &value2)
            fixed(float *value3Ptr = &value3)
            Delegates.alGetSource3f(source, sourceProperty, value1Ptr, value2Ptr, value3Ptr);
        }
        public static void GetSourcefv(uint source, SourceProperty sourceProperty, out float[] values, int count)
        {
            var arr = stackalloc float[count];
            values = new float[count];
            Delegates.alGetSourcefv(source, sourceProperty, arr);
            for(int i = 0; i < count; i++)
                values[i] = arr[i];
        }
        public static void GetSourcei(uint source,  SourceProperty sourceProperty, out int value)
        {
            fixed(int *valuePtr = &value)
            Delegates.alGetSourcei(source, sourceProperty, valuePtr);
        }
        public static void GetSource3i(uint source, SourceProperty sourceProperty, out int value1, out int value2, out int value3)
        {
            fixed(int *value1Ptr = &value1)
            fixed(int *value2Ptr = &value2)
            fixed(int *value3Ptr = &value3)
            Delegates.alGetSource3i(source, sourceProperty, value1Ptr, value2Ptr, value3Ptr);
        }
        public static void GetSourceiv(uint source,  SourceProperty sourceProperty, out int[] values, int count)
        {
            var arr = stackalloc int[count];
            values = new int[count];
            Delegates.alGetSourceiv(source, sourceProperty, arr);
            for(int i = 0; i < count; i++)
                values[i] = arr[i];
        }
        public static void SourcePlayv(int n, uint[] sources)
        {
            fixed(uint *sourcesPtr = sources)
            Delegates.alSourcePlayv(n, sourcesPtr);
        }
        public static void SourceStopv(int n, uint[] sources)
        {
            fixed(uint *sourcesPtr = sources)
            Delegates.alSourceStopv(n, sourcesPtr);
        }
        public static void SourceRewindv(int n, uint[] sources)
        {
            fixed(uint *sourcesPtr = sources)
            Delegates.alSourceRewindv(n, sourcesPtr);
        }
        public static void SourcePausev(int n, uint[] sources)
        {
            fixed(uint *sourcesPtr = sources)
            Delegates.alSourcePausev(n, sourcesPtr);
        }
        public static void SourcePlay(uint source) => Delegates.alSourcePlay(source);
        public static void SourceStop(uint source) => Delegates.alSourceStop(source);
        public static void SourceRewind(uint source) => Delegates.alSourceRewind(source);
        public static void SourcePause(uint source) => Delegates.alSourcePause(source);
        public static void SourceQueueBuffers(uint source, int nb, uint[] buffers)
        {
            fixed(uint *buffersPtr = buffers)
            Delegates.alSourceQueueBuffers(source, nb, buffersPtr);
        }
        public static void SourceUnqueueBuffers(uint source, int nb, uint[] buffers)
        {
            fixed(uint *buffersPtr = buffers)
            Delegates.alSourceUnqueueBuffers(source, nb, buffersPtr);
        }

        public static void GenBuffers(int n, out uint[] buffers)
        {
            var arr = stackalloc uint[n];
            buffers = new uint[n];
            Delegates.alGenBuffers(n, arr);
            for(int i = 0; i < n; i++)
                buffers[i] = arr[i];
        }
        public static void DeleteBuffers(int n, uint[] buffers)
        {
            fixed(uint *buffersPtr = buffers)
            Delegates.alDeleteBuffers(n, buffersPtr);
        }
        public static bool IsBuffer(uint buffer) => Delegates.alIsBuffer(buffer);
        public static void BufferData<T>(uint buffer, BufferFormat bufferFormat, T[] data, int size, int freq) where T : unmanaged
        {
            fixed(T *p = data)
            Delegates.alBufferData(buffer, bufferFormat, p, size, freq);
        }
        public static void Bufferf(uint buffer, BufferProperty bufferProperty, float value) => Delegates.alBufferf(buffer, bufferProperty, value);
        public static void Buffer3f(uint buffer, BufferProperty bufferProperty, float value1, float value2, float value3) => Delegates.alBuffer3f(buffer, bufferProperty, value1, value2, value3);
        public static void Bufferfv(uint buffer, BufferProperty bufferProperty, float[] values)
        {
            fixed(float *valuesPtr = values)
            Delegates.alBufferfv(buffer, bufferProperty, valuesPtr);
        }
        public static void Bufferi(uint buffer, BufferProperty bufferProperty, int value) => Delegates.alBufferi(buffer, bufferProperty, value);
        public static void Buffer3i(uint buffer, BufferProperty bufferProperty, int value1, int value2, int value3) => Delegates.alBuffer3i(buffer, bufferProperty, value1, value2, value3);
        public static void Bufferiv(uint buffer, BufferProperty bufferProperty, int[] values)
        {
            fixed(int *valuesPtr = values)
            Delegates.alBufferiv(buffer, bufferProperty, valuesPtr);
        }
        public static void GetBufferf(uint buffer, BufferProperty bufferProperty, out float value)
        {
            fixed(float *valuePtr = &value)
            Delegates.alGetBufferf(buffer, bufferProperty, valuePtr);
        }
        public static void GetBuffer3f(uint buffer, BufferProperty bufferProperty, out float value1, out float value2, out float value3)
        {
            fixed(float *value1Ptr = &value1)
            fixed(float *value2Ptr = &value2)
            fixed(float *value3Ptr = &value3)
            Delegates.alGetBuffer3f(buffer, bufferProperty, value1Ptr, value2Ptr, value3Ptr);
        }
        public static void GetBufferfv(uint buffer, BufferProperty bufferProperty, out float[] values, int count)
        {
            var arr = stackalloc float[count];
            values = new float[count];
            Delegates.alGetBufferfv(buffer, bufferProperty, arr);
            for(int i = 0; i < count; i++)
                values[i] = arr[i];
        }
        public static void GetBufferi(uint buffer, BufferProperty bufferProperty, out int value)
        {
            fixed(int *valuePtr = &value)
            Delegates.alGetBufferi(buffer, bufferProperty, valuePtr);
        }
        public static void GetBuffer3i(uint buffer, BufferProperty bufferProperty, out int value1, out int value2, out int value3)
        {
            fixed(int *value1Ptr = &value1)
            fixed(int *value2Ptr = &value2)
            fixed(int *value3Ptr = &value3)
            Delegates.alGetBuffer3i(buffer, bufferProperty, value1Ptr, value2Ptr, value3Ptr);
        }
        public static void GetBufferiv(uint buffer, BufferProperty bufferProperty, out int[] values, int count)
        {
            var arr = stackalloc int[count];
            values = new int[count];
            Delegates.alGetBufferiv(buffer, bufferProperty, arr);
            for(int i = 0; i < count; i++)
                values[i] = arr[i];
        }
    }
}
