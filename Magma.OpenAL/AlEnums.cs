namespace Magma.OpenAL
{
    public enum BufferProperty
    {
        None = 0,
        Frequency = 0x2001,
        Bits = 0x2002,
        Channels = 0x2003,
        Size = 0x2004,
    }

    public enum StateProperty
    {
        None = 0,
        DistanceModel = 0xD000,
        DopplerFactor = 0xC000,
        SpeedOfSound = 0xC003,
    }

    public enum ListenerProperty
    {
        None = 0,
        Gain = 0x100A,
        Position = 0x1004,
        Velocity = 0x1006,
        Orientation = 0x100F,
    }

    public enum SourceProperty
    {
        None = 0,
        Pitch = 0x1003,
        Gain = 0x100A,
        MaxDistance = 0x1023,
        RollOffFactor = 0x1021,
        ReferenceDistance = 0x1020,
        MinGain = 0x100D,
        MaxGain = 0x100E,
        ConeOuterGain = 0x1022,
        ConeInnerAngle = 0x1001,
        ConeOuterAngle = 0x1002,
        Position = 0x1004,
        Velocity = 0x1006,
        Direction = 0x1005,
        Relative = 0x202,
        Type = 0x1027,
        Looping = 0x1007,
        Buffer = 0x1009,
        State = 0x1010,
        BuffersQueued = 0x1015,
        BuffersProcessed = 0x1016,
        SecOffset = 0x1024,
        SampleOffset = 0x1025,
        ByteOffset = 0x1026,
    }

    public enum SoundState
    {
        None = 0,
        Initial = 0x1011,
        Playing = 0x1012,
        Paused = 0x1013,
        Stopped = 0x1014
    }

    public enum SourceType
    {
        None = 0,
        Static = 0x1028,
        Streaming = 0x1029,
        Undetermined = 0x1030,
    }

    public enum BufferFormat
    {
        None = 0,
        Mono8 = 0x1100,
        Mono16 = 0x1101,
        Stereo8 = 0x1102,
        Stereo16 = 0x1103,
    }

    public enum BufferState
    {
        None = 0,
        Unused = 0x2010,
        Pending = 0x2011,
        Processed = 0x2012
    }

    public enum AlResult
    {
        NoError = 0,
        InvalidName = 0xA001,
        InvalidEnum = 0xA002,
        InvalidValue = 0xA003,
        InvalidOperation = 0xA004,
        OutOfMemory = 0xA005
    }

    public enum AlContextString
    {
        None = 0,
        Vendor = 0xB001,
        Version = 0xB002,
        Renderer = 0xB003,
        Extensions = 0xB004
    }

    public enum DistanceModel
    {
        None = 0,
        InverseDistance = 0xD001,
        InverseDistanceClamped = 0xD002,
        LinearDistance = 0xD003,
        LinearDistanceClamped = 0xD004,
        ExponentDistance = 0xD005,
        ExponentDistanceClamped = 0xD006,
    }
}
