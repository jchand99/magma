using System;

namespace Magma.OpenAL
{
    public partial class Al
    {
        internal unsafe class Delegates
        {
            internal static readonly delegate*<float, void> alDopplerFactor = (delegate*<float, void>) OpenAL.GetStaticProcPointer("alDopplerFactor");
            internal static readonly delegate*<float, void> alDopplerVelocity = (delegate*<float, void>) OpenAL.GetStaticProcPointer("alDopplerVelocity");
            internal static readonly delegate*<float, void> alSpeedOfSound = (delegate*<float, void>) OpenAL.GetStaticProcPointer("alSpeedOfSound");
            internal static readonly delegate*<DistanceModel, void> alDistanceModel = (delegate*<DistanceModel, void>) OpenAL.GetStaticProcPointer("alDistanceModel");
            internal static readonly delegate*<int, void> alEnable = (delegate*<int, void>) OpenAL.GetStaticProcPointer("alEnable");
            internal static readonly delegate*<int, void> alDisable = (delegate*<int, void>) OpenAL.GetStaticProcPointer("alDisable");
            internal static readonly delegate*<int, bool> alIsEnabled = (delegate*<int, bool>) OpenAL.GetStaticProcPointer("alIsEnabled");
            internal static readonly delegate*<AlContextString, string> alGetString = (delegate*<AlContextString, string>) OpenAL.GetStaticProcPointer("alGetString");
            internal static readonly delegate*<StateProperty, bool*, void> alGetBooleanv = (delegate*<StateProperty, bool*, void>) OpenAL.GetStaticProcPointer("alGetBooleanv");
            internal static readonly delegate*<StateProperty, int*, void> alGetIntegerv = (delegate*<StateProperty, int*, void>) OpenAL.GetStaticProcPointer("alGetIntegerv");
            internal static readonly delegate*<StateProperty, float*, void> alGetFloatv = (delegate*<StateProperty, float*, void>) OpenAL.GetStaticProcPointer("alGetFloatv");
            internal static readonly delegate*<StateProperty, double*, void> alGetDoublev = (delegate*<StateProperty, double*, void>) OpenAL.GetStaticProcPointer("alGetDoublev");
            internal static readonly delegate*<StateProperty, bool> alGetBoolean = (delegate*<StateProperty, bool>) OpenAL.GetStaticProcPointer("alGetBoolean");
            internal static readonly delegate*<StateProperty, int> alGetInteger = (delegate*<StateProperty, int>) OpenAL.GetStaticProcPointer("alGetInteger");
            internal static readonly delegate*<StateProperty, float> alGetFloat = (delegate*<StateProperty, float>) OpenAL.GetStaticProcPointer("alGetFloat");
            internal static readonly delegate*<StateProperty, double> alGetDouble = (delegate*<StateProperty, double>) OpenAL.GetStaticProcPointer("alGetDouble");
            internal static readonly delegate*<AlResult> alGetError = (delegate*<AlResult>) OpenAL.GetStaticProcPointer("alGetError");
            internal static readonly delegate*<byte*, bool> alIsExtensionPresent = (delegate*<byte*, bool>) OpenAL.GetStaticProcPointer("alIsExtensionPresent");
            internal static readonly delegate*<byte*, IntPtr> alGetProcAddress = (delegate*<byte*, IntPtr>) OpenAL.GetStaticProcPointer("alGetProcAddress");
            internal static readonly delegate*<byte*, int> alGetEnumValue = (delegate*<byte*, int>) OpenAL.GetStaticProcPointer("alGetEnumValue");
            internal static readonly delegate*<ListenerProperty, float, void> alListenerf = (delegate*<ListenerProperty, float, void>) OpenAL.GetStaticProcPointer("alListenerf");
            internal static readonly delegate*<ListenerProperty, float, float, float, void> alListener3f = (delegate*<ListenerProperty, float, float, float, void>) OpenAL.GetStaticProcPointer("alListener3f");
            internal static readonly delegate*<ListenerProperty, float*, void> alListenerfv = (delegate*<ListenerProperty, float*, void>) OpenAL.GetStaticProcPointer("alListenerfv");
            internal static readonly delegate*<ListenerProperty, int, void> alListeneri = (delegate*<ListenerProperty, int, void>) OpenAL.GetStaticProcPointer("alListeneri");
            internal static readonly delegate*<ListenerProperty, int, int, int, void> alListener3i = (delegate*<ListenerProperty, int, int, int, void>) OpenAL.GetStaticProcPointer("alListener3i");
            internal static readonly delegate*<ListenerProperty, int*, void> alListeneriv = (delegate*<ListenerProperty, int*, void>) OpenAL.GetStaticProcPointer("alListeneriv");
            internal static readonly delegate*<ListenerProperty, float*, void> alGetListenerf = (delegate*<ListenerProperty, float*, void>) OpenAL.GetStaticProcPointer("alGetListenerf");
            internal static readonly delegate*<ListenerProperty, float*, float*, float*, void> alGetListener3f = (delegate*<ListenerProperty, float*, float*, float*, void>) OpenAL.GetStaticProcPointer("alGetListener3f");
            internal static readonly delegate*<ListenerProperty, float*, void> alGetListenerfv = (delegate*<ListenerProperty, float*, void>) OpenAL.GetStaticProcPointer("alGetListenerfv");
            internal static readonly delegate*<ListenerProperty, int*, void> alGetListeneri = (delegate*<ListenerProperty, int*, void>) OpenAL.GetStaticProcPointer("alGetListeneri");
            internal static readonly delegate*<ListenerProperty, int*, int*, int*, void> alGetListener3i = (delegate*<ListenerProperty, int*, int*, int*, void>) OpenAL.GetStaticProcPointer("alGetListener3i");
            internal static readonly delegate*<ListenerProperty, int*, void> alGetListeneriv = (delegate*<ListenerProperty, int*, void>) OpenAL.GetStaticProcPointer("alGetListeneriv");
            internal static readonly delegate*<int, uint*, void> alGenSources = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alGenSources");
            internal static readonly delegate*<int, uint*, void> alDeleteSources = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alDeleteSources");
            internal static readonly delegate*<uint, bool> alIsSource = (delegate*<uint, bool>) OpenAL.GetStaticProcPointer("alIsSource");
            internal static readonly delegate*<uint, SourceProperty, float, void> alSourcef = (delegate*<uint, SourceProperty, float, void>) OpenAL.GetStaticProcPointer("alSourcef");
            internal static readonly delegate*<uint, SourceProperty, float, float, float, void> alSource3f = (delegate*<uint, SourceProperty, float, float, float, void>) OpenAL.GetStaticProcPointer("alSource3f");
            internal static readonly delegate*<uint, SourceProperty, float*, void> alSourcefv = (delegate*<uint, SourceProperty, float*, void>) OpenAL.GetStaticProcPointer("alSourcefv");
            internal static readonly delegate*<uint, SourceProperty, int, void> alSourcei = (delegate*<uint, SourceProperty, int, void>) OpenAL.GetStaticProcPointer("alSourcei");
            internal static readonly delegate*<uint, SourceProperty, int, int, int, void> alSource3i = (delegate*<uint, SourceProperty, int, int, int, void>) OpenAL.GetStaticProcPointer("alSource3i");
            internal static readonly delegate*<uint, SourceProperty, int*, void> alSourceiv = (delegate*<uint, SourceProperty, int*, void>) OpenAL.GetStaticProcPointer("alSourceiv");
            internal static readonly delegate*<uint, SourceProperty, float*, void> alGetSourcef = (delegate*<uint, SourceProperty, float*, void>) OpenAL.GetStaticProcPointer("alGetSourcef");
            internal static readonly delegate*<uint, SourceProperty, float*, float*, float*, void> alGetSource3f = (delegate*<uint, SourceProperty, float*, float*, float*, void>) OpenAL.GetStaticProcPointer("alGetSource3f");
            internal static readonly delegate*<uint, SourceProperty, float*, void> alGetSourcefv = (delegate*<uint, SourceProperty, float*, void>) OpenAL.GetStaticProcPointer("alGetSourcefv");
            internal static readonly delegate*<uint, SourceProperty, int*, void> alGetSourcei = (delegate*<uint, SourceProperty, int*, void>) OpenAL.GetStaticProcPointer("alGetSourcei");
            internal static readonly delegate*<uint, SourceProperty, int*, int*, int*, void> alGetSource3i = (delegate*<uint, SourceProperty, int*, int*, int*, void>) OpenAL.GetStaticProcPointer("alGetSource3i");
            internal static readonly delegate*<uint, SourceProperty, int*, void> alGetSourceiv = (delegate*<uint, SourceProperty, int*, void>) OpenAL.GetStaticProcPointer("alGetSourceiv");
            internal static readonly delegate*<int, uint*, void> alSourcePlayv = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alSourcePlayv");
            internal static readonly delegate*<int, uint*, void> alSourceStopv = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alSourceStopv");
            internal static readonly delegate*<int, uint*, void> alSourceRewindv = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alSourceRewindv");
            internal static readonly delegate*<int, uint*, void> alSourcePausev = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alSourcePausev");
            internal static readonly delegate*<uint, void> alSourcePlay = (delegate*<uint, void>) OpenAL.GetStaticProcPointer("alSourcePlay");
            internal static readonly delegate*<uint, void> alSourceStop = (delegate*<uint, void>) OpenAL.GetStaticProcPointer("alSourceStop");
            internal static readonly delegate*<uint, void> alSourceRewind = (delegate*<uint, void>) OpenAL.GetStaticProcPointer("alSourceRewind");
            internal static readonly delegate*<uint, void> alSourcePause = (delegate*<uint, void>) OpenAL.GetStaticProcPointer("alSourcePause");
            internal static readonly delegate*<uint, int, uint*, void> alSourceQueueBuffers = (delegate*<uint, int, uint*, void>) OpenAL.GetStaticProcPointer("alSourceQueueBuffers");
            internal static readonly delegate*<uint, int, uint*, void> alSourceUnqueueBuffers = (delegate*<uint, int, uint*, void>) OpenAL.GetStaticProcPointer("alSourceUnqueueBuffers");
            internal static readonly delegate*<int, uint*, void> alGenBuffers = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alGenBuffers");
            internal static readonly delegate*<int, uint*, void> alDeleteBuffers = (delegate*<int, uint*, void>) OpenAL.GetStaticProcPointer("alDeleteBuffers");
            internal static readonly delegate*<uint, bool> alIsBuffer = (delegate*<uint, bool>) OpenAL.GetStaticProcPointer("alIsBuffer");
            internal static readonly delegate*<uint, BufferFormat, void*, int, int, void> alBufferData = (delegate*<uint, BufferFormat, void*, int, int, void>) OpenAL.GetStaticProcPointer("alBufferData");
            internal static readonly delegate*<uint, BufferProperty, float, void> alBufferf = (delegate*<uint, BufferProperty, float, void>) OpenAL.GetStaticProcPointer("alBufferf");
            internal static readonly delegate*<uint, BufferProperty, float, float, float, void> alBuffer3f = (delegate*<uint, BufferProperty, float, float, float, void>) OpenAL.GetStaticProcPointer("alBuffer3f");
            internal static readonly delegate*<uint, BufferProperty, float*, void> alBufferfv = (delegate*<uint, BufferProperty, float*, void>) OpenAL.GetStaticProcPointer("alBufferfv");
            internal static readonly delegate*<uint, BufferProperty, int, void> alBufferi = (delegate*<uint, BufferProperty, int, void>) OpenAL.GetStaticProcPointer("alBufferi");
            internal static readonly delegate*<uint, BufferProperty, int, int, int, void> alBuffer3i = (delegate*<uint, BufferProperty, int, int, int, void>) OpenAL.GetStaticProcPointer("alBuffer3i");
            internal static readonly delegate*<uint, BufferProperty, int*, void> alBufferiv = (delegate*<uint, BufferProperty, int*, void>) OpenAL.GetStaticProcPointer("alBufferiv");
            internal static readonly delegate*<uint, BufferProperty, float*, void> alGetBufferf = (delegate*<uint, BufferProperty, float*, void>) OpenAL.GetStaticProcPointer("alGetBufferf");
            internal static readonly delegate*<uint, BufferProperty, float*, float*, float*, void> alGetBuffer3f = (delegate*<uint, BufferProperty, float*, float*, float*, void>) OpenAL.GetStaticProcPointer("alGetBuffer3f");
            internal static readonly delegate*<uint, BufferProperty, float*, void> alGetBufferfv = (delegate*<uint, BufferProperty, float*, void>) OpenAL.GetStaticProcPointer("alGetBufferfv");
            internal static readonly delegate*<uint, BufferProperty, int*, void> alGetBufferi = (delegate*<uint, BufferProperty, int*, void>) OpenAL.GetStaticProcPointer("alGetBufferi");
            internal static readonly delegate*<uint, BufferProperty, int*, int*, int*, void> alGetBuffer3i = (delegate*<uint, BufferProperty, int*, int*, int*, void>) OpenAL.GetStaticProcPointer("alGetBuffer3i");
            internal static readonly delegate*<uint, BufferProperty, int*, void> alGetBufferiv = (delegate*<uint, BufferProperty, int*, void>) OpenAL.GetStaticProcPointer("alGetBufferiv");

        }
    }
}
