using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using static System.Runtime.InteropServices.OSPlatform;
using static System.Runtime.InteropServices.RuntimeInformation;

namespace Magma.OpenAL
{
    public static class OpenAL
    {
        private static readonly IntPtr _Handle;

        static OpenAL()
        {
            _Handle = GetOpenGLLibraryNameCandidates()
                .Select(LoadLibrary)
                .FirstOrDefault(handle => handle != IntPtr.Zero);
            if (_Handle == IntPtr.Zero)
                throw new NotImplementedException("OpenAL native library was not found.");
        }

        public unsafe static void *GetStaticProcPointer(string procName)
        {
            IntPtr handle;
            if (IsOSPlatform(Windows))
            {
                handle = Kernel32GetProcAddress(_Handle, procName);
            }
            else if (IsOSPlatform(Linux) || IsOSPlatform(OSX))
            {
                handle = LibDLGetProcAddress(_Handle, procName);
            }
            else
            {
                throw new NotImplementedException();
            }
            return handle == IntPtr.Zero ?
                null :
                (void *) handle;
        }

        private static IntPtr LoadLibrary(string fileName)
        {
            IntPtr handle;
            if (IsOSPlatform(Windows))
            {
                handle = Kernel32LoadLibrary(fileName);
            }
            else if (IsOSPlatform(Linux) || IsOSPlatform(OSX))
            {
                handle = LibDLLoadLibrary(fileName, LibDLRtldNow);
            }
            else
            {
                throw new NotImplementedException();
            }
            return handle;
        }

        private static IEnumerable<string> GetOpenGLLibraryNameCandidates()
        {
            if (IsOSPlatform(Windows))
            {
                yield return "openal32.dll";
            }
            else if (IsOSPlatform(Linux))
            {
                yield return "libopenal.so";
                yield return "libopenal.so.1";
                yield return "libopenal.so.1.20.1";
            }
            else if (IsOSPlatform(OSX))
            {
                yield return "libopenal.dylib";
            }
            throw new NotImplementedException("Ran out of places to look for the OpenAL native library");
        }

        [DllImport("kernel32", EntryPoint = "LoadLibrary")]
        private static extern IntPtr Kernel32LoadLibrary(string fileName);

        [DllImport("kernel32", EntryPoint = "GetProcAddress")]
        private static extern IntPtr Kernel32GetProcAddress(IntPtr module, string procName);

        [DllImport("libdl", EntryPoint = "dlopen")]
        private static extern IntPtr LibDLLoadLibrary(string fileName, int flags);

        [DllImport("libdl", EntryPoint = "dlsym")]
        private static extern IntPtr LibDLGetProcAddress(IntPtr handle, string name);

        private const int LibDLRtldNow = 2;
    }
}
